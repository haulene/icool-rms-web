/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FlexbaRmsWebTestModule } from '../../../test.module';
import { CustomerRatingDetailComponent } from 'app/entities/customer-rating/customer-rating-detail.component';
import { CustomerRating } from 'app/shared/model/customer-rating.model';

describe('Component Tests', () => {
    describe('CustomerRating Management Detail Component', () => {
        let comp: CustomerRatingDetailComponent;
        let fixture: ComponentFixture<CustomerRatingDetailComponent>;
        const route = ({ data: of({ customerRating: new CustomerRating(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [FlexbaRmsWebTestModule],
                declarations: [CustomerRatingDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CustomerRatingDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CustomerRatingDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.customerRating).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
