/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { FlexbaRmsWebTestModule } from '../../../test.module';
import { CustomerRatingDeleteDialogComponent } from 'app/entities/customer-rating/customer-rating-delete-dialog.component';
import { CustomerRatingService } from 'app/entities/customer-rating/customer-rating.service';

describe('Component Tests', () => {
    describe('CustomerRating Management Delete Component', () => {
        let comp: CustomerRatingDeleteDialogComponent;
        let fixture: ComponentFixture<CustomerRatingDeleteDialogComponent>;
        let service: CustomerRatingService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [FlexbaRmsWebTestModule],
                declarations: [CustomerRatingDeleteDialogComponent]
            })
                .overrideTemplate(CustomerRatingDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CustomerRatingDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomerRatingService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
