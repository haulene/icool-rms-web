/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { FlexbaRmsWebTestModule } from '../../../test.module';
import { CustomerRatingUpdateComponent } from 'app/entities/customer-rating/customer-rating-update.component';
import { CustomerRatingService } from 'app/entities/customer-rating/customer-rating.service';
import { CustomerRating } from 'app/shared/model/customer-rating.model';

describe('Component Tests', () => {
    describe('CustomerRating Management Update Component', () => {
        let comp: CustomerRatingUpdateComponent;
        let fixture: ComponentFixture<CustomerRatingUpdateComponent>;
        let service: CustomerRatingService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [FlexbaRmsWebTestModule],
                declarations: [CustomerRatingUpdateComponent]
            })
                .overrideTemplate(CustomerRatingUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CustomerRatingUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomerRatingService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CustomerRating(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.customerRating = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CustomerRating();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.customerRating = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
