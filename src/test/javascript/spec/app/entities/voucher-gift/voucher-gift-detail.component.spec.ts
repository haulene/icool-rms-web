/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ActivatedRoute } from "@angular/router";
import { of } from "rxjs";

import { FlexbaRmsWebTestModule } from "../../../test.module";
import { VoucherGiftDetailComponent } from "app/entities/voucher-gift/voucher-gift-detail.component";
import { VoucherGift } from "app/shared/model/voucher-gift.model";

describe("Component Tests", () => {
  describe("VoucherGift Management Detail Component", () => {
    let comp: VoucherGiftDetailComponent;
    let fixture: ComponentFixture<VoucherGiftDetailComponent>;
    const route = ({
      data: of({ voucherGift: new VoucherGift(123) })
    } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FlexbaRmsWebTestModule],
        declarations: [VoucherGiftDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(VoucherGiftDetailComponent, "")
        .compileComponents();
      fixture = TestBed.createComponent(VoucherGiftDetailComponent);
      comp = fixture.componentInstance;
    });

    describe("OnInit", () => {
      it("Should call load all on init", () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.voucherGift).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
