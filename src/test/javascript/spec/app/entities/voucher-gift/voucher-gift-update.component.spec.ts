/* tslint:disable max-line-length */
import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick
} from "@angular/core/testing";
import { HttpResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";

import { FlexbaRmsWebTestModule } from "../../../test.module";
import { VoucherGiftUpdateComponent } from "app/entities/voucher-gift/voucher-gift-update.component";
import { VoucherGiftService } from "app/entities/voucher-gift/voucher-gift.service";
import { VoucherGift } from "app/shared/model/voucher-gift.model";

describe("Component Tests", () => {
  describe("VoucherGift Management Update Component", () => {
    let comp: VoucherGiftUpdateComponent;
    let fixture: ComponentFixture<VoucherGiftUpdateComponent>;
    let service: VoucherGiftService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FlexbaRmsWebTestModule],
        declarations: [VoucherGiftUpdateComponent]
      })
        .overrideTemplate(VoucherGiftUpdateComponent, "")
        .compileComponents();

      fixture = TestBed.createComponent(VoucherGiftUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VoucherGiftService);
    });

    describe("save", () => {
      it(
        "Should call update service on save for existing entity",
        fakeAsync(() => {
          // GIVEN
          const entity = new VoucherGift(123);
          spyOn(service, "update").and.returnValue(
            of(new HttpResponse({ body: entity }))
          );
          comp.voucherGift = entity;
          // WHEN
          comp.save();
          tick(); // simulate async

          // THEN
          expect(service.update).toHaveBeenCalledWith(entity);
          expect(comp.isSaving).toEqual(false);
        })
      );

      it(
        "Should call create service on save for new entity",
        fakeAsync(() => {
          // GIVEN
          const entity = new VoucherGift();
          spyOn(service, "create").and.returnValue(
            of(new HttpResponse({ body: entity }))
          );
          comp.voucherGift = entity;
          // WHEN
          comp.save();
          tick(); // simulate async

          // THEN
          expect(service.create).toHaveBeenCalledWith(entity);
          expect(comp.isSaving).toEqual(false);
        })
      );
    });
  });
});
