/* tslint:disable max-line-length */
import {
  ComponentFixture,
  TestBed,
  inject,
  fakeAsync,
  tick
} from "@angular/core/testing";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, of } from "rxjs";
import { JhiEventManager } from "ng-jhipster";

import { FlexbaRmsWebTestModule } from "../../../test.module";
import { VoucherGiftDeleteDialogComponent } from "app/entities/voucher-gift/voucher-gift-delete-dialog.component";
import { VoucherGiftService } from "app/entities/voucher-gift/voucher-gift.service";

describe("Component Tests", () => {
  describe("VoucherGift Management Delete Component", () => {
    let comp: VoucherGiftDeleteDialogComponent;
    let fixture: ComponentFixture<VoucherGiftDeleteDialogComponent>;
    let service: VoucherGiftService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FlexbaRmsWebTestModule],
        declarations: [VoucherGiftDeleteDialogComponent]
      })
        .overrideTemplate(VoucherGiftDeleteDialogComponent, "")
        .compileComponents();
      fixture = TestBed.createComponent(VoucherGiftDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VoucherGiftService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe("confirmDelete", () => {
      it(
        "Should call delete service on confirmDelete",
        inject(
          [],
          fakeAsync(() => {
            // GIVEN
            spyOn(service, "delete").and.returnValue(of({}));

            // WHEN
            comp.confirmDelete(123);
            tick();

            // THEN
            expect(service.delete).toHaveBeenCalledWith(123);
            expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
            expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
          })
        )
      );
    });
  });
});
