/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ActivatedRoute } from "@angular/router";
import { of } from "rxjs";

import { FlexbaRmsWebTestModule } from "../../../test.module";
import { RatingQuestionDetailComponent } from "app/entities/rating-question/rating-question-detail.component";
import { RatingQuestion } from "app/shared/model/rating-question.model";

describe("Component Tests", () => {
  describe("RatingQuestion Management Detail Component", () => {
    let comp: RatingQuestionDetailComponent;
    let fixture: ComponentFixture<RatingQuestionDetailComponent>;
    const route = ({
      data: of({ ratingQuestion: new RatingQuestion(123) })
    } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FlexbaRmsWebTestModule],
        declarations: [RatingQuestionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RatingQuestionDetailComponent, "")
        .compileComponents();
      fixture = TestBed.createComponent(RatingQuestionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe("OnInit", () => {
      it("Should call load all on init", () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.ratingQuestion).toEqual(
          jasmine.objectContaining({ id: 123 })
        );
      });
    });
  });
});
