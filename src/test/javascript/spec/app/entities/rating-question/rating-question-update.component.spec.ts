/* tslint:disable max-line-length */
import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick
} from "@angular/core/testing";
import { HttpResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";

import { FlexbaRmsWebTestModule } from "../../../test.module";
import { RatingQuestionUpdateComponent } from "app/entities/rating-question/rating-question-update.component";
import { RatingQuestionService } from "app/entities/rating-question/rating-question.service";
import { RatingQuestion } from "app/shared/model/rating-question.model";

describe("Component Tests", () => {
  describe("RatingQuestion Management Update Component", () => {
    let comp: RatingQuestionUpdateComponent;
    let fixture: ComponentFixture<RatingQuestionUpdateComponent>;
    let service: RatingQuestionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FlexbaRmsWebTestModule],
        declarations: [RatingQuestionUpdateComponent]
      })
        .overrideTemplate(RatingQuestionUpdateComponent, "")
        .compileComponents();

      fixture = TestBed.createComponent(RatingQuestionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RatingQuestionService);
    });

    describe("save", () => {
      it(
        "Should call update service on save for existing entity",
        fakeAsync(() => {
          // GIVEN
          const entity = new RatingQuestion(123);
          spyOn(service, "update").and.returnValue(
            of(new HttpResponse({ body: entity }))
          );
          comp.ratingQuestion = entity;
          // WHEN
          comp.save();
          tick(); // simulate async

          // THEN
          expect(service.update).toHaveBeenCalledWith(entity);
          expect(comp.isSaving).toEqual(false);
        })
      );

      it(
        "Should call create service on save for new entity",
        fakeAsync(() => {
          // GIVEN
          const entity = new RatingQuestion();
          spyOn(service, "create").and.returnValue(
            of(new HttpResponse({ body: entity }))
          );
          comp.ratingQuestion = entity;
          // WHEN
          comp.save();
          tick(); // simulate async

          // THEN
          expect(service.create).toHaveBeenCalledWith(entity);
          expect(comp.isSaving).toEqual(false);
        })
      );
    });
  });
});
