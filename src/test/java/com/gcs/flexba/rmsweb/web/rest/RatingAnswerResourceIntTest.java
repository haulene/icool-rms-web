package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.RatingAnswer;
import com.gcs.flexba.rmsweb.repository.RatingAnswerRepository;
import com.gcs.flexba.rmsweb.service.RatingAnswerService;
import com.gcs.flexba.rmsweb.service.dto.RatingAnswerDTO;
import com.gcs.flexba.rmsweb.service.mapper.RatingAnswerMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.RatingAnswerCriteria;
import com.gcs.flexba.rmsweb.service.RatingAnswerQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RatingAnswerResource REST controller.
 *
 * @see RatingAnswerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class RatingAnswerResourceIntTest {

    private static final Integer DEFAULT_RATING_ID = 1;
    private static final Integer UPDATED_RATING_ID = 2;

    private static final Integer DEFAULT_RATING_QUESTION_ID = 1;
    private static final Integer UPDATED_RATING_QUESTION_ID = 2;

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final String DEFAULT_ANSWER_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER_CATEGORY = "BBBBBBBBBB";

    @Autowired
    private RatingAnswerRepository ratingAnswerRepository;


    @Autowired
    private RatingAnswerMapper ratingAnswerMapper;
    

    @Autowired
    private RatingAnswerService ratingAnswerService;

    @Autowired
    private RatingAnswerQueryService ratingAnswerQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRatingAnswerMockMvc;

    private RatingAnswer ratingAnswer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RatingAnswerResource ratingAnswerResource = new RatingAnswerResource(ratingAnswerService, ratingAnswerQueryService);
        this.restRatingAnswerMockMvc = MockMvcBuilders.standaloneSetup(ratingAnswerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RatingAnswer createEntity(EntityManager em) {
        RatingAnswer ratingAnswer = new RatingAnswer()
            .ratingId(DEFAULT_RATING_ID)
            .ratingQuestionId(DEFAULT_RATING_QUESTION_ID)
            .type(DEFAULT_TYPE)
            .value(DEFAULT_VALUE)
            .answerCategory(DEFAULT_ANSWER_CATEGORY);
        return ratingAnswer;
    }

    @Before
    public void initTest() {
        ratingAnswer = createEntity(em);
    }

    @Test
    @Transactional
    public void createRatingAnswer() throws Exception {
        int databaseSizeBeforeCreate = ratingAnswerRepository.findAll().size();

        // Create the RatingAnswer
        RatingAnswerDTO ratingAnswerDTO = ratingAnswerMapper.toDto(ratingAnswer);
        restRatingAnswerMockMvc.perform(post("/api/rating-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingAnswerDTO)))
            .andExpect(status().isCreated());

        // Validate the RatingAnswer in the database
        List<RatingAnswer> ratingAnswerList = ratingAnswerRepository.findAll();
        assertThat(ratingAnswerList).hasSize(databaseSizeBeforeCreate + 1);
        RatingAnswer testRatingAnswer = ratingAnswerList.get(ratingAnswerList.size() - 1);
        assertThat(testRatingAnswer.getRatingId()).isEqualTo(DEFAULT_RATING_ID);
        assertThat(testRatingAnswer.getRatingQuestionId()).isEqualTo(DEFAULT_RATING_QUESTION_ID);
        assertThat(testRatingAnswer.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testRatingAnswer.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testRatingAnswer.getAnswerCategory()).isEqualTo(DEFAULT_ANSWER_CATEGORY);
    }

    @Test
    @Transactional
    public void createRatingAnswerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ratingAnswerRepository.findAll().size();

        // Create the RatingAnswer with an existing ID
        ratingAnswer.setId(1L);
        RatingAnswerDTO ratingAnswerDTO = ratingAnswerMapper.toDto(ratingAnswer);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRatingAnswerMockMvc.perform(post("/api/rating-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingAnswerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RatingAnswer in the database
        List<RatingAnswer> ratingAnswerList = ratingAnswerRepository.findAll();
        assertThat(ratingAnswerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRatingAnswers() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList
        restRatingAnswerMockMvc.perform(get("/api/rating-answers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ratingAnswer.getId().intValue())))
            .andExpect(jsonPath("$.[*].ratingId").value(hasItem(DEFAULT_RATING_ID)))
            .andExpect(jsonPath("$.[*].ratingQuestionId").value(hasItem(DEFAULT_RATING_QUESTION_ID)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
            .andExpect(jsonPath("$.[*].answerCategory").value(hasItem(DEFAULT_ANSWER_CATEGORY.toString())));
    }
    

    @Test
    @Transactional
    public void getRatingAnswer() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get the ratingAnswer
        restRatingAnswerMockMvc.perform(get("/api/rating-answers/{id}", ratingAnswer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ratingAnswer.getId().intValue()))
            .andExpect(jsonPath("$.ratingId").value(DEFAULT_RATING_ID))
            .andExpect(jsonPath("$.ratingQuestionId").value(DEFAULT_RATING_QUESTION_ID))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()))
            .andExpect(jsonPath("$.answerCategory").value(DEFAULT_ANSWER_CATEGORY.toString()));
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingIdIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingId equals to DEFAULT_RATING_ID
        defaultRatingAnswerShouldBeFound("ratingId.equals=" + DEFAULT_RATING_ID);

        // Get all the ratingAnswerList where ratingId equals to UPDATED_RATING_ID
        defaultRatingAnswerShouldNotBeFound("ratingId.equals=" + UPDATED_RATING_ID);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingIdIsInShouldWork() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingId in DEFAULT_RATING_ID or UPDATED_RATING_ID
        defaultRatingAnswerShouldBeFound("ratingId.in=" + DEFAULT_RATING_ID + "," + UPDATED_RATING_ID);

        // Get all the ratingAnswerList where ratingId equals to UPDATED_RATING_ID
        defaultRatingAnswerShouldNotBeFound("ratingId.in=" + UPDATED_RATING_ID);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingId is not null
        defaultRatingAnswerShouldBeFound("ratingId.specified=true");

        // Get all the ratingAnswerList where ratingId is null
        defaultRatingAnswerShouldNotBeFound("ratingId.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingId greater than or equals to DEFAULT_RATING_ID
        defaultRatingAnswerShouldBeFound("ratingId.greaterOrEqualThan=" + DEFAULT_RATING_ID);

        // Get all the ratingAnswerList where ratingId greater than or equals to UPDATED_RATING_ID
        defaultRatingAnswerShouldNotBeFound("ratingId.greaterOrEqualThan=" + UPDATED_RATING_ID);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingIdIsLessThanSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingId less than or equals to DEFAULT_RATING_ID
        defaultRatingAnswerShouldNotBeFound("ratingId.lessThan=" + DEFAULT_RATING_ID);

        // Get all the ratingAnswerList where ratingId less than or equals to UPDATED_RATING_ID
        defaultRatingAnswerShouldBeFound("ratingId.lessThan=" + UPDATED_RATING_ID);
    }


    @Test
    @Transactional
    public void getAllRatingAnswersByRatingQuestionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingQuestionId equals to DEFAULT_RATING_QUESTION_ID
        defaultRatingAnswerShouldBeFound("ratingQuestionId.equals=" + DEFAULT_RATING_QUESTION_ID);

        // Get all the ratingAnswerList where ratingQuestionId equals to UPDATED_RATING_QUESTION_ID
        defaultRatingAnswerShouldNotBeFound("ratingQuestionId.equals=" + UPDATED_RATING_QUESTION_ID);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingQuestionIdIsInShouldWork() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingQuestionId in DEFAULT_RATING_QUESTION_ID or UPDATED_RATING_QUESTION_ID
        defaultRatingAnswerShouldBeFound("ratingQuestionId.in=" + DEFAULT_RATING_QUESTION_ID + "," + UPDATED_RATING_QUESTION_ID);

        // Get all the ratingAnswerList where ratingQuestionId equals to UPDATED_RATING_QUESTION_ID
        defaultRatingAnswerShouldNotBeFound("ratingQuestionId.in=" + UPDATED_RATING_QUESTION_ID);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingQuestionIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingQuestionId is not null
        defaultRatingAnswerShouldBeFound("ratingQuestionId.specified=true");

        // Get all the ratingAnswerList where ratingQuestionId is null
        defaultRatingAnswerShouldNotBeFound("ratingQuestionId.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingQuestionIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingQuestionId greater than or equals to DEFAULT_RATING_QUESTION_ID
        defaultRatingAnswerShouldBeFound("ratingQuestionId.greaterOrEqualThan=" + DEFAULT_RATING_QUESTION_ID);

        // Get all the ratingAnswerList where ratingQuestionId greater than or equals to UPDATED_RATING_QUESTION_ID
        defaultRatingAnswerShouldNotBeFound("ratingQuestionId.greaterOrEqualThan=" + UPDATED_RATING_QUESTION_ID);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByRatingQuestionIdIsLessThanSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where ratingQuestionId less than or equals to DEFAULT_RATING_QUESTION_ID
        defaultRatingAnswerShouldNotBeFound("ratingQuestionId.lessThan=" + DEFAULT_RATING_QUESTION_ID);

        // Get all the ratingAnswerList where ratingQuestionId less than or equals to UPDATED_RATING_QUESTION_ID
        defaultRatingAnswerShouldBeFound("ratingQuestionId.lessThan=" + UPDATED_RATING_QUESTION_ID);
    }


    @Test
    @Transactional
    public void getAllRatingAnswersByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where type equals to DEFAULT_TYPE
        defaultRatingAnswerShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the ratingAnswerList where type equals to UPDATED_TYPE
        defaultRatingAnswerShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultRatingAnswerShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the ratingAnswerList where type equals to UPDATED_TYPE
        defaultRatingAnswerShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where type is not null
        defaultRatingAnswerShouldBeFound("type.specified=true");

        // Get all the ratingAnswerList where type is null
        defaultRatingAnswerShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByTypeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where type greater than or equals to DEFAULT_TYPE
        defaultRatingAnswerShouldBeFound("type.greaterOrEqualThan=" + DEFAULT_TYPE);

        // Get all the ratingAnswerList where type greater than or equals to UPDATED_TYPE
        defaultRatingAnswerShouldNotBeFound("type.greaterOrEqualThan=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByTypeIsLessThanSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where type less than or equals to DEFAULT_TYPE
        defaultRatingAnswerShouldNotBeFound("type.lessThan=" + DEFAULT_TYPE);

        // Get all the ratingAnswerList where type less than or equals to UPDATED_TYPE
        defaultRatingAnswerShouldBeFound("type.lessThan=" + UPDATED_TYPE);
    }


    @Test
    @Transactional
    public void getAllRatingAnswersByValueIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where value equals to DEFAULT_VALUE
        defaultRatingAnswerShouldBeFound("value.equals=" + DEFAULT_VALUE);

        // Get all the ratingAnswerList where value equals to UPDATED_VALUE
        defaultRatingAnswerShouldNotBeFound("value.equals=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByValueIsInShouldWork() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where value in DEFAULT_VALUE or UPDATED_VALUE
        defaultRatingAnswerShouldBeFound("value.in=" + DEFAULT_VALUE + "," + UPDATED_VALUE);

        // Get all the ratingAnswerList where value equals to UPDATED_VALUE
        defaultRatingAnswerShouldNotBeFound("value.in=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where value is not null
        defaultRatingAnswerShouldBeFound("value.specified=true");

        // Get all the ratingAnswerList where value is null
        defaultRatingAnswerShouldNotBeFound("value.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByAnswerCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where answerCategory equals to DEFAULT_ANSWER_CATEGORY
        defaultRatingAnswerShouldBeFound("answerCategory.equals=" + DEFAULT_ANSWER_CATEGORY);

        // Get all the ratingAnswerList where answerCategory equals to UPDATED_ANSWER_CATEGORY
        defaultRatingAnswerShouldNotBeFound("answerCategory.equals=" + UPDATED_ANSWER_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByAnswerCategoryIsInShouldWork() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where answerCategory in DEFAULT_ANSWER_CATEGORY or UPDATED_ANSWER_CATEGORY
        defaultRatingAnswerShouldBeFound("answerCategory.in=" + DEFAULT_ANSWER_CATEGORY + "," + UPDATED_ANSWER_CATEGORY);

        // Get all the ratingAnswerList where answerCategory equals to UPDATED_ANSWER_CATEGORY
        defaultRatingAnswerShouldNotBeFound("answerCategory.in=" + UPDATED_ANSWER_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllRatingAnswersByAnswerCategoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        // Get all the ratingAnswerList where answerCategory is not null
        defaultRatingAnswerShouldBeFound("answerCategory.specified=true");

        // Get all the ratingAnswerList where answerCategory is null
        defaultRatingAnswerShouldNotBeFound("answerCategory.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultRatingAnswerShouldBeFound(String filter) throws Exception {
        restRatingAnswerMockMvc.perform(get("/api/rating-answers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ratingAnswer.getId().intValue())))
            .andExpect(jsonPath("$.[*].ratingId").value(hasItem(DEFAULT_RATING_ID)))
            .andExpect(jsonPath("$.[*].ratingQuestionId").value(hasItem(DEFAULT_RATING_QUESTION_ID)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
            .andExpect(jsonPath("$.[*].answerCategory").value(hasItem(DEFAULT_ANSWER_CATEGORY.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultRatingAnswerShouldNotBeFound(String filter) throws Exception {
        restRatingAnswerMockMvc.perform(get("/api/rating-answers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingRatingAnswer() throws Exception {
        // Get the ratingAnswer
        restRatingAnswerMockMvc.perform(get("/api/rating-answers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRatingAnswer() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        int databaseSizeBeforeUpdate = ratingAnswerRepository.findAll().size();

        // Update the ratingAnswer
        RatingAnswer updatedRatingAnswer = ratingAnswerRepository.findById(ratingAnswer.getId()).get();
        // Disconnect from session so that the updates on updatedRatingAnswer are not directly saved in db
        em.detach(updatedRatingAnswer);
        updatedRatingAnswer
            .ratingId(UPDATED_RATING_ID)
            .ratingQuestionId(UPDATED_RATING_QUESTION_ID)
            .type(UPDATED_TYPE)
            .value(UPDATED_VALUE)
            .answerCategory(UPDATED_ANSWER_CATEGORY);
        RatingAnswerDTO ratingAnswerDTO = ratingAnswerMapper.toDto(updatedRatingAnswer);

        restRatingAnswerMockMvc.perform(put("/api/rating-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingAnswerDTO)))
            .andExpect(status().isOk());

        // Validate the RatingAnswer in the database
        List<RatingAnswer> ratingAnswerList = ratingAnswerRepository.findAll();
        assertThat(ratingAnswerList).hasSize(databaseSizeBeforeUpdate);
        RatingAnswer testRatingAnswer = ratingAnswerList.get(ratingAnswerList.size() - 1);
        assertThat(testRatingAnswer.getRatingId()).isEqualTo(UPDATED_RATING_ID);
        assertThat(testRatingAnswer.getRatingQuestionId()).isEqualTo(UPDATED_RATING_QUESTION_ID);
        assertThat(testRatingAnswer.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testRatingAnswer.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testRatingAnswer.getAnswerCategory()).isEqualTo(UPDATED_ANSWER_CATEGORY);
    }

    @Test
    @Transactional
    public void updateNonExistingRatingAnswer() throws Exception {
        int databaseSizeBeforeUpdate = ratingAnswerRepository.findAll().size();

        // Create the RatingAnswer
        RatingAnswerDTO ratingAnswerDTO = ratingAnswerMapper.toDto(ratingAnswer);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRatingAnswerMockMvc.perform(put("/api/rating-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingAnswerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RatingAnswer in the database
        List<RatingAnswer> ratingAnswerList = ratingAnswerRepository.findAll();
        assertThat(ratingAnswerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRatingAnswer() throws Exception {
        // Initialize the database
        ratingAnswerRepository.saveAndFlush(ratingAnswer);

        int databaseSizeBeforeDelete = ratingAnswerRepository.findAll().size();

        // Get the ratingAnswer
        restRatingAnswerMockMvc.perform(delete("/api/rating-answers/{id}", ratingAnswer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RatingAnswer> ratingAnswerList = ratingAnswerRepository.findAll();
        assertThat(ratingAnswerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RatingAnswer.class);
        RatingAnswer ratingAnswer1 = new RatingAnswer();
        ratingAnswer1.setId(1L);
        RatingAnswer ratingAnswer2 = new RatingAnswer();
        ratingAnswer2.setId(ratingAnswer1.getId());
        assertThat(ratingAnswer1).isEqualTo(ratingAnswer2);
        ratingAnswer2.setId(2L);
        assertThat(ratingAnswer1).isNotEqualTo(ratingAnswer2);
        ratingAnswer1.setId(null);
        assertThat(ratingAnswer1).isNotEqualTo(ratingAnswer2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RatingAnswerDTO.class);
        RatingAnswerDTO ratingAnswerDTO1 = new RatingAnswerDTO();
        ratingAnswerDTO1.setId(1L);
        RatingAnswerDTO ratingAnswerDTO2 = new RatingAnswerDTO();
        assertThat(ratingAnswerDTO1).isNotEqualTo(ratingAnswerDTO2);
        ratingAnswerDTO2.setId(ratingAnswerDTO1.getId());
        assertThat(ratingAnswerDTO1).isEqualTo(ratingAnswerDTO2);
        ratingAnswerDTO2.setId(2L);
        assertThat(ratingAnswerDTO1).isNotEqualTo(ratingAnswerDTO2);
        ratingAnswerDTO1.setId(null);
        assertThat(ratingAnswerDTO1).isNotEqualTo(ratingAnswerDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(ratingAnswerMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(ratingAnswerMapper.fromId(null)).isNull();
    }
}
