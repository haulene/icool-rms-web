package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.Customer;
import com.gcs.flexba.rmsweb.domain.MembershipHistoty;
import com.gcs.flexba.rmsweb.repository.CustomerRepository;
import com.gcs.flexba.rmsweb.service.CustomerService;
import com.gcs.flexba.rmsweb.service.dto.CustomerDTO;
import com.gcs.flexba.rmsweb.service.mapper.CustomerMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.CustomerCriteria;
import com.gcs.flexba.rmsweb.service.CustomerQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CustomerResource REST controller.
 *
 * @see CustomerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class CustomerResourceIntTest {

    private static final String DEFAULT_CUSTOMER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIDDLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_GENDER = 1;
    private static final Integer UPDATED_GENDER = 2;

    private static final String DEFAULT_TEL_2 = "AAAAAAAAAA";
    private static final String UPDATED_TEL_2 = "BBBBBBBBBB";

    private static final String DEFAULT_TEL_1 = "AAAAAAAAAA";
    private static final String UPDATED_TEL_1 = "BBBBBBBBBB";

    private static final String DEFAULT_TEL_3 = "AAAAAAAAAA";
    private static final String UPDATED_TEL_3 = "BBBBBBBBBB";

    private static final String DEFAULT_FACEBOOK_ID = "AAAAAAAAAA";
    private static final String UPDATED_FACEBOOK_ID = "BBBBBBBBBB";

    @Autowired
    private CustomerRepository customerRepository;


    @Autowired
    private CustomerMapper customerMapper;
    

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerQueryService customerQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCustomerMockMvc;

    private Customer customer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CustomerResource customerResource = new CustomerResource(customerService, customerQueryService);
        this.restCustomerMockMvc = MockMvcBuilders.standaloneSetup(customerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Customer createEntity(EntityManager em) {
        Customer customer = new Customer()
            .customerCode(DEFAULT_CUSTOMER_CODE)
            .firstName(DEFAULT_FIRST_NAME)
            .middleName(DEFAULT_MIDDLE_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .fullName(DEFAULT_FULL_NAME)
            .gender(DEFAULT_GENDER)
            .tel2(DEFAULT_TEL_2)
            .tel1(DEFAULT_TEL_1)
            .tel3(DEFAULT_TEL_3)
            .facebookId(DEFAULT_FACEBOOK_ID);
        return customer;
    }

    @Before
    public void initTest() {
        customer = createEntity(em);
    }

    @Test
    @Transactional
    public void createCustomer() throws Exception {
        int databaseSizeBeforeCreate = customerRepository.findAll().size();

        // Create the Customer
        CustomerDTO customerDTO = customerMapper.toDto(customer);
        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isCreated());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeCreate + 1);
        Customer testCustomer = customerList.get(customerList.size() - 1);
        assertThat(testCustomer.getCustomerCode()).isEqualTo(DEFAULT_CUSTOMER_CODE);
        assertThat(testCustomer.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testCustomer.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
        assertThat(testCustomer.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testCustomer.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testCustomer.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testCustomer.getTel2()).isEqualTo(DEFAULT_TEL_2);
        assertThat(testCustomer.getTel1()).isEqualTo(DEFAULT_TEL_1);
        assertThat(testCustomer.getTel3()).isEqualTo(DEFAULT_TEL_3);
        assertThat(testCustomer.getFacebookId()).isEqualTo(DEFAULT_FACEBOOK_ID);
    }

    @Test
    @Transactional
    public void createCustomerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customerRepository.findAll().size();

        // Create the Customer with an existing ID
        customer.setId(1L);
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCustomers() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customer.getId().intValue())))
            .andExpect(jsonPath("$.[*].customerCode").value(hasItem(DEFAULT_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].tel2").value(hasItem(DEFAULT_TEL_2.toString())))
            .andExpect(jsonPath("$.[*].tel1").value(hasItem(DEFAULT_TEL_1.toString())))
            .andExpect(jsonPath("$.[*].tel3").value(hasItem(DEFAULT_TEL_3.toString())))
            .andExpect(jsonPath("$.[*].facebookId").value(hasItem(DEFAULT_FACEBOOK_ID.toString())));
    }
    

    @Test
    @Transactional
    public void getCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", customer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(customer.getId().intValue()))
            .andExpect(jsonPath("$.customerCode").value(DEFAULT_CUSTOMER_CODE.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.tel2").value(DEFAULT_TEL_2.toString()))
            .andExpect(jsonPath("$.tel1").value(DEFAULT_TEL_1.toString()))
            .andExpect(jsonPath("$.tel3").value(DEFAULT_TEL_3.toString()))
            .andExpect(jsonPath("$.facebookId").value(DEFAULT_FACEBOOK_ID.toString()));
    }

    @Test
    @Transactional
    public void getAllCustomersByCustomerCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where customerCode equals to DEFAULT_CUSTOMER_CODE
        defaultCustomerShouldBeFound("customerCode.equals=" + DEFAULT_CUSTOMER_CODE);

        // Get all the customerList where customerCode equals to UPDATED_CUSTOMER_CODE
        defaultCustomerShouldNotBeFound("customerCode.equals=" + UPDATED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllCustomersByCustomerCodeIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where customerCode in DEFAULT_CUSTOMER_CODE or UPDATED_CUSTOMER_CODE
        defaultCustomerShouldBeFound("customerCode.in=" + DEFAULT_CUSTOMER_CODE + "," + UPDATED_CUSTOMER_CODE);

        // Get all the customerList where customerCode equals to UPDATED_CUSTOMER_CODE
        defaultCustomerShouldNotBeFound("customerCode.in=" + UPDATED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllCustomersByCustomerCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where customerCode is not null
        defaultCustomerShouldBeFound("customerCode.specified=true");

        // Get all the customerList where customerCode is null
        defaultCustomerShouldNotBeFound("customerCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByFirstNameIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where firstName equals to DEFAULT_FIRST_NAME
        defaultCustomerShouldBeFound("firstName.equals=" + DEFAULT_FIRST_NAME);

        // Get all the customerList where firstName equals to UPDATED_FIRST_NAME
        defaultCustomerShouldNotBeFound("firstName.equals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByFirstNameIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where firstName in DEFAULT_FIRST_NAME or UPDATED_FIRST_NAME
        defaultCustomerShouldBeFound("firstName.in=" + DEFAULT_FIRST_NAME + "," + UPDATED_FIRST_NAME);

        // Get all the customerList where firstName equals to UPDATED_FIRST_NAME
        defaultCustomerShouldNotBeFound("firstName.in=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByFirstNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where firstName is not null
        defaultCustomerShouldBeFound("firstName.specified=true");

        // Get all the customerList where firstName is null
        defaultCustomerShouldNotBeFound("firstName.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByMiddleNameIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where middleName equals to DEFAULT_MIDDLE_NAME
        defaultCustomerShouldBeFound("middleName.equals=" + DEFAULT_MIDDLE_NAME);

        // Get all the customerList where middleName equals to UPDATED_MIDDLE_NAME
        defaultCustomerShouldNotBeFound("middleName.equals=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByMiddleNameIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where middleName in DEFAULT_MIDDLE_NAME or UPDATED_MIDDLE_NAME
        defaultCustomerShouldBeFound("middleName.in=" + DEFAULT_MIDDLE_NAME + "," + UPDATED_MIDDLE_NAME);

        // Get all the customerList where middleName equals to UPDATED_MIDDLE_NAME
        defaultCustomerShouldNotBeFound("middleName.in=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByMiddleNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where middleName is not null
        defaultCustomerShouldBeFound("middleName.specified=true");

        // Get all the customerList where middleName is null
        defaultCustomerShouldNotBeFound("middleName.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByLastNameIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where lastName equals to DEFAULT_LAST_NAME
        defaultCustomerShouldBeFound("lastName.equals=" + DEFAULT_LAST_NAME);

        // Get all the customerList where lastName equals to UPDATED_LAST_NAME
        defaultCustomerShouldNotBeFound("lastName.equals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByLastNameIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where lastName in DEFAULT_LAST_NAME or UPDATED_LAST_NAME
        defaultCustomerShouldBeFound("lastName.in=" + DEFAULT_LAST_NAME + "," + UPDATED_LAST_NAME);

        // Get all the customerList where lastName equals to UPDATED_LAST_NAME
        defaultCustomerShouldNotBeFound("lastName.in=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByLastNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where lastName is not null
        defaultCustomerShouldBeFound("lastName.specified=true");

        // Get all the customerList where lastName is null
        defaultCustomerShouldNotBeFound("lastName.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByFullNameIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fullName equals to DEFAULT_FULL_NAME
        defaultCustomerShouldBeFound("fullName.equals=" + DEFAULT_FULL_NAME);

        // Get all the customerList where fullName equals to UPDATED_FULL_NAME
        defaultCustomerShouldNotBeFound("fullName.equals=" + UPDATED_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByFullNameIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fullName in DEFAULT_FULL_NAME or UPDATED_FULL_NAME
        defaultCustomerShouldBeFound("fullName.in=" + DEFAULT_FULL_NAME + "," + UPDATED_FULL_NAME);

        // Get all the customerList where fullName equals to UPDATED_FULL_NAME
        defaultCustomerShouldNotBeFound("fullName.in=" + UPDATED_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByFullNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fullName is not null
        defaultCustomerShouldBeFound("fullName.specified=true");

        // Get all the customerList where fullName is null
        defaultCustomerShouldNotBeFound("fullName.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByGenderIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender equals to DEFAULT_GENDER
        defaultCustomerShouldBeFound("gender.equals=" + DEFAULT_GENDER);

        // Get all the customerList where gender equals to UPDATED_GENDER
        defaultCustomerShouldNotBeFound("gender.equals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllCustomersByGenderIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender in DEFAULT_GENDER or UPDATED_GENDER
        defaultCustomerShouldBeFound("gender.in=" + DEFAULT_GENDER + "," + UPDATED_GENDER);

        // Get all the customerList where gender equals to UPDATED_GENDER
        defaultCustomerShouldNotBeFound("gender.in=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllCustomersByGenderIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender is not null
        defaultCustomerShouldBeFound("gender.specified=true");

        // Get all the customerList where gender is null
        defaultCustomerShouldNotBeFound("gender.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByGenderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender greater than or equals to DEFAULT_GENDER
        defaultCustomerShouldBeFound("gender.greaterOrEqualThan=" + DEFAULT_GENDER);

        // Get all the customerList where gender greater than or equals to UPDATED_GENDER
        defaultCustomerShouldNotBeFound("gender.greaterOrEqualThan=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllCustomersByGenderIsLessThanSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender less than or equals to DEFAULT_GENDER
        defaultCustomerShouldNotBeFound("gender.lessThan=" + DEFAULT_GENDER);

        // Get all the customerList where gender less than or equals to UPDATED_GENDER
        defaultCustomerShouldBeFound("gender.lessThan=" + UPDATED_GENDER);
    }


    @Test
    @Transactional
    public void getAllCustomersByTel2IsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel2 equals to DEFAULT_TEL_2
        defaultCustomerShouldBeFound("tel2.equals=" + DEFAULT_TEL_2);

        // Get all the customerList where tel2 equals to UPDATED_TEL_2
        defaultCustomerShouldNotBeFound("tel2.equals=" + UPDATED_TEL_2);
    }

    @Test
    @Transactional
    public void getAllCustomersByTel2IsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel2 in DEFAULT_TEL_2 or UPDATED_TEL_2
        defaultCustomerShouldBeFound("tel2.in=" + DEFAULT_TEL_2 + "," + UPDATED_TEL_2);

        // Get all the customerList where tel2 equals to UPDATED_TEL_2
        defaultCustomerShouldNotBeFound("tel2.in=" + UPDATED_TEL_2);
    }

    @Test
    @Transactional
    public void getAllCustomersByTel2IsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel2 is not null
        defaultCustomerShouldBeFound("tel2.specified=true");

        // Get all the customerList where tel2 is null
        defaultCustomerShouldNotBeFound("tel2.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByTel1IsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel1 equals to DEFAULT_TEL_1
        defaultCustomerShouldBeFound("tel1.equals=" + DEFAULT_TEL_1);

        // Get all the customerList where tel1 equals to UPDATED_TEL_1
        defaultCustomerShouldNotBeFound("tel1.equals=" + UPDATED_TEL_1);
    }

    @Test
    @Transactional
    public void getAllCustomersByTel1IsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel1 in DEFAULT_TEL_1 or UPDATED_TEL_1
        defaultCustomerShouldBeFound("tel1.in=" + DEFAULT_TEL_1 + "," + UPDATED_TEL_1);

        // Get all the customerList where tel1 equals to UPDATED_TEL_1
        defaultCustomerShouldNotBeFound("tel1.in=" + UPDATED_TEL_1);
    }

    @Test
    @Transactional
    public void getAllCustomersByTel1IsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel1 is not null
        defaultCustomerShouldBeFound("tel1.specified=true");

        // Get all the customerList where tel1 is null
        defaultCustomerShouldNotBeFound("tel1.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByTel3IsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel3 equals to DEFAULT_TEL_3
        defaultCustomerShouldBeFound("tel3.equals=" + DEFAULT_TEL_3);

        // Get all the customerList where tel3 equals to UPDATED_TEL_3
        defaultCustomerShouldNotBeFound("tel3.equals=" + UPDATED_TEL_3);
    }

    @Test
    @Transactional
    public void getAllCustomersByTel3IsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel3 in DEFAULT_TEL_3 or UPDATED_TEL_3
        defaultCustomerShouldBeFound("tel3.in=" + DEFAULT_TEL_3 + "," + UPDATED_TEL_3);

        // Get all the customerList where tel3 equals to UPDATED_TEL_3
        defaultCustomerShouldNotBeFound("tel3.in=" + UPDATED_TEL_3);
    }

    @Test
    @Transactional
    public void getAllCustomersByTel3IsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel3 is not null
        defaultCustomerShouldBeFound("tel3.specified=true");

        // Get all the customerList where tel3 is null
        defaultCustomerShouldNotBeFound("tel3.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByFacebookIdIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where facebookId equals to DEFAULT_FACEBOOK_ID
        defaultCustomerShouldBeFound("facebookId.equals=" + DEFAULT_FACEBOOK_ID);

        // Get all the customerList where facebookId equals to UPDATED_FACEBOOK_ID
        defaultCustomerShouldNotBeFound("facebookId.equals=" + UPDATED_FACEBOOK_ID);
    }

    @Test
    @Transactional
    public void getAllCustomersByFacebookIdIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where facebookId in DEFAULT_FACEBOOK_ID or UPDATED_FACEBOOK_ID
        defaultCustomerShouldBeFound("facebookId.in=" + DEFAULT_FACEBOOK_ID + "," + UPDATED_FACEBOOK_ID);

        // Get all the customerList where facebookId equals to UPDATED_FACEBOOK_ID
        defaultCustomerShouldNotBeFound("facebookId.in=" + UPDATED_FACEBOOK_ID);
    }

    @Test
    @Transactional
    public void getAllCustomersByFacebookIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where facebookId is not null
        defaultCustomerShouldBeFound("facebookId.specified=true");

        // Get all the customerList where facebookId is null
        defaultCustomerShouldNotBeFound("facebookId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByMembershipHistotyIsEqualToSomething() throws Exception {
        // Initialize the database
        MembershipHistoty membershipHistoty = MembershipHistotyResourceIntTest.createEntity(em);
        em.persist(membershipHistoty);
        em.flush();
        customer.addMembershipHistoty(membershipHistoty);
        customerRepository.saveAndFlush(customer);
        Long membershipHistotyId = membershipHistoty.getId();

        // Get all the customerList where membershipHistoty equals to membershipHistotyId
        defaultCustomerShouldBeFound("membershipHistotyId.equals=" + membershipHistotyId);

        // Get all the customerList where membershipHistoty equals to membershipHistotyId + 1
        defaultCustomerShouldNotBeFound("membershipHistotyId.equals=" + (membershipHistotyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultCustomerShouldBeFound(String filter) throws Exception {
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customer.getId().intValue())))
            .andExpect(jsonPath("$.[*].customerCode").value(hasItem(DEFAULT_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].tel2").value(hasItem(DEFAULT_TEL_2.toString())))
            .andExpect(jsonPath("$.[*].tel1").value(hasItem(DEFAULT_TEL_1.toString())))
            .andExpect(jsonPath("$.[*].tel3").value(hasItem(DEFAULT_TEL_3.toString())))
            .andExpect(jsonPath("$.[*].facebookId").value(hasItem(DEFAULT_FACEBOOK_ID.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultCustomerShouldNotBeFound(String filter) throws Exception {
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingCustomer() throws Exception {
        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        int databaseSizeBeforeUpdate = customerRepository.findAll().size();

        // Update the customer
        Customer updatedCustomer = customerRepository.findById(customer.getId()).get();
        // Disconnect from session so that the updates on updatedCustomer are not directly saved in db
        em.detach(updatedCustomer);
        updatedCustomer
            .customerCode(UPDATED_CUSTOMER_CODE)
            .firstName(UPDATED_FIRST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .lastName(UPDATED_LAST_NAME)
            .fullName(UPDATED_FULL_NAME)
            .gender(UPDATED_GENDER)
            .tel2(UPDATED_TEL_2)
            .tel1(UPDATED_TEL_1)
            .tel3(UPDATED_TEL_3)
            .facebookId(UPDATED_FACEBOOK_ID);
        CustomerDTO customerDTO = customerMapper.toDto(updatedCustomer);

        restCustomerMockMvc.perform(put("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isOk());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeUpdate);
        Customer testCustomer = customerList.get(customerList.size() - 1);
        assertThat(testCustomer.getCustomerCode()).isEqualTo(UPDATED_CUSTOMER_CODE);
        assertThat(testCustomer.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testCustomer.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
        assertThat(testCustomer.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testCustomer.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testCustomer.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testCustomer.getTel2()).isEqualTo(UPDATED_TEL_2);
        assertThat(testCustomer.getTel1()).isEqualTo(UPDATED_TEL_1);
        assertThat(testCustomer.getTel3()).isEqualTo(UPDATED_TEL_3);
        assertThat(testCustomer.getFacebookId()).isEqualTo(UPDATED_FACEBOOK_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingCustomer() throws Exception {
        int databaseSizeBeforeUpdate = customerRepository.findAll().size();

        // Create the Customer
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCustomerMockMvc.perform(put("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        int databaseSizeBeforeDelete = customerRepository.findAll().size();

        // Get the customer
        restCustomerMockMvc.perform(delete("/api/customers/{id}", customer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Customer.class);
        Customer customer1 = new Customer();
        customer1.setId(1L);
        Customer customer2 = new Customer();
        customer2.setId(customer1.getId());
        assertThat(customer1).isEqualTo(customer2);
        customer2.setId(2L);
        assertThat(customer1).isNotEqualTo(customer2);
        customer1.setId(null);
        assertThat(customer1).isNotEqualTo(customer2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerDTO.class);
        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setId(1L);
        CustomerDTO customerDTO2 = new CustomerDTO();
        assertThat(customerDTO1).isNotEqualTo(customerDTO2);
        customerDTO2.setId(customerDTO1.getId());
        assertThat(customerDTO1).isEqualTo(customerDTO2);
        customerDTO2.setId(2L);
        assertThat(customerDTO1).isNotEqualTo(customerDTO2);
        customerDTO1.setId(null);
        assertThat(customerDTO1).isNotEqualTo(customerDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(customerMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(customerMapper.fromId(null)).isNull();
    }
}
