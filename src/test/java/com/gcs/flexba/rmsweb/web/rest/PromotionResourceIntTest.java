package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.Promotion;
import com.gcs.flexba.rmsweb.repository.PromotionRepository;
import com.gcs.flexba.rmsweb.service.PromotionService;
import com.gcs.flexba.rmsweb.service.SaleService;
import com.gcs.flexba.rmsweb.service.dto.PromotionDTO;
import com.gcs.flexba.rmsweb.service.mapper.PromotionMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.PromotionCriteria;
import com.gcs.flexba.rmsweb.service.PromotionQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PromotionResource REST controller.
 *
 * @see PromotionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class PromotionResourceIntTest {

    private static final String DEFAULT_COUPON_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COUPON_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PROMOTION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PROMOTION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_HTML_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_HTML_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final Boolean DEFAULT_STOP_NEXT_RULE = false;
    private static final Boolean UPDATED_STOP_NEXT_RULE = true;

    private static final Integer DEFAULT_SORT_ORDER = 1;
    private static final Integer UPDATED_SORT_ORDER = 2;

    private static final Boolean DEFAULT_USE_ORG_PRICE_FLAG = false;
    private static final Boolean UPDATED_USE_ORG_PRICE_FLAG = true;

    private static final Integer DEFAULT_APPLIED_ON = 1;
    private static final Integer UPDATED_APPLIED_ON = 2;

    private static final byte[] DEFAULT_RULE_DATA = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_RULE_DATA = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_RULE_DATA_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_RULE_DATA_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DROOLS_RULE = "AAAAAAAAAA";
    private static final String UPDATED_DROOLS_RULE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_ALLOCATION_DATA = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_ALLOCATION_DATA = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_ALLOCATION_DATA_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_ALLOCATION_DATA_CONTENT_TYPE = "image/png";

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;

    private static final Integer DEFAULT_RUN_MODE = 1;
    private static final Integer UPDATED_RUN_MODE = 2;

    private static final String DEFAULT_ON_STORE = "AAAAAAAAAA";
    private static final String UPDATED_ON_STORE = "BBBBBBBBBB";

    private static final Integer DEFAULT_PROMOTION_TYPE = 1;
    private static final Integer UPDATED_PROMOTION_TYPE = 2;

    private static final Boolean DEFAULT_AUTO_FLAG = false;
    private static final Boolean UPDATED_AUTO_FLAG = true;

    private static final ZonedDateTime DEFAULT_UPDATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETE_FLAG = false;
    private static final Boolean UPDATED_DELETE_FLAG = true;

    private static final String DEFAULT_CHANNEL = "AAAAAAAAAA";
    private static final String UPDATED_CHANNEL = "BBBBBBBBBB";

    private static final String DEFAULT_JS_CONDITION = "AAAAAAAAAA";
    private static final String UPDATED_JS_CONDITION = "BBBBBBBBBB";

    private static final String DEFAULT_JS_BONUS = "AAAAAAAAAA";
    private static final String UPDATED_JS_BONUS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_REDEEM_FLAG = false;
    private static final Boolean UPDATED_REDEEM_FLAG = true;

    private static final String DEFAULT_IMAGE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_PATH_2 = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_PATH_2 = "BBBBBBBBBB";

    private static final Double DEFAULT_REDEEM_POINTS = 1D;
    private static final Double UPDATED_REDEEM_POINTS = 2D;

    private static final byte[] DEFAULT_IMAGE_BLOB_1 = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE_BLOB_1 = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGE_BLOB_1_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_BLOB_1_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_IMAGE_BLOB_2 = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE_BLOB_2 = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGE_BLOB_2_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_BLOB_2_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_PUBLISH_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PUBLISH_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_PUBLISH_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PUBLISH_END_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PromotionRepository promotionRepository;


    @Autowired
    private PromotionMapper promotionMapper;
    

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private PromotionQueryService promotionQueryService;
    
    @Autowired
    private SaleService saleService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPromotionMockMvc;

    private Promotion promotion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PromotionResource promotionResource = new PromotionResource(promotionService, promotionQueryService, saleService);
        this.restPromotionMockMvc = MockMvcBuilders.standaloneSetup(promotionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Promotion createEntity(EntityManager em) {
        Promotion promotion = new Promotion()
            .couponCode(DEFAULT_COUPON_CODE)
            .promotionName(DEFAULT_PROMOTION_NAME)
            .description(DEFAULT_DESCRIPTION)
            .htmlDescription(DEFAULT_HTML_DESCRIPTION)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .status(DEFAULT_STATUS)
            .stopNextRule(DEFAULT_STOP_NEXT_RULE)
            .sortOrder(DEFAULT_SORT_ORDER)
            .useOrgPriceFlag(DEFAULT_USE_ORG_PRICE_FLAG)
            .appliedOn(DEFAULT_APPLIED_ON)
            .ruleData(DEFAULT_RULE_DATA)
            .ruleDataContentType(DEFAULT_RULE_DATA_CONTENT_TYPE)
            .droolsRule(DEFAULT_DROOLS_RULE)
            .allocationData(DEFAULT_ALLOCATION_DATA)
            .allocationDataContentType(DEFAULT_ALLOCATION_DATA_CONTENT_TYPE)
            .version(DEFAULT_VERSION)
            .runMode(DEFAULT_RUN_MODE)
            .onStore(DEFAULT_ON_STORE)
            .promotionType(DEFAULT_PROMOTION_TYPE)
            .autoFlag(DEFAULT_AUTO_FLAG)
            .updateDate(DEFAULT_UPDATE_DATE)
            .deleteFlag(DEFAULT_DELETE_FLAG)
            .channel(DEFAULT_CHANNEL)
            .jsCondition(DEFAULT_JS_CONDITION)
            .jsBonus(DEFAULT_JS_BONUS)
            .redeemFlag(DEFAULT_REDEEM_FLAG)
            .imagePath(DEFAULT_IMAGE_PATH)
            .imagePath2(DEFAULT_IMAGE_PATH_2)
            .redeemPoints(DEFAULT_REDEEM_POINTS)
            .imageBlob1(DEFAULT_IMAGE_BLOB_1)
            .imageBlob1ContentType(DEFAULT_IMAGE_BLOB_1_CONTENT_TYPE)
            .imageBlob2(DEFAULT_IMAGE_BLOB_2)
            .imageBlob2ContentType(DEFAULT_IMAGE_BLOB_2_CONTENT_TYPE)
            .publishStartDate(DEFAULT_PUBLISH_START_DATE)
            .publishEndDate(DEFAULT_PUBLISH_END_DATE);
        return promotion;
    }

    @Before
    public void initTest() {
        promotion = createEntity(em);
    }

    @Test
    @Transactional
    public void createPromotion() throws Exception {
        int databaseSizeBeforeCreate = promotionRepository.findAll().size();

        // Create the Promotion
        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);
        restPromotionMockMvc.perform(post("/api/promotions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
            .andExpect(status().isCreated());

        // Validate the Promotion in the database
        List<Promotion> promotionList = promotionRepository.findAll();
        assertThat(promotionList).hasSize(databaseSizeBeforeCreate + 1);
        Promotion testPromotion = promotionList.get(promotionList.size() - 1);
        assertThat(testPromotion.getCouponCode()).isEqualTo(DEFAULT_COUPON_CODE);
        assertThat(testPromotion.getPromotionName()).isEqualTo(DEFAULT_PROMOTION_NAME);
        assertThat(testPromotion.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPromotion.getHtmlDescription()).isEqualTo(DEFAULT_HTML_DESCRIPTION);
        assertThat(testPromotion.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testPromotion.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testPromotion.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPromotion.isStopNextRule()).isEqualTo(DEFAULT_STOP_NEXT_RULE);
        assertThat(testPromotion.getSortOrder()).isEqualTo(DEFAULT_SORT_ORDER);
        assertThat(testPromotion.isUseOrgPriceFlag()).isEqualTo(DEFAULT_USE_ORG_PRICE_FLAG);
        assertThat(testPromotion.getAppliedOn()).isEqualTo(DEFAULT_APPLIED_ON);
        assertThat(testPromotion.getRuleData()).isEqualTo(DEFAULT_RULE_DATA);
        assertThat(testPromotion.getRuleDataContentType()).isEqualTo(DEFAULT_RULE_DATA_CONTENT_TYPE);
        assertThat(testPromotion.getDroolsRule()).isEqualTo(DEFAULT_DROOLS_RULE);
        assertThat(testPromotion.getAllocationData()).isEqualTo(DEFAULT_ALLOCATION_DATA);
        assertThat(testPromotion.getAllocationDataContentType()).isEqualTo(DEFAULT_ALLOCATION_DATA_CONTENT_TYPE);
        assertThat(testPromotion.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testPromotion.getRunMode()).isEqualTo(DEFAULT_RUN_MODE);
        assertThat(testPromotion.getOnStore()).isEqualTo(DEFAULT_ON_STORE);
        assertThat(testPromotion.getPromotionType()).isEqualTo(DEFAULT_PROMOTION_TYPE);
        assertThat(testPromotion.isAutoFlag()).isEqualTo(DEFAULT_AUTO_FLAG);
        assertThat(testPromotion.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testPromotion.isDeleteFlag()).isEqualTo(DEFAULT_DELETE_FLAG);
        assertThat(testPromotion.getChannel()).isEqualTo(DEFAULT_CHANNEL);
        assertThat(testPromotion.getJsCondition()).isEqualTo(DEFAULT_JS_CONDITION);
        assertThat(testPromotion.getJsBonus()).isEqualTo(DEFAULT_JS_BONUS);
        assertThat(testPromotion.isRedeemFlag()).isEqualTo(DEFAULT_REDEEM_FLAG);
        assertThat(testPromotion.getImagePath()).isEqualTo(DEFAULT_IMAGE_PATH);
        assertThat(testPromotion.getImagePath2()).isEqualTo(DEFAULT_IMAGE_PATH_2);
        assertThat(testPromotion.getRedeemPoints()).isEqualTo(DEFAULT_REDEEM_POINTS);
        assertThat(testPromotion.getImageBlob1()).isEqualTo(DEFAULT_IMAGE_BLOB_1);
        assertThat(testPromotion.getImageBlob1ContentType()).isEqualTo(DEFAULT_IMAGE_BLOB_1_CONTENT_TYPE);
        assertThat(testPromotion.getImageBlob2()).isEqualTo(DEFAULT_IMAGE_BLOB_2);
        assertThat(testPromotion.getImageBlob2ContentType()).isEqualTo(DEFAULT_IMAGE_BLOB_2_CONTENT_TYPE);
        assertThat(testPromotion.getPublishStartDate()).isEqualTo(DEFAULT_PUBLISH_START_DATE);
        assertThat(testPromotion.getPublishEndDate()).isEqualTo(DEFAULT_PUBLISH_END_DATE);
    }

    @Test
    @Transactional
    public void createPromotionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = promotionRepository.findAll().size();

        // Create the Promotion with an existing ID
        promotion.setId(1L);
        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPromotionMockMvc.perform(post("/api/promotions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Promotion in the database
        List<Promotion> promotionList = promotionRepository.findAll();
        assertThat(promotionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPromotions() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList
        restPromotionMockMvc.perform(get("/api/promotions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(promotion.getId().intValue())))
            .andExpect(jsonPath("$.[*].couponCode").value(hasItem(DEFAULT_COUPON_CODE.toString())))
            .andExpect(jsonPath("$.[*].promotionName").value(hasItem(DEFAULT_PROMOTION_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].htmlDescription").value(hasItem(DEFAULT_HTML_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].start_date").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].stopNextRule").value(hasItem(DEFAULT_STOP_NEXT_RULE.booleanValue())))
            .andExpect(jsonPath("$.[*].sortOrder").value(hasItem(DEFAULT_SORT_ORDER)))
            .andExpect(jsonPath("$.[*].useOrgPriceFlag").value(hasItem(DEFAULT_USE_ORG_PRICE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].appliedOn").value(hasItem(DEFAULT_APPLIED_ON)))
            .andExpect(jsonPath("$.[*].ruleDataContentType").value(hasItem(DEFAULT_RULE_DATA_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].ruleData").value(hasItem(Base64Utils.encodeToString(DEFAULT_RULE_DATA))))
            .andExpect(jsonPath("$.[*].droolsRule").value(hasItem(DEFAULT_DROOLS_RULE.toString())))
            .andExpect(jsonPath("$.[*].allocationDataContentType").value(hasItem(DEFAULT_ALLOCATION_DATA_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].allocationData").value(hasItem(Base64Utils.encodeToString(DEFAULT_ALLOCATION_DATA))))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
            .andExpect(jsonPath("$.[*].runMode").value(hasItem(DEFAULT_RUN_MODE)))
            .andExpect(jsonPath("$.[*].onStore").value(hasItem(DEFAULT_ON_STORE.toString())))
            .andExpect(jsonPath("$.[*].promotionType").value(hasItem(DEFAULT_PROMOTION_TYPE)))
            .andExpect(jsonPath("$.[*].autoFlag").value(hasItem(DEFAULT_AUTO_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].channel").value(hasItem(DEFAULT_CHANNEL.toString())))
            .andExpect(jsonPath("$.[*].jsCondition").value(hasItem(DEFAULT_JS_CONDITION.toString())))
            .andExpect(jsonPath("$.[*].jsBonus").value(hasItem(DEFAULT_JS_BONUS.toString())))
            .andExpect(jsonPath("$.[*].redeemFlag").value(hasItem(DEFAULT_REDEEM_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].imagePath").value(hasItem(DEFAULT_IMAGE_PATH.toString())))
            .andExpect(jsonPath("$.[*].imagePath2").value(hasItem(DEFAULT_IMAGE_PATH_2.toString())))
            .andExpect(jsonPath("$.[*].redeemPoints").value(hasItem(DEFAULT_REDEEM_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].imageBlob1ContentType").value(hasItem(DEFAULT_IMAGE_BLOB_1_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageBlob1").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_BLOB_1))))
            .andExpect(jsonPath("$.[*].imageBlob2ContentType").value(hasItem(DEFAULT_IMAGE_BLOB_2_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageBlob2").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_BLOB_2))))
            .andExpect(jsonPath("$.[*].publishStartDate").value(hasItem(DEFAULT_PUBLISH_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].publishEndDate").value(hasItem(DEFAULT_PUBLISH_END_DATE.toString())));
    }
    

    @Test
    @Transactional
    public void getPromotion() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get the promotion
        restPromotionMockMvc.perform(get("/api/promotions/{id}", promotion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(promotion.getId().intValue()))
            .andExpect(jsonPath("$.couponCode").value(DEFAULT_COUPON_CODE.toString()))
            .andExpect(jsonPath("$.promotionName").value(DEFAULT_PROMOTION_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.htmlDescription").value(DEFAULT_HTML_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.start_date").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.stopNextRule").value(DEFAULT_STOP_NEXT_RULE.booleanValue()))
            .andExpect(jsonPath("$.sortOrder").value(DEFAULT_SORT_ORDER))
            .andExpect(jsonPath("$.useOrgPriceFlag").value(DEFAULT_USE_ORG_PRICE_FLAG.booleanValue()))
            .andExpect(jsonPath("$.appliedOn").value(DEFAULT_APPLIED_ON))
            .andExpect(jsonPath("$.ruleDataContentType").value(DEFAULT_RULE_DATA_CONTENT_TYPE))
            .andExpect(jsonPath("$.ruleData").value(Base64Utils.encodeToString(DEFAULT_RULE_DATA)))
            .andExpect(jsonPath("$.droolsRule").value(DEFAULT_DROOLS_RULE.toString()))
            .andExpect(jsonPath("$.allocationDataContentType").value(DEFAULT_ALLOCATION_DATA_CONTENT_TYPE))
            .andExpect(jsonPath("$.allocationData").value(Base64Utils.encodeToString(DEFAULT_ALLOCATION_DATA)))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.runMode").value(DEFAULT_RUN_MODE))
            .andExpect(jsonPath("$.onStore").value(DEFAULT_ON_STORE.toString()))
            .andExpect(jsonPath("$.promotionType").value(DEFAULT_PROMOTION_TYPE))
            .andExpect(jsonPath("$.autoFlag").value(DEFAULT_AUTO_FLAG.booleanValue()))
            .andExpect(jsonPath("$.updateDate").value(sameInstant(DEFAULT_UPDATE_DATE)))
            .andExpect(jsonPath("$.deleteFlag").value(DEFAULT_DELETE_FLAG.booleanValue()))
            .andExpect(jsonPath("$.channel").value(DEFAULT_CHANNEL.toString()))
            .andExpect(jsonPath("$.jsCondition").value(DEFAULT_JS_CONDITION.toString()))
            .andExpect(jsonPath("$.jsBonus").value(DEFAULT_JS_BONUS.toString()))
            .andExpect(jsonPath("$.redeemFlag").value(DEFAULT_REDEEM_FLAG.booleanValue()))
            .andExpect(jsonPath("$.imagePath").value(DEFAULT_IMAGE_PATH.toString()))
            .andExpect(jsonPath("$.imagePath2").value(DEFAULT_IMAGE_PATH_2.toString()))
            .andExpect(jsonPath("$.redeemPoints").value(DEFAULT_REDEEM_POINTS.doubleValue()))
            .andExpect(jsonPath("$.imageBlob1ContentType").value(DEFAULT_IMAGE_BLOB_1_CONTENT_TYPE))
            .andExpect(jsonPath("$.imageBlob1").value(Base64Utils.encodeToString(DEFAULT_IMAGE_BLOB_1)))
            .andExpect(jsonPath("$.imageBlob2ContentType").value(DEFAULT_IMAGE_BLOB_2_CONTENT_TYPE))
            .andExpect(jsonPath("$.imageBlob2").value(Base64Utils.encodeToString(DEFAULT_IMAGE_BLOB_2)))
            .andExpect(jsonPath("$.publishStartDate").value(DEFAULT_PUBLISH_START_DATE.toString()))
            .andExpect(jsonPath("$.publishEndDate").value(DEFAULT_PUBLISH_END_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllPromotionsByCouponCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where couponCode equals to DEFAULT_COUPON_CODE
        defaultPromotionShouldBeFound("couponCode.equals=" + DEFAULT_COUPON_CODE);

        // Get all the promotionList where couponCode equals to UPDATED_COUPON_CODE
        defaultPromotionShouldNotBeFound("couponCode.equals=" + UPDATED_COUPON_CODE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByCouponCodeIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where couponCode in DEFAULT_COUPON_CODE or UPDATED_COUPON_CODE
        defaultPromotionShouldBeFound("couponCode.in=" + DEFAULT_COUPON_CODE + "," + UPDATED_COUPON_CODE);

        // Get all the promotionList where couponCode equals to UPDATED_COUPON_CODE
        defaultPromotionShouldNotBeFound("couponCode.in=" + UPDATED_COUPON_CODE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByCouponCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where couponCode is not null
        defaultPromotionShouldBeFound("couponCode.specified=true");

        // Get all the promotionList where couponCode is null
        defaultPromotionShouldNotBeFound("couponCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByPromotionNameIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where promotionName equals to DEFAULT_PROMOTION_NAME
        defaultPromotionShouldBeFound("promotionName.equals=" + DEFAULT_PROMOTION_NAME);

        // Get all the promotionList where promotionName equals to UPDATED_PROMOTION_NAME
        defaultPromotionShouldNotBeFound("promotionName.equals=" + UPDATED_PROMOTION_NAME);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPromotionNameIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where promotionName in DEFAULT_PROMOTION_NAME or UPDATED_PROMOTION_NAME
        defaultPromotionShouldBeFound("promotionName.in=" + DEFAULT_PROMOTION_NAME + "," + UPDATED_PROMOTION_NAME);

        // Get all the promotionList where promotionName equals to UPDATED_PROMOTION_NAME
        defaultPromotionShouldNotBeFound("promotionName.in=" + UPDATED_PROMOTION_NAME);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPromotionNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where promotionName is not null
        defaultPromotionShouldBeFound("promotionName.specified=true");

        // Get all the promotionList where promotionName is null
        defaultPromotionShouldNotBeFound("promotionName.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where description equals to DEFAULT_DESCRIPTION
        defaultPromotionShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the promotionList where description equals to UPDATED_DESCRIPTION
        defaultPromotionShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultPromotionShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the promotionList where description equals to UPDATED_DESCRIPTION
        defaultPromotionShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where description is not null
        defaultPromotionShouldBeFound("description.specified=true");

        // Get all the promotionList where description is null
        defaultPromotionShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByHtmlDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where htmlDescription equals to DEFAULT_HTML_DESCRIPTION
        defaultPromotionShouldBeFound("htmlDescription.equals=" + DEFAULT_HTML_DESCRIPTION);

        // Get all the promotionList where htmlDescription equals to UPDATED_HTML_DESCRIPTION
        defaultPromotionShouldNotBeFound("htmlDescription.equals=" + UPDATED_HTML_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByHtmlDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where htmlDescription in DEFAULT_HTML_DESCRIPTION or UPDATED_HTML_DESCRIPTION
        defaultPromotionShouldBeFound("htmlDescription.in=" + DEFAULT_HTML_DESCRIPTION + "," + UPDATED_HTML_DESCRIPTION);

        // Get all the promotionList where htmlDescription equals to UPDATED_HTML_DESCRIPTION
        defaultPromotionShouldNotBeFound("htmlDescription.in=" + UPDATED_HTML_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByHtmlDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where htmlDescription is not null
        defaultPromotionShouldBeFound("htmlDescription.specified=true");

        // Get all the promotionList where htmlDescription is null
        defaultPromotionShouldNotBeFound("htmlDescription.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByStart_dateIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where start_date equals to DEFAULT_START_DATE
        defaultPromotionShouldBeFound("start_date.equals=" + DEFAULT_START_DATE);

        // Get all the promotionList where start_date equals to UPDATED_START_DATE
        defaultPromotionShouldNotBeFound("start_date.equals=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByStart_dateIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where start_date in DEFAULT_START_DATE or UPDATED_START_DATE
        defaultPromotionShouldBeFound("start_date.in=" + DEFAULT_START_DATE + "," + UPDATED_START_DATE);

        // Get all the promotionList where start_date equals to UPDATED_START_DATE
        defaultPromotionShouldNotBeFound("start_date.in=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByStart_dateIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where start_date is not null
        defaultPromotionShouldBeFound("start_date.specified=true");

        // Get all the promotionList where start_date is null
        defaultPromotionShouldNotBeFound("start_date.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByStart_dateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where start_date greater than or equals to DEFAULT_START_DATE
        defaultPromotionShouldBeFound("start_date.greaterOrEqualThan=" + DEFAULT_START_DATE);

        // Get all the promotionList where start_date greater than or equals to UPDATED_START_DATE
        defaultPromotionShouldNotBeFound("start_date.greaterOrEqualThan=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByStart_dateIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where start_date less than or equals to DEFAULT_START_DATE
        defaultPromotionShouldNotBeFound("start_date.lessThan=" + DEFAULT_START_DATE);

        // Get all the promotionList where start_date less than or equals to UPDATED_START_DATE
        defaultPromotionShouldBeFound("start_date.lessThan=" + UPDATED_START_DATE);
    }


    @Test
    @Transactional
    public void getAllPromotionsByEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where endDate equals to DEFAULT_END_DATE
        defaultPromotionShouldBeFound("endDate.equals=" + DEFAULT_END_DATE);

        // Get all the promotionList where endDate equals to UPDATED_END_DATE
        defaultPromotionShouldNotBeFound("endDate.equals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where endDate in DEFAULT_END_DATE or UPDATED_END_DATE
        defaultPromotionShouldBeFound("endDate.in=" + DEFAULT_END_DATE + "," + UPDATED_END_DATE);

        // Get all the promotionList where endDate equals to UPDATED_END_DATE
        defaultPromotionShouldNotBeFound("endDate.in=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where endDate is not null
        defaultPromotionShouldBeFound("endDate.specified=true");

        // Get all the promotionList where endDate is null
        defaultPromotionShouldNotBeFound("endDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByEndDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where endDate greater than or equals to DEFAULT_END_DATE
        defaultPromotionShouldBeFound("endDate.greaterOrEqualThan=" + DEFAULT_END_DATE);

        // Get all the promotionList where endDate greater than or equals to UPDATED_END_DATE
        defaultPromotionShouldNotBeFound("endDate.greaterOrEqualThan=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByEndDateIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where endDate less than or equals to DEFAULT_END_DATE
        defaultPromotionShouldNotBeFound("endDate.lessThan=" + DEFAULT_END_DATE);

        // Get all the promotionList where endDate less than or equals to UPDATED_END_DATE
        defaultPromotionShouldBeFound("endDate.lessThan=" + UPDATED_END_DATE);
    }


    @Test
    @Transactional
    public void getAllPromotionsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where status equals to DEFAULT_STATUS
        defaultPromotionShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the promotionList where status equals to UPDATED_STATUS
        defaultPromotionShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllPromotionsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultPromotionShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the promotionList where status equals to UPDATED_STATUS
        defaultPromotionShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllPromotionsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where status is not null
        defaultPromotionShouldBeFound("status.specified=true");

        // Get all the promotionList where status is null
        defaultPromotionShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where status greater than or equals to DEFAULT_STATUS
        defaultPromotionShouldBeFound("status.greaterOrEqualThan=" + DEFAULT_STATUS);

        // Get all the promotionList where status greater than or equals to UPDATED_STATUS
        defaultPromotionShouldNotBeFound("status.greaterOrEqualThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllPromotionsByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where status less than or equals to DEFAULT_STATUS
        defaultPromotionShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the promotionList where status less than or equals to UPDATED_STATUS
        defaultPromotionShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllPromotionsByStopNextRuleIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where stopNextRule equals to DEFAULT_STOP_NEXT_RULE
        defaultPromotionShouldBeFound("stopNextRule.equals=" + DEFAULT_STOP_NEXT_RULE);

        // Get all the promotionList where stopNextRule equals to UPDATED_STOP_NEXT_RULE
        defaultPromotionShouldNotBeFound("stopNextRule.equals=" + UPDATED_STOP_NEXT_RULE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByStopNextRuleIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where stopNextRule in DEFAULT_STOP_NEXT_RULE or UPDATED_STOP_NEXT_RULE
        defaultPromotionShouldBeFound("stopNextRule.in=" + DEFAULT_STOP_NEXT_RULE + "," + UPDATED_STOP_NEXT_RULE);

        // Get all the promotionList where stopNextRule equals to UPDATED_STOP_NEXT_RULE
        defaultPromotionShouldNotBeFound("stopNextRule.in=" + UPDATED_STOP_NEXT_RULE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByStopNextRuleIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where stopNextRule is not null
        defaultPromotionShouldBeFound("stopNextRule.specified=true");

        // Get all the promotionList where stopNextRule is null
        defaultPromotionShouldNotBeFound("stopNextRule.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsBySortOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where sortOrder equals to DEFAULT_SORT_ORDER
        defaultPromotionShouldBeFound("sortOrder.equals=" + DEFAULT_SORT_ORDER);

        // Get all the promotionList where sortOrder equals to UPDATED_SORT_ORDER
        defaultPromotionShouldNotBeFound("sortOrder.equals=" + UPDATED_SORT_ORDER);
    }

    @Test
    @Transactional
    public void getAllPromotionsBySortOrderIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where sortOrder in DEFAULT_SORT_ORDER or UPDATED_SORT_ORDER
        defaultPromotionShouldBeFound("sortOrder.in=" + DEFAULT_SORT_ORDER + "," + UPDATED_SORT_ORDER);

        // Get all the promotionList where sortOrder equals to UPDATED_SORT_ORDER
        defaultPromotionShouldNotBeFound("sortOrder.in=" + UPDATED_SORT_ORDER);
    }

    @Test
    @Transactional
    public void getAllPromotionsBySortOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where sortOrder is not null
        defaultPromotionShouldBeFound("sortOrder.specified=true");

        // Get all the promotionList where sortOrder is null
        defaultPromotionShouldNotBeFound("sortOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsBySortOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where sortOrder greater than or equals to DEFAULT_SORT_ORDER
        defaultPromotionShouldBeFound("sortOrder.greaterOrEqualThan=" + DEFAULT_SORT_ORDER);

        // Get all the promotionList where sortOrder greater than or equals to UPDATED_SORT_ORDER
        defaultPromotionShouldNotBeFound("sortOrder.greaterOrEqualThan=" + UPDATED_SORT_ORDER);
    }

    @Test
    @Transactional
    public void getAllPromotionsBySortOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where sortOrder less than or equals to DEFAULT_SORT_ORDER
        defaultPromotionShouldNotBeFound("sortOrder.lessThan=" + DEFAULT_SORT_ORDER);

        // Get all the promotionList where sortOrder less than or equals to UPDATED_SORT_ORDER
        defaultPromotionShouldBeFound("sortOrder.lessThan=" + UPDATED_SORT_ORDER);
    }


    @Test
    @Transactional
    public void getAllPromotionsByUseOrgPriceFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where useOrgPriceFlag equals to DEFAULT_USE_ORG_PRICE_FLAG
        defaultPromotionShouldBeFound("useOrgPriceFlag.equals=" + DEFAULT_USE_ORG_PRICE_FLAG);

        // Get all the promotionList where useOrgPriceFlag equals to UPDATED_USE_ORG_PRICE_FLAG
        defaultPromotionShouldNotBeFound("useOrgPriceFlag.equals=" + UPDATED_USE_ORG_PRICE_FLAG);
    }

    @Test
    @Transactional
    public void getAllPromotionsByUseOrgPriceFlagIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where useOrgPriceFlag in DEFAULT_USE_ORG_PRICE_FLAG or UPDATED_USE_ORG_PRICE_FLAG
        defaultPromotionShouldBeFound("useOrgPriceFlag.in=" + DEFAULT_USE_ORG_PRICE_FLAG + "," + UPDATED_USE_ORG_PRICE_FLAG);

        // Get all the promotionList where useOrgPriceFlag equals to UPDATED_USE_ORG_PRICE_FLAG
        defaultPromotionShouldNotBeFound("useOrgPriceFlag.in=" + UPDATED_USE_ORG_PRICE_FLAG);
    }

    @Test
    @Transactional
    public void getAllPromotionsByUseOrgPriceFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where useOrgPriceFlag is not null
        defaultPromotionShouldBeFound("useOrgPriceFlag.specified=true");

        // Get all the promotionList where useOrgPriceFlag is null
        defaultPromotionShouldNotBeFound("useOrgPriceFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByAppliedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where appliedOn equals to DEFAULT_APPLIED_ON
        defaultPromotionShouldBeFound("appliedOn.equals=" + DEFAULT_APPLIED_ON);

        // Get all the promotionList where appliedOn equals to UPDATED_APPLIED_ON
        defaultPromotionShouldNotBeFound("appliedOn.equals=" + UPDATED_APPLIED_ON);
    }

    @Test
    @Transactional
    public void getAllPromotionsByAppliedOnIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where appliedOn in DEFAULT_APPLIED_ON or UPDATED_APPLIED_ON
        defaultPromotionShouldBeFound("appliedOn.in=" + DEFAULT_APPLIED_ON + "," + UPDATED_APPLIED_ON);

        // Get all the promotionList where appliedOn equals to UPDATED_APPLIED_ON
        defaultPromotionShouldNotBeFound("appliedOn.in=" + UPDATED_APPLIED_ON);
    }

    @Test
    @Transactional
    public void getAllPromotionsByAppliedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where appliedOn is not null
        defaultPromotionShouldBeFound("appliedOn.specified=true");

        // Get all the promotionList where appliedOn is null
        defaultPromotionShouldNotBeFound("appliedOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByAppliedOnIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where appliedOn greater than or equals to DEFAULT_APPLIED_ON
        defaultPromotionShouldBeFound("appliedOn.greaterOrEqualThan=" + DEFAULT_APPLIED_ON);

        // Get all the promotionList where appliedOn greater than or equals to UPDATED_APPLIED_ON
        defaultPromotionShouldNotBeFound("appliedOn.greaterOrEqualThan=" + UPDATED_APPLIED_ON);
    }

    @Test
    @Transactional
    public void getAllPromotionsByAppliedOnIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where appliedOn less than or equals to DEFAULT_APPLIED_ON
        defaultPromotionShouldNotBeFound("appliedOn.lessThan=" + DEFAULT_APPLIED_ON);

        // Get all the promotionList where appliedOn less than or equals to UPDATED_APPLIED_ON
        defaultPromotionShouldBeFound("appliedOn.lessThan=" + UPDATED_APPLIED_ON);
    }


    @Test
    @Transactional
    public void getAllPromotionsByDroolsRuleIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where droolsRule equals to DEFAULT_DROOLS_RULE
        defaultPromotionShouldBeFound("droolsRule.equals=" + DEFAULT_DROOLS_RULE);

        // Get all the promotionList where droolsRule equals to UPDATED_DROOLS_RULE
        defaultPromotionShouldNotBeFound("droolsRule.equals=" + UPDATED_DROOLS_RULE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByDroolsRuleIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where droolsRule in DEFAULT_DROOLS_RULE or UPDATED_DROOLS_RULE
        defaultPromotionShouldBeFound("droolsRule.in=" + DEFAULT_DROOLS_RULE + "," + UPDATED_DROOLS_RULE);

        // Get all the promotionList where droolsRule equals to UPDATED_DROOLS_RULE
        defaultPromotionShouldNotBeFound("droolsRule.in=" + UPDATED_DROOLS_RULE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByDroolsRuleIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where droolsRule is not null
        defaultPromotionShouldBeFound("droolsRule.specified=true");

        // Get all the promotionList where droolsRule is null
        defaultPromotionShouldNotBeFound("droolsRule.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByVersionIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where version equals to DEFAULT_VERSION
        defaultPromotionShouldBeFound("version.equals=" + DEFAULT_VERSION);

        // Get all the promotionList where version equals to UPDATED_VERSION
        defaultPromotionShouldNotBeFound("version.equals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByVersionIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where version in DEFAULT_VERSION or UPDATED_VERSION
        defaultPromotionShouldBeFound("version.in=" + DEFAULT_VERSION + "," + UPDATED_VERSION);

        // Get all the promotionList where version equals to UPDATED_VERSION
        defaultPromotionShouldNotBeFound("version.in=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByVersionIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where version is not null
        defaultPromotionShouldBeFound("version.specified=true");

        // Get all the promotionList where version is null
        defaultPromotionShouldNotBeFound("version.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByVersionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where version greater than or equals to DEFAULT_VERSION
        defaultPromotionShouldBeFound("version.greaterOrEqualThan=" + DEFAULT_VERSION);

        // Get all the promotionList where version greater than or equals to UPDATED_VERSION
        defaultPromotionShouldNotBeFound("version.greaterOrEqualThan=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByVersionIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where version less than or equals to DEFAULT_VERSION
        defaultPromotionShouldNotBeFound("version.lessThan=" + DEFAULT_VERSION);

        // Get all the promotionList where version less than or equals to UPDATED_VERSION
        defaultPromotionShouldBeFound("version.lessThan=" + UPDATED_VERSION);
    }


    @Test
    @Transactional
    public void getAllPromotionsByRunModeIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where runMode equals to DEFAULT_RUN_MODE
        defaultPromotionShouldBeFound("runMode.equals=" + DEFAULT_RUN_MODE);

        // Get all the promotionList where runMode equals to UPDATED_RUN_MODE
        defaultPromotionShouldNotBeFound("runMode.equals=" + UPDATED_RUN_MODE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByRunModeIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where runMode in DEFAULT_RUN_MODE or UPDATED_RUN_MODE
        defaultPromotionShouldBeFound("runMode.in=" + DEFAULT_RUN_MODE + "," + UPDATED_RUN_MODE);

        // Get all the promotionList where runMode equals to UPDATED_RUN_MODE
        defaultPromotionShouldNotBeFound("runMode.in=" + UPDATED_RUN_MODE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByRunModeIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where runMode is not null
        defaultPromotionShouldBeFound("runMode.specified=true");

        // Get all the promotionList where runMode is null
        defaultPromotionShouldNotBeFound("runMode.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByRunModeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where runMode greater than or equals to DEFAULT_RUN_MODE
        defaultPromotionShouldBeFound("runMode.greaterOrEqualThan=" + DEFAULT_RUN_MODE);

        // Get all the promotionList where runMode greater than or equals to UPDATED_RUN_MODE
        defaultPromotionShouldNotBeFound("runMode.greaterOrEqualThan=" + UPDATED_RUN_MODE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByRunModeIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where runMode less than or equals to DEFAULT_RUN_MODE
        defaultPromotionShouldNotBeFound("runMode.lessThan=" + DEFAULT_RUN_MODE);

        // Get all the promotionList where runMode less than or equals to UPDATED_RUN_MODE
        defaultPromotionShouldBeFound("runMode.lessThan=" + UPDATED_RUN_MODE);
    }


    @Test
    @Transactional
    public void getAllPromotionsByOnStoreIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where onStore equals to DEFAULT_ON_STORE
        defaultPromotionShouldBeFound("onStore.equals=" + DEFAULT_ON_STORE);

        // Get all the promotionList where onStore equals to UPDATED_ON_STORE
        defaultPromotionShouldNotBeFound("onStore.equals=" + UPDATED_ON_STORE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByOnStoreIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where onStore in DEFAULT_ON_STORE or UPDATED_ON_STORE
        defaultPromotionShouldBeFound("onStore.in=" + DEFAULT_ON_STORE + "," + UPDATED_ON_STORE);

        // Get all the promotionList where onStore equals to UPDATED_ON_STORE
        defaultPromotionShouldNotBeFound("onStore.in=" + UPDATED_ON_STORE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByOnStoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where onStore is not null
        defaultPromotionShouldBeFound("onStore.specified=true");

        // Get all the promotionList where onStore is null
        defaultPromotionShouldNotBeFound("onStore.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByPromotionTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where promotionType equals to DEFAULT_PROMOTION_TYPE
        defaultPromotionShouldBeFound("promotionType.equals=" + DEFAULT_PROMOTION_TYPE);

        // Get all the promotionList where promotionType equals to UPDATED_PROMOTION_TYPE
        defaultPromotionShouldNotBeFound("promotionType.equals=" + UPDATED_PROMOTION_TYPE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPromotionTypeIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where promotionType in DEFAULT_PROMOTION_TYPE or UPDATED_PROMOTION_TYPE
        defaultPromotionShouldBeFound("promotionType.in=" + DEFAULT_PROMOTION_TYPE + "," + UPDATED_PROMOTION_TYPE);

        // Get all the promotionList where promotionType equals to UPDATED_PROMOTION_TYPE
        defaultPromotionShouldNotBeFound("promotionType.in=" + UPDATED_PROMOTION_TYPE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPromotionTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where promotionType is not null
        defaultPromotionShouldBeFound("promotionType.specified=true");

        // Get all the promotionList where promotionType is null
        defaultPromotionShouldNotBeFound("promotionType.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByPromotionTypeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where promotionType greater than or equals to DEFAULT_PROMOTION_TYPE
        defaultPromotionShouldBeFound("promotionType.greaterOrEqualThan=" + DEFAULT_PROMOTION_TYPE);

        // Get all the promotionList where promotionType greater than or equals to UPDATED_PROMOTION_TYPE
        defaultPromotionShouldNotBeFound("promotionType.greaterOrEqualThan=" + UPDATED_PROMOTION_TYPE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPromotionTypeIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where promotionType less than or equals to DEFAULT_PROMOTION_TYPE
        defaultPromotionShouldNotBeFound("promotionType.lessThan=" + DEFAULT_PROMOTION_TYPE);

        // Get all the promotionList where promotionType less than or equals to UPDATED_PROMOTION_TYPE
        defaultPromotionShouldBeFound("promotionType.lessThan=" + UPDATED_PROMOTION_TYPE);
    }


    @Test
    @Transactional
    public void getAllPromotionsByAutoFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where autoFlag equals to DEFAULT_AUTO_FLAG
        defaultPromotionShouldBeFound("autoFlag.equals=" + DEFAULT_AUTO_FLAG);

        // Get all the promotionList where autoFlag equals to UPDATED_AUTO_FLAG
        defaultPromotionShouldNotBeFound("autoFlag.equals=" + UPDATED_AUTO_FLAG);
    }

    @Test
    @Transactional
    public void getAllPromotionsByAutoFlagIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where autoFlag in DEFAULT_AUTO_FLAG or UPDATED_AUTO_FLAG
        defaultPromotionShouldBeFound("autoFlag.in=" + DEFAULT_AUTO_FLAG + "," + UPDATED_AUTO_FLAG);

        // Get all the promotionList where autoFlag equals to UPDATED_AUTO_FLAG
        defaultPromotionShouldNotBeFound("autoFlag.in=" + UPDATED_AUTO_FLAG);
    }

    @Test
    @Transactional
    public void getAllPromotionsByAutoFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where autoFlag is not null
        defaultPromotionShouldBeFound("autoFlag.specified=true");

        // Get all the promotionList where autoFlag is null
        defaultPromotionShouldNotBeFound("autoFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultPromotionShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the promotionList where updateDate equals to UPDATED_UPDATE_DATE
        defaultPromotionShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultPromotionShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the promotionList where updateDate equals to UPDATED_UPDATE_DATE
        defaultPromotionShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where updateDate is not null
        defaultPromotionShouldBeFound("updateDate.specified=true");

        // Get all the promotionList where updateDate is null
        defaultPromotionShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByUpdateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where updateDate greater than or equals to DEFAULT_UPDATE_DATE
        defaultPromotionShouldBeFound("updateDate.greaterOrEqualThan=" + DEFAULT_UPDATE_DATE);

        // Get all the promotionList where updateDate greater than or equals to UPDATED_UPDATE_DATE
        defaultPromotionShouldNotBeFound("updateDate.greaterOrEqualThan=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByUpdateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where updateDate less than or equals to DEFAULT_UPDATE_DATE
        defaultPromotionShouldNotBeFound("updateDate.lessThan=" + DEFAULT_UPDATE_DATE);

        // Get all the promotionList where updateDate less than or equals to UPDATED_UPDATE_DATE
        defaultPromotionShouldBeFound("updateDate.lessThan=" + UPDATED_UPDATE_DATE);
    }


    @Test
    @Transactional
    public void getAllPromotionsByDeleteFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where deleteFlag equals to DEFAULT_DELETE_FLAG
        defaultPromotionShouldBeFound("deleteFlag.equals=" + DEFAULT_DELETE_FLAG);

        // Get all the promotionList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultPromotionShouldNotBeFound("deleteFlag.equals=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllPromotionsByDeleteFlagIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where deleteFlag in DEFAULT_DELETE_FLAG or UPDATED_DELETE_FLAG
        defaultPromotionShouldBeFound("deleteFlag.in=" + DEFAULT_DELETE_FLAG + "," + UPDATED_DELETE_FLAG);

        // Get all the promotionList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultPromotionShouldNotBeFound("deleteFlag.in=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllPromotionsByDeleteFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where deleteFlag is not null
        defaultPromotionShouldBeFound("deleteFlag.specified=true");

        // Get all the promotionList where deleteFlag is null
        defaultPromotionShouldNotBeFound("deleteFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByChannelIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where channel equals to DEFAULT_CHANNEL
        defaultPromotionShouldBeFound("channel.equals=" + DEFAULT_CHANNEL);

        // Get all the promotionList where channel equals to UPDATED_CHANNEL
        defaultPromotionShouldNotBeFound("channel.equals=" + UPDATED_CHANNEL);
    }

    @Test
    @Transactional
    public void getAllPromotionsByChannelIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where channel in DEFAULT_CHANNEL or UPDATED_CHANNEL
        defaultPromotionShouldBeFound("channel.in=" + DEFAULT_CHANNEL + "," + UPDATED_CHANNEL);

        // Get all the promotionList where channel equals to UPDATED_CHANNEL
        defaultPromotionShouldNotBeFound("channel.in=" + UPDATED_CHANNEL);
    }

    @Test
    @Transactional
    public void getAllPromotionsByChannelIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where channel is not null
        defaultPromotionShouldBeFound("channel.specified=true");

        // Get all the promotionList where channel is null
        defaultPromotionShouldNotBeFound("channel.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByJsConditionIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where jsCondition equals to DEFAULT_JS_CONDITION
        defaultPromotionShouldBeFound("jsCondition.equals=" + DEFAULT_JS_CONDITION);

        // Get all the promotionList where jsCondition equals to UPDATED_JS_CONDITION
        defaultPromotionShouldNotBeFound("jsCondition.equals=" + UPDATED_JS_CONDITION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByJsConditionIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where jsCondition in DEFAULT_JS_CONDITION or UPDATED_JS_CONDITION
        defaultPromotionShouldBeFound("jsCondition.in=" + DEFAULT_JS_CONDITION + "," + UPDATED_JS_CONDITION);

        // Get all the promotionList where jsCondition equals to UPDATED_JS_CONDITION
        defaultPromotionShouldNotBeFound("jsCondition.in=" + UPDATED_JS_CONDITION);
    }

    @Test
    @Transactional
    public void getAllPromotionsByJsConditionIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where jsCondition is not null
        defaultPromotionShouldBeFound("jsCondition.specified=true");

        // Get all the promotionList where jsCondition is null
        defaultPromotionShouldNotBeFound("jsCondition.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByJsBonusIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where jsBonus equals to DEFAULT_JS_BONUS
        defaultPromotionShouldBeFound("jsBonus.equals=" + DEFAULT_JS_BONUS);

        // Get all the promotionList where jsBonus equals to UPDATED_JS_BONUS
        defaultPromotionShouldNotBeFound("jsBonus.equals=" + UPDATED_JS_BONUS);
    }

    @Test
    @Transactional
    public void getAllPromotionsByJsBonusIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where jsBonus in DEFAULT_JS_BONUS or UPDATED_JS_BONUS
        defaultPromotionShouldBeFound("jsBonus.in=" + DEFAULT_JS_BONUS + "," + UPDATED_JS_BONUS);

        // Get all the promotionList where jsBonus equals to UPDATED_JS_BONUS
        defaultPromotionShouldNotBeFound("jsBonus.in=" + UPDATED_JS_BONUS);
    }

    @Test
    @Transactional
    public void getAllPromotionsByJsBonusIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where jsBonus is not null
        defaultPromotionShouldBeFound("jsBonus.specified=true");

        // Get all the promotionList where jsBonus is null
        defaultPromotionShouldNotBeFound("jsBonus.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByRedeemFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where redeemFlag equals to DEFAULT_REDEEM_FLAG
        defaultPromotionShouldBeFound("redeemFlag.equals=" + DEFAULT_REDEEM_FLAG);

        // Get all the promotionList where redeemFlag equals to UPDATED_REDEEM_FLAG
        defaultPromotionShouldNotBeFound("redeemFlag.equals=" + UPDATED_REDEEM_FLAG);
    }

    @Test
    @Transactional
    public void getAllPromotionsByRedeemFlagIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where redeemFlag in DEFAULT_REDEEM_FLAG or UPDATED_REDEEM_FLAG
        defaultPromotionShouldBeFound("redeemFlag.in=" + DEFAULT_REDEEM_FLAG + "," + UPDATED_REDEEM_FLAG);

        // Get all the promotionList where redeemFlag equals to UPDATED_REDEEM_FLAG
        defaultPromotionShouldNotBeFound("redeemFlag.in=" + UPDATED_REDEEM_FLAG);
    }

    @Test
    @Transactional
    public void getAllPromotionsByRedeemFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where redeemFlag is not null
        defaultPromotionShouldBeFound("redeemFlag.specified=true");

        // Get all the promotionList where redeemFlag is null
        defaultPromotionShouldNotBeFound("redeemFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByImagePathIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where imagePath equals to DEFAULT_IMAGE_PATH
        defaultPromotionShouldBeFound("imagePath.equals=" + DEFAULT_IMAGE_PATH);

        // Get all the promotionList where imagePath equals to UPDATED_IMAGE_PATH
        defaultPromotionShouldNotBeFound("imagePath.equals=" + UPDATED_IMAGE_PATH);
    }

    @Test
    @Transactional
    public void getAllPromotionsByImagePathIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where imagePath in DEFAULT_IMAGE_PATH or UPDATED_IMAGE_PATH
        defaultPromotionShouldBeFound("imagePath.in=" + DEFAULT_IMAGE_PATH + "," + UPDATED_IMAGE_PATH);

        // Get all the promotionList where imagePath equals to UPDATED_IMAGE_PATH
        defaultPromotionShouldNotBeFound("imagePath.in=" + UPDATED_IMAGE_PATH);
    }

    @Test
    @Transactional
    public void getAllPromotionsByImagePathIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where imagePath is not null
        defaultPromotionShouldBeFound("imagePath.specified=true");

        // Get all the promotionList where imagePath is null
        defaultPromotionShouldNotBeFound("imagePath.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByImagePath2IsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where imagePath2 equals to DEFAULT_IMAGE_PATH_2
        defaultPromotionShouldBeFound("imagePath2.equals=" + DEFAULT_IMAGE_PATH_2);

        // Get all the promotionList where imagePath2 equals to UPDATED_IMAGE_PATH_2
        defaultPromotionShouldNotBeFound("imagePath2.equals=" + UPDATED_IMAGE_PATH_2);
    }

    @Test
    @Transactional
    public void getAllPromotionsByImagePath2IsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where imagePath2 in DEFAULT_IMAGE_PATH_2 or UPDATED_IMAGE_PATH_2
        defaultPromotionShouldBeFound("imagePath2.in=" + DEFAULT_IMAGE_PATH_2 + "," + UPDATED_IMAGE_PATH_2);

        // Get all the promotionList where imagePath2 equals to UPDATED_IMAGE_PATH_2
        defaultPromotionShouldNotBeFound("imagePath2.in=" + UPDATED_IMAGE_PATH_2);
    }

    @Test
    @Transactional
    public void getAllPromotionsByImagePath2IsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where imagePath2 is not null
        defaultPromotionShouldBeFound("imagePath2.specified=true");

        // Get all the promotionList where imagePath2 is null
        defaultPromotionShouldNotBeFound("imagePath2.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByRedeemPointsIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where redeemPoints equals to DEFAULT_REDEEM_POINTS
        defaultPromotionShouldBeFound("redeemPoints.equals=" + DEFAULT_REDEEM_POINTS);

        // Get all the promotionList where redeemPoints equals to UPDATED_REDEEM_POINTS
        defaultPromotionShouldNotBeFound("redeemPoints.equals=" + UPDATED_REDEEM_POINTS);
    }

    @Test
    @Transactional
    public void getAllPromotionsByRedeemPointsIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where redeemPoints in DEFAULT_REDEEM_POINTS or UPDATED_REDEEM_POINTS
        defaultPromotionShouldBeFound("redeemPoints.in=" + DEFAULT_REDEEM_POINTS + "," + UPDATED_REDEEM_POINTS);

        // Get all the promotionList where redeemPoints equals to UPDATED_REDEEM_POINTS
        defaultPromotionShouldNotBeFound("redeemPoints.in=" + UPDATED_REDEEM_POINTS);
    }

    @Test
    @Transactional
    public void getAllPromotionsByRedeemPointsIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where redeemPoints is not null
        defaultPromotionShouldBeFound("redeemPoints.specified=true");

        // Get all the promotionList where redeemPoints is null
        defaultPromotionShouldNotBeFound("redeemPoints.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishStartDateIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishStartDate equals to DEFAULT_PUBLISH_START_DATE
        defaultPromotionShouldBeFound("publishStartDate.equals=" + DEFAULT_PUBLISH_START_DATE);

        // Get all the promotionList where publishStartDate equals to UPDATED_PUBLISH_START_DATE
        defaultPromotionShouldNotBeFound("publishStartDate.equals=" + UPDATED_PUBLISH_START_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishStartDateIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishStartDate in DEFAULT_PUBLISH_START_DATE or UPDATED_PUBLISH_START_DATE
        defaultPromotionShouldBeFound("publishStartDate.in=" + DEFAULT_PUBLISH_START_DATE + "," + UPDATED_PUBLISH_START_DATE);

        // Get all the promotionList where publishStartDate equals to UPDATED_PUBLISH_START_DATE
        defaultPromotionShouldNotBeFound("publishStartDate.in=" + UPDATED_PUBLISH_START_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishStartDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishStartDate is not null
        defaultPromotionShouldBeFound("publishStartDate.specified=true");

        // Get all the promotionList where publishStartDate is null
        defaultPromotionShouldNotBeFound("publishStartDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishStartDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishStartDate greater than or equals to DEFAULT_PUBLISH_START_DATE
        defaultPromotionShouldBeFound("publishStartDate.greaterOrEqualThan=" + DEFAULT_PUBLISH_START_DATE);

        // Get all the promotionList where publishStartDate greater than or equals to UPDATED_PUBLISH_START_DATE
        defaultPromotionShouldNotBeFound("publishStartDate.greaterOrEqualThan=" + UPDATED_PUBLISH_START_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishStartDateIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishStartDate less than or equals to DEFAULT_PUBLISH_START_DATE
        defaultPromotionShouldNotBeFound("publishStartDate.lessThan=" + DEFAULT_PUBLISH_START_DATE);

        // Get all the promotionList where publishStartDate less than or equals to UPDATED_PUBLISH_START_DATE
        defaultPromotionShouldBeFound("publishStartDate.lessThan=" + UPDATED_PUBLISH_START_DATE);
    }


    @Test
    @Transactional
    public void getAllPromotionsByPublishEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishEndDate equals to DEFAULT_PUBLISH_END_DATE
        defaultPromotionShouldBeFound("publishEndDate.equals=" + DEFAULT_PUBLISH_END_DATE);

        // Get all the promotionList where publishEndDate equals to UPDATED_PUBLISH_END_DATE
        defaultPromotionShouldNotBeFound("publishEndDate.equals=" + UPDATED_PUBLISH_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishEndDate in DEFAULT_PUBLISH_END_DATE or UPDATED_PUBLISH_END_DATE
        defaultPromotionShouldBeFound("publishEndDate.in=" + DEFAULT_PUBLISH_END_DATE + "," + UPDATED_PUBLISH_END_DATE);

        // Get all the promotionList where publishEndDate equals to UPDATED_PUBLISH_END_DATE
        defaultPromotionShouldNotBeFound("publishEndDate.in=" + UPDATED_PUBLISH_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishEndDate is not null
        defaultPromotionShouldBeFound("publishEndDate.specified=true");

        // Get all the promotionList where publishEndDate is null
        defaultPromotionShouldNotBeFound("publishEndDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishEndDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishEndDate greater than or equals to DEFAULT_PUBLISH_END_DATE
        defaultPromotionShouldBeFound("publishEndDate.greaterOrEqualThan=" + DEFAULT_PUBLISH_END_DATE);

        // Get all the promotionList where publishEndDate greater than or equals to UPDATED_PUBLISH_END_DATE
        defaultPromotionShouldNotBeFound("publishEndDate.greaterOrEqualThan=" + UPDATED_PUBLISH_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPromotionsByPublishEndDateIsLessThanSomething() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        // Get all the promotionList where publishEndDate less than or equals to DEFAULT_PUBLISH_END_DATE
        defaultPromotionShouldNotBeFound("publishEndDate.lessThan=" + DEFAULT_PUBLISH_END_DATE);

        // Get all the promotionList where publishEndDate less than or equals to UPDATED_PUBLISH_END_DATE
        defaultPromotionShouldBeFound("publishEndDate.lessThan=" + UPDATED_PUBLISH_END_DATE);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultPromotionShouldBeFound(String filter) throws Exception {
        restPromotionMockMvc.perform(get("/api/promotions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(promotion.getId().intValue())))
            .andExpect(jsonPath("$.[*].couponCode").value(hasItem(DEFAULT_COUPON_CODE.toString())))
            .andExpect(jsonPath("$.[*].promotionName").value(hasItem(DEFAULT_PROMOTION_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].htmlDescription").value(hasItem(DEFAULT_HTML_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].start_date").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].stopNextRule").value(hasItem(DEFAULT_STOP_NEXT_RULE.booleanValue())))
            .andExpect(jsonPath("$.[*].sortOrder").value(hasItem(DEFAULT_SORT_ORDER)))
            .andExpect(jsonPath("$.[*].useOrgPriceFlag").value(hasItem(DEFAULT_USE_ORG_PRICE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].appliedOn").value(hasItem(DEFAULT_APPLIED_ON)))
            .andExpect(jsonPath("$.[*].ruleDataContentType").value(hasItem(DEFAULT_RULE_DATA_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].ruleData").value(hasItem(Base64Utils.encodeToString(DEFAULT_RULE_DATA))))
            .andExpect(jsonPath("$.[*].droolsRule").value(hasItem(DEFAULT_DROOLS_RULE.toString())))
            .andExpect(jsonPath("$.[*].allocationDataContentType").value(hasItem(DEFAULT_ALLOCATION_DATA_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].allocationData").value(hasItem(Base64Utils.encodeToString(DEFAULT_ALLOCATION_DATA))))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
            .andExpect(jsonPath("$.[*].runMode").value(hasItem(DEFAULT_RUN_MODE)))
            .andExpect(jsonPath("$.[*].onStore").value(hasItem(DEFAULT_ON_STORE.toString())))
            .andExpect(jsonPath("$.[*].promotionType").value(hasItem(DEFAULT_PROMOTION_TYPE)))
            .andExpect(jsonPath("$.[*].autoFlag").value(hasItem(DEFAULT_AUTO_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].channel").value(hasItem(DEFAULT_CHANNEL.toString())))
            .andExpect(jsonPath("$.[*].jsCondition").value(hasItem(DEFAULT_JS_CONDITION.toString())))
            .andExpect(jsonPath("$.[*].jsBonus").value(hasItem(DEFAULT_JS_BONUS.toString())))
            .andExpect(jsonPath("$.[*].redeemFlag").value(hasItem(DEFAULT_REDEEM_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].imagePath").value(hasItem(DEFAULT_IMAGE_PATH.toString())))
            .andExpect(jsonPath("$.[*].imagePath2").value(hasItem(DEFAULT_IMAGE_PATH_2.toString())))
            .andExpect(jsonPath("$.[*].redeemPoints").value(hasItem(DEFAULT_REDEEM_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].imageBlob1ContentType").value(hasItem(DEFAULT_IMAGE_BLOB_1_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageBlob1").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_BLOB_1))))
            .andExpect(jsonPath("$.[*].imageBlob2ContentType").value(hasItem(DEFAULT_IMAGE_BLOB_2_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageBlob2").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_BLOB_2))))
            .andExpect(jsonPath("$.[*].publishStartDate").value(hasItem(DEFAULT_PUBLISH_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].publishEndDate").value(hasItem(DEFAULT_PUBLISH_END_DATE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultPromotionShouldNotBeFound(String filter) throws Exception {
        restPromotionMockMvc.perform(get("/api/promotions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingPromotion() throws Exception {
        // Get the promotion
        restPromotionMockMvc.perform(get("/api/promotions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePromotion() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        int databaseSizeBeforeUpdate = promotionRepository.findAll().size();

        // Update the promotion
        Promotion updatedPromotion = promotionRepository.findById(promotion.getId()).get();
        // Disconnect from session so that the updates on updatedPromotion are not directly saved in db
        em.detach(updatedPromotion);
        updatedPromotion
            .couponCode(UPDATED_COUPON_CODE)
            .promotionName(UPDATED_PROMOTION_NAME)
            .description(UPDATED_DESCRIPTION)
            .htmlDescription(UPDATED_HTML_DESCRIPTION)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .status(UPDATED_STATUS)
            .stopNextRule(UPDATED_STOP_NEXT_RULE)
            .sortOrder(UPDATED_SORT_ORDER)
            .useOrgPriceFlag(UPDATED_USE_ORG_PRICE_FLAG)
            .appliedOn(UPDATED_APPLIED_ON)
            .ruleData(UPDATED_RULE_DATA)
            .ruleDataContentType(UPDATED_RULE_DATA_CONTENT_TYPE)
            .droolsRule(UPDATED_DROOLS_RULE)
            .allocationData(UPDATED_ALLOCATION_DATA)
            .allocationDataContentType(UPDATED_ALLOCATION_DATA_CONTENT_TYPE)
            .version(UPDATED_VERSION)
            .runMode(UPDATED_RUN_MODE)
            .onStore(UPDATED_ON_STORE)
            .promotionType(UPDATED_PROMOTION_TYPE)
            .autoFlag(UPDATED_AUTO_FLAG)
            .updateDate(UPDATED_UPDATE_DATE)
            .deleteFlag(UPDATED_DELETE_FLAG)
            .channel(UPDATED_CHANNEL)
            .jsCondition(UPDATED_JS_CONDITION)
            .jsBonus(UPDATED_JS_BONUS)
            .redeemFlag(UPDATED_REDEEM_FLAG)
            .imagePath(UPDATED_IMAGE_PATH)
            .imagePath2(UPDATED_IMAGE_PATH_2)
            .redeemPoints(UPDATED_REDEEM_POINTS)
            .imageBlob1(UPDATED_IMAGE_BLOB_1)
            .imageBlob1ContentType(UPDATED_IMAGE_BLOB_1_CONTENT_TYPE)
            .imageBlob2(UPDATED_IMAGE_BLOB_2)
            .imageBlob2ContentType(UPDATED_IMAGE_BLOB_2_CONTENT_TYPE)
            .publishStartDate(UPDATED_PUBLISH_START_DATE)
            .publishEndDate(UPDATED_PUBLISH_END_DATE);
        PromotionDTO promotionDTO = promotionMapper.toDto(updatedPromotion);

        restPromotionMockMvc.perform(put("/api/promotions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
            .andExpect(status().isOk());

        // Validate the Promotion in the database
        List<Promotion> promotionList = promotionRepository.findAll();
        assertThat(promotionList).hasSize(databaseSizeBeforeUpdate);
        Promotion testPromotion = promotionList.get(promotionList.size() - 1);
        assertThat(testPromotion.getCouponCode()).isEqualTo(UPDATED_COUPON_CODE);
        assertThat(testPromotion.getPromotionName()).isEqualTo(UPDATED_PROMOTION_NAME);
        assertThat(testPromotion.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPromotion.getHtmlDescription()).isEqualTo(UPDATED_HTML_DESCRIPTION);
        assertThat(testPromotion.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testPromotion.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testPromotion.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPromotion.isStopNextRule()).isEqualTo(UPDATED_STOP_NEXT_RULE);
        assertThat(testPromotion.getSortOrder()).isEqualTo(UPDATED_SORT_ORDER);
        assertThat(testPromotion.isUseOrgPriceFlag()).isEqualTo(UPDATED_USE_ORG_PRICE_FLAG);
        assertThat(testPromotion.getAppliedOn()).isEqualTo(UPDATED_APPLIED_ON);
        assertThat(testPromotion.getRuleData()).isEqualTo(UPDATED_RULE_DATA);
        assertThat(testPromotion.getRuleDataContentType()).isEqualTo(UPDATED_RULE_DATA_CONTENT_TYPE);
        assertThat(testPromotion.getDroolsRule()).isEqualTo(UPDATED_DROOLS_RULE);
        assertThat(testPromotion.getAllocationData()).isEqualTo(UPDATED_ALLOCATION_DATA);
        assertThat(testPromotion.getAllocationDataContentType()).isEqualTo(UPDATED_ALLOCATION_DATA_CONTENT_TYPE);
        assertThat(testPromotion.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testPromotion.getRunMode()).isEqualTo(UPDATED_RUN_MODE);
        assertThat(testPromotion.getOnStore()).isEqualTo(UPDATED_ON_STORE);
        assertThat(testPromotion.getPromotionType()).isEqualTo(UPDATED_PROMOTION_TYPE);
        assertThat(testPromotion.isAutoFlag()).isEqualTo(UPDATED_AUTO_FLAG);
        assertThat(testPromotion.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testPromotion.isDeleteFlag()).isEqualTo(UPDATED_DELETE_FLAG);
        assertThat(testPromotion.getChannel()).isEqualTo(UPDATED_CHANNEL);
        assertThat(testPromotion.getJsCondition()).isEqualTo(UPDATED_JS_CONDITION);
        assertThat(testPromotion.getJsBonus()).isEqualTo(UPDATED_JS_BONUS);
        assertThat(testPromotion.isRedeemFlag()).isEqualTo(UPDATED_REDEEM_FLAG);
        assertThat(testPromotion.getImagePath()).isEqualTo(UPDATED_IMAGE_PATH);
        assertThat(testPromotion.getImagePath2()).isEqualTo(UPDATED_IMAGE_PATH_2);
        assertThat(testPromotion.getRedeemPoints()).isEqualTo(UPDATED_REDEEM_POINTS);
        assertThat(testPromotion.getImageBlob1()).isEqualTo(UPDATED_IMAGE_BLOB_1);
        assertThat(testPromotion.getImageBlob1ContentType()).isEqualTo(UPDATED_IMAGE_BLOB_1_CONTENT_TYPE);
        assertThat(testPromotion.getImageBlob2()).isEqualTo(UPDATED_IMAGE_BLOB_2);
        assertThat(testPromotion.getImageBlob2ContentType()).isEqualTo(UPDATED_IMAGE_BLOB_2_CONTENT_TYPE);
        assertThat(testPromotion.getPublishStartDate()).isEqualTo(UPDATED_PUBLISH_START_DATE);
        assertThat(testPromotion.getPublishEndDate()).isEqualTo(UPDATED_PUBLISH_END_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPromotion() throws Exception {
        int databaseSizeBeforeUpdate = promotionRepository.findAll().size();

        // Create the Promotion
        PromotionDTO promotionDTO = promotionMapper.toDto(promotion);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPromotionMockMvc.perform(put("/api/promotions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promotionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Promotion in the database
        List<Promotion> promotionList = promotionRepository.findAll();
        assertThat(promotionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePromotion() throws Exception {
        // Initialize the database
        promotionRepository.saveAndFlush(promotion);

        int databaseSizeBeforeDelete = promotionRepository.findAll().size();

        // Get the promotion
        restPromotionMockMvc.perform(delete("/api/promotions/{id}", promotion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Promotion> promotionList = promotionRepository.findAll();
        assertThat(promotionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Promotion.class);
        Promotion promotion1 = new Promotion();
        promotion1.setId(1L);
        Promotion promotion2 = new Promotion();
        promotion2.setId(promotion1.getId());
        assertThat(promotion1).isEqualTo(promotion2);
        promotion2.setId(2L);
        assertThat(promotion1).isNotEqualTo(promotion2);
        promotion1.setId(null);
        assertThat(promotion1).isNotEqualTo(promotion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PromotionDTO.class);
        PromotionDTO promotionDTO1 = new PromotionDTO();
        promotionDTO1.setId(1L);
        PromotionDTO promotionDTO2 = new PromotionDTO();
        assertThat(promotionDTO1).isNotEqualTo(promotionDTO2);
        promotionDTO2.setId(promotionDTO1.getId());
        assertThat(promotionDTO1).isEqualTo(promotionDTO2);
        promotionDTO2.setId(2L);
        assertThat(promotionDTO1).isNotEqualTo(promotionDTO2);
        promotionDTO1.setId(null);
        assertThat(promotionDTO1).isNotEqualTo(promotionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(promotionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(promotionMapper.fromId(null)).isNull();
    }
}
