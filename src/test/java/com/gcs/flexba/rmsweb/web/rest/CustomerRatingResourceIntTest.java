package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.CustomerRating;
import com.gcs.flexba.rmsweb.repository.CustomerRatingRepository;
import com.gcs.flexba.rmsweb.service.CustomerRatingService;
import com.gcs.flexba.rmsweb.service.dto.CustomerRatingDTO;
import com.gcs.flexba.rmsweb.service.mapper.CustomerRatingMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.CustomerRatingCriteria;
import com.gcs.flexba.rmsweb.service.CustomerRatingQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CustomerRatingResource REST controller.
 *
 * @see CustomerRatingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class CustomerRatingResourceIntTest {

    private static final Integer DEFAULT_NUM_STAR = 1;
    private static final Integer UPDATED_NUM_STAR = 2;

    private static final String DEFAULT_CUSTOMER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_INVENTORY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_INVENTORY_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_DELETE_FLAG = 1;
    private static final Integer UPDATED_DELETE_FLAG = 2;

    @Autowired
    private CustomerRatingRepository customerRatingRepository;


    @Autowired
    private CustomerRatingMapper customerRatingMapper;
    

    @Autowired
    private CustomerRatingService customerRatingService;

    @Autowired
    private CustomerRatingQueryService customerRatingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCustomerRatingMockMvc;

    private CustomerRating customerRating;

    @Before
    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final CustomerRatingResource customerRatingResource = new CustomerRatingResource(customerRatingService, customerRatingQueryService);
//        this.restCustomerRatingMockMvc = MockMvcBuilders.standaloneSetup(customerRatingResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerRating createEntity(EntityManager em) {
        CustomerRating customerRating = new CustomerRating()
            .numStar(DEFAULT_NUM_STAR)
            .customerCode(DEFAULT_CUSTOMER_CODE)
            .source(DEFAULT_SOURCE)
            .inventoryCode(DEFAULT_INVENTORY_CODE)
            .type(DEFAULT_TYPE)
            .comment(DEFAULT_COMMENT)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .createdDate(DEFAULT_CREATED_DATE)
            .deleteFlag(DEFAULT_DELETE_FLAG);
        return customerRating;
    }

    @Before
    public void initTest() {
        customerRating = createEntity(em);
    }

    @Test
    @Transactional
    public void createCustomerRating() throws Exception {
        int databaseSizeBeforeCreate = customerRatingRepository.findAll().size();

        // Create the CustomerRating
        CustomerRatingDTO customerRatingDTO = customerRatingMapper.toDto(customerRating);
        restCustomerRatingMockMvc.perform(post("/api/customer-ratings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerRatingDTO)))
            .andExpect(status().isCreated());

        // Validate the CustomerRating in the database
        List<CustomerRating> customerRatingList = customerRatingRepository.findAll();
        assertThat(customerRatingList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerRating testCustomerRating = customerRatingList.get(customerRatingList.size() - 1);
        assertThat(testCustomerRating.getNumStar()).isEqualTo(DEFAULT_NUM_STAR);
        assertThat(testCustomerRating.getCustomerCode()).isEqualTo(DEFAULT_CUSTOMER_CODE);
        assertThat(testCustomerRating.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testCustomerRating.getInventoryCode()).isEqualTo(DEFAULT_INVENTORY_CODE);
        assertThat(testCustomerRating.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testCustomerRating.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testCustomerRating.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testCustomerRating.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCustomerRating.getDeleteFlag()).isEqualTo(DEFAULT_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void createCustomerRatingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customerRatingRepository.findAll().size();

        // Create the CustomerRating with an existing ID
        customerRating.setId(1L);
        CustomerRatingDTO customerRatingDTO = customerRatingMapper.toDto(customerRating);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerRatingMockMvc.perform(post("/api/customer-ratings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerRatingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CustomerRating in the database
        List<CustomerRating> customerRatingList = customerRatingRepository.findAll();
        assertThat(customerRatingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCustomerRatings() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList
        restCustomerRatingMockMvc.perform(get("/api/customer-ratings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerRating.getId().intValue())))
            .andExpect(jsonPath("$.[*].numStar").value(hasItem(DEFAULT_NUM_STAR)))
            .andExpect(jsonPath("$.[*].customerCode").value(hasItem(DEFAULT_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
            .andExpect(jsonPath("$.[*].inventoryCode").value(hasItem(DEFAULT_INVENTORY_CODE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(sameInstant(DEFAULT_UPDATED_DATE))))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG)));
    }
    

    @Test
    @Transactional
    public void getCustomerRating() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get the customerRating
        restCustomerRatingMockMvc.perform(get("/api/customer-ratings/{id}", customerRating.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(customerRating.getId().intValue()))
            .andExpect(jsonPath("$.numStar").value(DEFAULT_NUM_STAR))
            .andExpect(jsonPath("$.customerCode").value(DEFAULT_CUSTOMER_CODE.toString()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()))
            .andExpect(jsonPath("$.inventoryCode").value(DEFAULT_INVENTORY_CODE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
            .andExpect(jsonPath("$.updatedDate").value(sameInstant(DEFAULT_UPDATED_DATE)))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.deleteFlag").value(DEFAULT_DELETE_FLAG));
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByNumStarIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where numStar equals to DEFAULT_NUM_STAR
        defaultCustomerRatingShouldBeFound("numStar.equals=" + DEFAULT_NUM_STAR);

        // Get all the customerRatingList where numStar equals to UPDATED_NUM_STAR
        defaultCustomerRatingShouldNotBeFound("numStar.equals=" + UPDATED_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByNumStarIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where numStar in DEFAULT_NUM_STAR or UPDATED_NUM_STAR
        defaultCustomerRatingShouldBeFound("numStar.in=" + DEFAULT_NUM_STAR + "," + UPDATED_NUM_STAR);

        // Get all the customerRatingList where numStar equals to UPDATED_NUM_STAR
        defaultCustomerRatingShouldNotBeFound("numStar.in=" + UPDATED_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByNumStarIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where numStar is not null
        defaultCustomerRatingShouldBeFound("numStar.specified=true");

        // Get all the customerRatingList where numStar is null
        defaultCustomerRatingShouldNotBeFound("numStar.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByNumStarIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where numStar greater than or equals to DEFAULT_NUM_STAR
        defaultCustomerRatingShouldBeFound("numStar.greaterOrEqualThan=" + DEFAULT_NUM_STAR);

        // Get all the customerRatingList where numStar greater than or equals to UPDATED_NUM_STAR
        defaultCustomerRatingShouldNotBeFound("numStar.greaterOrEqualThan=" + UPDATED_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByNumStarIsLessThanSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where numStar less than or equals to DEFAULT_NUM_STAR
        defaultCustomerRatingShouldNotBeFound("numStar.lessThan=" + DEFAULT_NUM_STAR);

        // Get all the customerRatingList where numStar less than or equals to UPDATED_NUM_STAR
        defaultCustomerRatingShouldBeFound("numStar.lessThan=" + UPDATED_NUM_STAR);
    }


    @Test
    @Transactional
    public void getAllCustomerRatingsByCustomerCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where customerCode equals to DEFAULT_CUSTOMER_CODE
        defaultCustomerRatingShouldBeFound("customerCode.equals=" + DEFAULT_CUSTOMER_CODE);

        // Get all the customerRatingList where customerCode equals to UPDATED_CUSTOMER_CODE
        defaultCustomerRatingShouldNotBeFound("customerCode.equals=" + UPDATED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByCustomerCodeIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where customerCode in DEFAULT_CUSTOMER_CODE or UPDATED_CUSTOMER_CODE
        defaultCustomerRatingShouldBeFound("customerCode.in=" + DEFAULT_CUSTOMER_CODE + "," + UPDATED_CUSTOMER_CODE);

        // Get all the customerRatingList where customerCode equals to UPDATED_CUSTOMER_CODE
        defaultCustomerRatingShouldNotBeFound("customerCode.in=" + UPDATED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByCustomerCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where customerCode is not null
        defaultCustomerRatingShouldBeFound("customerCode.specified=true");

        // Get all the customerRatingList where customerCode is null
        defaultCustomerRatingShouldNotBeFound("customerCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsBySourceIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where source equals to DEFAULT_SOURCE
        defaultCustomerRatingShouldBeFound("source.equals=" + DEFAULT_SOURCE);

        // Get all the customerRatingList where source equals to UPDATED_SOURCE
        defaultCustomerRatingShouldNotBeFound("source.equals=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsBySourceIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where source in DEFAULT_SOURCE or UPDATED_SOURCE
        defaultCustomerRatingShouldBeFound("source.in=" + DEFAULT_SOURCE + "," + UPDATED_SOURCE);

        // Get all the customerRatingList where source equals to UPDATED_SOURCE
        defaultCustomerRatingShouldNotBeFound("source.in=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsBySourceIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where source is not null
        defaultCustomerRatingShouldBeFound("source.specified=true");

        // Get all the customerRatingList where source is null
        defaultCustomerRatingShouldNotBeFound("source.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByInventoryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where inventoryCode equals to DEFAULT_INVENTORY_CODE
        defaultCustomerRatingShouldBeFound("inventoryCode.equals=" + DEFAULT_INVENTORY_CODE);

        // Get all the customerRatingList where inventoryCode equals to UPDATED_INVENTORY_CODE
        defaultCustomerRatingShouldNotBeFound("inventoryCode.equals=" + UPDATED_INVENTORY_CODE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByInventoryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where inventoryCode in DEFAULT_INVENTORY_CODE or UPDATED_INVENTORY_CODE
        defaultCustomerRatingShouldBeFound("inventoryCode.in=" + DEFAULT_INVENTORY_CODE + "," + UPDATED_INVENTORY_CODE);

        // Get all the customerRatingList where inventoryCode equals to UPDATED_INVENTORY_CODE
        defaultCustomerRatingShouldNotBeFound("inventoryCode.in=" + UPDATED_INVENTORY_CODE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByInventoryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where inventoryCode is not null
        defaultCustomerRatingShouldBeFound("inventoryCode.specified=true");

        // Get all the customerRatingList where inventoryCode is null
        defaultCustomerRatingShouldNotBeFound("inventoryCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where type equals to DEFAULT_TYPE
        defaultCustomerRatingShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the customerRatingList where type equals to UPDATED_TYPE
        defaultCustomerRatingShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultCustomerRatingShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the customerRatingList where type equals to UPDATED_TYPE
        defaultCustomerRatingShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where type is not null
        defaultCustomerRatingShouldBeFound("type.specified=true");

        // Get all the customerRatingList where type is null
        defaultCustomerRatingShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByTypeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where type greater than or equals to DEFAULT_TYPE
        defaultCustomerRatingShouldBeFound("type.greaterOrEqualThan=" + DEFAULT_TYPE);

        // Get all the customerRatingList where type greater than or equals to UPDATED_TYPE
        defaultCustomerRatingShouldNotBeFound("type.greaterOrEqualThan=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByTypeIsLessThanSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where type less than or equals to DEFAULT_TYPE
        defaultCustomerRatingShouldNotBeFound("type.lessThan=" + DEFAULT_TYPE);

        // Get all the customerRatingList where type less than or equals to UPDATED_TYPE
        defaultCustomerRatingShouldBeFound("type.lessThan=" + UPDATED_TYPE);
    }


    @Test
    @Transactional
    public void getAllCustomerRatingsByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where comment equals to DEFAULT_COMMENT
        defaultCustomerRatingShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the customerRatingList where comment equals to UPDATED_COMMENT
        defaultCustomerRatingShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultCustomerRatingShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the customerRatingList where comment equals to UPDATED_COMMENT
        defaultCustomerRatingShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where comment is not null
        defaultCustomerRatingShouldBeFound("comment.specified=true");

        // Get all the customerRatingList where comment is null
        defaultCustomerRatingShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultCustomerRatingShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the customerRatingList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultCustomerRatingShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultCustomerRatingShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the customerRatingList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultCustomerRatingShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where updatedDate is not null
        defaultCustomerRatingShouldBeFound("updatedDate.specified=true");

        // Get all the customerRatingList where updatedDate is null
        defaultCustomerRatingShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByUpdatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where updatedDate greater than or equals to DEFAULT_UPDATED_DATE
        defaultCustomerRatingShouldBeFound("updatedDate.greaterOrEqualThan=" + DEFAULT_UPDATED_DATE);

        // Get all the customerRatingList where updatedDate greater than or equals to UPDATED_UPDATED_DATE
        defaultCustomerRatingShouldNotBeFound("updatedDate.greaterOrEqualThan=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByUpdatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where updatedDate less than or equals to DEFAULT_UPDATED_DATE
        defaultCustomerRatingShouldNotBeFound("updatedDate.lessThan=" + DEFAULT_UPDATED_DATE);

        // Get all the customerRatingList where updatedDate less than or equals to UPDATED_UPDATED_DATE
        defaultCustomerRatingShouldBeFound("updatedDate.lessThan=" + UPDATED_UPDATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCustomerRatingsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCustomerRatingShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the customerRatingList where createdDate equals to UPDATED_CREATED_DATE
        defaultCustomerRatingShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCustomerRatingShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the customerRatingList where createdDate equals to UPDATED_CREATED_DATE
        defaultCustomerRatingShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where createdDate is not null
        defaultCustomerRatingShouldBeFound("createdDate.specified=true");

        // Get all the customerRatingList where createdDate is null
        defaultCustomerRatingShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where createdDate greater than or equals to DEFAULT_CREATED_DATE
        defaultCustomerRatingShouldBeFound("createdDate.greaterOrEqualThan=" + DEFAULT_CREATED_DATE);

        // Get all the customerRatingList where createdDate greater than or equals to UPDATED_CREATED_DATE
        defaultCustomerRatingShouldNotBeFound("createdDate.greaterOrEqualThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where createdDate less than or equals to DEFAULT_CREATED_DATE
        defaultCustomerRatingShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the customerRatingList where createdDate less than or equals to UPDATED_CREATED_DATE
        defaultCustomerRatingShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCustomerRatingsByDeleteFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where deleteFlag equals to DEFAULT_DELETE_FLAG
        defaultCustomerRatingShouldBeFound("deleteFlag.equals=" + DEFAULT_DELETE_FLAG);

        // Get all the customerRatingList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultCustomerRatingShouldNotBeFound("deleteFlag.equals=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByDeleteFlagIsInShouldWork() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where deleteFlag in DEFAULT_DELETE_FLAG or UPDATED_DELETE_FLAG
        defaultCustomerRatingShouldBeFound("deleteFlag.in=" + DEFAULT_DELETE_FLAG + "," + UPDATED_DELETE_FLAG);

        // Get all the customerRatingList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultCustomerRatingShouldNotBeFound("deleteFlag.in=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByDeleteFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where deleteFlag is not null
        defaultCustomerRatingShouldBeFound("deleteFlag.specified=true");

        // Get all the customerRatingList where deleteFlag is null
        defaultCustomerRatingShouldNotBeFound("deleteFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByDeleteFlagIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where deleteFlag greater than or equals to DEFAULT_DELETE_FLAG
        defaultCustomerRatingShouldBeFound("deleteFlag.greaterOrEqualThan=" + DEFAULT_DELETE_FLAG);

        // Get all the customerRatingList where deleteFlag greater than or equals to UPDATED_DELETE_FLAG
        defaultCustomerRatingShouldNotBeFound("deleteFlag.greaterOrEqualThan=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllCustomerRatingsByDeleteFlagIsLessThanSomething() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        // Get all the customerRatingList where deleteFlag less than or equals to DEFAULT_DELETE_FLAG
        defaultCustomerRatingShouldNotBeFound("deleteFlag.lessThan=" + DEFAULT_DELETE_FLAG);

        // Get all the customerRatingList where deleteFlag less than or equals to UPDATED_DELETE_FLAG
        defaultCustomerRatingShouldBeFound("deleteFlag.lessThan=" + UPDATED_DELETE_FLAG);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultCustomerRatingShouldBeFound(String filter) throws Exception {
        restCustomerRatingMockMvc.perform(get("/api/customer-ratings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerRating.getId().intValue())))
            .andExpect(jsonPath("$.[*].numStar").value(hasItem(DEFAULT_NUM_STAR)))
            .andExpect(jsonPath("$.[*].customerCode").value(hasItem(DEFAULT_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
            .andExpect(jsonPath("$.[*].inventoryCode").value(hasItem(DEFAULT_INVENTORY_CODE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(sameInstant(DEFAULT_UPDATED_DATE))))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultCustomerRatingShouldNotBeFound(String filter) throws Exception {
        restCustomerRatingMockMvc.perform(get("/api/customer-ratings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingCustomerRating() throws Exception {
        // Get the customerRating
        restCustomerRatingMockMvc.perform(get("/api/customer-ratings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomerRating() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        int databaseSizeBeforeUpdate = customerRatingRepository.findAll().size();

        // Update the customerRating
        CustomerRating updatedCustomerRating = customerRatingRepository.findById(customerRating.getId()).get();
        // Disconnect from session so that the updates on updatedCustomerRating are not directly saved in db
        em.detach(updatedCustomerRating);
        updatedCustomerRating
            .numStar(UPDATED_NUM_STAR)
            .customerCode(UPDATED_CUSTOMER_CODE)
            .source(UPDATED_SOURCE)
            .inventoryCode(UPDATED_INVENTORY_CODE)
            .type(UPDATED_TYPE)
            .comment(UPDATED_COMMENT)
            .updatedDate(UPDATED_UPDATED_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .deleteFlag(UPDATED_DELETE_FLAG);
        CustomerRatingDTO customerRatingDTO = customerRatingMapper.toDto(updatedCustomerRating);

        restCustomerRatingMockMvc.perform(put("/api/customer-ratings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerRatingDTO)))
            .andExpect(status().isOk());

        // Validate the CustomerRating in the database
        List<CustomerRating> customerRatingList = customerRatingRepository.findAll();
        assertThat(customerRatingList).hasSize(databaseSizeBeforeUpdate);
        CustomerRating testCustomerRating = customerRatingList.get(customerRatingList.size() - 1);
        assertThat(testCustomerRating.getNumStar()).isEqualTo(UPDATED_NUM_STAR);
        assertThat(testCustomerRating.getCustomerCode()).isEqualTo(UPDATED_CUSTOMER_CODE);
        assertThat(testCustomerRating.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testCustomerRating.getInventoryCode()).isEqualTo(UPDATED_INVENTORY_CODE);
        assertThat(testCustomerRating.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testCustomerRating.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testCustomerRating.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testCustomerRating.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCustomerRating.getDeleteFlag()).isEqualTo(UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void updateNonExistingCustomerRating() throws Exception {
        int databaseSizeBeforeUpdate = customerRatingRepository.findAll().size();

        // Create the CustomerRating
        CustomerRatingDTO customerRatingDTO = customerRatingMapper.toDto(customerRating);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCustomerRatingMockMvc.perform(put("/api/customer-ratings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerRatingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CustomerRating in the database
        List<CustomerRating> customerRatingList = customerRatingRepository.findAll();
        assertThat(customerRatingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCustomerRating() throws Exception {
        // Initialize the database
        customerRatingRepository.saveAndFlush(customerRating);

        int databaseSizeBeforeDelete = customerRatingRepository.findAll().size();

        // Get the customerRating
        restCustomerRatingMockMvc.perform(delete("/api/customer-ratings/{id}", customerRating.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CustomerRating> customerRatingList = customerRatingRepository.findAll();
        assertThat(customerRatingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerRating.class);
        CustomerRating customerRating1 = new CustomerRating();
        customerRating1.setId(1L);
        CustomerRating customerRating2 = new CustomerRating();
        customerRating2.setId(customerRating1.getId());
        assertThat(customerRating1).isEqualTo(customerRating2);
        customerRating2.setId(2L);
        assertThat(customerRating1).isNotEqualTo(customerRating2);
        customerRating1.setId(null);
        assertThat(customerRating1).isNotEqualTo(customerRating2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerRatingDTO.class);
        CustomerRatingDTO customerRatingDTO1 = new CustomerRatingDTO();
        customerRatingDTO1.setId(1L);
        CustomerRatingDTO customerRatingDTO2 = new CustomerRatingDTO();
        assertThat(customerRatingDTO1).isNotEqualTo(customerRatingDTO2);
        customerRatingDTO2.setId(customerRatingDTO1.getId());
        assertThat(customerRatingDTO1).isEqualTo(customerRatingDTO2);
        customerRatingDTO2.setId(2L);
        assertThat(customerRatingDTO1).isNotEqualTo(customerRatingDTO2);
        customerRatingDTO1.setId(null);
        assertThat(customerRatingDTO1).isNotEqualTo(customerRatingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(customerRatingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(customerRatingMapper.fromId(null)).isNull();
    }
}
