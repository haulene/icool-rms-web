package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.Inventory;
import com.gcs.flexba.rmsweb.repository.InventoryRepository;
import com.gcs.flexba.rmsweb.service.InventoryService;
import com.gcs.flexba.rmsweb.service.dto.InventoryDTO;
import com.gcs.flexba.rmsweb.service.mapper.InventoryMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.InventoryCriteria;
import com.gcs.flexba.rmsweb.service.InventoryQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InventoryResource REST controller.
 *
 * @see InventoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class InventoryResourceIntTest {

    private static final String DEFAULT_INVENTORY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_INVENTORY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_INVENTORY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_INVENTORY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REGION_CODE = "AAAAAAAAAA";
    private static final String UPDATED_REGION_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_INVENTORY_TYPE = 1;
    private static final Integer UPDATED_INVENTORY_TYPE = 2;

    private static final String DEFAULT_BRAND_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BRAND_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_TEL = "AAAAAAAAAA";
    private static final String UPDATED_TEL = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_CHANNEL = "AAAAAAAAAA";
    private static final String UPDATED_CHANNEL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ALLOW_PICKUP_FLAG = false;
    private static final Boolean UPDATED_ALLOW_PICKUP_FLAG = true;

    private static final Boolean DEFAULT_ALLOW_URGENT_ORDER_FLAG = false;
    private static final Boolean UPDATED_ALLOW_URGENT_ORDER_FLAG = true;

    private static final Boolean DEFAULT_ALLOW_BOOK_STUDIO_FLAG = false;
    private static final Boolean UPDATED_ALLOW_BOOK_STUDIO_FLAG = true;

    private static final ZonedDateTime DEFAULT_UPDATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETE_FLAG = false;
    private static final Boolean UPDATED_DELETE_FLAG = true;

    private static final String DEFAULT_LATITUDE = "AAAAAAAAAA";
    private static final String UPDATED_LATITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_LONGITUDE = "AAAAAAAAAA";
    private static final String UPDATED_LONGITUDE = "BBBBBBBBBB";

    @Autowired
    private InventoryRepository inventoryRepository;


    @Autowired
    private InventoryMapper inventoryMapper;
    

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private InventoryQueryService inventoryQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInventoryMockMvc;

    private Inventory inventory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InventoryResource inventoryResource = new InventoryResource(inventoryService, inventoryQueryService);
        this.restInventoryMockMvc = MockMvcBuilders.standaloneSetup(inventoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inventory createEntity(EntityManager em) {
        Inventory inventory = new Inventory()
            .inventoryCode(DEFAULT_INVENTORY_CODE)
            .inventoryName(DEFAULT_INVENTORY_NAME)
            .regionCode(DEFAULT_REGION_CODE)
            .inventoryType(DEFAULT_INVENTORY_TYPE)
            .brandCode(DEFAULT_BRAND_CODE)
            .address(DEFAULT_ADDRESS)
            .tel(DEFAULT_TEL)
            .fax(DEFAULT_FAX)
            .email(DEFAULT_EMAIL)
            .status(DEFAULT_STATUS)
            .channel(DEFAULT_CHANNEL)
            .allowPickupFlag(DEFAULT_ALLOW_PICKUP_FLAG)
            .allowUrgentOrderFlag(DEFAULT_ALLOW_URGENT_ORDER_FLAG)
            .allowBookStudioFlag(DEFAULT_ALLOW_BOOK_STUDIO_FLAG)
            .updateDate(DEFAULT_UPDATE_DATE)
            .deleteFlag(DEFAULT_DELETE_FLAG)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE);
        return inventory;
    }

    @Before
    public void initTest() {
        inventory = createEntity(em);
    }

    @Test
    @Transactional
    public void createInventory() throws Exception {
        int databaseSizeBeforeCreate = inventoryRepository.findAll().size();

        // Create the Inventory
        InventoryDTO inventoryDTO = inventoryMapper.toDto(inventory);
        restInventoryMockMvc.perform(post("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isCreated());

        // Validate the Inventory in the database
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeCreate + 1);
        Inventory testInventory = inventoryList.get(inventoryList.size() - 1);
        assertThat(testInventory.getInventoryCode()).isEqualTo(DEFAULT_INVENTORY_CODE);
        assertThat(testInventory.getInventoryName()).isEqualTo(DEFAULT_INVENTORY_NAME);
        assertThat(testInventory.getRegionCode()).isEqualTo(DEFAULT_REGION_CODE);
        assertThat(testInventory.getInventoryType()).isEqualTo(DEFAULT_INVENTORY_TYPE);
        assertThat(testInventory.getBrandCode()).isEqualTo(DEFAULT_BRAND_CODE);
        assertThat(testInventory.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testInventory.getTel()).isEqualTo(DEFAULT_TEL);
        assertThat(testInventory.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testInventory.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testInventory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testInventory.getChannel()).isEqualTo(DEFAULT_CHANNEL);
        assertThat(testInventory.isAllowPickupFlag()).isEqualTo(DEFAULT_ALLOW_PICKUP_FLAG);
        assertThat(testInventory.isAllowUrgentOrderFlag()).isEqualTo(DEFAULT_ALLOW_URGENT_ORDER_FLAG);
        assertThat(testInventory.isAllowBookStudioFlag()).isEqualTo(DEFAULT_ALLOW_BOOK_STUDIO_FLAG);
        assertThat(testInventory.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testInventory.isDeleteFlag()).isEqualTo(DEFAULT_DELETE_FLAG);
        assertThat(testInventory.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testInventory.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
    }

    @Test
    @Transactional
    public void createInventoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inventoryRepository.findAll().size();

        // Create the Inventory with an existing ID
        inventory.setId(1L);
        InventoryDTO inventoryDTO = inventoryMapper.toDto(inventory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInventoryMockMvc.perform(post("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Inventory in the database
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllInventories() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList
        restInventoryMockMvc.perform(get("/api/inventories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventory.getId().intValue())))
            .andExpect(jsonPath("$.[*].inventoryCode").value(hasItem(DEFAULT_INVENTORY_CODE.toString())))
            .andExpect(jsonPath("$.[*].inventoryName").value(hasItem(DEFAULT_INVENTORY_NAME.toString())))
            .andExpect(jsonPath("$.[*].regionCode").value(hasItem(DEFAULT_REGION_CODE.toString())))
            .andExpect(jsonPath("$.[*].inventoryType").value(hasItem(DEFAULT_INVENTORY_TYPE)))
            .andExpect(jsonPath("$.[*].brandCode").value(hasItem(DEFAULT_BRAND_CODE.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].channel").value(hasItem(DEFAULT_CHANNEL.toString())))
            .andExpect(jsonPath("$.[*].allowPickupFlag").value(hasItem(DEFAULT_ALLOW_PICKUP_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].allowUrgentOrderFlag").value(hasItem(DEFAULT_ALLOW_URGENT_ORDER_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].allowBookStudioFlag").value(hasItem(DEFAULT_ALLOW_BOOK_STUDIO_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.toString())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.toString())));
    }
    

    @Test
    @Transactional
    public void getInventory() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get the inventory
        restInventoryMockMvc.perform(get("/api/inventories/{id}", inventory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(inventory.getId().intValue()))
            .andExpect(jsonPath("$.inventoryCode").value(DEFAULT_INVENTORY_CODE.toString()))
            .andExpect(jsonPath("$.inventoryName").value(DEFAULT_INVENTORY_NAME.toString()))
            .andExpect(jsonPath("$.regionCode").value(DEFAULT_REGION_CODE.toString()))
            .andExpect(jsonPath("$.inventoryType").value(DEFAULT_INVENTORY_TYPE))
            .andExpect(jsonPath("$.brandCode").value(DEFAULT_BRAND_CODE.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.tel").value(DEFAULT_TEL.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.channel").value(DEFAULT_CHANNEL.toString()))
            .andExpect(jsonPath("$.allowPickupFlag").value(DEFAULT_ALLOW_PICKUP_FLAG.booleanValue()))
            .andExpect(jsonPath("$.allowUrgentOrderFlag").value(DEFAULT_ALLOW_URGENT_ORDER_FLAG.booleanValue()))
            .andExpect(jsonPath("$.allowBookStudioFlag").value(DEFAULT_ALLOW_BOOK_STUDIO_FLAG.booleanValue()))
            .andExpect(jsonPath("$.updateDate").value(sameInstant(DEFAULT_UPDATE_DATE)))
            .andExpect(jsonPath("$.deleteFlag").value(DEFAULT_DELETE_FLAG.booleanValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.toString()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.toString()));
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryCode equals to DEFAULT_INVENTORY_CODE
        defaultInventoryShouldBeFound("inventoryCode.equals=" + DEFAULT_INVENTORY_CODE);

        // Get all the inventoryList where inventoryCode equals to UPDATED_INVENTORY_CODE
        defaultInventoryShouldNotBeFound("inventoryCode.equals=" + UPDATED_INVENTORY_CODE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryCode in DEFAULT_INVENTORY_CODE or UPDATED_INVENTORY_CODE
        defaultInventoryShouldBeFound("inventoryCode.in=" + DEFAULT_INVENTORY_CODE + "," + UPDATED_INVENTORY_CODE);

        // Get all the inventoryList where inventoryCode equals to UPDATED_INVENTORY_CODE
        defaultInventoryShouldNotBeFound("inventoryCode.in=" + UPDATED_INVENTORY_CODE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryCode is not null
        defaultInventoryShouldBeFound("inventoryCode.specified=true");

        // Get all the inventoryList where inventoryCode is null
        defaultInventoryShouldNotBeFound("inventoryCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryNameIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryName equals to DEFAULT_INVENTORY_NAME
        defaultInventoryShouldBeFound("inventoryName.equals=" + DEFAULT_INVENTORY_NAME);

        // Get all the inventoryList where inventoryName equals to UPDATED_INVENTORY_NAME
        defaultInventoryShouldNotBeFound("inventoryName.equals=" + UPDATED_INVENTORY_NAME);
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryNameIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryName in DEFAULT_INVENTORY_NAME or UPDATED_INVENTORY_NAME
        defaultInventoryShouldBeFound("inventoryName.in=" + DEFAULT_INVENTORY_NAME + "," + UPDATED_INVENTORY_NAME);

        // Get all the inventoryList where inventoryName equals to UPDATED_INVENTORY_NAME
        defaultInventoryShouldNotBeFound("inventoryName.in=" + UPDATED_INVENTORY_NAME);
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryName is not null
        defaultInventoryShouldBeFound("inventoryName.specified=true");

        // Get all the inventoryList where inventoryName is null
        defaultInventoryShouldNotBeFound("inventoryName.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByRegionCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where regionCode equals to DEFAULT_REGION_CODE
        defaultInventoryShouldBeFound("regionCode.equals=" + DEFAULT_REGION_CODE);

        // Get all the inventoryList where regionCode equals to UPDATED_REGION_CODE
        defaultInventoryShouldNotBeFound("regionCode.equals=" + UPDATED_REGION_CODE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByRegionCodeIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where regionCode in DEFAULT_REGION_CODE or UPDATED_REGION_CODE
        defaultInventoryShouldBeFound("regionCode.in=" + DEFAULT_REGION_CODE + "," + UPDATED_REGION_CODE);

        // Get all the inventoryList where regionCode equals to UPDATED_REGION_CODE
        defaultInventoryShouldNotBeFound("regionCode.in=" + UPDATED_REGION_CODE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByRegionCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where regionCode is not null
        defaultInventoryShouldBeFound("regionCode.specified=true");

        // Get all the inventoryList where regionCode is null
        defaultInventoryShouldNotBeFound("regionCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryType equals to DEFAULT_INVENTORY_TYPE
        defaultInventoryShouldBeFound("inventoryType.equals=" + DEFAULT_INVENTORY_TYPE);

        // Get all the inventoryList where inventoryType equals to UPDATED_INVENTORY_TYPE
        defaultInventoryShouldNotBeFound("inventoryType.equals=" + UPDATED_INVENTORY_TYPE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryTypeIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryType in DEFAULT_INVENTORY_TYPE or UPDATED_INVENTORY_TYPE
        defaultInventoryShouldBeFound("inventoryType.in=" + DEFAULT_INVENTORY_TYPE + "," + UPDATED_INVENTORY_TYPE);

        // Get all the inventoryList where inventoryType equals to UPDATED_INVENTORY_TYPE
        defaultInventoryShouldNotBeFound("inventoryType.in=" + UPDATED_INVENTORY_TYPE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryType is not null
        defaultInventoryShouldBeFound("inventoryType.specified=true");

        // Get all the inventoryList where inventoryType is null
        defaultInventoryShouldNotBeFound("inventoryType.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryTypeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryType greater than or equals to DEFAULT_INVENTORY_TYPE
        defaultInventoryShouldBeFound("inventoryType.greaterOrEqualThan=" + DEFAULT_INVENTORY_TYPE);

        // Get all the inventoryList where inventoryType greater than or equals to UPDATED_INVENTORY_TYPE
        defaultInventoryShouldNotBeFound("inventoryType.greaterOrEqualThan=" + UPDATED_INVENTORY_TYPE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryTypeIsLessThanSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where inventoryType less than or equals to DEFAULT_INVENTORY_TYPE
        defaultInventoryShouldNotBeFound("inventoryType.lessThan=" + DEFAULT_INVENTORY_TYPE);

        // Get all the inventoryList where inventoryType less than or equals to UPDATED_INVENTORY_TYPE
        defaultInventoryShouldBeFound("inventoryType.lessThan=" + UPDATED_INVENTORY_TYPE);
    }


    @Test
    @Transactional
    public void getAllInventoriesByBrandCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where brandCode equals to DEFAULT_BRAND_CODE
        defaultInventoryShouldBeFound("brandCode.equals=" + DEFAULT_BRAND_CODE);

        // Get all the inventoryList where brandCode equals to UPDATED_BRAND_CODE
        defaultInventoryShouldNotBeFound("brandCode.equals=" + UPDATED_BRAND_CODE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByBrandCodeIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where brandCode in DEFAULT_BRAND_CODE or UPDATED_BRAND_CODE
        defaultInventoryShouldBeFound("brandCode.in=" + DEFAULT_BRAND_CODE + "," + UPDATED_BRAND_CODE);

        // Get all the inventoryList where brandCode equals to UPDATED_BRAND_CODE
        defaultInventoryShouldNotBeFound("brandCode.in=" + UPDATED_BRAND_CODE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByBrandCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where brandCode is not null
        defaultInventoryShouldBeFound("brandCode.specified=true");

        // Get all the inventoryList where brandCode is null
        defaultInventoryShouldNotBeFound("brandCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where address equals to DEFAULT_ADDRESS
        defaultInventoryShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the inventoryList where address equals to UPDATED_ADDRESS
        defaultInventoryShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllInventoriesByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultInventoryShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the inventoryList where address equals to UPDATED_ADDRESS
        defaultInventoryShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllInventoriesByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where address is not null
        defaultInventoryShouldBeFound("address.specified=true");

        // Get all the inventoryList where address is null
        defaultInventoryShouldNotBeFound("address.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByTelIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where tel equals to DEFAULT_TEL
        defaultInventoryShouldBeFound("tel.equals=" + DEFAULT_TEL);

        // Get all the inventoryList where tel equals to UPDATED_TEL
        defaultInventoryShouldNotBeFound("tel.equals=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllInventoriesByTelIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where tel in DEFAULT_TEL or UPDATED_TEL
        defaultInventoryShouldBeFound("tel.in=" + DEFAULT_TEL + "," + UPDATED_TEL);

        // Get all the inventoryList where tel equals to UPDATED_TEL
        defaultInventoryShouldNotBeFound("tel.in=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllInventoriesByTelIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where tel is not null
        defaultInventoryShouldBeFound("tel.specified=true");

        // Get all the inventoryList where tel is null
        defaultInventoryShouldNotBeFound("tel.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where fax equals to DEFAULT_FAX
        defaultInventoryShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the inventoryList where fax equals to UPDATED_FAX
        defaultInventoryShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllInventoriesByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultInventoryShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the inventoryList where fax equals to UPDATED_FAX
        defaultInventoryShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllInventoriesByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where fax is not null
        defaultInventoryShouldBeFound("fax.specified=true");

        // Get all the inventoryList where fax is null
        defaultInventoryShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where email equals to DEFAULT_EMAIL
        defaultInventoryShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the inventoryList where email equals to UPDATED_EMAIL
        defaultInventoryShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllInventoriesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultInventoryShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the inventoryList where email equals to UPDATED_EMAIL
        defaultInventoryShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllInventoriesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where email is not null
        defaultInventoryShouldBeFound("email.specified=true");

        // Get all the inventoryList where email is null
        defaultInventoryShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where status equals to DEFAULT_STATUS
        defaultInventoryShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the inventoryList where status equals to UPDATED_STATUS
        defaultInventoryShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllInventoriesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultInventoryShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the inventoryList where status equals to UPDATED_STATUS
        defaultInventoryShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllInventoriesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where status is not null
        defaultInventoryShouldBeFound("status.specified=true");

        // Get all the inventoryList where status is null
        defaultInventoryShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where status greater than or equals to DEFAULT_STATUS
        defaultInventoryShouldBeFound("status.greaterOrEqualThan=" + DEFAULT_STATUS);

        // Get all the inventoryList where status greater than or equals to UPDATED_STATUS
        defaultInventoryShouldNotBeFound("status.greaterOrEqualThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllInventoriesByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where status less than or equals to DEFAULT_STATUS
        defaultInventoryShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the inventoryList where status less than or equals to UPDATED_STATUS
        defaultInventoryShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllInventoriesByChannelIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where channel equals to DEFAULT_CHANNEL
        defaultInventoryShouldBeFound("channel.equals=" + DEFAULT_CHANNEL);

        // Get all the inventoryList where channel equals to UPDATED_CHANNEL
        defaultInventoryShouldNotBeFound("channel.equals=" + UPDATED_CHANNEL);
    }

    @Test
    @Transactional
    public void getAllInventoriesByChannelIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where channel in DEFAULT_CHANNEL or UPDATED_CHANNEL
        defaultInventoryShouldBeFound("channel.in=" + DEFAULT_CHANNEL + "," + UPDATED_CHANNEL);

        // Get all the inventoryList where channel equals to UPDATED_CHANNEL
        defaultInventoryShouldNotBeFound("channel.in=" + UPDATED_CHANNEL);
    }

    @Test
    @Transactional
    public void getAllInventoriesByChannelIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where channel is not null
        defaultInventoryShouldBeFound("channel.specified=true");

        // Get all the inventoryList where channel is null
        defaultInventoryShouldNotBeFound("channel.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowPickupFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowPickupFlag equals to DEFAULT_ALLOW_PICKUP_FLAG
        defaultInventoryShouldBeFound("allowPickupFlag.equals=" + DEFAULT_ALLOW_PICKUP_FLAG);

        // Get all the inventoryList where allowPickupFlag equals to UPDATED_ALLOW_PICKUP_FLAG
        defaultInventoryShouldNotBeFound("allowPickupFlag.equals=" + UPDATED_ALLOW_PICKUP_FLAG);
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowPickupFlagIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowPickupFlag in DEFAULT_ALLOW_PICKUP_FLAG or UPDATED_ALLOW_PICKUP_FLAG
        defaultInventoryShouldBeFound("allowPickupFlag.in=" + DEFAULT_ALLOW_PICKUP_FLAG + "," + UPDATED_ALLOW_PICKUP_FLAG);

        // Get all the inventoryList where allowPickupFlag equals to UPDATED_ALLOW_PICKUP_FLAG
        defaultInventoryShouldNotBeFound("allowPickupFlag.in=" + UPDATED_ALLOW_PICKUP_FLAG);
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowPickupFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowPickupFlag is not null
        defaultInventoryShouldBeFound("allowPickupFlag.specified=true");

        // Get all the inventoryList where allowPickupFlag is null
        defaultInventoryShouldNotBeFound("allowPickupFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowUrgentOrderFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowUrgentOrderFlag equals to DEFAULT_ALLOW_URGENT_ORDER_FLAG
        defaultInventoryShouldBeFound("allowUrgentOrderFlag.equals=" + DEFAULT_ALLOW_URGENT_ORDER_FLAG);

        // Get all the inventoryList where allowUrgentOrderFlag equals to UPDATED_ALLOW_URGENT_ORDER_FLAG
        defaultInventoryShouldNotBeFound("allowUrgentOrderFlag.equals=" + UPDATED_ALLOW_URGENT_ORDER_FLAG);
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowUrgentOrderFlagIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowUrgentOrderFlag in DEFAULT_ALLOW_URGENT_ORDER_FLAG or UPDATED_ALLOW_URGENT_ORDER_FLAG
        defaultInventoryShouldBeFound("allowUrgentOrderFlag.in=" + DEFAULT_ALLOW_URGENT_ORDER_FLAG + "," + UPDATED_ALLOW_URGENT_ORDER_FLAG);

        // Get all the inventoryList where allowUrgentOrderFlag equals to UPDATED_ALLOW_URGENT_ORDER_FLAG
        defaultInventoryShouldNotBeFound("allowUrgentOrderFlag.in=" + UPDATED_ALLOW_URGENT_ORDER_FLAG);
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowUrgentOrderFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowUrgentOrderFlag is not null
        defaultInventoryShouldBeFound("allowUrgentOrderFlag.specified=true");

        // Get all the inventoryList where allowUrgentOrderFlag is null
        defaultInventoryShouldNotBeFound("allowUrgentOrderFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowBookStudioFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowBookStudioFlag equals to DEFAULT_ALLOW_BOOK_STUDIO_FLAG
        defaultInventoryShouldBeFound("allowBookStudioFlag.equals=" + DEFAULT_ALLOW_BOOK_STUDIO_FLAG);

        // Get all the inventoryList where allowBookStudioFlag equals to UPDATED_ALLOW_BOOK_STUDIO_FLAG
        defaultInventoryShouldNotBeFound("allowBookStudioFlag.equals=" + UPDATED_ALLOW_BOOK_STUDIO_FLAG);
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowBookStudioFlagIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowBookStudioFlag in DEFAULT_ALLOW_BOOK_STUDIO_FLAG or UPDATED_ALLOW_BOOK_STUDIO_FLAG
        defaultInventoryShouldBeFound("allowBookStudioFlag.in=" + DEFAULT_ALLOW_BOOK_STUDIO_FLAG + "," + UPDATED_ALLOW_BOOK_STUDIO_FLAG);

        // Get all the inventoryList where allowBookStudioFlag equals to UPDATED_ALLOW_BOOK_STUDIO_FLAG
        defaultInventoryShouldNotBeFound("allowBookStudioFlag.in=" + UPDATED_ALLOW_BOOK_STUDIO_FLAG);
    }

    @Test
    @Transactional
    public void getAllInventoriesByAllowBookStudioFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where allowBookStudioFlag is not null
        defaultInventoryShouldBeFound("allowBookStudioFlag.specified=true");

        // Get all the inventoryList where allowBookStudioFlag is null
        defaultInventoryShouldNotBeFound("allowBookStudioFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultInventoryShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the inventoryList where updateDate equals to UPDATED_UPDATE_DATE
        defaultInventoryShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultInventoryShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the inventoryList where updateDate equals to UPDATED_UPDATE_DATE
        defaultInventoryShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where updateDate is not null
        defaultInventoryShouldBeFound("updateDate.specified=true");

        // Get all the inventoryList where updateDate is null
        defaultInventoryShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByUpdateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where updateDate greater than or equals to DEFAULT_UPDATE_DATE
        defaultInventoryShouldBeFound("updateDate.greaterOrEqualThan=" + DEFAULT_UPDATE_DATE);

        // Get all the inventoryList where updateDate greater than or equals to UPDATED_UPDATE_DATE
        defaultInventoryShouldNotBeFound("updateDate.greaterOrEqualThan=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByUpdateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where updateDate less than or equals to DEFAULT_UPDATE_DATE
        defaultInventoryShouldNotBeFound("updateDate.lessThan=" + DEFAULT_UPDATE_DATE);

        // Get all the inventoryList where updateDate less than or equals to UPDATED_UPDATE_DATE
        defaultInventoryShouldBeFound("updateDate.lessThan=" + UPDATED_UPDATE_DATE);
    }


    @Test
    @Transactional
    public void getAllInventoriesByDeleteFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where deleteFlag equals to DEFAULT_DELETE_FLAG
        defaultInventoryShouldBeFound("deleteFlag.equals=" + DEFAULT_DELETE_FLAG);

        // Get all the inventoryList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultInventoryShouldNotBeFound("deleteFlag.equals=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllInventoriesByDeleteFlagIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where deleteFlag in DEFAULT_DELETE_FLAG or UPDATED_DELETE_FLAG
        defaultInventoryShouldBeFound("deleteFlag.in=" + DEFAULT_DELETE_FLAG + "," + UPDATED_DELETE_FLAG);

        // Get all the inventoryList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultInventoryShouldNotBeFound("deleteFlag.in=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllInventoriesByDeleteFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where deleteFlag is not null
        defaultInventoryShouldBeFound("deleteFlag.specified=true");

        // Get all the inventoryList where deleteFlag is null
        defaultInventoryShouldNotBeFound("deleteFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where latitude equals to DEFAULT_LATITUDE
        defaultInventoryShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the inventoryList where latitude equals to UPDATED_LATITUDE
        defaultInventoryShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultInventoryShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the inventoryList where latitude equals to UPDATED_LATITUDE
        defaultInventoryShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where latitude is not null
        defaultInventoryShouldBeFound("latitude.specified=true");

        // Get all the inventoryList where latitude is null
        defaultInventoryShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where longitude equals to DEFAULT_LONGITUDE
        defaultInventoryShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the inventoryList where longitude equals to UPDATED_LONGITUDE
        defaultInventoryShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultInventoryShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the inventoryList where longitude equals to UPDATED_LONGITUDE
        defaultInventoryShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllInventoriesByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where longitude is not null
        defaultInventoryShouldBeFound("longitude.specified=true");

        // Get all the inventoryList where longitude is null
        defaultInventoryShouldNotBeFound("longitude.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultInventoryShouldBeFound(String filter) throws Exception {
        restInventoryMockMvc.perform(get("/api/inventories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventory.getId().intValue())))
            .andExpect(jsonPath("$.[*].inventoryCode").value(hasItem(DEFAULT_INVENTORY_CODE.toString())))
            .andExpect(jsonPath("$.[*].inventoryName").value(hasItem(DEFAULT_INVENTORY_NAME.toString())))
            .andExpect(jsonPath("$.[*].regionCode").value(hasItem(DEFAULT_REGION_CODE.toString())))
            .andExpect(jsonPath("$.[*].inventoryType").value(hasItem(DEFAULT_INVENTORY_TYPE)))
            .andExpect(jsonPath("$.[*].brandCode").value(hasItem(DEFAULT_BRAND_CODE.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].channel").value(hasItem(DEFAULT_CHANNEL.toString())))
            .andExpect(jsonPath("$.[*].allowPickupFlag").value(hasItem(DEFAULT_ALLOW_PICKUP_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].allowUrgentOrderFlag").value(hasItem(DEFAULT_ALLOW_URGENT_ORDER_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].allowBookStudioFlag").value(hasItem(DEFAULT_ALLOW_BOOK_STUDIO_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.toString())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultInventoryShouldNotBeFound(String filter) throws Exception {
        restInventoryMockMvc.perform(get("/api/inventories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingInventory() throws Exception {
        // Get the inventory
        restInventoryMockMvc.perform(get("/api/inventories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInventory() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        int databaseSizeBeforeUpdate = inventoryRepository.findAll().size();

        // Update the inventory
        Inventory updatedInventory = inventoryRepository.findById(inventory.getId()).get();
        // Disconnect from session so that the updates on updatedInventory are not directly saved in db
        em.detach(updatedInventory);
        updatedInventory
            .inventoryCode(UPDATED_INVENTORY_CODE)
            .inventoryName(UPDATED_INVENTORY_NAME)
            .regionCode(UPDATED_REGION_CODE)
            .inventoryType(UPDATED_INVENTORY_TYPE)
            .brandCode(UPDATED_BRAND_CODE)
            .address(UPDATED_ADDRESS)
            .tel(UPDATED_TEL)
            .fax(UPDATED_FAX)
            .email(UPDATED_EMAIL)
            .status(UPDATED_STATUS)
            .channel(UPDATED_CHANNEL)
            .allowPickupFlag(UPDATED_ALLOW_PICKUP_FLAG)
            .allowUrgentOrderFlag(UPDATED_ALLOW_URGENT_ORDER_FLAG)
            .allowBookStudioFlag(UPDATED_ALLOW_BOOK_STUDIO_FLAG)
            .updateDate(UPDATED_UPDATE_DATE)
            .deleteFlag(UPDATED_DELETE_FLAG)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE);
        InventoryDTO inventoryDTO = inventoryMapper.toDto(updatedInventory);

        restInventoryMockMvc.perform(put("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isOk());

        // Validate the Inventory in the database
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeUpdate);
        Inventory testInventory = inventoryList.get(inventoryList.size() - 1);
        assertThat(testInventory.getInventoryCode()).isEqualTo(UPDATED_INVENTORY_CODE);
        assertThat(testInventory.getInventoryName()).isEqualTo(UPDATED_INVENTORY_NAME);
        assertThat(testInventory.getRegionCode()).isEqualTo(UPDATED_REGION_CODE);
        assertThat(testInventory.getInventoryType()).isEqualTo(UPDATED_INVENTORY_TYPE);
        assertThat(testInventory.getBrandCode()).isEqualTo(UPDATED_BRAND_CODE);
        assertThat(testInventory.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testInventory.getTel()).isEqualTo(UPDATED_TEL);
        assertThat(testInventory.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testInventory.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testInventory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testInventory.getChannel()).isEqualTo(UPDATED_CHANNEL);
        assertThat(testInventory.isAllowPickupFlag()).isEqualTo(UPDATED_ALLOW_PICKUP_FLAG);
        assertThat(testInventory.isAllowUrgentOrderFlag()).isEqualTo(UPDATED_ALLOW_URGENT_ORDER_FLAG);
        assertThat(testInventory.isAllowBookStudioFlag()).isEqualTo(UPDATED_ALLOW_BOOK_STUDIO_FLAG);
        assertThat(testInventory.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testInventory.isDeleteFlag()).isEqualTo(UPDATED_DELETE_FLAG);
        assertThat(testInventory.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testInventory.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void updateNonExistingInventory() throws Exception {
        int databaseSizeBeforeUpdate = inventoryRepository.findAll().size();

        // Create the Inventory
        InventoryDTO inventoryDTO = inventoryMapper.toDto(inventory);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInventoryMockMvc.perform(put("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Inventory in the database
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInventory() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        int databaseSizeBeforeDelete = inventoryRepository.findAll().size();

        // Get the inventory
        restInventoryMockMvc.perform(delete("/api/inventories/{id}", inventory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Inventory.class);
        Inventory inventory1 = new Inventory();
        inventory1.setId(1L);
        Inventory inventory2 = new Inventory();
        inventory2.setId(inventory1.getId());
        assertThat(inventory1).isEqualTo(inventory2);
        inventory2.setId(2L);
        assertThat(inventory1).isNotEqualTo(inventory2);
        inventory1.setId(null);
        assertThat(inventory1).isNotEqualTo(inventory2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InventoryDTO.class);
        InventoryDTO inventoryDTO1 = new InventoryDTO();
        inventoryDTO1.setId(1L);
        InventoryDTO inventoryDTO2 = new InventoryDTO();
        assertThat(inventoryDTO1).isNotEqualTo(inventoryDTO2);
        inventoryDTO2.setId(inventoryDTO1.getId());
        assertThat(inventoryDTO1).isEqualTo(inventoryDTO2);
        inventoryDTO2.setId(2L);
        assertThat(inventoryDTO1).isNotEqualTo(inventoryDTO2);
        inventoryDTO1.setId(null);
        assertThat(inventoryDTO1).isNotEqualTo(inventoryDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(inventoryMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(inventoryMapper.fromId(null)).isNull();
    }
}
