package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.VoucherGift;
import com.gcs.flexba.rmsweb.repository.VoucherGiftRepository;
import com.gcs.flexba.rmsweb.service.VoucherGiftService;
import com.gcs.flexba.rmsweb.service.dto.VoucherGiftDTO;
import com.gcs.flexba.rmsweb.service.mapper.VoucherGiftMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.VoucherGiftCriteria;
import com.gcs.flexba.rmsweb.service.VoucherGiftQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VoucherGiftResource REST controller.
 *
 * @see VoucherGiftResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class VoucherGiftResourceIntTest {

    private static final String DEFAULT_VOUCHER_GIFT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_VOUCHER_GIFT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_VOUCHER_GIFT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_VOUCHER_GIFT_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_VOUCHER_CATEGORY = 1;
    private static final Integer UPDATED_VOUCHER_CATEGORY = 2;

    private static final Integer DEFAULT_VOUCHER_TYPE = 1;
    private static final Integer UPDATED_VOUCHER_TYPE = 2;

    private static final String DEFAULT_CUSTOMER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_PHONE = "BBBBBBBBBB";

    private static final Double DEFAULT_TOTAL_VALUE = 1D;
    private static final Double UPDATED_TOTAL_VALUE = 2D;

    private static final Double DEFAULT_AVAILABLE_VALUE = 1D;
    private static final Double UPDATED_AVAILABLE_VALUE = 2D;

    private static final ZonedDateTime DEFAULT_VALID_FROM = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALID_FROM = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_VALID_TO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALID_TO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_REF_SKU = "AAAAAAAAAA";
    private static final String UPDATED_REF_SKU = "BBBBBBBBBB";

    private static final String DEFAULT_STORE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_STORE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Integer DEFAULT_EXPIRED_DURATION = 1;
    private static final Integer UPDATED_EXPIRED_DURATION = 2;

    private static final Integer DEFAULT_EFFECTED_DAY = 1;
    private static final Integer UPDATED_EFFECTED_DAY = 2;

    private static final BigDecimal DEFAULT_VOUCHER_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VOUCHER_VALUE = new BigDecimal(2);

    private static final String DEFAULT_USED_CUSTOMER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_USED_CUSTOMER_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_USED_CUSTOMER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_USED_CUSTOMER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_USED_CUSTOMER_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_USED_CUSTOMER_PHONE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_UPDATE_FLAG = false;
    private static final Boolean UPDATED_UPDATE_FLAG = true;

    private static final BigDecimal DEFAULT_MAX_APPLIED_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_APPLIED_AMOUNT = new BigDecimal(2);

    private static final Integer DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER = 1;
    private static final Integer UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER = 2;

    private static final Integer DEFAULT_VOUCHER_APPLIED_TYPE = 1;
    private static final Integer UPDATED_VOUCHER_APPLIED_TYPE = 2;

    private static final ZonedDateTime DEFAULT_UPDATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETE_FLAG = false;
    private static final Boolean UPDATED_DELETE_FLAG = true;

    private static final String DEFAULT_PERSONAL_ID = "AAAAAAAAAA";
    private static final String UPDATED_PERSONAL_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_VOUCHER_GIFT_ID = 1;
    private static final Integer UPDATED_VOUCHER_GIFT_ID = 2;

    @Autowired
    private VoucherGiftRepository voucherGiftRepository;


    @Autowired
    private VoucherGiftMapper voucherGiftMapper;
    

    @Autowired
    private VoucherGiftService voucherGiftService;

    @Autowired
    private VoucherGiftQueryService voucherGiftQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVoucherGiftMockMvc;

    private VoucherGift voucherGift;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VoucherGiftResource voucherGiftResource = new VoucherGiftResource(voucherGiftService, voucherGiftQueryService);
        this.restVoucherGiftMockMvc = MockMvcBuilders.standaloneSetup(voucherGiftResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VoucherGift createEntity(EntityManager em) {
        VoucherGift voucherGift = new VoucherGift()
            .voucherGiftCode(DEFAULT_VOUCHER_GIFT_CODE)
            .voucherGiftName(DEFAULT_VOUCHER_GIFT_NAME)
            .voucherCategory(DEFAULT_VOUCHER_CATEGORY)
            .voucherType(DEFAULT_VOUCHER_TYPE)
            .customerCode(DEFAULT_CUSTOMER_CODE)
            .customerName(DEFAULT_CUSTOMER_NAME)
            .customerPhone(DEFAULT_CUSTOMER_PHONE)
            .totalValue(DEFAULT_TOTAL_VALUE)
            .availableValue(DEFAULT_AVAILABLE_VALUE)
            .validFrom(DEFAULT_VALID_FROM)
            .validTo(DEFAULT_VALID_TO)
            .status(DEFAULT_STATUS)
            .refSku(DEFAULT_REF_SKU)
            .storeCode(DEFAULT_STORE_CODE)
            .note(DEFAULT_NOTE)
            .createdBy(DEFAULT_CREATED_BY)
            .expiredDuration(DEFAULT_EXPIRED_DURATION)
            .effectedDay(DEFAULT_EFFECTED_DAY)
            .voucherValue(DEFAULT_VOUCHER_VALUE)
            .usedCustomerCode(DEFAULT_USED_CUSTOMER_CODE)
            .usedCustomerName(DEFAULT_USED_CUSTOMER_NAME)
            .usedCustomerPhone(DEFAULT_USED_CUSTOMER_PHONE)
            .updateFlag(DEFAULT_UPDATE_FLAG)
            .maxAppliedAmount(DEFAULT_MAX_APPLIED_AMOUNT)
            .max_appliedTimePerCustomer(DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER)
            .voucherAppliedType(DEFAULT_VOUCHER_APPLIED_TYPE)
            .updateDate(DEFAULT_UPDATE_DATE)
            .deleteFlag(DEFAULT_DELETE_FLAG)
            .personalId(DEFAULT_PERSONAL_ID);
        return voucherGift;
    }

    @Before
    public void initTest() {
        voucherGift = createEntity(em);
    }

    @Test
    @Transactional
    public void createVoucherGift() throws Exception {
        int databaseSizeBeforeCreate = voucherGiftRepository.findAll().size();

        // Create the VoucherGift
        VoucherGiftDTO voucherGiftDTO = voucherGiftMapper.toDto(voucherGift);
        restVoucherGiftMockMvc.perform(post("/api/voucher-gifts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(voucherGiftDTO)))
            .andExpect(status().isCreated());

        // Validate the VoucherGift in the database
        List<VoucherGift> voucherGiftList = voucherGiftRepository.findAll();
        assertThat(voucherGiftList).hasSize(databaseSizeBeforeCreate + 1);
        VoucherGift testVoucherGift = voucherGiftList.get(voucherGiftList.size() - 1);
        assertThat(testVoucherGift.getVoucherGiftCode()).isEqualTo(DEFAULT_VOUCHER_GIFT_CODE);
        assertThat(testVoucherGift.getVoucherGiftName()).isEqualTo(DEFAULT_VOUCHER_GIFT_NAME);
        assertThat(testVoucherGift.getVoucherCategory()).isEqualTo(DEFAULT_VOUCHER_CATEGORY);
        assertThat(testVoucherGift.getVoucherType()).isEqualTo(DEFAULT_VOUCHER_TYPE);
        assertThat(testVoucherGift.getCustomerCode()).isEqualTo(DEFAULT_CUSTOMER_CODE);
        assertThat(testVoucherGift.getCustomerName()).isEqualTo(DEFAULT_CUSTOMER_NAME);
        assertThat(testVoucherGift.getCustomerPhone()).isEqualTo(DEFAULT_CUSTOMER_PHONE);
        assertThat(testVoucherGift.getTotalValue()).isEqualTo(DEFAULT_TOTAL_VALUE);
        assertThat(testVoucherGift.getAvailableValue()).isEqualTo(DEFAULT_AVAILABLE_VALUE);
        assertThat(testVoucherGift.getValidFrom()).isEqualTo(DEFAULT_VALID_FROM);
        assertThat(testVoucherGift.getValidTo()).isEqualTo(DEFAULT_VALID_TO);
        assertThat(testVoucherGift.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testVoucherGift.getRefSku()).isEqualTo(DEFAULT_REF_SKU);
        assertThat(testVoucherGift.getStoreCode()).isEqualTo(DEFAULT_STORE_CODE);
        assertThat(testVoucherGift.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testVoucherGift.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testVoucherGift.getExpiredDuration()).isEqualTo(DEFAULT_EXPIRED_DURATION);
        assertThat(testVoucherGift.getEffectedDay()).isEqualTo(DEFAULT_EFFECTED_DAY);
        assertThat(testVoucherGift.getVoucherValue()).isEqualTo(DEFAULT_VOUCHER_VALUE);
        assertThat(testVoucherGift.getUsedCustomerCode()).isEqualTo(DEFAULT_USED_CUSTOMER_CODE);
        assertThat(testVoucherGift.getUsedCustomerName()).isEqualTo(DEFAULT_USED_CUSTOMER_NAME);
        assertThat(testVoucherGift.getUsedCustomerPhone()).isEqualTo(DEFAULT_USED_CUSTOMER_PHONE);
        assertThat(testVoucherGift.isUpdateFlag()).isEqualTo(DEFAULT_UPDATE_FLAG);
        assertThat(testVoucherGift.getMaxAppliedAmount()).isEqualTo(DEFAULT_MAX_APPLIED_AMOUNT);
        assertThat(testVoucherGift.getMax_appliedTimePerCustomer()).isEqualTo(DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER);
        assertThat(testVoucherGift.getVoucherAppliedType()).isEqualTo(DEFAULT_VOUCHER_APPLIED_TYPE);
        assertThat(testVoucherGift.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testVoucherGift.isDeleteFlag()).isEqualTo(DEFAULT_DELETE_FLAG);
        assertThat(testVoucherGift.getPersonalId()).isEqualTo(DEFAULT_PERSONAL_ID);
    }

    @Test
    @Transactional
    public void createVoucherGiftWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = voucherGiftRepository.findAll().size();

        // Create the VoucherGift with an existing ID
        voucherGift.setId(1L);
        VoucherGiftDTO voucherGiftDTO = voucherGiftMapper.toDto(voucherGift);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVoucherGiftMockMvc.perform(post("/api/voucher-gifts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(voucherGiftDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VoucherGift in the database
        List<VoucherGift> voucherGiftList = voucherGiftRepository.findAll();
        assertThat(voucherGiftList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllVoucherGifts() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList
        restVoucherGiftMockMvc.perform(get("/api/voucher-gifts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(voucherGift.getId().intValue())))
            .andExpect(jsonPath("$.[*].voucherGiftCode").value(hasItem(DEFAULT_VOUCHER_GIFT_CODE.toString())))
            .andExpect(jsonPath("$.[*].voucherGiftName").value(hasItem(DEFAULT_VOUCHER_GIFT_NAME.toString())))
            .andExpect(jsonPath("$.[*].voucherCategory").value(hasItem(DEFAULT_VOUCHER_CATEGORY)))
            .andExpect(jsonPath("$.[*].voucherType").value(hasItem(DEFAULT_VOUCHER_TYPE)))
            .andExpect(jsonPath("$.[*].customerCode").value(hasItem(DEFAULT_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].customerName").value(hasItem(DEFAULT_CUSTOMER_NAME.toString())))
            .andExpect(jsonPath("$.[*].customerPhone").value(hasItem(DEFAULT_CUSTOMER_PHONE.toString())))
            .andExpect(jsonPath("$.[*].totalValue").value(hasItem(DEFAULT_TOTAL_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].availableValue").value(hasItem(DEFAULT_AVAILABLE_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].validFrom").value(hasItem(sameInstant(DEFAULT_VALID_FROM))))
            .andExpect(jsonPath("$.[*].validTo").value(hasItem(sameInstant(DEFAULT_VALID_TO))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].refSku").value(hasItem(DEFAULT_REF_SKU.toString())))
            .andExpect(jsonPath("$.[*].storeCode").value(hasItem(DEFAULT_STORE_CODE.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].expiredDuration").value(hasItem(DEFAULT_EXPIRED_DURATION)))
            .andExpect(jsonPath("$.[*].effectedDay").value(hasItem(DEFAULT_EFFECTED_DAY)))
            .andExpect(jsonPath("$.[*].voucherValue").value(hasItem(DEFAULT_VOUCHER_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].usedCustomerCode").value(hasItem(DEFAULT_USED_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].usedCustomerName").value(hasItem(DEFAULT_USED_CUSTOMER_NAME.toString())))
            .andExpect(jsonPath("$.[*].usedCustomerPhone").value(hasItem(DEFAULT_USED_CUSTOMER_PHONE.toString())))
            .andExpect(jsonPath("$.[*].updateFlag").value(hasItem(DEFAULT_UPDATE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].maxAppliedAmount").value(hasItem(DEFAULT_MAX_APPLIED_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].max_appliedTimePerCustomer").value(hasItem(DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER)))
            .andExpect(jsonPath("$.[*].voucherAppliedType").value(hasItem(DEFAULT_VOUCHER_APPLIED_TYPE)))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].personalId").value(hasItem(DEFAULT_PERSONAL_ID.toString())))
            .andExpect(jsonPath("$.[*].voucherGiftId").value(hasItem(DEFAULT_VOUCHER_GIFT_ID)));
    }
    

    @Test
    @Transactional
    public void getVoucherGift() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get the voucherGift
        restVoucherGiftMockMvc.perform(get("/api/voucher-gifts/{id}", voucherGift.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(voucherGift.getId().intValue()))
            .andExpect(jsonPath("$.voucherGiftCode").value(DEFAULT_VOUCHER_GIFT_CODE.toString()))
            .andExpect(jsonPath("$.voucherGiftName").value(DEFAULT_VOUCHER_GIFT_NAME.toString()))
            .andExpect(jsonPath("$.voucherCategory").value(DEFAULT_VOUCHER_CATEGORY))
            .andExpect(jsonPath("$.voucherType").value(DEFAULT_VOUCHER_TYPE))
            .andExpect(jsonPath("$.customerCode").value(DEFAULT_CUSTOMER_CODE.toString()))
            .andExpect(jsonPath("$.customerName").value(DEFAULT_CUSTOMER_NAME.toString()))
            .andExpect(jsonPath("$.customerPhone").value(DEFAULT_CUSTOMER_PHONE.toString()))
            .andExpect(jsonPath("$.totalValue").value(DEFAULT_TOTAL_VALUE.doubleValue()))
            .andExpect(jsonPath("$.availableValue").value(DEFAULT_AVAILABLE_VALUE.doubleValue()))
            .andExpect(jsonPath("$.validFrom").value(sameInstant(DEFAULT_VALID_FROM)))
            .andExpect(jsonPath("$.validTo").value(sameInstant(DEFAULT_VALID_TO)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.refSku").value(DEFAULT_REF_SKU.toString()))
            .andExpect(jsonPath("$.storeCode").value(DEFAULT_STORE_CODE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.expiredDuration").value(DEFAULT_EXPIRED_DURATION))
            .andExpect(jsonPath("$.effectedDay").value(DEFAULT_EFFECTED_DAY))
            .andExpect(jsonPath("$.voucherValue").value(DEFAULT_VOUCHER_VALUE.intValue()))
            .andExpect(jsonPath("$.usedCustomerCode").value(DEFAULT_USED_CUSTOMER_CODE.toString()))
            .andExpect(jsonPath("$.usedCustomerName").value(DEFAULT_USED_CUSTOMER_NAME.toString()))
            .andExpect(jsonPath("$.usedCustomerPhone").value(DEFAULT_USED_CUSTOMER_PHONE.toString()))
            .andExpect(jsonPath("$.updateFlag").value(DEFAULT_UPDATE_FLAG.booleanValue()))
            .andExpect(jsonPath("$.maxAppliedAmount").value(DEFAULT_MAX_APPLIED_AMOUNT.intValue()))
            .andExpect(jsonPath("$.max_appliedTimePerCustomer").value(DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER))
            .andExpect(jsonPath("$.voucherAppliedType").value(DEFAULT_VOUCHER_APPLIED_TYPE))
            .andExpect(jsonPath("$.updateDate").value(sameInstant(DEFAULT_UPDATE_DATE)))
            .andExpect(jsonPath("$.deleteFlag").value(DEFAULT_DELETE_FLAG.booleanValue()))
            .andExpect(jsonPath("$.personalId").value(DEFAULT_PERSONAL_ID.toString()))
            .andExpect(jsonPath("$.voucherGiftId").value(DEFAULT_VOUCHER_GIFT_ID));
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftCode equals to DEFAULT_VOUCHER_GIFT_CODE
        defaultVoucherGiftShouldBeFound("voucherGiftCode.equals=" + DEFAULT_VOUCHER_GIFT_CODE);

        // Get all the voucherGiftList where voucherGiftCode equals to UPDATED_VOUCHER_GIFT_CODE
        defaultVoucherGiftShouldNotBeFound("voucherGiftCode.equals=" + UPDATED_VOUCHER_GIFT_CODE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftCodeIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftCode in DEFAULT_VOUCHER_GIFT_CODE or UPDATED_VOUCHER_GIFT_CODE
        defaultVoucherGiftShouldBeFound("voucherGiftCode.in=" + DEFAULT_VOUCHER_GIFT_CODE + "," + UPDATED_VOUCHER_GIFT_CODE);

        // Get all the voucherGiftList where voucherGiftCode equals to UPDATED_VOUCHER_GIFT_CODE
        defaultVoucherGiftShouldNotBeFound("voucherGiftCode.in=" + UPDATED_VOUCHER_GIFT_CODE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftCode is not null
        defaultVoucherGiftShouldBeFound("voucherGiftCode.specified=true");

        // Get all the voucherGiftList where voucherGiftCode is null
        defaultVoucherGiftShouldNotBeFound("voucherGiftCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftNameIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftName equals to DEFAULT_VOUCHER_GIFT_NAME
        defaultVoucherGiftShouldBeFound("voucherGiftName.equals=" + DEFAULT_VOUCHER_GIFT_NAME);

        // Get all the voucherGiftList where voucherGiftName equals to UPDATED_VOUCHER_GIFT_NAME
        defaultVoucherGiftShouldNotBeFound("voucherGiftName.equals=" + UPDATED_VOUCHER_GIFT_NAME);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftNameIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftName in DEFAULT_VOUCHER_GIFT_NAME or UPDATED_VOUCHER_GIFT_NAME
        defaultVoucherGiftShouldBeFound("voucherGiftName.in=" + DEFAULT_VOUCHER_GIFT_NAME + "," + UPDATED_VOUCHER_GIFT_NAME);

        // Get all the voucherGiftList where voucherGiftName equals to UPDATED_VOUCHER_GIFT_NAME
        defaultVoucherGiftShouldNotBeFound("voucherGiftName.in=" + UPDATED_VOUCHER_GIFT_NAME);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftName is not null
        defaultVoucherGiftShouldBeFound("voucherGiftName.specified=true");

        // Get all the voucherGiftList where voucherGiftName is null
        defaultVoucherGiftShouldNotBeFound("voucherGiftName.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherCategory equals to DEFAULT_VOUCHER_CATEGORY
        defaultVoucherGiftShouldBeFound("voucherCategory.equals=" + DEFAULT_VOUCHER_CATEGORY);

        // Get all the voucherGiftList where voucherCategory equals to UPDATED_VOUCHER_CATEGORY
        defaultVoucherGiftShouldNotBeFound("voucherCategory.equals=" + UPDATED_VOUCHER_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherCategoryIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherCategory in DEFAULT_VOUCHER_CATEGORY or UPDATED_VOUCHER_CATEGORY
        defaultVoucherGiftShouldBeFound("voucherCategory.in=" + DEFAULT_VOUCHER_CATEGORY + "," + UPDATED_VOUCHER_CATEGORY);

        // Get all the voucherGiftList where voucherCategory equals to UPDATED_VOUCHER_CATEGORY
        defaultVoucherGiftShouldNotBeFound("voucherCategory.in=" + UPDATED_VOUCHER_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherCategoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherCategory is not null
        defaultVoucherGiftShouldBeFound("voucherCategory.specified=true");

        // Get all the voucherGiftList where voucherCategory is null
        defaultVoucherGiftShouldNotBeFound("voucherCategory.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherCategoryIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherCategory greater than or equals to DEFAULT_VOUCHER_CATEGORY
        defaultVoucherGiftShouldBeFound("voucherCategory.greaterOrEqualThan=" + DEFAULT_VOUCHER_CATEGORY);

        // Get all the voucherGiftList where voucherCategory greater than or equals to UPDATED_VOUCHER_CATEGORY
        defaultVoucherGiftShouldNotBeFound("voucherCategory.greaterOrEqualThan=" + UPDATED_VOUCHER_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherCategoryIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherCategory less than or equals to DEFAULT_VOUCHER_CATEGORY
        defaultVoucherGiftShouldNotBeFound("voucherCategory.lessThan=" + DEFAULT_VOUCHER_CATEGORY);

        // Get all the voucherGiftList where voucherCategory less than or equals to UPDATED_VOUCHER_CATEGORY
        defaultVoucherGiftShouldBeFound("voucherCategory.lessThan=" + UPDATED_VOUCHER_CATEGORY);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherType equals to DEFAULT_VOUCHER_TYPE
        defaultVoucherGiftShouldBeFound("voucherType.equals=" + DEFAULT_VOUCHER_TYPE);

        // Get all the voucherGiftList where voucherType equals to UPDATED_VOUCHER_TYPE
        defaultVoucherGiftShouldNotBeFound("voucherType.equals=" + UPDATED_VOUCHER_TYPE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherTypeIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherType in DEFAULT_VOUCHER_TYPE or UPDATED_VOUCHER_TYPE
        defaultVoucherGiftShouldBeFound("voucherType.in=" + DEFAULT_VOUCHER_TYPE + "," + UPDATED_VOUCHER_TYPE);

        // Get all the voucherGiftList where voucherType equals to UPDATED_VOUCHER_TYPE
        defaultVoucherGiftShouldNotBeFound("voucherType.in=" + UPDATED_VOUCHER_TYPE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherType is not null
        defaultVoucherGiftShouldBeFound("voucherType.specified=true");

        // Get all the voucherGiftList where voucherType is null
        defaultVoucherGiftShouldNotBeFound("voucherType.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherTypeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherType greater than or equals to DEFAULT_VOUCHER_TYPE
        defaultVoucherGiftShouldBeFound("voucherType.greaterOrEqualThan=" + DEFAULT_VOUCHER_TYPE);

        // Get all the voucherGiftList where voucherType greater than or equals to UPDATED_VOUCHER_TYPE
        defaultVoucherGiftShouldNotBeFound("voucherType.greaterOrEqualThan=" + UPDATED_VOUCHER_TYPE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherTypeIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherType less than or equals to DEFAULT_VOUCHER_TYPE
        defaultVoucherGiftShouldNotBeFound("voucherType.lessThan=" + DEFAULT_VOUCHER_TYPE);

        // Get all the voucherGiftList where voucherType less than or equals to UPDATED_VOUCHER_TYPE
        defaultVoucherGiftShouldBeFound("voucherType.lessThan=" + UPDATED_VOUCHER_TYPE);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerCode equals to DEFAULT_CUSTOMER_CODE
        defaultVoucherGiftShouldBeFound("customerCode.equals=" + DEFAULT_CUSTOMER_CODE);

        // Get all the voucherGiftList where customerCode equals to UPDATED_CUSTOMER_CODE
        defaultVoucherGiftShouldNotBeFound("customerCode.equals=" + UPDATED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerCodeIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerCode in DEFAULT_CUSTOMER_CODE or UPDATED_CUSTOMER_CODE
        defaultVoucherGiftShouldBeFound("customerCode.in=" + DEFAULT_CUSTOMER_CODE + "," + UPDATED_CUSTOMER_CODE);

        // Get all the voucherGiftList where customerCode equals to UPDATED_CUSTOMER_CODE
        defaultVoucherGiftShouldNotBeFound("customerCode.in=" + UPDATED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerCode is not null
        defaultVoucherGiftShouldBeFound("customerCode.specified=true");

        // Get all the voucherGiftList where customerCode is null
        defaultVoucherGiftShouldNotBeFound("customerCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerNameIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerName equals to DEFAULT_CUSTOMER_NAME
        defaultVoucherGiftShouldBeFound("customerName.equals=" + DEFAULT_CUSTOMER_NAME);

        // Get all the voucherGiftList where customerName equals to UPDATED_CUSTOMER_NAME
        defaultVoucherGiftShouldNotBeFound("customerName.equals=" + UPDATED_CUSTOMER_NAME);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerNameIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerName in DEFAULT_CUSTOMER_NAME or UPDATED_CUSTOMER_NAME
        defaultVoucherGiftShouldBeFound("customerName.in=" + DEFAULT_CUSTOMER_NAME + "," + UPDATED_CUSTOMER_NAME);

        // Get all the voucherGiftList where customerName equals to UPDATED_CUSTOMER_NAME
        defaultVoucherGiftShouldNotBeFound("customerName.in=" + UPDATED_CUSTOMER_NAME);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerName is not null
        defaultVoucherGiftShouldBeFound("customerName.specified=true");

        // Get all the voucherGiftList where customerName is null
        defaultVoucherGiftShouldNotBeFound("customerName.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerPhone equals to DEFAULT_CUSTOMER_PHONE
        defaultVoucherGiftShouldBeFound("customerPhone.equals=" + DEFAULT_CUSTOMER_PHONE);

        // Get all the voucherGiftList where customerPhone equals to UPDATED_CUSTOMER_PHONE
        defaultVoucherGiftShouldNotBeFound("customerPhone.equals=" + UPDATED_CUSTOMER_PHONE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerPhone in DEFAULT_CUSTOMER_PHONE or UPDATED_CUSTOMER_PHONE
        defaultVoucherGiftShouldBeFound("customerPhone.in=" + DEFAULT_CUSTOMER_PHONE + "," + UPDATED_CUSTOMER_PHONE);

        // Get all the voucherGiftList where customerPhone equals to UPDATED_CUSTOMER_PHONE
        defaultVoucherGiftShouldNotBeFound("customerPhone.in=" + UPDATED_CUSTOMER_PHONE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCustomerPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where customerPhone is not null
        defaultVoucherGiftShouldBeFound("customerPhone.specified=true");

        // Get all the voucherGiftList where customerPhone is null
        defaultVoucherGiftShouldNotBeFound("customerPhone.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByTotalValueIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where totalValue equals to DEFAULT_TOTAL_VALUE
        defaultVoucherGiftShouldBeFound("totalValue.equals=" + DEFAULT_TOTAL_VALUE);

        // Get all the voucherGiftList where totalValue equals to UPDATED_TOTAL_VALUE
        defaultVoucherGiftShouldNotBeFound("totalValue.equals=" + UPDATED_TOTAL_VALUE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByTotalValueIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where totalValue in DEFAULT_TOTAL_VALUE or UPDATED_TOTAL_VALUE
        defaultVoucherGiftShouldBeFound("totalValue.in=" + DEFAULT_TOTAL_VALUE + "," + UPDATED_TOTAL_VALUE);

        // Get all the voucherGiftList where totalValue equals to UPDATED_TOTAL_VALUE
        defaultVoucherGiftShouldNotBeFound("totalValue.in=" + UPDATED_TOTAL_VALUE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByTotalValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where totalValue is not null
        defaultVoucherGiftShouldBeFound("totalValue.specified=true");

        // Get all the voucherGiftList where totalValue is null
        defaultVoucherGiftShouldNotBeFound("totalValue.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByAvailableValueIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where availableValue equals to DEFAULT_AVAILABLE_VALUE
        defaultVoucherGiftShouldBeFound("availableValue.equals=" + DEFAULT_AVAILABLE_VALUE);

        // Get all the voucherGiftList where availableValue equals to UPDATED_AVAILABLE_VALUE
        defaultVoucherGiftShouldNotBeFound("availableValue.equals=" + UPDATED_AVAILABLE_VALUE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByAvailableValueIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where availableValue in DEFAULT_AVAILABLE_VALUE or UPDATED_AVAILABLE_VALUE
        defaultVoucherGiftShouldBeFound("availableValue.in=" + DEFAULT_AVAILABLE_VALUE + "," + UPDATED_AVAILABLE_VALUE);

        // Get all the voucherGiftList where availableValue equals to UPDATED_AVAILABLE_VALUE
        defaultVoucherGiftShouldNotBeFound("availableValue.in=" + UPDATED_AVAILABLE_VALUE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByAvailableValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where availableValue is not null
        defaultVoucherGiftShouldBeFound("availableValue.specified=true");

        // Get all the voucherGiftList where availableValue is null
        defaultVoucherGiftShouldNotBeFound("availableValue.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidFromIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validFrom equals to DEFAULT_VALID_FROM
        defaultVoucherGiftShouldBeFound("validFrom.equals=" + DEFAULT_VALID_FROM);

        // Get all the voucherGiftList where validFrom equals to UPDATED_VALID_FROM
        defaultVoucherGiftShouldNotBeFound("validFrom.equals=" + UPDATED_VALID_FROM);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidFromIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validFrom in DEFAULT_VALID_FROM or UPDATED_VALID_FROM
        defaultVoucherGiftShouldBeFound("validFrom.in=" + DEFAULT_VALID_FROM + "," + UPDATED_VALID_FROM);

        // Get all the voucherGiftList where validFrom equals to UPDATED_VALID_FROM
        defaultVoucherGiftShouldNotBeFound("validFrom.in=" + UPDATED_VALID_FROM);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validFrom is not null
        defaultVoucherGiftShouldBeFound("validFrom.specified=true");

        // Get all the voucherGiftList where validFrom is null
        defaultVoucherGiftShouldNotBeFound("validFrom.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidFromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validFrom greater than or equals to DEFAULT_VALID_FROM
        defaultVoucherGiftShouldBeFound("validFrom.greaterOrEqualThan=" + DEFAULT_VALID_FROM);

        // Get all the voucherGiftList where validFrom greater than or equals to UPDATED_VALID_FROM
        defaultVoucherGiftShouldNotBeFound("validFrom.greaterOrEqualThan=" + UPDATED_VALID_FROM);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidFromIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validFrom less than or equals to DEFAULT_VALID_FROM
        defaultVoucherGiftShouldNotBeFound("validFrom.lessThan=" + DEFAULT_VALID_FROM);

        // Get all the voucherGiftList where validFrom less than or equals to UPDATED_VALID_FROM
        defaultVoucherGiftShouldBeFound("validFrom.lessThan=" + UPDATED_VALID_FROM);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByValidToIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validTo equals to DEFAULT_VALID_TO
        defaultVoucherGiftShouldBeFound("validTo.equals=" + DEFAULT_VALID_TO);

        // Get all the voucherGiftList where validTo equals to UPDATED_VALID_TO
        defaultVoucherGiftShouldNotBeFound("validTo.equals=" + UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidToIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validTo in DEFAULT_VALID_TO or UPDATED_VALID_TO
        defaultVoucherGiftShouldBeFound("validTo.in=" + DEFAULT_VALID_TO + "," + UPDATED_VALID_TO);

        // Get all the voucherGiftList where validTo equals to UPDATED_VALID_TO
        defaultVoucherGiftShouldNotBeFound("validTo.in=" + UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidToIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validTo is not null
        defaultVoucherGiftShouldBeFound("validTo.specified=true");

        // Get all the voucherGiftList where validTo is null
        defaultVoucherGiftShouldNotBeFound("validTo.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidToIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validTo greater than or equals to DEFAULT_VALID_TO
        defaultVoucherGiftShouldBeFound("validTo.greaterOrEqualThan=" + DEFAULT_VALID_TO);

        // Get all the voucherGiftList where validTo greater than or equals to UPDATED_VALID_TO
        defaultVoucherGiftShouldNotBeFound("validTo.greaterOrEqualThan=" + UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByValidToIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where validTo less than or equals to DEFAULT_VALID_TO
        defaultVoucherGiftShouldNotBeFound("validTo.lessThan=" + DEFAULT_VALID_TO);

        // Get all the voucherGiftList where validTo less than or equals to UPDATED_VALID_TO
        defaultVoucherGiftShouldBeFound("validTo.lessThan=" + UPDATED_VALID_TO);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where status equals to DEFAULT_STATUS
        defaultVoucherGiftShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the voucherGiftList where status equals to UPDATED_STATUS
        defaultVoucherGiftShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultVoucherGiftShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the voucherGiftList where status equals to UPDATED_STATUS
        defaultVoucherGiftShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where status is not null
        defaultVoucherGiftShouldBeFound("status.specified=true");

        // Get all the voucherGiftList where status is null
        defaultVoucherGiftShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where status greater than or equals to DEFAULT_STATUS
        defaultVoucherGiftShouldBeFound("status.greaterOrEqualThan=" + DEFAULT_STATUS);

        // Get all the voucherGiftList where status greater than or equals to UPDATED_STATUS
        defaultVoucherGiftShouldNotBeFound("status.greaterOrEqualThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where status less than or equals to DEFAULT_STATUS
        defaultVoucherGiftShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the voucherGiftList where status less than or equals to UPDATED_STATUS
        defaultVoucherGiftShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByRefSkuIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where refSku equals to DEFAULT_REF_SKU
        defaultVoucherGiftShouldBeFound("refSku.equals=" + DEFAULT_REF_SKU);

        // Get all the voucherGiftList where refSku equals to UPDATED_REF_SKU
        defaultVoucherGiftShouldNotBeFound("refSku.equals=" + UPDATED_REF_SKU);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByRefSkuIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where refSku in DEFAULT_REF_SKU or UPDATED_REF_SKU
        defaultVoucherGiftShouldBeFound("refSku.in=" + DEFAULT_REF_SKU + "," + UPDATED_REF_SKU);

        // Get all the voucherGiftList where refSku equals to UPDATED_REF_SKU
        defaultVoucherGiftShouldNotBeFound("refSku.in=" + UPDATED_REF_SKU);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByRefSkuIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where refSku is not null
        defaultVoucherGiftShouldBeFound("refSku.specified=true");

        // Get all the voucherGiftList where refSku is null
        defaultVoucherGiftShouldNotBeFound("refSku.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByStoreCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where storeCode equals to DEFAULT_STORE_CODE
        defaultVoucherGiftShouldBeFound("storeCode.equals=" + DEFAULT_STORE_CODE);

        // Get all the voucherGiftList where storeCode equals to UPDATED_STORE_CODE
        defaultVoucherGiftShouldNotBeFound("storeCode.equals=" + UPDATED_STORE_CODE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByStoreCodeIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where storeCode in DEFAULT_STORE_CODE or UPDATED_STORE_CODE
        defaultVoucherGiftShouldBeFound("storeCode.in=" + DEFAULT_STORE_CODE + "," + UPDATED_STORE_CODE);

        // Get all the voucherGiftList where storeCode equals to UPDATED_STORE_CODE
        defaultVoucherGiftShouldNotBeFound("storeCode.in=" + UPDATED_STORE_CODE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByStoreCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where storeCode is not null
        defaultVoucherGiftShouldBeFound("storeCode.specified=true");

        // Get all the voucherGiftList where storeCode is null
        defaultVoucherGiftShouldNotBeFound("storeCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByNoteIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where note equals to DEFAULT_NOTE
        defaultVoucherGiftShouldBeFound("note.equals=" + DEFAULT_NOTE);

        // Get all the voucherGiftList where note equals to UPDATED_NOTE
        defaultVoucherGiftShouldNotBeFound("note.equals=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByNoteIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where note in DEFAULT_NOTE or UPDATED_NOTE
        defaultVoucherGiftShouldBeFound("note.in=" + DEFAULT_NOTE + "," + UPDATED_NOTE);

        // Get all the voucherGiftList where note equals to UPDATED_NOTE
        defaultVoucherGiftShouldNotBeFound("note.in=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByNoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where note is not null
        defaultVoucherGiftShouldBeFound("note.specified=true");

        // Get all the voucherGiftList where note is null
        defaultVoucherGiftShouldNotBeFound("note.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where createdBy equals to DEFAULT_CREATED_BY
        defaultVoucherGiftShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the voucherGiftList where createdBy equals to UPDATED_CREATED_BY
        defaultVoucherGiftShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultVoucherGiftShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the voucherGiftList where createdBy equals to UPDATED_CREATED_BY
        defaultVoucherGiftShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where createdBy is not null
        defaultVoucherGiftShouldBeFound("createdBy.specified=true");

        // Get all the voucherGiftList where createdBy is null
        defaultVoucherGiftShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByExpiredDurationIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where expiredDuration equals to DEFAULT_EXPIRED_DURATION
        defaultVoucherGiftShouldBeFound("expiredDuration.equals=" + DEFAULT_EXPIRED_DURATION);

        // Get all the voucherGiftList where expiredDuration equals to UPDATED_EXPIRED_DURATION
        defaultVoucherGiftShouldNotBeFound("expiredDuration.equals=" + UPDATED_EXPIRED_DURATION);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByExpiredDurationIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where expiredDuration in DEFAULT_EXPIRED_DURATION or UPDATED_EXPIRED_DURATION
        defaultVoucherGiftShouldBeFound("expiredDuration.in=" + DEFAULT_EXPIRED_DURATION + "," + UPDATED_EXPIRED_DURATION);

        // Get all the voucherGiftList where expiredDuration equals to UPDATED_EXPIRED_DURATION
        defaultVoucherGiftShouldNotBeFound("expiredDuration.in=" + UPDATED_EXPIRED_DURATION);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByExpiredDurationIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where expiredDuration is not null
        defaultVoucherGiftShouldBeFound("expiredDuration.specified=true");

        // Get all the voucherGiftList where expiredDuration is null
        defaultVoucherGiftShouldNotBeFound("expiredDuration.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByExpiredDurationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where expiredDuration greater than or equals to DEFAULT_EXPIRED_DURATION
        defaultVoucherGiftShouldBeFound("expiredDuration.greaterOrEqualThan=" + DEFAULT_EXPIRED_DURATION);

        // Get all the voucherGiftList where expiredDuration greater than or equals to UPDATED_EXPIRED_DURATION
        defaultVoucherGiftShouldNotBeFound("expiredDuration.greaterOrEqualThan=" + UPDATED_EXPIRED_DURATION);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByExpiredDurationIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where expiredDuration less than or equals to DEFAULT_EXPIRED_DURATION
        defaultVoucherGiftShouldNotBeFound("expiredDuration.lessThan=" + DEFAULT_EXPIRED_DURATION);

        // Get all the voucherGiftList where expiredDuration less than or equals to UPDATED_EXPIRED_DURATION
        defaultVoucherGiftShouldBeFound("expiredDuration.lessThan=" + UPDATED_EXPIRED_DURATION);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByEffectedDayIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where effectedDay equals to DEFAULT_EFFECTED_DAY
        defaultVoucherGiftShouldBeFound("effectedDay.equals=" + DEFAULT_EFFECTED_DAY);

        // Get all the voucherGiftList where effectedDay equals to UPDATED_EFFECTED_DAY
        defaultVoucherGiftShouldNotBeFound("effectedDay.equals=" + UPDATED_EFFECTED_DAY);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByEffectedDayIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where effectedDay in DEFAULT_EFFECTED_DAY or UPDATED_EFFECTED_DAY
        defaultVoucherGiftShouldBeFound("effectedDay.in=" + DEFAULT_EFFECTED_DAY + "," + UPDATED_EFFECTED_DAY);

        // Get all the voucherGiftList where effectedDay equals to UPDATED_EFFECTED_DAY
        defaultVoucherGiftShouldNotBeFound("effectedDay.in=" + UPDATED_EFFECTED_DAY);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByEffectedDayIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where effectedDay is not null
        defaultVoucherGiftShouldBeFound("effectedDay.specified=true");

        // Get all the voucherGiftList where effectedDay is null
        defaultVoucherGiftShouldNotBeFound("effectedDay.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByEffectedDayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where effectedDay greater than or equals to DEFAULT_EFFECTED_DAY
        defaultVoucherGiftShouldBeFound("effectedDay.greaterOrEqualThan=" + DEFAULT_EFFECTED_DAY);

        // Get all the voucherGiftList where effectedDay greater than or equals to UPDATED_EFFECTED_DAY
        defaultVoucherGiftShouldNotBeFound("effectedDay.greaterOrEqualThan=" + UPDATED_EFFECTED_DAY);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByEffectedDayIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where effectedDay less than or equals to DEFAULT_EFFECTED_DAY
        defaultVoucherGiftShouldNotBeFound("effectedDay.lessThan=" + DEFAULT_EFFECTED_DAY);

        // Get all the voucherGiftList where effectedDay less than or equals to UPDATED_EFFECTED_DAY
        defaultVoucherGiftShouldBeFound("effectedDay.lessThan=" + UPDATED_EFFECTED_DAY);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherValueIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherValue equals to DEFAULT_VOUCHER_VALUE
        defaultVoucherGiftShouldBeFound("voucherValue.equals=" + DEFAULT_VOUCHER_VALUE);

        // Get all the voucherGiftList where voucherValue equals to UPDATED_VOUCHER_VALUE
        defaultVoucherGiftShouldNotBeFound("voucherValue.equals=" + UPDATED_VOUCHER_VALUE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherValueIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherValue in DEFAULT_VOUCHER_VALUE or UPDATED_VOUCHER_VALUE
        defaultVoucherGiftShouldBeFound("voucherValue.in=" + DEFAULT_VOUCHER_VALUE + "," + UPDATED_VOUCHER_VALUE);

        // Get all the voucherGiftList where voucherValue equals to UPDATED_VOUCHER_VALUE
        defaultVoucherGiftShouldNotBeFound("voucherValue.in=" + UPDATED_VOUCHER_VALUE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherValue is not null
        defaultVoucherGiftShouldBeFound("voucherValue.specified=true");

        // Get all the voucherGiftList where voucherValue is null
        defaultVoucherGiftShouldNotBeFound("voucherValue.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerCode equals to DEFAULT_USED_CUSTOMER_CODE
        defaultVoucherGiftShouldBeFound("usedCustomerCode.equals=" + DEFAULT_USED_CUSTOMER_CODE);

        // Get all the voucherGiftList where usedCustomerCode equals to UPDATED_USED_CUSTOMER_CODE
        defaultVoucherGiftShouldNotBeFound("usedCustomerCode.equals=" + UPDATED_USED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerCodeIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerCode in DEFAULT_USED_CUSTOMER_CODE or UPDATED_USED_CUSTOMER_CODE
        defaultVoucherGiftShouldBeFound("usedCustomerCode.in=" + DEFAULT_USED_CUSTOMER_CODE + "," + UPDATED_USED_CUSTOMER_CODE);

        // Get all the voucherGiftList where usedCustomerCode equals to UPDATED_USED_CUSTOMER_CODE
        defaultVoucherGiftShouldNotBeFound("usedCustomerCode.in=" + UPDATED_USED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerCode is not null
        defaultVoucherGiftShouldBeFound("usedCustomerCode.specified=true");

        // Get all the voucherGiftList where usedCustomerCode is null
        defaultVoucherGiftShouldNotBeFound("usedCustomerCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerNameIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerName equals to DEFAULT_USED_CUSTOMER_NAME
        defaultVoucherGiftShouldBeFound("usedCustomerName.equals=" + DEFAULT_USED_CUSTOMER_NAME);

        // Get all the voucherGiftList where usedCustomerName equals to UPDATED_USED_CUSTOMER_NAME
        defaultVoucherGiftShouldNotBeFound("usedCustomerName.equals=" + UPDATED_USED_CUSTOMER_NAME);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerNameIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerName in DEFAULT_USED_CUSTOMER_NAME or UPDATED_USED_CUSTOMER_NAME
        defaultVoucherGiftShouldBeFound("usedCustomerName.in=" + DEFAULT_USED_CUSTOMER_NAME + "," + UPDATED_USED_CUSTOMER_NAME);

        // Get all the voucherGiftList where usedCustomerName equals to UPDATED_USED_CUSTOMER_NAME
        defaultVoucherGiftShouldNotBeFound("usedCustomerName.in=" + UPDATED_USED_CUSTOMER_NAME);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerName is not null
        defaultVoucherGiftShouldBeFound("usedCustomerName.specified=true");

        // Get all the voucherGiftList where usedCustomerName is null
        defaultVoucherGiftShouldNotBeFound("usedCustomerName.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerPhone equals to DEFAULT_USED_CUSTOMER_PHONE
        defaultVoucherGiftShouldBeFound("usedCustomerPhone.equals=" + DEFAULT_USED_CUSTOMER_PHONE);

        // Get all the voucherGiftList where usedCustomerPhone equals to UPDATED_USED_CUSTOMER_PHONE
        defaultVoucherGiftShouldNotBeFound("usedCustomerPhone.equals=" + UPDATED_USED_CUSTOMER_PHONE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerPhone in DEFAULT_USED_CUSTOMER_PHONE or UPDATED_USED_CUSTOMER_PHONE
        defaultVoucherGiftShouldBeFound("usedCustomerPhone.in=" + DEFAULT_USED_CUSTOMER_PHONE + "," + UPDATED_USED_CUSTOMER_PHONE);

        // Get all the voucherGiftList where usedCustomerPhone equals to UPDATED_USED_CUSTOMER_PHONE
        defaultVoucherGiftShouldNotBeFound("usedCustomerPhone.in=" + UPDATED_USED_CUSTOMER_PHONE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUsedCustomerPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where usedCustomerPhone is not null
        defaultVoucherGiftShouldBeFound("usedCustomerPhone.specified=true");

        // Get all the voucherGiftList where usedCustomerPhone is null
        defaultVoucherGiftShouldNotBeFound("usedCustomerPhone.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUpdateFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where updateFlag equals to DEFAULT_UPDATE_FLAG
        defaultVoucherGiftShouldBeFound("updateFlag.equals=" + DEFAULT_UPDATE_FLAG);

        // Get all the voucherGiftList where updateFlag equals to UPDATED_UPDATE_FLAG
        defaultVoucherGiftShouldNotBeFound("updateFlag.equals=" + UPDATED_UPDATE_FLAG);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUpdateFlagIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where updateFlag in DEFAULT_UPDATE_FLAG or UPDATED_UPDATE_FLAG
        defaultVoucherGiftShouldBeFound("updateFlag.in=" + DEFAULT_UPDATE_FLAG + "," + UPDATED_UPDATE_FLAG);

        // Get all the voucherGiftList where updateFlag equals to UPDATED_UPDATE_FLAG
        defaultVoucherGiftShouldNotBeFound("updateFlag.in=" + UPDATED_UPDATE_FLAG);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUpdateFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where updateFlag is not null
        defaultVoucherGiftShouldBeFound("updateFlag.specified=true");

        // Get all the voucherGiftList where updateFlag is null
        defaultVoucherGiftShouldNotBeFound("updateFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByMaxAppliedAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where maxAppliedAmount equals to DEFAULT_MAX_APPLIED_AMOUNT
        defaultVoucherGiftShouldBeFound("maxAppliedAmount.equals=" + DEFAULT_MAX_APPLIED_AMOUNT);

        // Get all the voucherGiftList where maxAppliedAmount equals to UPDATED_MAX_APPLIED_AMOUNT
        defaultVoucherGiftShouldNotBeFound("maxAppliedAmount.equals=" + UPDATED_MAX_APPLIED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByMaxAppliedAmountIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where maxAppliedAmount in DEFAULT_MAX_APPLIED_AMOUNT or UPDATED_MAX_APPLIED_AMOUNT
        defaultVoucherGiftShouldBeFound("maxAppliedAmount.in=" + DEFAULT_MAX_APPLIED_AMOUNT + "," + UPDATED_MAX_APPLIED_AMOUNT);

        // Get all the voucherGiftList where maxAppliedAmount equals to UPDATED_MAX_APPLIED_AMOUNT
        defaultVoucherGiftShouldNotBeFound("maxAppliedAmount.in=" + UPDATED_MAX_APPLIED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByMaxAppliedAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where maxAppliedAmount is not null
        defaultVoucherGiftShouldBeFound("maxAppliedAmount.specified=true");

        // Get all the voucherGiftList where maxAppliedAmount is null
        defaultVoucherGiftShouldNotBeFound("maxAppliedAmount.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByMax_appliedTimePerCustomerIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where max_appliedTimePerCustomer equals to DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER
        defaultVoucherGiftShouldBeFound("max_appliedTimePerCustomer.equals=" + DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER);

        // Get all the voucherGiftList where max_appliedTimePerCustomer equals to UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER
        defaultVoucherGiftShouldNotBeFound("max_appliedTimePerCustomer.equals=" + UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByMax_appliedTimePerCustomerIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where max_appliedTimePerCustomer in DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER or UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER
        defaultVoucherGiftShouldBeFound("max_appliedTimePerCustomer.in=" + DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER + "," + UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER);

        // Get all the voucherGiftList where max_appliedTimePerCustomer equals to UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER
        defaultVoucherGiftShouldNotBeFound("max_appliedTimePerCustomer.in=" + UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByMax_appliedTimePerCustomerIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where max_appliedTimePerCustomer is not null
        defaultVoucherGiftShouldBeFound("max_appliedTimePerCustomer.specified=true");

        // Get all the voucherGiftList where max_appliedTimePerCustomer is null
        defaultVoucherGiftShouldNotBeFound("max_appliedTimePerCustomer.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByMax_appliedTimePerCustomerIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where max_appliedTimePerCustomer greater than or equals to DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER
        defaultVoucherGiftShouldBeFound("max_appliedTimePerCustomer.greaterOrEqualThan=" + DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER);

        // Get all the voucherGiftList where max_appliedTimePerCustomer greater than or equals to UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER
        defaultVoucherGiftShouldNotBeFound("max_appliedTimePerCustomer.greaterOrEqualThan=" + UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByMax_appliedTimePerCustomerIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where max_appliedTimePerCustomer less than or equals to DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER
        defaultVoucherGiftShouldNotBeFound("max_appliedTimePerCustomer.lessThan=" + DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER);

        // Get all the voucherGiftList where max_appliedTimePerCustomer less than or equals to UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER
        defaultVoucherGiftShouldBeFound("max_appliedTimePerCustomer.lessThan=" + UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherAppliedTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherAppliedType equals to DEFAULT_VOUCHER_APPLIED_TYPE
        defaultVoucherGiftShouldBeFound("voucherAppliedType.equals=" + DEFAULT_VOUCHER_APPLIED_TYPE);

        // Get all the voucherGiftList where voucherAppliedType equals to UPDATED_VOUCHER_APPLIED_TYPE
        defaultVoucherGiftShouldNotBeFound("voucherAppliedType.equals=" + UPDATED_VOUCHER_APPLIED_TYPE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherAppliedTypeIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherAppliedType in DEFAULT_VOUCHER_APPLIED_TYPE or UPDATED_VOUCHER_APPLIED_TYPE
        defaultVoucherGiftShouldBeFound("voucherAppliedType.in=" + DEFAULT_VOUCHER_APPLIED_TYPE + "," + UPDATED_VOUCHER_APPLIED_TYPE);

        // Get all the voucherGiftList where voucherAppliedType equals to UPDATED_VOUCHER_APPLIED_TYPE
        defaultVoucherGiftShouldNotBeFound("voucherAppliedType.in=" + UPDATED_VOUCHER_APPLIED_TYPE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherAppliedTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherAppliedType is not null
        defaultVoucherGiftShouldBeFound("voucherAppliedType.specified=true");

        // Get all the voucherGiftList where voucherAppliedType is null
        defaultVoucherGiftShouldNotBeFound("voucherAppliedType.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherAppliedTypeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherAppliedType greater than or equals to DEFAULT_VOUCHER_APPLIED_TYPE
        defaultVoucherGiftShouldBeFound("voucherAppliedType.greaterOrEqualThan=" + DEFAULT_VOUCHER_APPLIED_TYPE);

        // Get all the voucherGiftList where voucherAppliedType greater than or equals to UPDATED_VOUCHER_APPLIED_TYPE
        defaultVoucherGiftShouldNotBeFound("voucherAppliedType.greaterOrEqualThan=" + UPDATED_VOUCHER_APPLIED_TYPE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherAppliedTypeIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherAppliedType less than or equals to DEFAULT_VOUCHER_APPLIED_TYPE
        defaultVoucherGiftShouldNotBeFound("voucherAppliedType.lessThan=" + DEFAULT_VOUCHER_APPLIED_TYPE);

        // Get all the voucherGiftList where voucherAppliedType less than or equals to UPDATED_VOUCHER_APPLIED_TYPE
        defaultVoucherGiftShouldBeFound("voucherAppliedType.lessThan=" + UPDATED_VOUCHER_APPLIED_TYPE);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultVoucherGiftShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the voucherGiftList where updateDate equals to UPDATED_UPDATE_DATE
        defaultVoucherGiftShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultVoucherGiftShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the voucherGiftList where updateDate equals to UPDATED_UPDATE_DATE
        defaultVoucherGiftShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where updateDate is not null
        defaultVoucherGiftShouldBeFound("updateDate.specified=true");

        // Get all the voucherGiftList where updateDate is null
        defaultVoucherGiftShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUpdateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where updateDate greater than or equals to DEFAULT_UPDATE_DATE
        defaultVoucherGiftShouldBeFound("updateDate.greaterOrEqualThan=" + DEFAULT_UPDATE_DATE);

        // Get all the voucherGiftList where updateDate greater than or equals to UPDATED_UPDATE_DATE
        defaultVoucherGiftShouldNotBeFound("updateDate.greaterOrEqualThan=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByUpdateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where updateDate less than or equals to DEFAULT_UPDATE_DATE
        defaultVoucherGiftShouldNotBeFound("updateDate.lessThan=" + DEFAULT_UPDATE_DATE);

        // Get all the voucherGiftList where updateDate less than or equals to UPDATED_UPDATE_DATE
        defaultVoucherGiftShouldBeFound("updateDate.lessThan=" + UPDATED_UPDATE_DATE);
    }


    @Test
    @Transactional
    public void getAllVoucherGiftsByDeleteFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where deleteFlag equals to DEFAULT_DELETE_FLAG
        defaultVoucherGiftShouldBeFound("deleteFlag.equals=" + DEFAULT_DELETE_FLAG);

        // Get all the voucherGiftList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultVoucherGiftShouldNotBeFound("deleteFlag.equals=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByDeleteFlagIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where deleteFlag in DEFAULT_DELETE_FLAG or UPDATED_DELETE_FLAG
        defaultVoucherGiftShouldBeFound("deleteFlag.in=" + DEFAULT_DELETE_FLAG + "," + UPDATED_DELETE_FLAG);

        // Get all the voucherGiftList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultVoucherGiftShouldNotBeFound("deleteFlag.in=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByDeleteFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where deleteFlag is not null
        defaultVoucherGiftShouldBeFound("deleteFlag.specified=true");

        // Get all the voucherGiftList where deleteFlag is null
        defaultVoucherGiftShouldNotBeFound("deleteFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByPersonalIdIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where personalId equals to DEFAULT_PERSONAL_ID
        defaultVoucherGiftShouldBeFound("personalId.equals=" + DEFAULT_PERSONAL_ID);

        // Get all the voucherGiftList where personalId equals to UPDATED_PERSONAL_ID
        defaultVoucherGiftShouldNotBeFound("personalId.equals=" + UPDATED_PERSONAL_ID);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByPersonalIdIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where personalId in DEFAULT_PERSONAL_ID or UPDATED_PERSONAL_ID
        defaultVoucherGiftShouldBeFound("personalId.in=" + DEFAULT_PERSONAL_ID + "," + UPDATED_PERSONAL_ID);

        // Get all the voucherGiftList where personalId equals to UPDATED_PERSONAL_ID
        defaultVoucherGiftShouldNotBeFound("personalId.in=" + UPDATED_PERSONAL_ID);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByPersonalIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where personalId is not null
        defaultVoucherGiftShouldBeFound("personalId.specified=true");

        // Get all the voucherGiftList where personalId is null
        defaultVoucherGiftShouldNotBeFound("personalId.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftIdIsEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftId equals to DEFAULT_VOUCHER_GIFT_ID
        defaultVoucherGiftShouldBeFound("voucherGiftId.equals=" + DEFAULT_VOUCHER_GIFT_ID);

        // Get all the voucherGiftList where voucherGiftId equals to UPDATED_VOUCHER_GIFT_ID
        defaultVoucherGiftShouldNotBeFound("voucherGiftId.equals=" + UPDATED_VOUCHER_GIFT_ID);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftIdIsInShouldWork() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftId in DEFAULT_VOUCHER_GIFT_ID or UPDATED_VOUCHER_GIFT_ID
        defaultVoucherGiftShouldBeFound("voucherGiftId.in=" + DEFAULT_VOUCHER_GIFT_ID + "," + UPDATED_VOUCHER_GIFT_ID);

        // Get all the voucherGiftList where voucherGiftId equals to UPDATED_VOUCHER_GIFT_ID
        defaultVoucherGiftShouldNotBeFound("voucherGiftId.in=" + UPDATED_VOUCHER_GIFT_ID);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftId is not null
        defaultVoucherGiftShouldBeFound("voucherGiftId.specified=true");

        // Get all the voucherGiftList where voucherGiftId is null
        defaultVoucherGiftShouldNotBeFound("voucherGiftId.specified=false");
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftId greater than or equals to DEFAULT_VOUCHER_GIFT_ID
        defaultVoucherGiftShouldBeFound("voucherGiftId.greaterOrEqualThan=" + DEFAULT_VOUCHER_GIFT_ID);

        // Get all the voucherGiftList where voucherGiftId greater than or equals to UPDATED_VOUCHER_GIFT_ID
        defaultVoucherGiftShouldNotBeFound("voucherGiftId.greaterOrEqualThan=" + UPDATED_VOUCHER_GIFT_ID);
    }

    @Test
    @Transactional
    public void getAllVoucherGiftsByVoucherGiftIdIsLessThanSomething() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        // Get all the voucherGiftList where voucherGiftId less than or equals to DEFAULT_VOUCHER_GIFT_ID
        defaultVoucherGiftShouldNotBeFound("voucherGiftId.lessThan=" + DEFAULT_VOUCHER_GIFT_ID);

        // Get all the voucherGiftList where voucherGiftId less than or equals to UPDATED_VOUCHER_GIFT_ID
        defaultVoucherGiftShouldBeFound("voucherGiftId.lessThan=" + UPDATED_VOUCHER_GIFT_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultVoucherGiftShouldBeFound(String filter) throws Exception {
        restVoucherGiftMockMvc.perform(get("/api/voucher-gifts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(voucherGift.getId().intValue())))
            .andExpect(jsonPath("$.[*].voucherGiftCode").value(hasItem(DEFAULT_VOUCHER_GIFT_CODE.toString())))
            .andExpect(jsonPath("$.[*].voucherGiftName").value(hasItem(DEFAULT_VOUCHER_GIFT_NAME.toString())))
            .andExpect(jsonPath("$.[*].voucherCategory").value(hasItem(DEFAULT_VOUCHER_CATEGORY)))
            .andExpect(jsonPath("$.[*].voucherType").value(hasItem(DEFAULT_VOUCHER_TYPE)))
            .andExpect(jsonPath("$.[*].customerCode").value(hasItem(DEFAULT_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].customerName").value(hasItem(DEFAULT_CUSTOMER_NAME.toString())))
            .andExpect(jsonPath("$.[*].customerPhone").value(hasItem(DEFAULT_CUSTOMER_PHONE.toString())))
            .andExpect(jsonPath("$.[*].totalValue").value(hasItem(DEFAULT_TOTAL_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].availableValue").value(hasItem(DEFAULT_AVAILABLE_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].validFrom").value(hasItem(sameInstant(DEFAULT_VALID_FROM))))
            .andExpect(jsonPath("$.[*].validTo").value(hasItem(sameInstant(DEFAULT_VALID_TO))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].refSku").value(hasItem(DEFAULT_REF_SKU.toString())))
            .andExpect(jsonPath("$.[*].storeCode").value(hasItem(DEFAULT_STORE_CODE.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].expiredDuration").value(hasItem(DEFAULT_EXPIRED_DURATION)))
            .andExpect(jsonPath("$.[*].effectedDay").value(hasItem(DEFAULT_EFFECTED_DAY)))
            .andExpect(jsonPath("$.[*].voucherValue").value(hasItem(DEFAULT_VOUCHER_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].usedCustomerCode").value(hasItem(DEFAULT_USED_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].usedCustomerName").value(hasItem(DEFAULT_USED_CUSTOMER_NAME.toString())))
            .andExpect(jsonPath("$.[*].usedCustomerPhone").value(hasItem(DEFAULT_USED_CUSTOMER_PHONE.toString())))
            .andExpect(jsonPath("$.[*].updateFlag").value(hasItem(DEFAULT_UPDATE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].maxAppliedAmount").value(hasItem(DEFAULT_MAX_APPLIED_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].max_appliedTimePerCustomer").value(hasItem(DEFAULT_MAX_APPLIED_TIME_PER_CUSTOMER)))
            .andExpect(jsonPath("$.[*].voucherAppliedType").value(hasItem(DEFAULT_VOUCHER_APPLIED_TYPE)))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].personalId").value(hasItem(DEFAULT_PERSONAL_ID.toString())))
            .andExpect(jsonPath("$.[*].voucherGiftId").value(hasItem(DEFAULT_VOUCHER_GIFT_ID)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultVoucherGiftShouldNotBeFound(String filter) throws Exception {
        restVoucherGiftMockMvc.perform(get("/api/voucher-gifts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingVoucherGift() throws Exception {
        // Get the voucherGift
        restVoucherGiftMockMvc.perform(get("/api/voucher-gifts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVoucherGift() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        int databaseSizeBeforeUpdate = voucherGiftRepository.findAll().size();

        // Update the voucherGift
        VoucherGift updatedVoucherGift = voucherGiftRepository.findById(voucherGift.getId()).get();
        // Disconnect from session so that the updates on updatedVoucherGift are not directly saved in db
        em.detach(updatedVoucherGift);
        updatedVoucherGift
            .voucherGiftCode(UPDATED_VOUCHER_GIFT_CODE)
            .voucherGiftName(UPDATED_VOUCHER_GIFT_NAME)
            .voucherCategory(UPDATED_VOUCHER_CATEGORY)
            .voucherType(UPDATED_VOUCHER_TYPE)
            .customerCode(UPDATED_CUSTOMER_CODE)
            .customerName(UPDATED_CUSTOMER_NAME)
            .customerPhone(UPDATED_CUSTOMER_PHONE)
            .totalValue(UPDATED_TOTAL_VALUE)
            .availableValue(UPDATED_AVAILABLE_VALUE)
            .validFrom(UPDATED_VALID_FROM)
            .validTo(UPDATED_VALID_TO)
            .status(UPDATED_STATUS)
            .refSku(UPDATED_REF_SKU)
            .storeCode(UPDATED_STORE_CODE)
            .note(UPDATED_NOTE)
            .createdBy(UPDATED_CREATED_BY)
            .expiredDuration(UPDATED_EXPIRED_DURATION)
            .effectedDay(UPDATED_EFFECTED_DAY)
            .voucherValue(UPDATED_VOUCHER_VALUE)
            .usedCustomerCode(UPDATED_USED_CUSTOMER_CODE)
            .usedCustomerName(UPDATED_USED_CUSTOMER_NAME)
            .usedCustomerPhone(UPDATED_USED_CUSTOMER_PHONE)
            .updateFlag(UPDATED_UPDATE_FLAG)
            .maxAppliedAmount(UPDATED_MAX_APPLIED_AMOUNT)
            .max_appliedTimePerCustomer(UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER)
            .voucherAppliedType(UPDATED_VOUCHER_APPLIED_TYPE)
            .updateDate(UPDATED_UPDATE_DATE)
            .deleteFlag(UPDATED_DELETE_FLAG)
            .personalId(UPDATED_PERSONAL_ID);
        VoucherGiftDTO voucherGiftDTO = voucherGiftMapper.toDto(updatedVoucherGift);

        restVoucherGiftMockMvc.perform(put("/api/voucher-gifts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(voucherGiftDTO)))
            .andExpect(status().isOk());

        // Validate the VoucherGift in the database
        List<VoucherGift> voucherGiftList = voucherGiftRepository.findAll();
        assertThat(voucherGiftList).hasSize(databaseSizeBeforeUpdate);
        VoucherGift testVoucherGift = voucherGiftList.get(voucherGiftList.size() - 1);
        assertThat(testVoucherGift.getVoucherGiftCode()).isEqualTo(UPDATED_VOUCHER_GIFT_CODE);
        assertThat(testVoucherGift.getVoucherGiftName()).isEqualTo(UPDATED_VOUCHER_GIFT_NAME);
        assertThat(testVoucherGift.getVoucherCategory()).isEqualTo(UPDATED_VOUCHER_CATEGORY);
        assertThat(testVoucherGift.getVoucherType()).isEqualTo(UPDATED_VOUCHER_TYPE);
        assertThat(testVoucherGift.getCustomerCode()).isEqualTo(UPDATED_CUSTOMER_CODE);
        assertThat(testVoucherGift.getCustomerName()).isEqualTo(UPDATED_CUSTOMER_NAME);
        assertThat(testVoucherGift.getCustomerPhone()).isEqualTo(UPDATED_CUSTOMER_PHONE);
        assertThat(testVoucherGift.getTotalValue()).isEqualTo(UPDATED_TOTAL_VALUE);
        assertThat(testVoucherGift.getAvailableValue()).isEqualTo(UPDATED_AVAILABLE_VALUE);
        assertThat(testVoucherGift.getValidFrom()).isEqualTo(UPDATED_VALID_FROM);
        assertThat(testVoucherGift.getValidTo()).isEqualTo(UPDATED_VALID_TO);
        assertThat(testVoucherGift.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testVoucherGift.getRefSku()).isEqualTo(UPDATED_REF_SKU);
        assertThat(testVoucherGift.getStoreCode()).isEqualTo(UPDATED_STORE_CODE);
        assertThat(testVoucherGift.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testVoucherGift.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testVoucherGift.getExpiredDuration()).isEqualTo(UPDATED_EXPIRED_DURATION);
        assertThat(testVoucherGift.getEffectedDay()).isEqualTo(UPDATED_EFFECTED_DAY);
        assertThat(testVoucherGift.getVoucherValue()).isEqualTo(UPDATED_VOUCHER_VALUE);
        assertThat(testVoucherGift.getUsedCustomerCode()).isEqualTo(UPDATED_USED_CUSTOMER_CODE);
        assertThat(testVoucherGift.getUsedCustomerName()).isEqualTo(UPDATED_USED_CUSTOMER_NAME);
        assertThat(testVoucherGift.getUsedCustomerPhone()).isEqualTo(UPDATED_USED_CUSTOMER_PHONE);
        assertThat(testVoucherGift.isUpdateFlag()).isEqualTo(UPDATED_UPDATE_FLAG);
        assertThat(testVoucherGift.getMaxAppliedAmount()).isEqualTo(UPDATED_MAX_APPLIED_AMOUNT);
        assertThat(testVoucherGift.getMax_appliedTimePerCustomer()).isEqualTo(UPDATED_MAX_APPLIED_TIME_PER_CUSTOMER);
        assertThat(testVoucherGift.getVoucherAppliedType()).isEqualTo(UPDATED_VOUCHER_APPLIED_TYPE);
        assertThat(testVoucherGift.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testVoucherGift.isDeleteFlag()).isEqualTo(UPDATED_DELETE_FLAG);
        assertThat(testVoucherGift.getPersonalId()).isEqualTo(UPDATED_PERSONAL_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingVoucherGift() throws Exception {
        int databaseSizeBeforeUpdate = voucherGiftRepository.findAll().size();

        // Create the VoucherGift
        VoucherGiftDTO voucherGiftDTO = voucherGiftMapper.toDto(voucherGift);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVoucherGiftMockMvc.perform(put("/api/voucher-gifts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(voucherGiftDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VoucherGift in the database
        List<VoucherGift> voucherGiftList = voucherGiftRepository.findAll();
        assertThat(voucherGiftList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVoucherGift() throws Exception {
        // Initialize the database
        voucherGiftRepository.saveAndFlush(voucherGift);

        int databaseSizeBeforeDelete = voucherGiftRepository.findAll().size();

        // Get the voucherGift
        restVoucherGiftMockMvc.perform(delete("/api/voucher-gifts/{id}", voucherGift.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<VoucherGift> voucherGiftList = voucherGiftRepository.findAll();
        assertThat(voucherGiftList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VoucherGift.class);
        VoucherGift voucherGift1 = new VoucherGift();
        voucherGift1.setId(1L);
        VoucherGift voucherGift2 = new VoucherGift();
        voucherGift2.setId(voucherGift1.getId());
        assertThat(voucherGift1).isEqualTo(voucherGift2);
        voucherGift2.setId(2L);
        assertThat(voucherGift1).isNotEqualTo(voucherGift2);
        voucherGift1.setId(null);
        assertThat(voucherGift1).isNotEqualTo(voucherGift2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VoucherGiftDTO.class);
        VoucherGiftDTO voucherGiftDTO1 = new VoucherGiftDTO();
        voucherGiftDTO1.setId(1L);
        VoucherGiftDTO voucherGiftDTO2 = new VoucherGiftDTO();
        assertThat(voucherGiftDTO1).isNotEqualTo(voucherGiftDTO2);
        voucherGiftDTO2.setId(voucherGiftDTO1.getId());
        assertThat(voucherGiftDTO1).isEqualTo(voucherGiftDTO2);
        voucherGiftDTO2.setId(2L);
        assertThat(voucherGiftDTO1).isNotEqualTo(voucherGiftDTO2);
        voucherGiftDTO1.setId(null);
        assertThat(voucherGiftDTO1).isNotEqualTo(voucherGiftDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(voucherGiftMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(voucherGiftMapper.fromId(null)).isNull();
    }
}
