package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.MembershipHistoty;
import com.gcs.flexba.rmsweb.domain.Customer;
import com.gcs.flexba.rmsweb.repository.MembershipHistotyRepository;
import com.gcs.flexba.rmsweb.service.MembershipHistotyService;
import com.gcs.flexba.rmsweb.service.dto.MembershipHistotyDTO;
import com.gcs.flexba.rmsweb.service.mapper.MembershipHistotyMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.MembershipHistotyCriteria;
import com.gcs.flexba.rmsweb.service.MembershipHistotyQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MembershipHistotyResource REST controller.
 *
 * @see MembershipHistotyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class MembershipHistotyResourceIntTest {

    private static final String DEFAULT_CUSTOMER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MEMBERSHIP_LEVEL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_MEMBERSHIP_LEVEL_CODE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_EFFECTED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EFFECTED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_EXPIRED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EXPIRED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final Double DEFAULT_ACCUMULATION_POINTS = 1D;
    private static final Double UPDATED_ACCUMULATION_POINTS = 2D;

    private static final Double DEFAULT_PENDING_POINTS = 1D;
    private static final Double UPDATED_PENDING_POINTS = 2D;

    private static final Double DEFAULT_ACTIVATED_POINTS = 1D;
    private static final Double UPDATED_ACTIVATED_POINTS = 2D;

    private static final Double DEFAULT_EXPIRED_POINTS = 1D;
    private static final Double UPDATED_EXPIRED_POINTS = 2D;

    private static final Double DEFAULT_REDEEM_POINTS = 1D;
    private static final Double UPDATED_REDEEM_POINTS = 2D;

    private static final Double DEFAULT_REMAIN_POINTS = 1D;
    private static final Double UPDATED_REMAIN_POINTS = 2D;

    private static final ZonedDateTime DEFAULT_UPDATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Double DEFAULT_TRANSFERABLE_POINTS = 1D;
    private static final Double UPDATED_TRANSFERABLE_POINTS = 2D;

    private static final Integer DEFAULT_ATTEMPT = 1;
    private static final Integer UPDATED_ATTEMPT = 2;

    private static final Integer DEFAULT_DELETE_FLAG = 1;
    private static final Integer UPDATED_DELETE_FLAG = 2;

    @Autowired
    private MembershipHistotyRepository membershipHistotyRepository;


    @Autowired
    private MembershipHistotyMapper membershipHistotyMapper;
    

    @Autowired
    private MembershipHistotyService membershipHistotyService;

    @Autowired
    private MembershipHistotyQueryService membershipHistotyQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMembershipHistotyMockMvc;

    private MembershipHistoty membershipHistoty;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MembershipHistotyResource membershipHistotyResource = new MembershipHistotyResource(membershipHistotyService, membershipHistotyQueryService);
        this.restMembershipHistotyMockMvc = MockMvcBuilders.standaloneSetup(membershipHistotyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MembershipHistoty createEntity(EntityManager em) {
        MembershipHistoty membershipHistoty = new MembershipHistoty()
            .membershipLevelCode(DEFAULT_MEMBERSHIP_LEVEL_CODE)
            .effectedDate(DEFAULT_EFFECTED_DATE)
            .startDate(DEFAULT_START_DATE)
            .expiredDate(DEFAULT_EXPIRED_DATE)
            .status(DEFAULT_STATUS)
            .accumulationPoints(DEFAULT_ACCUMULATION_POINTS)
            .pendingPoints(DEFAULT_PENDING_POINTS)
            .activatedPoints(DEFAULT_ACTIVATED_POINTS)
            .expiredPoints(DEFAULT_EXPIRED_POINTS)
            .redeemPoints(DEFAULT_REDEEM_POINTS)
            .remainPoints(DEFAULT_REMAIN_POINTS)
            .updateDate(DEFAULT_UPDATE_DATE)
            .transferablePoints(DEFAULT_TRANSFERABLE_POINTS)
            .deleteFlag(DEFAULT_DELETE_FLAG);
        return membershipHistoty;
    }

    @Before
    public void initTest() {
        membershipHistoty = createEntity(em);
    }

    @Test
    @Transactional
    public void createMembershipHistoty() throws Exception {
        int databaseSizeBeforeCreate = membershipHistotyRepository.findAll().size();

        // Create the MembershipHistoty
        MembershipHistotyDTO membershipHistotyDTO = membershipHistotyMapper.toDto(membershipHistoty);
        restMembershipHistotyMockMvc.perform(post("/api/membership-histoties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipHistotyDTO)))
            .andExpect(status().isCreated());

        // Validate the MembershipHistoty in the database
        List<MembershipHistoty> membershipHistotyList = membershipHistotyRepository.findAll();
        assertThat(membershipHistotyList).hasSize(databaseSizeBeforeCreate + 1);
        MembershipHistoty testMembershipHistoty = membershipHistotyList.get(membershipHistotyList.size() - 1);
        assertThat(testMembershipHistoty.getMembershipLevelCode()).isEqualTo(DEFAULT_MEMBERSHIP_LEVEL_CODE);
        assertThat(testMembershipHistoty.getEffectedDate()).isEqualTo(DEFAULT_EFFECTED_DATE);
        assertThat(testMembershipHistoty.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testMembershipHistoty.getExpiredDate()).isEqualTo(DEFAULT_EXPIRED_DATE);
        assertThat(testMembershipHistoty.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testMembershipHistoty.getAccumulationPoints()).isEqualTo(DEFAULT_ACCUMULATION_POINTS);
        assertThat(testMembershipHistoty.getPendingPoints()).isEqualTo(DEFAULT_PENDING_POINTS);
        assertThat(testMembershipHistoty.getActivatedPoints()).isEqualTo(DEFAULT_ACTIVATED_POINTS);
        assertThat(testMembershipHistoty.getExpiredPoints()).isEqualTo(DEFAULT_EXPIRED_POINTS);
        assertThat(testMembershipHistoty.getRedeemPoints()).isEqualTo(DEFAULT_REDEEM_POINTS);
        assertThat(testMembershipHistoty.getRemainPoints()).isEqualTo(DEFAULT_REMAIN_POINTS);
        assertThat(testMembershipHistoty.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testMembershipHistoty.getTransferablePoints()).isEqualTo(DEFAULT_TRANSFERABLE_POINTS);
        assertThat(testMembershipHistoty.getDeleteFlag()).isEqualTo(DEFAULT_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void createMembershipHistotyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = membershipHistotyRepository.findAll().size();

        // Create the MembershipHistoty with an existing ID
        membershipHistoty.setId(1L);
        MembershipHistotyDTO membershipHistotyDTO = membershipHistotyMapper.toDto(membershipHistoty);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMembershipHistotyMockMvc.perform(post("/api/membership-histoties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipHistotyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MembershipHistoty in the database
        List<MembershipHistoty> membershipHistotyList = membershipHistotyRepository.findAll();
        assertThat(membershipHistotyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllMembershipHistoties() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList
        restMembershipHistotyMockMvc.perform(get("/api/membership-histoties?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(membershipHistoty.getId().intValue())))
            .andExpect(jsonPath("$.[*].customerCode").value(hasItem(DEFAULT_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].membershipLevelCode").value(hasItem(DEFAULT_MEMBERSHIP_LEVEL_CODE.toString())))
            .andExpect(jsonPath("$.[*].effectedDate").value(hasItem(sameInstant(DEFAULT_EFFECTED_DATE))))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(sameInstant(DEFAULT_START_DATE))))
            .andExpect(jsonPath("$.[*].expiredDate").value(hasItem(sameInstant(DEFAULT_EXPIRED_DATE))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].accumulationPoints").value(hasItem(DEFAULT_ACCUMULATION_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].pendingPoints").value(hasItem(DEFAULT_PENDING_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].activatedPoints").value(hasItem(DEFAULT_ACTIVATED_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].expiredPoints").value(hasItem(DEFAULT_EXPIRED_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].redeemPoints").value(hasItem(DEFAULT_REDEEM_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].remainPoints").value(hasItem(DEFAULT_REMAIN_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].transferablePoints").value(hasItem(DEFAULT_TRANSFERABLE_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].attempt").value(hasItem(DEFAULT_ATTEMPT)))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG)));
    }
    

    @Test
    @Transactional
    public void getMembershipHistoty() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get the membershipHistoty
        restMembershipHistotyMockMvc.perform(get("/api/membership-histoties/{id}", membershipHistoty.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(membershipHistoty.getId().intValue()))
            .andExpect(jsonPath("$.customerCode").value(DEFAULT_CUSTOMER_CODE.toString()))
            .andExpect(jsonPath("$.membershipLevelCode").value(DEFAULT_MEMBERSHIP_LEVEL_CODE.toString()))
            .andExpect(jsonPath("$.effectedDate").value(sameInstant(DEFAULT_EFFECTED_DATE)))
            .andExpect(jsonPath("$.startDate").value(sameInstant(DEFAULT_START_DATE)))
            .andExpect(jsonPath("$.expiredDate").value(sameInstant(DEFAULT_EXPIRED_DATE)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.accumulationPoints").value(DEFAULT_ACCUMULATION_POINTS.doubleValue()))
            .andExpect(jsonPath("$.pendingPoints").value(DEFAULT_PENDING_POINTS.doubleValue()))
            .andExpect(jsonPath("$.activatedPoints").value(DEFAULT_ACTIVATED_POINTS.doubleValue()))
            .andExpect(jsonPath("$.expiredPoints").value(DEFAULT_EXPIRED_POINTS.doubleValue()))
            .andExpect(jsonPath("$.redeemPoints").value(DEFAULT_REDEEM_POINTS.doubleValue()))
            .andExpect(jsonPath("$.remainPoints").value(DEFAULT_REMAIN_POINTS.doubleValue()))
            .andExpect(jsonPath("$.updateDate").value(sameInstant(DEFAULT_UPDATE_DATE)))
            .andExpect(jsonPath("$.transferablePoints").value(DEFAULT_TRANSFERABLE_POINTS.doubleValue()))
            .andExpect(jsonPath("$.attempt").value(DEFAULT_ATTEMPT))
            .andExpect(jsonPath("$.deleteFlag").value(DEFAULT_DELETE_FLAG));
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByCustomerCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where customerCode equals to DEFAULT_CUSTOMER_CODE
        defaultMembershipHistotyShouldBeFound("customerCode.equals=" + DEFAULT_CUSTOMER_CODE);

        // Get all the membershipHistotyList where customerCode equals to UPDATED_CUSTOMER_CODE
        defaultMembershipHistotyShouldNotBeFound("customerCode.equals=" + UPDATED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByCustomerCodeIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where customerCode in DEFAULT_CUSTOMER_CODE or UPDATED_CUSTOMER_CODE
        defaultMembershipHistotyShouldBeFound("customerCode.in=" + DEFAULT_CUSTOMER_CODE + "," + UPDATED_CUSTOMER_CODE);

        // Get all the membershipHistotyList where customerCode equals to UPDATED_CUSTOMER_CODE
        defaultMembershipHistotyShouldNotBeFound("customerCode.in=" + UPDATED_CUSTOMER_CODE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByCustomerCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where customerCode is not null
        defaultMembershipHistotyShouldBeFound("customerCode.specified=true");

        // Get all the membershipHistotyList where customerCode is null
        defaultMembershipHistotyShouldNotBeFound("customerCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByMembershipLevelCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where membershipLevelCode equals to DEFAULT_MEMBERSHIP_LEVEL_CODE
        defaultMembershipHistotyShouldBeFound("membershipLevelCode.equals=" + DEFAULT_MEMBERSHIP_LEVEL_CODE);

        // Get all the membershipHistotyList where membershipLevelCode equals to UPDATED_MEMBERSHIP_LEVEL_CODE
        defaultMembershipHistotyShouldNotBeFound("membershipLevelCode.equals=" + UPDATED_MEMBERSHIP_LEVEL_CODE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByMembershipLevelCodeIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where membershipLevelCode in DEFAULT_MEMBERSHIP_LEVEL_CODE or UPDATED_MEMBERSHIP_LEVEL_CODE
        defaultMembershipHistotyShouldBeFound("membershipLevelCode.in=" + DEFAULT_MEMBERSHIP_LEVEL_CODE + "," + UPDATED_MEMBERSHIP_LEVEL_CODE);

        // Get all the membershipHistotyList where membershipLevelCode equals to UPDATED_MEMBERSHIP_LEVEL_CODE
        defaultMembershipHistotyShouldNotBeFound("membershipLevelCode.in=" + UPDATED_MEMBERSHIP_LEVEL_CODE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByMembershipLevelCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where membershipLevelCode is not null
        defaultMembershipHistotyShouldBeFound("membershipLevelCode.specified=true");

        // Get all the membershipHistotyList where membershipLevelCode is null
        defaultMembershipHistotyShouldNotBeFound("membershipLevelCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByEffectedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where effectedDate equals to DEFAULT_EFFECTED_DATE
        defaultMembershipHistotyShouldBeFound("effectedDate.equals=" + DEFAULT_EFFECTED_DATE);

        // Get all the membershipHistotyList where effectedDate equals to UPDATED_EFFECTED_DATE
        defaultMembershipHistotyShouldNotBeFound("effectedDate.equals=" + UPDATED_EFFECTED_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByEffectedDateIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where effectedDate in DEFAULT_EFFECTED_DATE or UPDATED_EFFECTED_DATE
        defaultMembershipHistotyShouldBeFound("effectedDate.in=" + DEFAULT_EFFECTED_DATE + "," + UPDATED_EFFECTED_DATE);

        // Get all the membershipHistotyList where effectedDate equals to UPDATED_EFFECTED_DATE
        defaultMembershipHistotyShouldNotBeFound("effectedDate.in=" + UPDATED_EFFECTED_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByEffectedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where effectedDate is not null
        defaultMembershipHistotyShouldBeFound("effectedDate.specified=true");

        // Get all the membershipHistotyList where effectedDate is null
        defaultMembershipHistotyShouldNotBeFound("effectedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByEffectedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where effectedDate greater than or equals to DEFAULT_EFFECTED_DATE
        defaultMembershipHistotyShouldBeFound("effectedDate.greaterOrEqualThan=" + DEFAULT_EFFECTED_DATE);

        // Get all the membershipHistotyList where effectedDate greater than or equals to UPDATED_EFFECTED_DATE
        defaultMembershipHistotyShouldNotBeFound("effectedDate.greaterOrEqualThan=" + UPDATED_EFFECTED_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByEffectedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where effectedDate less than or equals to DEFAULT_EFFECTED_DATE
        defaultMembershipHistotyShouldNotBeFound("effectedDate.lessThan=" + DEFAULT_EFFECTED_DATE);

        // Get all the membershipHistotyList where effectedDate less than or equals to UPDATED_EFFECTED_DATE
        defaultMembershipHistotyShouldBeFound("effectedDate.lessThan=" + UPDATED_EFFECTED_DATE);
    }


    @Test
    @Transactional
    public void getAllMembershipHistotiesByStartDateIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where startDate equals to DEFAULT_START_DATE
        defaultMembershipHistotyShouldBeFound("startDate.equals=" + DEFAULT_START_DATE);

        // Get all the membershipHistotyList where startDate equals to UPDATED_START_DATE
        defaultMembershipHistotyShouldNotBeFound("startDate.equals=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByStartDateIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where startDate in DEFAULT_START_DATE or UPDATED_START_DATE
        defaultMembershipHistotyShouldBeFound("startDate.in=" + DEFAULT_START_DATE + "," + UPDATED_START_DATE);

        // Get all the membershipHistotyList where startDate equals to UPDATED_START_DATE
        defaultMembershipHistotyShouldNotBeFound("startDate.in=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByStartDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where startDate is not null
        defaultMembershipHistotyShouldBeFound("startDate.specified=true");

        // Get all the membershipHistotyList where startDate is null
        defaultMembershipHistotyShouldNotBeFound("startDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByStartDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where startDate greater than or equals to DEFAULT_START_DATE
        defaultMembershipHistotyShouldBeFound("startDate.greaterOrEqualThan=" + DEFAULT_START_DATE);

        // Get all the membershipHistotyList where startDate greater than or equals to UPDATED_START_DATE
        defaultMembershipHistotyShouldNotBeFound("startDate.greaterOrEqualThan=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByStartDateIsLessThanSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where startDate less than or equals to DEFAULT_START_DATE
        defaultMembershipHistotyShouldNotBeFound("startDate.lessThan=" + DEFAULT_START_DATE);

        // Get all the membershipHistotyList where startDate less than or equals to UPDATED_START_DATE
        defaultMembershipHistotyShouldBeFound("startDate.lessThan=" + UPDATED_START_DATE);
    }


    @Test
    @Transactional
    public void getAllMembershipHistotiesByExpiredDateIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where expiredDate equals to DEFAULT_EXPIRED_DATE
        defaultMembershipHistotyShouldBeFound("expiredDate.equals=" + DEFAULT_EXPIRED_DATE);

        // Get all the membershipHistotyList where expiredDate equals to UPDATED_EXPIRED_DATE
        defaultMembershipHistotyShouldNotBeFound("expiredDate.equals=" + UPDATED_EXPIRED_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByExpiredDateIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where expiredDate in DEFAULT_EXPIRED_DATE or UPDATED_EXPIRED_DATE
        defaultMembershipHistotyShouldBeFound("expiredDate.in=" + DEFAULT_EXPIRED_DATE + "," + UPDATED_EXPIRED_DATE);

        // Get all the membershipHistotyList where expiredDate equals to UPDATED_EXPIRED_DATE
        defaultMembershipHistotyShouldNotBeFound("expiredDate.in=" + UPDATED_EXPIRED_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByExpiredDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where expiredDate is not null
        defaultMembershipHistotyShouldBeFound("expiredDate.specified=true");

        // Get all the membershipHistotyList where expiredDate is null
        defaultMembershipHistotyShouldNotBeFound("expiredDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByExpiredDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where expiredDate greater than or equals to DEFAULT_EXPIRED_DATE
        defaultMembershipHistotyShouldBeFound("expiredDate.greaterOrEqualThan=" + DEFAULT_EXPIRED_DATE);

        // Get all the membershipHistotyList where expiredDate greater than or equals to UPDATED_EXPIRED_DATE
        defaultMembershipHistotyShouldNotBeFound("expiredDate.greaterOrEqualThan=" + UPDATED_EXPIRED_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByExpiredDateIsLessThanSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where expiredDate less than or equals to DEFAULT_EXPIRED_DATE
        defaultMembershipHistotyShouldNotBeFound("expiredDate.lessThan=" + DEFAULT_EXPIRED_DATE);

        // Get all the membershipHistotyList where expiredDate less than or equals to UPDATED_EXPIRED_DATE
        defaultMembershipHistotyShouldBeFound("expiredDate.lessThan=" + UPDATED_EXPIRED_DATE);
    }


    @Test
    @Transactional
    public void getAllMembershipHistotiesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where status equals to DEFAULT_STATUS
        defaultMembershipHistotyShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the membershipHistotyList where status equals to UPDATED_STATUS
        defaultMembershipHistotyShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultMembershipHistotyShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the membershipHistotyList where status equals to UPDATED_STATUS
        defaultMembershipHistotyShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where status is not null
        defaultMembershipHistotyShouldBeFound("status.specified=true");

        // Get all the membershipHistotyList where status is null
        defaultMembershipHistotyShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where status greater than or equals to DEFAULT_STATUS
        defaultMembershipHistotyShouldBeFound("status.greaterOrEqualThan=" + DEFAULT_STATUS);

        // Get all the membershipHistotyList where status greater than or equals to UPDATED_STATUS
        defaultMembershipHistotyShouldNotBeFound("status.greaterOrEqualThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where status less than or equals to DEFAULT_STATUS
        defaultMembershipHistotyShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the membershipHistotyList where status less than or equals to UPDATED_STATUS
        defaultMembershipHistotyShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllMembershipHistotiesByAccumulationPointsIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where accumulationPoints equals to DEFAULT_ACCUMULATION_POINTS
        defaultMembershipHistotyShouldBeFound("accumulationPoints.equals=" + DEFAULT_ACCUMULATION_POINTS);

        // Get all the membershipHistotyList where accumulationPoints equals to UPDATED_ACCUMULATION_POINTS
        defaultMembershipHistotyShouldNotBeFound("accumulationPoints.equals=" + UPDATED_ACCUMULATION_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByAccumulationPointsIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where accumulationPoints in DEFAULT_ACCUMULATION_POINTS or UPDATED_ACCUMULATION_POINTS
        defaultMembershipHistotyShouldBeFound("accumulationPoints.in=" + DEFAULT_ACCUMULATION_POINTS + "," + UPDATED_ACCUMULATION_POINTS);

        // Get all the membershipHistotyList where accumulationPoints equals to UPDATED_ACCUMULATION_POINTS
        defaultMembershipHistotyShouldNotBeFound("accumulationPoints.in=" + UPDATED_ACCUMULATION_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByAccumulationPointsIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where accumulationPoints is not null
        defaultMembershipHistotyShouldBeFound("accumulationPoints.specified=true");

        // Get all the membershipHistotyList where accumulationPoints is null
        defaultMembershipHistotyShouldNotBeFound("accumulationPoints.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByPendingPointsIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where pendingPoints equals to DEFAULT_PENDING_POINTS
        defaultMembershipHistotyShouldBeFound("pendingPoints.equals=" + DEFAULT_PENDING_POINTS);

        // Get all the membershipHistotyList where pendingPoints equals to UPDATED_PENDING_POINTS
        defaultMembershipHistotyShouldNotBeFound("pendingPoints.equals=" + UPDATED_PENDING_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByPendingPointsIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where pendingPoints in DEFAULT_PENDING_POINTS or UPDATED_PENDING_POINTS
        defaultMembershipHistotyShouldBeFound("pendingPoints.in=" + DEFAULT_PENDING_POINTS + "," + UPDATED_PENDING_POINTS);

        // Get all the membershipHistotyList where pendingPoints equals to UPDATED_PENDING_POINTS
        defaultMembershipHistotyShouldNotBeFound("pendingPoints.in=" + UPDATED_PENDING_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByPendingPointsIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where pendingPoints is not null
        defaultMembershipHistotyShouldBeFound("pendingPoints.specified=true");

        // Get all the membershipHistotyList where pendingPoints is null
        defaultMembershipHistotyShouldNotBeFound("pendingPoints.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByActivatedPointsIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where activatedPoints equals to DEFAULT_ACTIVATED_POINTS
        defaultMembershipHistotyShouldBeFound("activatedPoints.equals=" + DEFAULT_ACTIVATED_POINTS);

        // Get all the membershipHistotyList where activatedPoints equals to UPDATED_ACTIVATED_POINTS
        defaultMembershipHistotyShouldNotBeFound("activatedPoints.equals=" + UPDATED_ACTIVATED_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByActivatedPointsIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where activatedPoints in DEFAULT_ACTIVATED_POINTS or UPDATED_ACTIVATED_POINTS
        defaultMembershipHistotyShouldBeFound("activatedPoints.in=" + DEFAULT_ACTIVATED_POINTS + "," + UPDATED_ACTIVATED_POINTS);

        // Get all the membershipHistotyList where activatedPoints equals to UPDATED_ACTIVATED_POINTS
        defaultMembershipHistotyShouldNotBeFound("activatedPoints.in=" + UPDATED_ACTIVATED_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByActivatedPointsIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where activatedPoints is not null
        defaultMembershipHistotyShouldBeFound("activatedPoints.specified=true");

        // Get all the membershipHistotyList where activatedPoints is null
        defaultMembershipHistotyShouldNotBeFound("activatedPoints.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByExpiredPointsIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where expiredPoints equals to DEFAULT_EXPIRED_POINTS
        defaultMembershipHistotyShouldBeFound("expiredPoints.equals=" + DEFAULT_EXPIRED_POINTS);

        // Get all the membershipHistotyList where expiredPoints equals to UPDATED_EXPIRED_POINTS
        defaultMembershipHistotyShouldNotBeFound("expiredPoints.equals=" + UPDATED_EXPIRED_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByExpiredPointsIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where expiredPoints in DEFAULT_EXPIRED_POINTS or UPDATED_EXPIRED_POINTS
        defaultMembershipHistotyShouldBeFound("expiredPoints.in=" + DEFAULT_EXPIRED_POINTS + "," + UPDATED_EXPIRED_POINTS);

        // Get all the membershipHistotyList where expiredPoints equals to UPDATED_EXPIRED_POINTS
        defaultMembershipHistotyShouldNotBeFound("expiredPoints.in=" + UPDATED_EXPIRED_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByExpiredPointsIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where expiredPoints is not null
        defaultMembershipHistotyShouldBeFound("expiredPoints.specified=true");

        // Get all the membershipHistotyList where expiredPoints is null
        defaultMembershipHistotyShouldNotBeFound("expiredPoints.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByRedeemPointsIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where redeemPoints equals to DEFAULT_REDEEM_POINTS
        defaultMembershipHistotyShouldBeFound("redeemPoints.equals=" + DEFAULT_REDEEM_POINTS);

        // Get all the membershipHistotyList where redeemPoints equals to UPDATED_REDEEM_POINTS
        defaultMembershipHistotyShouldNotBeFound("redeemPoints.equals=" + UPDATED_REDEEM_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByRedeemPointsIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where redeemPoints in DEFAULT_REDEEM_POINTS or UPDATED_REDEEM_POINTS
        defaultMembershipHistotyShouldBeFound("redeemPoints.in=" + DEFAULT_REDEEM_POINTS + "," + UPDATED_REDEEM_POINTS);

        // Get all the membershipHistotyList where redeemPoints equals to UPDATED_REDEEM_POINTS
        defaultMembershipHistotyShouldNotBeFound("redeemPoints.in=" + UPDATED_REDEEM_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByRedeemPointsIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where redeemPoints is not null
        defaultMembershipHistotyShouldBeFound("redeemPoints.specified=true");

        // Get all the membershipHistotyList where redeemPoints is null
        defaultMembershipHistotyShouldNotBeFound("redeemPoints.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByRemainPointsIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where remainPoints equals to DEFAULT_REMAIN_POINTS
        defaultMembershipHistotyShouldBeFound("remainPoints.equals=" + DEFAULT_REMAIN_POINTS);

        // Get all the membershipHistotyList where remainPoints equals to UPDATED_REMAIN_POINTS
        defaultMembershipHistotyShouldNotBeFound("remainPoints.equals=" + UPDATED_REMAIN_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByRemainPointsIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where remainPoints in DEFAULT_REMAIN_POINTS or UPDATED_REMAIN_POINTS
        defaultMembershipHistotyShouldBeFound("remainPoints.in=" + DEFAULT_REMAIN_POINTS + "," + UPDATED_REMAIN_POINTS);

        // Get all the membershipHistotyList where remainPoints equals to UPDATED_REMAIN_POINTS
        defaultMembershipHistotyShouldNotBeFound("remainPoints.in=" + UPDATED_REMAIN_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByRemainPointsIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where remainPoints is not null
        defaultMembershipHistotyShouldBeFound("remainPoints.specified=true");

        // Get all the membershipHistotyList where remainPoints is null
        defaultMembershipHistotyShouldNotBeFound("remainPoints.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultMembershipHistotyShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the membershipHistotyList where updateDate equals to UPDATED_UPDATE_DATE
        defaultMembershipHistotyShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultMembershipHistotyShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the membershipHistotyList where updateDate equals to UPDATED_UPDATE_DATE
        defaultMembershipHistotyShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where updateDate is not null
        defaultMembershipHistotyShouldBeFound("updateDate.specified=true");

        // Get all the membershipHistotyList where updateDate is null
        defaultMembershipHistotyShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByUpdateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where updateDate greater than or equals to DEFAULT_UPDATE_DATE
        defaultMembershipHistotyShouldBeFound("updateDate.greaterOrEqualThan=" + DEFAULT_UPDATE_DATE);

        // Get all the membershipHistotyList where updateDate greater than or equals to UPDATED_UPDATE_DATE
        defaultMembershipHistotyShouldNotBeFound("updateDate.greaterOrEqualThan=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByUpdateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where updateDate less than or equals to DEFAULT_UPDATE_DATE
        defaultMembershipHistotyShouldNotBeFound("updateDate.lessThan=" + DEFAULT_UPDATE_DATE);

        // Get all the membershipHistotyList where updateDate less than or equals to UPDATED_UPDATE_DATE
        defaultMembershipHistotyShouldBeFound("updateDate.lessThan=" + UPDATED_UPDATE_DATE);
    }


    @Test
    @Transactional
    public void getAllMembershipHistotiesByTransferablePointsIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where transferablePoints equals to DEFAULT_TRANSFERABLE_POINTS
        defaultMembershipHistotyShouldBeFound("transferablePoints.equals=" + DEFAULT_TRANSFERABLE_POINTS);

        // Get all the membershipHistotyList where transferablePoints equals to UPDATED_TRANSFERABLE_POINTS
        defaultMembershipHistotyShouldNotBeFound("transferablePoints.equals=" + UPDATED_TRANSFERABLE_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByTransferablePointsIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where transferablePoints in DEFAULT_TRANSFERABLE_POINTS or UPDATED_TRANSFERABLE_POINTS
        defaultMembershipHistotyShouldBeFound("transferablePoints.in=" + DEFAULT_TRANSFERABLE_POINTS + "," + UPDATED_TRANSFERABLE_POINTS);

        // Get all the membershipHistotyList where transferablePoints equals to UPDATED_TRANSFERABLE_POINTS
        defaultMembershipHistotyShouldNotBeFound("transferablePoints.in=" + UPDATED_TRANSFERABLE_POINTS);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByTransferablePointsIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where transferablePoints is not null
        defaultMembershipHistotyShouldBeFound("transferablePoints.specified=true");

        // Get all the membershipHistotyList where transferablePoints is null
        defaultMembershipHistotyShouldNotBeFound("transferablePoints.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByAttemptIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where attempt equals to DEFAULT_ATTEMPT
        defaultMembershipHistotyShouldBeFound("attempt.equals=" + DEFAULT_ATTEMPT);

        // Get all the membershipHistotyList where attempt equals to UPDATED_ATTEMPT
        defaultMembershipHistotyShouldNotBeFound("attempt.equals=" + UPDATED_ATTEMPT);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByAttemptIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where attempt in DEFAULT_ATTEMPT or UPDATED_ATTEMPT
        defaultMembershipHistotyShouldBeFound("attempt.in=" + DEFAULT_ATTEMPT + "," + UPDATED_ATTEMPT);

        // Get all the membershipHistotyList where attempt equals to UPDATED_ATTEMPT
        defaultMembershipHistotyShouldNotBeFound("attempt.in=" + UPDATED_ATTEMPT);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByAttemptIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where attempt is not null
        defaultMembershipHistotyShouldBeFound("attempt.specified=true");

        // Get all the membershipHistotyList where attempt is null
        defaultMembershipHistotyShouldNotBeFound("attempt.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByAttemptIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where attempt greater than or equals to DEFAULT_ATTEMPT
        defaultMembershipHistotyShouldBeFound("attempt.greaterOrEqualThan=" + DEFAULT_ATTEMPT);

        // Get all the membershipHistotyList where attempt greater than or equals to UPDATED_ATTEMPT
        defaultMembershipHistotyShouldNotBeFound("attempt.greaterOrEqualThan=" + UPDATED_ATTEMPT);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByAttemptIsLessThanSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where attempt less than or equals to DEFAULT_ATTEMPT
        defaultMembershipHistotyShouldNotBeFound("attempt.lessThan=" + DEFAULT_ATTEMPT);

        // Get all the membershipHistotyList where attempt less than or equals to UPDATED_ATTEMPT
        defaultMembershipHistotyShouldBeFound("attempt.lessThan=" + UPDATED_ATTEMPT);
    }


    @Test
    @Transactional
    public void getAllMembershipHistotiesByDeleteFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where deleteFlag equals to DEFAULT_DELETE_FLAG
        defaultMembershipHistotyShouldBeFound("deleteFlag.equals=" + DEFAULT_DELETE_FLAG);

        // Get all the membershipHistotyList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultMembershipHistotyShouldNotBeFound("deleteFlag.equals=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByDeleteFlagIsInShouldWork() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where deleteFlag in DEFAULT_DELETE_FLAG or UPDATED_DELETE_FLAG
        defaultMembershipHistotyShouldBeFound("deleteFlag.in=" + DEFAULT_DELETE_FLAG + "," + UPDATED_DELETE_FLAG);

        // Get all the membershipHistotyList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultMembershipHistotyShouldNotBeFound("deleteFlag.in=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByDeleteFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where deleteFlag is not null
        defaultMembershipHistotyShouldBeFound("deleteFlag.specified=true");

        // Get all the membershipHistotyList where deleteFlag is null
        defaultMembershipHistotyShouldNotBeFound("deleteFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByDeleteFlagIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where deleteFlag greater than or equals to DEFAULT_DELETE_FLAG
        defaultMembershipHistotyShouldBeFound("deleteFlag.greaterOrEqualThan=" + DEFAULT_DELETE_FLAG);

        // Get all the membershipHistotyList where deleteFlag greater than or equals to UPDATED_DELETE_FLAG
        defaultMembershipHistotyShouldNotBeFound("deleteFlag.greaterOrEqualThan=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllMembershipHistotiesByDeleteFlagIsLessThanSomething() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        // Get all the membershipHistotyList where deleteFlag less than or equals to DEFAULT_DELETE_FLAG
        defaultMembershipHistotyShouldNotBeFound("deleteFlag.lessThan=" + DEFAULT_DELETE_FLAG);

        // Get all the membershipHistotyList where deleteFlag less than or equals to UPDATED_DELETE_FLAG
        defaultMembershipHistotyShouldBeFound("deleteFlag.lessThan=" + UPDATED_DELETE_FLAG);
    }


    @Test
    @Transactional
    public void getAllMembershipHistotiesByCustomerIsEqualToSomething() throws Exception {
        // Initialize the database
        Customer customer = CustomerResourceIntTest.createEntity(em);
        em.persist(customer);
        em.flush();
        membershipHistoty.setCustomer(customer);
        membershipHistotyRepository.saveAndFlush(membershipHistoty);
        Long customerId = customer.getId();

        // Get all the membershipHistotyList where customer equals to customerId
        defaultMembershipHistotyShouldBeFound("customerId.equals=" + customerId);

        // Get all the membershipHistotyList where customer equals to customerId + 1
        defaultMembershipHistotyShouldNotBeFound("customerId.equals=" + (customerId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultMembershipHistotyShouldBeFound(String filter) throws Exception {
        restMembershipHistotyMockMvc.perform(get("/api/membership-histoties?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(membershipHistoty.getId().intValue())))
            .andExpect(jsonPath("$.[*].customerCode").value(hasItem(DEFAULT_CUSTOMER_CODE.toString())))
            .andExpect(jsonPath("$.[*].membershipLevelCode").value(hasItem(DEFAULT_MEMBERSHIP_LEVEL_CODE.toString())))
            .andExpect(jsonPath("$.[*].effectedDate").value(hasItem(sameInstant(DEFAULT_EFFECTED_DATE))))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(sameInstant(DEFAULT_START_DATE))))
            .andExpect(jsonPath("$.[*].expiredDate").value(hasItem(sameInstant(DEFAULT_EXPIRED_DATE))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].accumulationPoints").value(hasItem(DEFAULT_ACCUMULATION_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].pendingPoints").value(hasItem(DEFAULT_PENDING_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].activatedPoints").value(hasItem(DEFAULT_ACTIVATED_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].expiredPoints").value(hasItem(DEFAULT_EXPIRED_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].redeemPoints").value(hasItem(DEFAULT_REDEEM_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].remainPoints").value(hasItem(DEFAULT_REMAIN_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].transferablePoints").value(hasItem(DEFAULT_TRANSFERABLE_POINTS.doubleValue())))
            .andExpect(jsonPath("$.[*].attempt").value(hasItem(DEFAULT_ATTEMPT)))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultMembershipHistotyShouldNotBeFound(String filter) throws Exception {
        restMembershipHistotyMockMvc.perform(get("/api/membership-histoties?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingMembershipHistoty() throws Exception {
        // Get the membershipHistoty
        restMembershipHistotyMockMvc.perform(get("/api/membership-histoties/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMembershipHistoty() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        int databaseSizeBeforeUpdate = membershipHistotyRepository.findAll().size();

        // Update the membershipHistoty
        MembershipHistoty updatedMembershipHistoty = membershipHistotyRepository.findById(membershipHistoty.getId()).get();
        // Disconnect from session so that the updates on updatedMembershipHistoty are not directly saved in db
        em.detach(updatedMembershipHistoty);
        updatedMembershipHistoty
            .membershipLevelCode(UPDATED_MEMBERSHIP_LEVEL_CODE)
            .effectedDate(UPDATED_EFFECTED_DATE)
            .startDate(UPDATED_START_DATE)
            .expiredDate(UPDATED_EXPIRED_DATE)
            .status(UPDATED_STATUS)
            .accumulationPoints(UPDATED_ACCUMULATION_POINTS)
            .pendingPoints(UPDATED_PENDING_POINTS)
            .activatedPoints(UPDATED_ACTIVATED_POINTS)
            .expiredPoints(UPDATED_EXPIRED_POINTS)
            .redeemPoints(UPDATED_REDEEM_POINTS)
            .remainPoints(UPDATED_REMAIN_POINTS)
            .updateDate(UPDATED_UPDATE_DATE)
            .transferablePoints(UPDATED_TRANSFERABLE_POINTS)
            .deleteFlag(UPDATED_DELETE_FLAG);
        MembershipHistotyDTO membershipHistotyDTO = membershipHistotyMapper.toDto(updatedMembershipHistoty);

        restMembershipHistotyMockMvc.perform(put("/api/membership-histoties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipHistotyDTO)))
            .andExpect(status().isOk());

        // Validate the MembershipHistoty in the database
        List<MembershipHistoty> membershipHistotyList = membershipHistotyRepository.findAll();
        assertThat(membershipHistotyList).hasSize(databaseSizeBeforeUpdate);
        MembershipHistoty testMembershipHistoty = membershipHistotyList.get(membershipHistotyList.size() - 1);
        assertThat(testMembershipHistoty.getMembershipLevelCode()).isEqualTo(UPDATED_MEMBERSHIP_LEVEL_CODE);
        assertThat(testMembershipHistoty.getEffectedDate()).isEqualTo(UPDATED_EFFECTED_DATE);
        assertThat(testMembershipHistoty.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testMembershipHistoty.getExpiredDate()).isEqualTo(UPDATED_EXPIRED_DATE);
        assertThat(testMembershipHistoty.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMembershipHistoty.getAccumulationPoints()).isEqualTo(UPDATED_ACCUMULATION_POINTS);
        assertThat(testMembershipHistoty.getPendingPoints()).isEqualTo(UPDATED_PENDING_POINTS);
        assertThat(testMembershipHistoty.getActivatedPoints()).isEqualTo(UPDATED_ACTIVATED_POINTS);
        assertThat(testMembershipHistoty.getExpiredPoints()).isEqualTo(UPDATED_EXPIRED_POINTS);
        assertThat(testMembershipHistoty.getRedeemPoints()).isEqualTo(UPDATED_REDEEM_POINTS);
        assertThat(testMembershipHistoty.getRemainPoints()).isEqualTo(UPDATED_REMAIN_POINTS);
        assertThat(testMembershipHistoty.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testMembershipHistoty.getTransferablePoints()).isEqualTo(UPDATED_TRANSFERABLE_POINTS);
        assertThat(testMembershipHistoty.getDeleteFlag()).isEqualTo(UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void updateNonExistingMembershipHistoty() throws Exception {
        int databaseSizeBeforeUpdate = membershipHistotyRepository.findAll().size();

        // Create the MembershipHistoty
        MembershipHistotyDTO membershipHistotyDTO = membershipHistotyMapper.toDto(membershipHistoty);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMembershipHistotyMockMvc.perform(put("/api/membership-histoties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipHistotyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MembershipHistoty in the database
        List<MembershipHistoty> membershipHistotyList = membershipHistotyRepository.findAll();
        assertThat(membershipHistotyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMembershipHistoty() throws Exception {
        // Initialize the database
        membershipHistotyRepository.saveAndFlush(membershipHistoty);

        int databaseSizeBeforeDelete = membershipHistotyRepository.findAll().size();

        // Get the membershipHistoty
        restMembershipHistotyMockMvc.perform(delete("/api/membership-histoties/{id}", membershipHistoty.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MembershipHistoty> membershipHistotyList = membershipHistotyRepository.findAll();
        assertThat(membershipHistotyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MembershipHistoty.class);
        MembershipHistoty membershipHistoty1 = new MembershipHistoty();
        membershipHistoty1.setId(1L);
        MembershipHistoty membershipHistoty2 = new MembershipHistoty();
        membershipHistoty2.setId(membershipHistoty1.getId());
        assertThat(membershipHistoty1).isEqualTo(membershipHistoty2);
        membershipHistoty2.setId(2L);
        assertThat(membershipHistoty1).isNotEqualTo(membershipHistoty2);
        membershipHistoty1.setId(null);
        assertThat(membershipHistoty1).isNotEqualTo(membershipHistoty2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MembershipHistotyDTO.class);
        MembershipHistotyDTO membershipHistotyDTO1 = new MembershipHistotyDTO();
        membershipHistotyDTO1.setId(1L);
        MembershipHistotyDTO membershipHistotyDTO2 = new MembershipHistotyDTO();
        assertThat(membershipHistotyDTO1).isNotEqualTo(membershipHistotyDTO2);
        membershipHistotyDTO2.setId(membershipHistotyDTO1.getId());
        assertThat(membershipHistotyDTO1).isEqualTo(membershipHistotyDTO2);
        membershipHistotyDTO2.setId(2L);
        assertThat(membershipHistotyDTO1).isNotEqualTo(membershipHistotyDTO2);
        membershipHistotyDTO1.setId(null);
        assertThat(membershipHistotyDTO1).isNotEqualTo(membershipHistotyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(membershipHistotyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(membershipHistotyMapper.fromId(null)).isNull();
    }
}
