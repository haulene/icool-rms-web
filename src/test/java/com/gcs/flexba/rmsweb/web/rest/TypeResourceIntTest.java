package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.Type;
import com.gcs.flexba.rmsweb.repository.TypeRepository;
import com.gcs.flexba.rmsweb.service.TypeService;
import com.gcs.flexba.rmsweb.service.dto.TypeDTO;
import com.gcs.flexba.rmsweb.service.mapper.TypeMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.TypeCriteria;
import com.gcs.flexba.rmsweb.service.TypeQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TypeResource REST controller.
 *
 * @see TypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class TypeResourceIntTest {

    private static final String DEFAULT_TYPE_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_CLASS = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_NAME_EN = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_NAME_EN = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final Integer DEFAULT_SORT_ORDER = 1;
    private static final Integer UPDATED_SORT_ORDER = 2;

    private static final ZonedDateTime DEFAULT_UPDATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_DELETE_FLAG = false;
    private static final Boolean UPDATED_DELETE_FLAG = true;

    @Autowired
    private TypeRepository typeRepository;


    @Autowired
    private TypeMapper typeMapper;
    

    @Autowired
    private TypeService typeService;

    @Autowired
    private TypeQueryService typeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTypeMockMvc;

    private Type type;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TypeResource typeResource = new TypeResource(typeService, typeQueryService);
        this.restTypeMockMvc = MockMvcBuilders.standaloneSetup(typeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Type createEntity(EntityManager em) {
        Type type = new Type()
            .typeClass(DEFAULT_TYPE_CLASS)
            .typeCode(DEFAULT_TYPE_CODE)
            .typeName(DEFAULT_TYPE_NAME)
            .typeNameEn(DEFAULT_TYPE_NAME_EN)
            .note(DEFAULT_NOTE)
            .sortOrder(DEFAULT_SORT_ORDER)
            .updateDate(DEFAULT_UPDATE_DATE)
            .deleteFlag(DEFAULT_DELETE_FLAG);
        return type;
    }

    @Before
    public void initTest() {
        type = createEntity(em);
    }

    @Test
    @Transactional
    public void createType() throws Exception {
        int databaseSizeBeforeCreate = typeRepository.findAll().size();

        // Create the Type
        TypeDTO typeDTO = typeMapper.toDto(type);
        restTypeMockMvc.perform(post("/api/types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeDTO)))
            .andExpect(status().isCreated());

        // Validate the Type in the database
        List<Type> typeList = typeRepository.findAll();
        assertThat(typeList).hasSize(databaseSizeBeforeCreate + 1);
        Type testType = typeList.get(typeList.size() - 1);
        assertThat(testType.getTypeClass()).isEqualTo(DEFAULT_TYPE_CLASS);
        assertThat(testType.getTypeCode()).isEqualTo(DEFAULT_TYPE_CODE);
        assertThat(testType.getTypeName()).isEqualTo(DEFAULT_TYPE_NAME);
        assertThat(testType.getTypeNameEn()).isEqualTo(DEFAULT_TYPE_NAME_EN);
        assertThat(testType.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testType.getSortOrder()).isEqualTo(DEFAULT_SORT_ORDER);
        assertThat(testType.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testType.isDeleteFlag()).isEqualTo(DEFAULT_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void createTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typeRepository.findAll().size();

        // Create the Type with an existing ID
        type.setId(1L);
        TypeDTO typeDTO = typeMapper.toDto(type);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypeMockMvc.perform(post("/api/types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Type in the database
        List<Type> typeList = typeRepository.findAll();
        assertThat(typeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTypes() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList
        restTypeMockMvc.perform(get("/api/types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(type.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeClass").value(hasItem(DEFAULT_TYPE_CLASS.toString())))
            .andExpect(jsonPath("$.[*].typeCode").value(hasItem(DEFAULT_TYPE_CODE.toString())))
            .andExpect(jsonPath("$.[*].typeName").value(hasItem(DEFAULT_TYPE_NAME.toString())))
            .andExpect(jsonPath("$.[*].typeNameEn").value(hasItem(DEFAULT_TYPE_NAME_EN.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].sortOrder").value(hasItem(DEFAULT_SORT_ORDER)))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG.booleanValue())));
    }
    

    @Test
    @Transactional
    public void getType() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get the type
        restTypeMockMvc.perform(get("/api/types/{id}", type.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(type.getId().intValue()))
            .andExpect(jsonPath("$.typeClass").value(DEFAULT_TYPE_CLASS.toString()))
            .andExpect(jsonPath("$.typeCode").value(DEFAULT_TYPE_CODE.toString()))
            .andExpect(jsonPath("$.typeName").value(DEFAULT_TYPE_NAME.toString()))
            .andExpect(jsonPath("$.typeNameEn").value(DEFAULT_TYPE_NAME_EN.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.sortOrder").value(DEFAULT_SORT_ORDER))
            .andExpect(jsonPath("$.updateDate").value(sameInstant(DEFAULT_UPDATE_DATE)))
            .andExpect(jsonPath("$.deleteFlag").value(DEFAULT_DELETE_FLAG.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllTypesByTypeClassIsEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeClass equals to DEFAULT_TYPE_CLASS
        defaultTypeShouldBeFound("typeClass.equals=" + DEFAULT_TYPE_CLASS);

        // Get all the typeList where typeClass equals to UPDATED_TYPE_CLASS
        defaultTypeShouldNotBeFound("typeClass.equals=" + UPDATED_TYPE_CLASS);
    }

    @Test
    @Transactional
    public void getAllTypesByTypeClassIsInShouldWork() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeClass in DEFAULT_TYPE_CLASS or UPDATED_TYPE_CLASS
        defaultTypeShouldBeFound("typeClass.in=" + DEFAULT_TYPE_CLASS + "," + UPDATED_TYPE_CLASS);

        // Get all the typeList where typeClass equals to UPDATED_TYPE_CLASS
        defaultTypeShouldNotBeFound("typeClass.in=" + UPDATED_TYPE_CLASS);
    }

    @Test
    @Transactional
    public void getAllTypesByTypeClassIsNullOrNotNull() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeClass is not null
        defaultTypeShouldBeFound("typeClass.specified=true");

        // Get all the typeList where typeClass is null
        defaultTypeShouldNotBeFound("typeClass.specified=false");
    }

    @Test
    @Transactional
    public void getAllTypesByTypeCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeCode equals to DEFAULT_TYPE_CODE
        defaultTypeShouldBeFound("typeCode.equals=" + DEFAULT_TYPE_CODE);

        // Get all the typeList where typeCode equals to UPDATED_TYPE_CODE
        defaultTypeShouldNotBeFound("typeCode.equals=" + UPDATED_TYPE_CODE);
    }

    @Test
    @Transactional
    public void getAllTypesByTypeCodeIsInShouldWork() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeCode in DEFAULT_TYPE_CODE or UPDATED_TYPE_CODE
        defaultTypeShouldBeFound("typeCode.in=" + DEFAULT_TYPE_CODE + "," + UPDATED_TYPE_CODE);

        // Get all the typeList where typeCode equals to UPDATED_TYPE_CODE
        defaultTypeShouldNotBeFound("typeCode.in=" + UPDATED_TYPE_CODE);
    }

    @Test
    @Transactional
    public void getAllTypesByTypeCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeCode is not null
        defaultTypeShouldBeFound("typeCode.specified=true");

        // Get all the typeList where typeCode is null
        defaultTypeShouldNotBeFound("typeCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllTypesByTypeNameIsEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeName equals to DEFAULT_TYPE_NAME
        defaultTypeShouldBeFound("typeName.equals=" + DEFAULT_TYPE_NAME);

        // Get all the typeList where typeName equals to UPDATED_TYPE_NAME
        defaultTypeShouldNotBeFound("typeName.equals=" + UPDATED_TYPE_NAME);
    }

    @Test
    @Transactional
    public void getAllTypesByTypeNameIsInShouldWork() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeName in DEFAULT_TYPE_NAME or UPDATED_TYPE_NAME
        defaultTypeShouldBeFound("typeName.in=" + DEFAULT_TYPE_NAME + "," + UPDATED_TYPE_NAME);

        // Get all the typeList where typeName equals to UPDATED_TYPE_NAME
        defaultTypeShouldNotBeFound("typeName.in=" + UPDATED_TYPE_NAME);
    }

    @Test
    @Transactional
    public void getAllTypesByTypeNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeName is not null
        defaultTypeShouldBeFound("typeName.specified=true");

        // Get all the typeList where typeName is null
        defaultTypeShouldNotBeFound("typeName.specified=false");
    }

    @Test
    @Transactional
    public void getAllTypesByTypeNameEnIsEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeNameEn equals to DEFAULT_TYPE_NAME_EN
        defaultTypeShouldBeFound("typeNameEn.equals=" + DEFAULT_TYPE_NAME_EN);

        // Get all the typeList where typeNameEn equals to UPDATED_TYPE_NAME_EN
        defaultTypeShouldNotBeFound("typeNameEn.equals=" + UPDATED_TYPE_NAME_EN);
    }

    @Test
    @Transactional
    public void getAllTypesByTypeNameEnIsInShouldWork() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeNameEn in DEFAULT_TYPE_NAME_EN or UPDATED_TYPE_NAME_EN
        defaultTypeShouldBeFound("typeNameEn.in=" + DEFAULT_TYPE_NAME_EN + "," + UPDATED_TYPE_NAME_EN);

        // Get all the typeList where typeNameEn equals to UPDATED_TYPE_NAME_EN
        defaultTypeShouldNotBeFound("typeNameEn.in=" + UPDATED_TYPE_NAME_EN);
    }

    @Test
    @Transactional
    public void getAllTypesByTypeNameEnIsNullOrNotNull() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where typeNameEn is not null
        defaultTypeShouldBeFound("typeNameEn.specified=true");

        // Get all the typeList where typeNameEn is null
        defaultTypeShouldNotBeFound("typeNameEn.specified=false");
    }

    @Test
    @Transactional
    public void getAllTypesByNoteIsEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where note equals to DEFAULT_NOTE
        defaultTypeShouldBeFound("note.equals=" + DEFAULT_NOTE);

        // Get all the typeList where note equals to UPDATED_NOTE
        defaultTypeShouldNotBeFound("note.equals=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllTypesByNoteIsInShouldWork() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where note in DEFAULT_NOTE or UPDATED_NOTE
        defaultTypeShouldBeFound("note.in=" + DEFAULT_NOTE + "," + UPDATED_NOTE);

        // Get all the typeList where note equals to UPDATED_NOTE
        defaultTypeShouldNotBeFound("note.in=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllTypesByNoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where note is not null
        defaultTypeShouldBeFound("note.specified=true");

        // Get all the typeList where note is null
        defaultTypeShouldNotBeFound("note.specified=false");
    }

    @Test
    @Transactional
    public void getAllTypesBySortOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where sortOrder equals to DEFAULT_SORT_ORDER
        defaultTypeShouldBeFound("sortOrder.equals=" + DEFAULT_SORT_ORDER);

        // Get all the typeList where sortOrder equals to UPDATED_SORT_ORDER
        defaultTypeShouldNotBeFound("sortOrder.equals=" + UPDATED_SORT_ORDER);
    }

    @Test
    @Transactional
    public void getAllTypesBySortOrderIsInShouldWork() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where sortOrder in DEFAULT_SORT_ORDER or UPDATED_SORT_ORDER
        defaultTypeShouldBeFound("sortOrder.in=" + DEFAULT_SORT_ORDER + "," + UPDATED_SORT_ORDER);

        // Get all the typeList where sortOrder equals to UPDATED_SORT_ORDER
        defaultTypeShouldNotBeFound("sortOrder.in=" + UPDATED_SORT_ORDER);
    }

    @Test
    @Transactional
    public void getAllTypesBySortOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where sortOrder is not null
        defaultTypeShouldBeFound("sortOrder.specified=true");

        // Get all the typeList where sortOrder is null
        defaultTypeShouldNotBeFound("sortOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllTypesBySortOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where sortOrder greater than or equals to DEFAULT_SORT_ORDER
        defaultTypeShouldBeFound("sortOrder.greaterOrEqualThan=" + DEFAULT_SORT_ORDER);

        // Get all the typeList where sortOrder greater than or equals to UPDATED_SORT_ORDER
        defaultTypeShouldNotBeFound("sortOrder.greaterOrEqualThan=" + UPDATED_SORT_ORDER);
    }

    @Test
    @Transactional
    public void getAllTypesBySortOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where sortOrder less than or equals to DEFAULT_SORT_ORDER
        defaultTypeShouldNotBeFound("sortOrder.lessThan=" + DEFAULT_SORT_ORDER);

        // Get all the typeList where sortOrder less than or equals to UPDATED_SORT_ORDER
        defaultTypeShouldBeFound("sortOrder.lessThan=" + UPDATED_SORT_ORDER);
    }


    @Test
    @Transactional
    public void getAllTypesByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultTypeShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the typeList where updateDate equals to UPDATED_UPDATE_DATE
        defaultTypeShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllTypesByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultTypeShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the typeList where updateDate equals to UPDATED_UPDATE_DATE
        defaultTypeShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllTypesByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where updateDate is not null
        defaultTypeShouldBeFound("updateDate.specified=true");

        // Get all the typeList where updateDate is null
        defaultTypeShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllTypesByUpdateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where updateDate greater than or equals to DEFAULT_UPDATE_DATE
        defaultTypeShouldBeFound("updateDate.greaterOrEqualThan=" + DEFAULT_UPDATE_DATE);

        // Get all the typeList where updateDate greater than or equals to UPDATED_UPDATE_DATE
        defaultTypeShouldNotBeFound("updateDate.greaterOrEqualThan=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllTypesByUpdateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where updateDate less than or equals to DEFAULT_UPDATE_DATE
        defaultTypeShouldNotBeFound("updateDate.lessThan=" + DEFAULT_UPDATE_DATE);

        // Get all the typeList where updateDate less than or equals to UPDATED_UPDATE_DATE
        defaultTypeShouldBeFound("updateDate.lessThan=" + UPDATED_UPDATE_DATE);
    }


    @Test
    @Transactional
    public void getAllTypesByDeleteFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where deleteFlag equals to DEFAULT_DELETE_FLAG
        defaultTypeShouldBeFound("deleteFlag.equals=" + DEFAULT_DELETE_FLAG);

        // Get all the typeList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultTypeShouldNotBeFound("deleteFlag.equals=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllTypesByDeleteFlagIsInShouldWork() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where deleteFlag in DEFAULT_DELETE_FLAG or UPDATED_DELETE_FLAG
        defaultTypeShouldBeFound("deleteFlag.in=" + DEFAULT_DELETE_FLAG + "," + UPDATED_DELETE_FLAG);

        // Get all the typeList where deleteFlag equals to UPDATED_DELETE_FLAG
        defaultTypeShouldNotBeFound("deleteFlag.in=" + UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void getAllTypesByDeleteFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        // Get all the typeList where deleteFlag is not null
        defaultTypeShouldBeFound("deleteFlag.specified=true");

        // Get all the typeList where deleteFlag is null
        defaultTypeShouldNotBeFound("deleteFlag.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultTypeShouldBeFound(String filter) throws Exception {
        restTypeMockMvc.perform(get("/api/types?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(type.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeClass").value(hasItem(DEFAULT_TYPE_CLASS.toString())))
            .andExpect(jsonPath("$.[*].typeCode").value(hasItem(DEFAULT_TYPE_CODE.toString())))
            .andExpect(jsonPath("$.[*].typeName").value(hasItem(DEFAULT_TYPE_NAME.toString())))
            .andExpect(jsonPath("$.[*].typeNameEn").value(hasItem(DEFAULT_TYPE_NAME_EN.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].sortOrder").value(hasItem(DEFAULT_SORT_ORDER)))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].deleteFlag").value(hasItem(DEFAULT_DELETE_FLAG.booleanValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultTypeShouldNotBeFound(String filter) throws Exception {
        restTypeMockMvc.perform(get("/api/types?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingType() throws Exception {
        // Get the type
        restTypeMockMvc.perform(get("/api/types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateType() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        int databaseSizeBeforeUpdate = typeRepository.findAll().size();

        // Update the type
        Type updatedType = typeRepository.findById(type.getId()).get();
        // Disconnect from session so that the updates on updatedType are not directly saved in db
        em.detach(updatedType);
        updatedType
            .typeClass(UPDATED_TYPE_CLASS)
            .typeCode(UPDATED_TYPE_CODE)
            .typeName(UPDATED_TYPE_NAME)
            .typeNameEn(UPDATED_TYPE_NAME_EN)
            .note(UPDATED_NOTE)
            .sortOrder(UPDATED_SORT_ORDER)
            .updateDate(UPDATED_UPDATE_DATE)
            .deleteFlag(UPDATED_DELETE_FLAG);
        TypeDTO typeDTO = typeMapper.toDto(updatedType);

        restTypeMockMvc.perform(put("/api/types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeDTO)))
            .andExpect(status().isOk());

        // Validate the Type in the database
        List<Type> typeList = typeRepository.findAll();
        assertThat(typeList).hasSize(databaseSizeBeforeUpdate);
        Type testType = typeList.get(typeList.size() - 1);
        assertThat(testType.getTypeClass()).isEqualTo(UPDATED_TYPE_CLASS);
        assertThat(testType.getTypeCode()).isEqualTo(UPDATED_TYPE_CODE);
        assertThat(testType.getTypeName()).isEqualTo(UPDATED_TYPE_NAME);
        assertThat(testType.getTypeNameEn()).isEqualTo(UPDATED_TYPE_NAME_EN);
        assertThat(testType.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testType.getSortOrder()).isEqualTo(UPDATED_SORT_ORDER);
        assertThat(testType.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testType.isDeleteFlag()).isEqualTo(UPDATED_DELETE_FLAG);
    }

    @Test
    @Transactional
    public void updateNonExistingType() throws Exception {
        int databaseSizeBeforeUpdate = typeRepository.findAll().size();

        // Create the Type
        TypeDTO typeDTO = typeMapper.toDto(type);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTypeMockMvc.perform(put("/api/types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Type in the database
        List<Type> typeList = typeRepository.findAll();
        assertThat(typeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteType() throws Exception {
        // Initialize the database
        typeRepository.saveAndFlush(type);

        int databaseSizeBeforeDelete = typeRepository.findAll().size();

        // Get the type
        restTypeMockMvc.perform(delete("/api/types/{id}", type.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Type> typeList = typeRepository.findAll();
        assertThat(typeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Type.class);
        Type type1 = new Type();
        type1.setId(1L);
        Type type2 = new Type();
        type2.setId(type1.getId());
        assertThat(type1).isEqualTo(type2);
        type2.setId(2L);
        assertThat(type1).isNotEqualTo(type2);
        type1.setId(null);
        assertThat(type1).isNotEqualTo(type2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypeDTO.class);
        TypeDTO typeDTO1 = new TypeDTO();
        typeDTO1.setId(1L);
        TypeDTO typeDTO2 = new TypeDTO();
        assertThat(typeDTO1).isNotEqualTo(typeDTO2);
        typeDTO2.setId(typeDTO1.getId());
        assertThat(typeDTO1).isEqualTo(typeDTO2);
        typeDTO2.setId(2L);
        assertThat(typeDTO1).isNotEqualTo(typeDTO2);
        typeDTO1.setId(null);
        assertThat(typeDTO1).isNotEqualTo(typeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(typeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(typeMapper.fromId(null)).isNull();
    }
}
