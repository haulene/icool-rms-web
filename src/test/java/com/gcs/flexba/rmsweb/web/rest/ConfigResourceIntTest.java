package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.Config;
import com.gcs.flexba.rmsweb.repository.ConfigRepository;
import com.gcs.flexba.rmsweb.service.ConfigService;
import com.gcs.flexba.rmsweb.service.dto.ConfigDTO;
import com.gcs.flexba.rmsweb.service.mapper.ConfigMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.ConfigCriteria;
import com.gcs.flexba.rmsweb.service.ConfigQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConfigResource REST controller.
 *
 * @see ConfigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class ConfigResourceIntTest {

    private static final String DEFAULT_CONFIG_KEY = "AAAAAAAAAA";
    private static final String UPDATED_CONFIG_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_CONFIG_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_CONFIG_VALUE = "BBBBBBBBBB";

    private static final String DEFAULT_SYNC_CONFIG_FLAG = "AAAAAAAAAA";
    private static final String UPDATED_SYNC_CONFIG_FLAG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG = false;
    private static final Boolean UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG = true;

    private static final ZonedDateTime DEFAULT_UPDATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CHANNEL = "AAAAAAAAAA";
    private static final String UPDATED_CHANNEL = "BBBBBBBBBB";

    @Autowired
    private ConfigRepository configRepository;


    @Autowired
    private ConfigMapper configMapper;
    

    @Autowired
    private ConfigService configService;

    @Autowired
    private ConfigQueryService configQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restConfigMockMvc;

    private Config config;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConfigResource configResource = new ConfigResource(configService, configQueryService);
        this.restConfigMockMvc = MockMvcBuilders.standaloneSetup(configResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Config createEntity(EntityManager em) {
        Config config = new Config()
            .configValue(DEFAULT_CONFIG_VALUE)
            .syncConfigFlag(DEFAULT_SYNC_CONFIG_FLAG)
            .allowChangeSyncConfigFlag(DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG)
            .updateDate(DEFAULT_UPDATE_DATE)
            .channel(DEFAULT_CHANNEL);
        return config;
    }

    @Before
    public void initTest() {
        config = createEntity(em);
    }

    @Test
    @Transactional
    public void createConfig() throws Exception {
        int databaseSizeBeforeCreate = configRepository.findAll().size();

        // Create the Config
        ConfigDTO configDTO = configMapper.toDto(config);
        restConfigMockMvc.perform(post("/api/configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configDTO)))
            .andExpect(status().isCreated());

        // Validate the Config in the database
        List<Config> configList = configRepository.findAll();
        assertThat(configList).hasSize(databaseSizeBeforeCreate + 1);
        Config testConfig = configList.get(configList.size() - 1);
        assertThat(testConfig.getConfigValue()).isEqualTo(DEFAULT_CONFIG_VALUE);
        assertThat(testConfig.getSyncConfigFlag()).isEqualTo(DEFAULT_SYNC_CONFIG_FLAG);
        assertThat(testConfig.isAllowChangeSyncConfigFlag()).isEqualTo(DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG);
        assertThat(testConfig.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testConfig.getChannel()).isEqualTo(DEFAULT_CHANNEL);
    }

    @Test
    @Transactional
    public void createConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = configRepository.findAll().size();

        // Create the Config with an existing ID
        config.setId("1");
        ConfigDTO configDTO = configMapper.toDto(config);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConfigMockMvc.perform(post("/api/configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Config in the database
        List<Config> configList = configRepository.findAll();
        assertThat(configList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllConfigs() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList
        restConfigMockMvc.perform(get("/api/configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(config.getId())))
            .andExpect(jsonPath("$.[*].configKey").value(hasItem(DEFAULT_CONFIG_KEY.toString())))
            .andExpect(jsonPath("$.[*].configValue").value(hasItem(DEFAULT_CONFIG_VALUE.toString())))
            .andExpect(jsonPath("$.[*].syncConfigFlag").value(hasItem(DEFAULT_SYNC_CONFIG_FLAG.toString())))
            .andExpect(jsonPath("$.[*].allowChangeSyncConfigFlag").value(hasItem(DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].channel").value(hasItem(DEFAULT_CHANNEL.toString())));
    }
    

    @Test
    @Transactional
    public void getConfig() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get the config
        restConfigMockMvc.perform(get("/api/configs/{id}", config.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(config.getId()))
            .andExpect(jsonPath("$.configKey").value(DEFAULT_CONFIG_KEY.toString()))
            .andExpect(jsonPath("$.configValue").value(DEFAULT_CONFIG_VALUE.toString()))
            .andExpect(jsonPath("$.syncConfigFlag").value(DEFAULT_SYNC_CONFIG_FLAG.toString()))
            .andExpect(jsonPath("$.allowChangeSyncConfigFlag").value(DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG.booleanValue()))
            .andExpect(jsonPath("$.updateDate").value(sameInstant(DEFAULT_UPDATE_DATE)))
            .andExpect(jsonPath("$.channel").value(DEFAULT_CHANNEL.toString()));
    }

    @Test
    @Transactional
    public void getAllConfigsByConfigKeyIsEqualToSomething() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where configKey equals to DEFAULT_CONFIG_KEY
        defaultConfigShouldBeFound("configKey.equals=" + DEFAULT_CONFIG_KEY);

        // Get all the configList where configKey equals to UPDATED_CONFIG_KEY
        defaultConfigShouldNotBeFound("configKey.equals=" + UPDATED_CONFIG_KEY);
    }

    @Test
    @Transactional
    public void getAllConfigsByConfigKeyIsInShouldWork() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where configKey in DEFAULT_CONFIG_KEY or UPDATED_CONFIG_KEY
        defaultConfigShouldBeFound("configKey.in=" + DEFAULT_CONFIG_KEY + "," + UPDATED_CONFIG_KEY);

        // Get all the configList where configKey equals to UPDATED_CONFIG_KEY
        defaultConfigShouldNotBeFound("configKey.in=" + UPDATED_CONFIG_KEY);
    }

    @Test
    @Transactional
    public void getAllConfigsByConfigKeyIsNullOrNotNull() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where configKey is not null
        defaultConfigShouldBeFound("configKey.specified=true");

        // Get all the configList where configKey is null
        defaultConfigShouldNotBeFound("configKey.specified=false");
    }

    @Test
    @Transactional
    public void getAllConfigsByConfigValueIsEqualToSomething() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where configValue equals to DEFAULT_CONFIG_VALUE
        defaultConfigShouldBeFound("configValue.equals=" + DEFAULT_CONFIG_VALUE);

        // Get all the configList where configValue equals to UPDATED_CONFIG_VALUE
        defaultConfigShouldNotBeFound("configValue.equals=" + UPDATED_CONFIG_VALUE);
    }

    @Test
    @Transactional
    public void getAllConfigsByConfigValueIsInShouldWork() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where configValue in DEFAULT_CONFIG_VALUE or UPDATED_CONFIG_VALUE
        defaultConfigShouldBeFound("configValue.in=" + DEFAULT_CONFIG_VALUE + "," + UPDATED_CONFIG_VALUE);

        // Get all the configList where configValue equals to UPDATED_CONFIG_VALUE
        defaultConfigShouldNotBeFound("configValue.in=" + UPDATED_CONFIG_VALUE);
    }

    @Test
    @Transactional
    public void getAllConfigsByConfigValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where configValue is not null
        defaultConfigShouldBeFound("configValue.specified=true");

        // Get all the configList where configValue is null
        defaultConfigShouldNotBeFound("configValue.specified=false");
    }

    @Test
    @Transactional
    public void getAllConfigsBySyncConfigFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where syncConfigFlag equals to DEFAULT_SYNC_CONFIG_FLAG
        defaultConfigShouldBeFound("syncConfigFlag.equals=" + DEFAULT_SYNC_CONFIG_FLAG);

        // Get all the configList where syncConfigFlag equals to UPDATED_SYNC_CONFIG_FLAG
        defaultConfigShouldNotBeFound("syncConfigFlag.equals=" + UPDATED_SYNC_CONFIG_FLAG);
    }

    @Test
    @Transactional
    public void getAllConfigsBySyncConfigFlagIsInShouldWork() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where syncConfigFlag in DEFAULT_SYNC_CONFIG_FLAG or UPDATED_SYNC_CONFIG_FLAG
        defaultConfigShouldBeFound("syncConfigFlag.in=" + DEFAULT_SYNC_CONFIG_FLAG + "," + UPDATED_SYNC_CONFIG_FLAG);

        // Get all the configList where syncConfigFlag equals to UPDATED_SYNC_CONFIG_FLAG
        defaultConfigShouldNotBeFound("syncConfigFlag.in=" + UPDATED_SYNC_CONFIG_FLAG);
    }

    @Test
    @Transactional
    public void getAllConfigsBySyncConfigFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where syncConfigFlag is not null
        defaultConfigShouldBeFound("syncConfigFlag.specified=true");

        // Get all the configList where syncConfigFlag is null
        defaultConfigShouldNotBeFound("syncConfigFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllConfigsByAllowChangeSyncConfigFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where allowChangeSyncConfigFlag equals to DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG
        defaultConfigShouldBeFound("allowChangeSyncConfigFlag.equals=" + DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG);

        // Get all the configList where allowChangeSyncConfigFlag equals to UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG
        defaultConfigShouldNotBeFound("allowChangeSyncConfigFlag.equals=" + UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG);
    }

    @Test
    @Transactional
    public void getAllConfigsByAllowChangeSyncConfigFlagIsInShouldWork() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where allowChangeSyncConfigFlag in DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG or UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG
        defaultConfigShouldBeFound("allowChangeSyncConfigFlag.in=" + DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG + "," + UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG);

        // Get all the configList where allowChangeSyncConfigFlag equals to UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG
        defaultConfigShouldNotBeFound("allowChangeSyncConfigFlag.in=" + UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG);
    }

    @Test
    @Transactional
    public void getAllConfigsByAllowChangeSyncConfigFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where allowChangeSyncConfigFlag is not null
        defaultConfigShouldBeFound("allowChangeSyncConfigFlag.specified=true");

        // Get all the configList where allowChangeSyncConfigFlag is null
        defaultConfigShouldNotBeFound("allowChangeSyncConfigFlag.specified=false");
    }

    @Test
    @Transactional
    public void getAllConfigsByUpdateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where updateDate equals to DEFAULT_UPDATE_DATE
        defaultConfigShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

        // Get all the configList where updateDate equals to UPDATED_UPDATE_DATE
        defaultConfigShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllConfigsByUpdateDateIsInShouldWork() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where updateDate in DEFAULT_UPDATE_DATE or UPDATED_UPDATE_DATE
        defaultConfigShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

        // Get all the configList where updateDate equals to UPDATED_UPDATE_DATE
        defaultConfigShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllConfigsByUpdateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where updateDate is not null
        defaultConfigShouldBeFound("updateDate.specified=true");

        // Get all the configList where updateDate is null
        defaultConfigShouldNotBeFound("updateDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllConfigsByUpdateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where updateDate greater than or equals to DEFAULT_UPDATE_DATE
        defaultConfigShouldBeFound("updateDate.greaterOrEqualThan=" + DEFAULT_UPDATE_DATE);

        // Get all the configList where updateDate greater than or equals to UPDATED_UPDATE_DATE
        defaultConfigShouldNotBeFound("updateDate.greaterOrEqualThan=" + UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllConfigsByUpdateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where updateDate less than or equals to DEFAULT_UPDATE_DATE
        defaultConfigShouldNotBeFound("updateDate.lessThan=" + DEFAULT_UPDATE_DATE);

        // Get all the configList where updateDate less than or equals to UPDATED_UPDATE_DATE
        defaultConfigShouldBeFound("updateDate.lessThan=" + UPDATED_UPDATE_DATE);
    }


    @Test
    @Transactional
    public void getAllConfigsByChannelIsEqualToSomething() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where channel equals to DEFAULT_CHANNEL
        defaultConfigShouldBeFound("channel.equals=" + DEFAULT_CHANNEL);

        // Get all the configList where channel equals to UPDATED_CHANNEL
        defaultConfigShouldNotBeFound("channel.equals=" + UPDATED_CHANNEL);
    }

    @Test
    @Transactional
    public void getAllConfigsByChannelIsInShouldWork() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where channel in DEFAULT_CHANNEL or UPDATED_CHANNEL
        defaultConfigShouldBeFound("channel.in=" + DEFAULT_CHANNEL + "," + UPDATED_CHANNEL);

        // Get all the configList where channel equals to UPDATED_CHANNEL
        defaultConfigShouldNotBeFound("channel.in=" + UPDATED_CHANNEL);
    }

    @Test
    @Transactional
    public void getAllConfigsByChannelIsNullOrNotNull() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        // Get all the configList where channel is not null
        defaultConfigShouldBeFound("channel.specified=true");

        // Get all the configList where channel is null
        defaultConfigShouldNotBeFound("channel.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultConfigShouldBeFound(String filter) throws Exception {
        restConfigMockMvc.perform(get("/api/configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(config.getId())))
            .andExpect(jsonPath("$.[*].configKey").value(hasItem(DEFAULT_CONFIG_KEY.toString())))
            .andExpect(jsonPath("$.[*].configValue").value(hasItem(DEFAULT_CONFIG_VALUE.toString())))
            .andExpect(jsonPath("$.[*].syncConfigFlag").value(hasItem(DEFAULT_SYNC_CONFIG_FLAG.toString())))
            .andExpect(jsonPath("$.[*].allowChangeSyncConfigFlag").value(hasItem(DEFAULT_ALLOW_CHANGE_SYNC_CONFIG_FLAG.booleanValue())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE))))
            .andExpect(jsonPath("$.[*].channel").value(hasItem(DEFAULT_CHANNEL.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultConfigShouldNotBeFound(String filter) throws Exception {
        restConfigMockMvc.perform(get("/api/configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingConfig() throws Exception {
        // Get the config
        restConfigMockMvc.perform(get("/api/configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConfig() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        int databaseSizeBeforeUpdate = configRepository.findAll().size();

        // Update the config
        Config updatedConfig = configRepository.findById(config.getId()).get();
        // Disconnect from session so that the updates on updatedConfig are not directly saved in db
        em.detach(updatedConfig);
        updatedConfig
            .configValue(UPDATED_CONFIG_VALUE)
            .syncConfigFlag(UPDATED_SYNC_CONFIG_FLAG)
            .allowChangeSyncConfigFlag(UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG)
            .updateDate(UPDATED_UPDATE_DATE)
            .channel(UPDATED_CHANNEL);
        ConfigDTO configDTO = configMapper.toDto(updatedConfig);

        restConfigMockMvc.perform(put("/api/configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configDTO)))
            .andExpect(status().isOk());

        // Validate the Config in the database
        List<Config> configList = configRepository.findAll();
        assertThat(configList).hasSize(databaseSizeBeforeUpdate);
        Config testConfig = configList.get(configList.size() - 1);
        assertThat(testConfig.getConfigValue()).isEqualTo(UPDATED_CONFIG_VALUE);
        assertThat(testConfig.getSyncConfigFlag()).isEqualTo(UPDATED_SYNC_CONFIG_FLAG);
        assertThat(testConfig.isAllowChangeSyncConfigFlag()).isEqualTo(UPDATED_ALLOW_CHANGE_SYNC_CONFIG_FLAG);
        assertThat(testConfig.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testConfig.getChannel()).isEqualTo(UPDATED_CHANNEL);
    }

    @Test
    @Transactional
    public void updateNonExistingConfig() throws Exception {
        int databaseSizeBeforeUpdate = configRepository.findAll().size();

        // Create the Config
        ConfigDTO configDTO = configMapper.toDto(config);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConfigMockMvc.perform(put("/api/configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Config in the database
        List<Config> configList = configRepository.findAll();
        assertThat(configList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteConfig() throws Exception {
        // Initialize the database
        configRepository.saveAndFlush(config);

        int databaseSizeBeforeDelete = configRepository.findAll().size();

        // Get the config
        restConfigMockMvc.perform(delete("/api/configs/{id}", config.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Config> configList = configRepository.findAll();
        assertThat(configList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Config.class);
        Config config1 = new Config();
        config1.setId("1");
        Config config2 = new Config();
        config2.setId(config1.getId());
        assertThat(config1).isEqualTo(config2);
        config2.setId("2");
        assertThat(config1).isNotEqualTo(config2);
        config1.setId(null);
        assertThat(config1).isNotEqualTo(config2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigDTO.class);
        ConfigDTO configDTO1 = new ConfigDTO();
        configDTO1.setId("1");
        ConfigDTO configDTO2 = new ConfigDTO();
        assertThat(configDTO1).isNotEqualTo(configDTO2);
        configDTO2.setId(configDTO1.getId());
        assertThat(configDTO1).isEqualTo(configDTO2);
        configDTO2.setId("2");
        assertThat(configDTO1).isNotEqualTo(configDTO2);
        configDTO1.setId(null);
        assertThat(configDTO1).isNotEqualTo(configDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(configMapper.fromId("42").getId()).isEqualTo("42");
        assertThat(configMapper.fromId(null)).isNull();
    }
}
