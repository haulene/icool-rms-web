package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.Sale;
import com.gcs.flexba.rmsweb.repository.SaleRepository;
import com.gcs.flexba.rmsweb.service.SaleService;
import com.gcs.flexba.rmsweb.service.dto.SaleDTO;
import com.gcs.flexba.rmsweb.service.mapper.SaleMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.SaleCriteria;
import com.gcs.flexba.rmsweb.service.SaleQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SaleResource REST controller.
 *
 * @see SaleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class SaleResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_IS_BEGIN = 1;
    private static final Integer UPDATED_IS_BEGIN = 2;

    private static final Integer DEFAULT_IS_END = 1;
    private static final Integer UPDATED_IS_END = 2;

    private static final ZonedDateTime DEFAULT_BEGIN = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_BEGIN = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_END = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_TRANGTHAI = 1;
    private static final Integer UPDATED_TRANGTHAI = 2;

    private static final Integer DEFAULT_SALE_TYPE = 1;
    private static final Integer UPDATED_SALE_TYPE = 2;

    @Autowired
    private SaleRepository saleRepository;


    @Autowired
    private SaleMapper saleMapper;
    

    @Autowired
    private SaleService saleService;

    @Autowired
    private SaleQueryService saleQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSaleMockMvc;

    private Sale sale;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SaleResource saleResource = new SaleResource(saleService, saleQueryService);
        this.restSaleMockMvc = MockMvcBuilders.standaloneSetup(saleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sale createEntity(EntityManager em) {
        Sale sale = new Sale()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .isBegin(DEFAULT_IS_BEGIN)
            .isEnd(DEFAULT_IS_END)
            .begin(DEFAULT_BEGIN)
            .end(DEFAULT_END)
            .trangthai(DEFAULT_TRANGTHAI)
            .saleType(DEFAULT_SALE_TYPE);
        return sale;
    }

    @Before
    public void initTest() {
        sale = createEntity(em);
    }

    @Test
    @Transactional
    public void createSale() throws Exception {
        int databaseSizeBeforeCreate = saleRepository.findAll().size();

        // Create the Sale
        SaleDTO saleDTO = saleMapper.toDto(sale);
        restSaleMockMvc.perform(post("/api/sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saleDTO)))
            .andExpect(status().isCreated());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeCreate + 1);
        Sale testSale = saleList.get(saleList.size() - 1);
        assertThat(testSale.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testSale.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSale.getIsBegin()).isEqualTo(DEFAULT_IS_BEGIN);
        assertThat(testSale.getIsEnd()).isEqualTo(DEFAULT_IS_END);
        assertThat(testSale.getBegin()).isEqualTo(DEFAULT_BEGIN);
        assertThat(testSale.getEnd()).isEqualTo(DEFAULT_END);
        assertThat(testSale.getTrangthai()).isEqualTo(DEFAULT_TRANGTHAI);
        assertThat(testSale.getSaleType()).isEqualTo(DEFAULT_SALE_TYPE);
    }


    @Test
    @Transactional
    public void getAllSalesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where code equals to DEFAULT_CODE
        defaultSaleShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the saleList where code equals to UPDATED_CODE
        defaultSaleShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllSalesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where code in DEFAULT_CODE or UPDATED_CODE
        defaultSaleShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the saleList where code equals to UPDATED_CODE
        defaultSaleShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllSalesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where code is not null
        defaultSaleShouldBeFound("code.specified=true");

        // Get all the saleList where code is null
        defaultSaleShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where name equals to DEFAULT_NAME
        defaultSaleShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the saleList where name equals to UPDATED_NAME
        defaultSaleShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSalesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where name in DEFAULT_NAME or UPDATED_NAME
        defaultSaleShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the saleList where name equals to UPDATED_NAME
        defaultSaleShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSalesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where name is not null
        defaultSaleShouldBeFound("name.specified=true");

        // Get all the saleList where name is null
        defaultSaleShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByIsBeginIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isBegin equals to DEFAULT_IS_BEGIN
        defaultSaleShouldBeFound("isBegin.equals=" + DEFAULT_IS_BEGIN);

        // Get all the saleList where isBegin equals to UPDATED_IS_BEGIN
        defaultSaleShouldNotBeFound("isBegin.equals=" + UPDATED_IS_BEGIN);
    }

    @Test
    @Transactional
    public void getAllSalesByIsBeginIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isBegin in DEFAULT_IS_BEGIN or UPDATED_IS_BEGIN
        defaultSaleShouldBeFound("isBegin.in=" + DEFAULT_IS_BEGIN + "," + UPDATED_IS_BEGIN);

        // Get all the saleList where isBegin equals to UPDATED_IS_BEGIN
        defaultSaleShouldNotBeFound("isBegin.in=" + UPDATED_IS_BEGIN);
    }

    @Test
    @Transactional
    public void getAllSalesByIsBeginIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isBegin is not null
        defaultSaleShouldBeFound("isBegin.specified=true");

        // Get all the saleList where isBegin is null
        defaultSaleShouldNotBeFound("isBegin.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByIsBeginIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isBegin greater than or equals to DEFAULT_IS_BEGIN
        defaultSaleShouldBeFound("isBegin.greaterOrEqualThan=" + DEFAULT_IS_BEGIN);

        // Get all the saleList where isBegin greater than or equals to UPDATED_IS_BEGIN
        defaultSaleShouldNotBeFound("isBegin.greaterOrEqualThan=" + UPDATED_IS_BEGIN);
    }

    @Test
    @Transactional
    public void getAllSalesByIsBeginIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isBegin less than or equals to DEFAULT_IS_BEGIN
        defaultSaleShouldNotBeFound("isBegin.lessThan=" + DEFAULT_IS_BEGIN);

        // Get all the saleList where isBegin less than or equals to UPDATED_IS_BEGIN
        defaultSaleShouldBeFound("isBegin.lessThan=" + UPDATED_IS_BEGIN);
    }


    @Test
    @Transactional
    public void getAllSalesByIsEndIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isEnd equals to DEFAULT_IS_END
        defaultSaleShouldBeFound("isEnd.equals=" + DEFAULT_IS_END);

        // Get all the saleList where isEnd equals to UPDATED_IS_END
        defaultSaleShouldNotBeFound("isEnd.equals=" + UPDATED_IS_END);
    }

    @Test
    @Transactional
    public void getAllSalesByIsEndIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isEnd in DEFAULT_IS_END or UPDATED_IS_END
        defaultSaleShouldBeFound("isEnd.in=" + DEFAULT_IS_END + "," + UPDATED_IS_END);

        // Get all the saleList where isEnd equals to UPDATED_IS_END
        defaultSaleShouldNotBeFound("isEnd.in=" + UPDATED_IS_END);
    }

    @Test
    @Transactional
    public void getAllSalesByIsEndIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isEnd is not null
        defaultSaleShouldBeFound("isEnd.specified=true");

        // Get all the saleList where isEnd is null
        defaultSaleShouldNotBeFound("isEnd.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByIsEndIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isEnd greater than or equals to DEFAULT_IS_END
        defaultSaleShouldBeFound("isEnd.greaterOrEqualThan=" + DEFAULT_IS_END);

        // Get all the saleList where isEnd greater than or equals to UPDATED_IS_END
        defaultSaleShouldNotBeFound("isEnd.greaterOrEqualThan=" + UPDATED_IS_END);
    }

    @Test
    @Transactional
    public void getAllSalesByIsEndIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where isEnd less than or equals to DEFAULT_IS_END
        defaultSaleShouldNotBeFound("isEnd.lessThan=" + DEFAULT_IS_END);

        // Get all the saleList where isEnd less than or equals to UPDATED_IS_END
        defaultSaleShouldBeFound("isEnd.lessThan=" + UPDATED_IS_END);
    }


    @Test
    @Transactional
    public void getAllSalesByBeginIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where begin equals to DEFAULT_BEGIN
        defaultSaleShouldBeFound("begin.equals=" + DEFAULT_BEGIN);

        // Get all the saleList where begin equals to UPDATED_BEGIN
        defaultSaleShouldNotBeFound("begin.equals=" + UPDATED_BEGIN);
    }

    @Test
    @Transactional
    public void getAllSalesByBeginIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where begin in DEFAULT_BEGIN or UPDATED_BEGIN
        defaultSaleShouldBeFound("begin.in=" + DEFAULT_BEGIN + "," + UPDATED_BEGIN);

        // Get all the saleList where begin equals to UPDATED_BEGIN
        defaultSaleShouldNotBeFound("begin.in=" + UPDATED_BEGIN);
    }

    @Test
    @Transactional
    public void getAllSalesByBeginIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where begin is not null
        defaultSaleShouldBeFound("begin.specified=true");

        // Get all the saleList where begin is null
        defaultSaleShouldNotBeFound("begin.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByBeginIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where begin greater than or equals to DEFAULT_BEGIN
        defaultSaleShouldBeFound("begin.greaterOrEqualThan=" + DEFAULT_BEGIN);

        // Get all the saleList where begin greater than or equals to UPDATED_BEGIN
        defaultSaleShouldNotBeFound("begin.greaterOrEqualThan=" + UPDATED_BEGIN);
    }

    @Test
    @Transactional
    public void getAllSalesByBeginIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where begin less than or equals to DEFAULT_BEGIN
        defaultSaleShouldNotBeFound("begin.lessThan=" + DEFAULT_BEGIN);

        // Get all the saleList where begin less than or equals to UPDATED_BEGIN
        defaultSaleShouldBeFound("begin.lessThan=" + UPDATED_BEGIN);
    }


    @Test
    @Transactional
    public void getAllSalesByEndIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where end equals to DEFAULT_END
        defaultSaleShouldBeFound("end.equals=" + DEFAULT_END);

        // Get all the saleList where end equals to UPDATED_END
        defaultSaleShouldNotBeFound("end.equals=" + UPDATED_END);
    }

    @Test
    @Transactional
    public void getAllSalesByEndIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where end in DEFAULT_END or UPDATED_END
        defaultSaleShouldBeFound("end.in=" + DEFAULT_END + "," + UPDATED_END);

        // Get all the saleList where end equals to UPDATED_END
        defaultSaleShouldNotBeFound("end.in=" + UPDATED_END);
    }

    @Test
    @Transactional
    public void getAllSalesByEndIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where end is not null
        defaultSaleShouldBeFound("end.specified=true");

        // Get all the saleList where end is null
        defaultSaleShouldNotBeFound("end.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByEndIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where end greater than or equals to DEFAULT_END
        defaultSaleShouldBeFound("end.greaterOrEqualThan=" + DEFAULT_END);

        // Get all the saleList where end greater than or equals to UPDATED_END
        defaultSaleShouldNotBeFound("end.greaterOrEqualThan=" + UPDATED_END);
    }

    @Test
    @Transactional
    public void getAllSalesByEndIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where end less than or equals to DEFAULT_END
        defaultSaleShouldNotBeFound("end.lessThan=" + DEFAULT_END);

        // Get all the saleList where end less than or equals to UPDATED_END
        defaultSaleShouldBeFound("end.lessThan=" + UPDATED_END);
    }


    @Test
    @Transactional
    public void getAllSalesByTrangthaiIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where trangthai equals to DEFAULT_TRANGTHAI
        defaultSaleShouldBeFound("trangthai.equals=" + DEFAULT_TRANGTHAI);

        // Get all the saleList where trangthai equals to UPDATED_TRANGTHAI
        defaultSaleShouldNotBeFound("trangthai.equals=" + UPDATED_TRANGTHAI);
    }

    @Test
    @Transactional
    public void getAllSalesByTrangthaiIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where trangthai in DEFAULT_TRANGTHAI or UPDATED_TRANGTHAI
        defaultSaleShouldBeFound("trangthai.in=" + DEFAULT_TRANGTHAI + "," + UPDATED_TRANGTHAI);

        // Get all the saleList where trangthai equals to UPDATED_TRANGTHAI
        defaultSaleShouldNotBeFound("trangthai.in=" + UPDATED_TRANGTHAI);
    }

    @Test
    @Transactional
    public void getAllSalesByTrangthaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where trangthai is not null
        defaultSaleShouldBeFound("trangthai.specified=true");

        // Get all the saleList where trangthai is null
        defaultSaleShouldNotBeFound("trangthai.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesByTrangthaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where trangthai greater than or equals to DEFAULT_TRANGTHAI
        defaultSaleShouldBeFound("trangthai.greaterOrEqualThan=" + DEFAULT_TRANGTHAI);

        // Get all the saleList where trangthai greater than or equals to UPDATED_TRANGTHAI
        defaultSaleShouldNotBeFound("trangthai.greaterOrEqualThan=" + UPDATED_TRANGTHAI);
    }

    @Test
    @Transactional
    public void getAllSalesByTrangthaiIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where trangthai less than or equals to DEFAULT_TRANGTHAI
        defaultSaleShouldNotBeFound("trangthai.lessThan=" + DEFAULT_TRANGTHAI);

        // Get all the saleList where trangthai less than or equals to UPDATED_TRANGTHAI
        defaultSaleShouldBeFound("trangthai.lessThan=" + UPDATED_TRANGTHAI);
    }


    @Test
    @Transactional
    public void getAllSalesBySaleTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where saleType equals to DEFAULT_SALE_TYPE
        defaultSaleShouldBeFound("saleType.equals=" + DEFAULT_SALE_TYPE);

        // Get all the saleList where saleType equals to UPDATED_SALE_TYPE
        defaultSaleShouldNotBeFound("saleType.equals=" + UPDATED_SALE_TYPE);
    }

    @Test
    @Transactional
    public void getAllSalesBySaleTypeIsInShouldWork() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where saleType in DEFAULT_SALE_TYPE or UPDATED_SALE_TYPE
        defaultSaleShouldBeFound("saleType.in=" + DEFAULT_SALE_TYPE + "," + UPDATED_SALE_TYPE);

        // Get all the saleList where saleType equals to UPDATED_SALE_TYPE
        defaultSaleShouldNotBeFound("saleType.in=" + UPDATED_SALE_TYPE);
    }

    @Test
    @Transactional
    public void getAllSalesBySaleTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where saleType is not null
        defaultSaleShouldBeFound("saleType.specified=true");

        // Get all the saleList where saleType is null
        defaultSaleShouldNotBeFound("saleType.specified=false");
    }

    @Test
    @Transactional
    public void getAllSalesBySaleTypeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where saleType greater than or equals to DEFAULT_SALE_TYPE
        defaultSaleShouldBeFound("saleType.greaterOrEqualThan=" + DEFAULT_SALE_TYPE);

        // Get all the saleList where saleType greater than or equals to UPDATED_SALE_TYPE
        defaultSaleShouldNotBeFound("saleType.greaterOrEqualThan=" + UPDATED_SALE_TYPE);
    }

    @Test
    @Transactional
    public void getAllSalesBySaleTypeIsLessThanSomething() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        // Get all the saleList where saleType less than or equals to DEFAULT_SALE_TYPE
        defaultSaleShouldNotBeFound("saleType.lessThan=" + DEFAULT_SALE_TYPE);

        // Get all the saleList where saleType less than or equals to UPDATED_SALE_TYPE
        defaultSaleShouldBeFound("saleType.lessThan=" + UPDATED_SALE_TYPE);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSaleShouldBeFound(String filter) throws Exception {
        restSaleMockMvc.perform(get("/api/sales?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sale.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].isBegin").value(hasItem(DEFAULT_IS_BEGIN)))
            .andExpect(jsonPath("$.[*].isEnd").value(hasItem(DEFAULT_IS_END)))
            .andExpect(jsonPath("$.[*].begin").value(hasItem(sameInstant(DEFAULT_BEGIN))))
            .andExpect(jsonPath("$.[*].end").value(hasItem(sameInstant(DEFAULT_END))))
            .andExpect(jsonPath("$.[*].trangthai").value(hasItem(DEFAULT_TRANGTHAI)))
            .andExpect(jsonPath("$.[*].saleType").value(hasItem(DEFAULT_SALE_TYPE)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSaleShouldNotBeFound(String filter) throws Exception {
        restSaleMockMvc.perform(get("/api/sales?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingSale() throws Exception {
        // Get the sale
        restSaleMockMvc.perform(get("/api/sales/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSale() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        int databaseSizeBeforeUpdate = saleRepository.findAll().size();

        // Update the sale
        Sale updatedSale = saleRepository.findById(sale.getId()).get();
        // Disconnect from session so that the updates on updatedSale are not directly saved in db
        em.detach(updatedSale);
        updatedSale
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .isBegin(UPDATED_IS_BEGIN)
            .isEnd(UPDATED_IS_END)
            .begin(UPDATED_BEGIN)
            .end(UPDATED_END)
            .trangthai(UPDATED_TRANGTHAI)
            .saleType(UPDATED_SALE_TYPE);
        SaleDTO saleDTO = saleMapper.toDto(updatedSale);

        restSaleMockMvc.perform(put("/api/sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saleDTO)))
            .andExpect(status().isOk());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeUpdate);
        Sale testSale = saleList.get(saleList.size() - 1);
        assertThat(testSale.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testSale.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSale.getIsBegin()).isEqualTo(UPDATED_IS_BEGIN);
        assertThat(testSale.getIsEnd()).isEqualTo(UPDATED_IS_END);
        assertThat(testSale.getBegin()).isEqualTo(UPDATED_BEGIN);
        assertThat(testSale.getEnd()).isEqualTo(UPDATED_END);
        assertThat(testSale.getTrangthai()).isEqualTo(UPDATED_TRANGTHAI);
        assertThat(testSale.getSaleType()).isEqualTo(UPDATED_SALE_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingSale() throws Exception {
        int databaseSizeBeforeUpdate = saleRepository.findAll().size();

        // Create the Sale
        SaleDTO saleDTO = saleMapper.toDto(sale);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSaleMockMvc.perform(put("/api/sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sale in the database
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSale() throws Exception {
        // Initialize the database
        saleRepository.saveAndFlush(sale);

        int databaseSizeBeforeDelete = saleRepository.findAll().size();

        // Get the sale
        restSaleMockMvc.perform(delete("/api/sales/{id}", sale.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Sale> saleList = saleRepository.findAll();
        assertThat(saleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
