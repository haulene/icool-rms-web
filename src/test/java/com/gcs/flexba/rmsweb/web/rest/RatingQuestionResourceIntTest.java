package com.gcs.flexba.rmsweb.web.rest;

import com.gcs.flexba.rmsweb.FlexbaRmsWebApp;

import com.gcs.flexba.rmsweb.domain.RatingQuestion;
import com.gcs.flexba.rmsweb.repository.RatingQuestionRepository;
import com.gcs.flexba.rmsweb.service.RatingQuestionService;
import com.gcs.flexba.rmsweb.service.dto.RatingQuestionDTO;
import com.gcs.flexba.rmsweb.service.mapper.RatingQuestionMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.ExceptionTranslator;
import com.gcs.flexba.rmsweb.service.dto.RatingQuestionCriteria;
import com.gcs.flexba.rmsweb.service.RatingQuestionQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.gcs.flexba.rmsweb.web.rest.TestUtil.sameInstant;
import static com.gcs.flexba.rmsweb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RatingQuestionResource REST controller.
 *
 * @see RatingQuestionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlexbaRmsWebApp.class)
public class RatingQuestionResourceIntTest {

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    private static final Integer DEFAULT_START_NUM_STAR = 1;
    private static final Integer UPDATED_START_NUM_STAR = 2;

    private static final Integer DEFAULT_END_NUM_STAR = 1;
    private static final Integer UPDATED_END_NUM_STAR = 2;

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_ANSWER_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_ANSWER_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER_CATEGORY = "BBBBBBBBBB";

    @Autowired
    private RatingQuestionRepository ratingQuestionRepository;


    @Autowired
    private RatingQuestionMapper ratingQuestionMapper;
    

    @Autowired
    private RatingQuestionService ratingQuestionService;

    @Autowired
    private RatingQuestionQueryService ratingQuestionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRatingQuestionMockMvc;

    private RatingQuestion ratingQuestion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RatingQuestionResource ratingQuestionResource = new RatingQuestionResource(ratingQuestionService, ratingQuestionQueryService);
        this.restRatingQuestionMockMvc = MockMvcBuilders.standaloneSetup(ratingQuestionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RatingQuestion createEntity(EntityManager em) {
        RatingQuestion ratingQuestion = new RatingQuestion()
            .content(DEFAULT_CONTENT)
            .type(DEFAULT_TYPE)
            .startNumStar(DEFAULT_START_NUM_STAR)
            .endNumStar(DEFAULT_END_NUM_STAR)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .answerType(DEFAULT_ANSWER_TYPE)
            .answerCategory(DEFAULT_ANSWER_CATEGORY);
        return ratingQuestion;
    }

    @Before
    public void initTest() {
        ratingQuestion = createEntity(em);
    }

    @Test
    @Transactional
    public void createRatingQuestion() throws Exception {
        int databaseSizeBeforeCreate = ratingQuestionRepository.findAll().size();

        // Create the RatingQuestion
        RatingQuestionDTO ratingQuestionDTO = ratingQuestionMapper.toDto(ratingQuestion);
        restRatingQuestionMockMvc.perform(post("/api/rating-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingQuestionDTO)))
            .andExpect(status().isCreated());

        // Validate the RatingQuestion in the database
        List<RatingQuestion> ratingQuestionList = ratingQuestionRepository.findAll();
        assertThat(ratingQuestionList).hasSize(databaseSizeBeforeCreate + 1);
        RatingQuestion testRatingQuestion = ratingQuestionList.get(ratingQuestionList.size() - 1);
        assertThat(testRatingQuestion.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testRatingQuestion.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testRatingQuestion.getStartNumStar()).isEqualTo(DEFAULT_START_NUM_STAR);
        assertThat(testRatingQuestion.getEndNumStar()).isEqualTo(DEFAULT_END_NUM_STAR);
        assertThat(testRatingQuestion.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testRatingQuestion.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testRatingQuestion.getAnswerType()).isEqualTo(DEFAULT_ANSWER_TYPE);
        assertThat(testRatingQuestion.getAnswerCategory()).isEqualTo(DEFAULT_ANSWER_CATEGORY);
    }

    @Test
    @Transactional
    public void createRatingQuestionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ratingQuestionRepository.findAll().size();

        // Create the RatingQuestion with an existing ID
        ratingQuestion.setId(1L);
        RatingQuestionDTO ratingQuestionDTO = ratingQuestionMapper.toDto(ratingQuestion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRatingQuestionMockMvc.perform(post("/api/rating-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingQuestionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RatingQuestion in the database
        List<RatingQuestion> ratingQuestionList = ratingQuestionRepository.findAll();
        assertThat(ratingQuestionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRatingQuestions() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList
        restRatingQuestionMockMvc.perform(get("/api/rating-questions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ratingQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].startNumStar").value(hasItem(DEFAULT_START_NUM_STAR)))
            .andExpect(jsonPath("$.[*].endNumStar").value(hasItem(DEFAULT_END_NUM_STAR)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(sameInstant(DEFAULT_UPDATED_DATE))))
            .andExpect(jsonPath("$.[*].answerType").value(hasItem(DEFAULT_ANSWER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].answerCategory").value(hasItem(DEFAULT_ANSWER_CATEGORY.toString())));
    }
    

    @Test
    @Transactional
    public void getRatingQuestion() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get the ratingQuestion
        restRatingQuestionMockMvc.perform(get("/api/rating-questions/{id}", ratingQuestion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ratingQuestion.getId().intValue()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.startNumStar").value(DEFAULT_START_NUM_STAR))
            .andExpect(jsonPath("$.endNumStar").value(DEFAULT_END_NUM_STAR))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.updatedDate").value(sameInstant(DEFAULT_UPDATED_DATE)))
            .andExpect(jsonPath("$.answerType").value(DEFAULT_ANSWER_TYPE.toString()))
            .andExpect(jsonPath("$.answerCategory").value(DEFAULT_ANSWER_CATEGORY.toString()));
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByContentIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where content equals to DEFAULT_CONTENT
        defaultRatingQuestionShouldBeFound("content.equals=" + DEFAULT_CONTENT);

        // Get all the ratingQuestionList where content equals to UPDATED_CONTENT
        defaultRatingQuestionShouldNotBeFound("content.equals=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByContentIsInShouldWork() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where content in DEFAULT_CONTENT or UPDATED_CONTENT
        defaultRatingQuestionShouldBeFound("content.in=" + DEFAULT_CONTENT + "," + UPDATED_CONTENT);

        // Get all the ratingQuestionList where content equals to UPDATED_CONTENT
        defaultRatingQuestionShouldNotBeFound("content.in=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByContentIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where content is not null
        defaultRatingQuestionShouldBeFound("content.specified=true");

        // Get all the ratingQuestionList where content is null
        defaultRatingQuestionShouldNotBeFound("content.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where type equals to DEFAULT_TYPE
        defaultRatingQuestionShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the ratingQuestionList where type equals to UPDATED_TYPE
        defaultRatingQuestionShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultRatingQuestionShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the ratingQuestionList where type equals to UPDATED_TYPE
        defaultRatingQuestionShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where type is not null
        defaultRatingQuestionShouldBeFound("type.specified=true");

        // Get all the ratingQuestionList where type is null
        defaultRatingQuestionShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByTypeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where type greater than or equals to DEFAULT_TYPE
        defaultRatingQuestionShouldBeFound("type.greaterOrEqualThan=" + DEFAULT_TYPE);

        // Get all the ratingQuestionList where type greater than or equals to UPDATED_TYPE
        defaultRatingQuestionShouldNotBeFound("type.greaterOrEqualThan=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByTypeIsLessThanSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where type less than or equals to DEFAULT_TYPE
        defaultRatingQuestionShouldNotBeFound("type.lessThan=" + DEFAULT_TYPE);

        // Get all the ratingQuestionList where type less than or equals to UPDATED_TYPE
        defaultRatingQuestionShouldBeFound("type.lessThan=" + UPDATED_TYPE);
    }


    @Test
    @Transactional
    public void getAllRatingQuestionsByStartNumStarIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where startNumStar equals to DEFAULT_START_NUM_STAR
        defaultRatingQuestionShouldBeFound("startNumStar.equals=" + DEFAULT_START_NUM_STAR);

        // Get all the ratingQuestionList where startNumStar equals to UPDATED_START_NUM_STAR
        defaultRatingQuestionShouldNotBeFound("startNumStar.equals=" + UPDATED_START_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByStartNumStarIsInShouldWork() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where startNumStar in DEFAULT_START_NUM_STAR or UPDATED_START_NUM_STAR
        defaultRatingQuestionShouldBeFound("startNumStar.in=" + DEFAULT_START_NUM_STAR + "," + UPDATED_START_NUM_STAR);

        // Get all the ratingQuestionList where startNumStar equals to UPDATED_START_NUM_STAR
        defaultRatingQuestionShouldNotBeFound("startNumStar.in=" + UPDATED_START_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByStartNumStarIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where startNumStar is not null
        defaultRatingQuestionShouldBeFound("startNumStar.specified=true");

        // Get all the ratingQuestionList where startNumStar is null
        defaultRatingQuestionShouldNotBeFound("startNumStar.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByStartNumStarIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where startNumStar greater than or equals to DEFAULT_START_NUM_STAR
        defaultRatingQuestionShouldBeFound("startNumStar.greaterOrEqualThan=" + DEFAULT_START_NUM_STAR);

        // Get all the ratingQuestionList where startNumStar greater than or equals to UPDATED_START_NUM_STAR
        defaultRatingQuestionShouldNotBeFound("startNumStar.greaterOrEqualThan=" + UPDATED_START_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByStartNumStarIsLessThanSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where startNumStar less than or equals to DEFAULT_START_NUM_STAR
        defaultRatingQuestionShouldNotBeFound("startNumStar.lessThan=" + DEFAULT_START_NUM_STAR);

        // Get all the ratingQuestionList where startNumStar less than or equals to UPDATED_START_NUM_STAR
        defaultRatingQuestionShouldBeFound("startNumStar.lessThan=" + UPDATED_START_NUM_STAR);
    }


    @Test
    @Transactional
    public void getAllRatingQuestionsByEndNumStarIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where endNumStar equals to DEFAULT_END_NUM_STAR
        defaultRatingQuestionShouldBeFound("endNumStar.equals=" + DEFAULT_END_NUM_STAR);

        // Get all the ratingQuestionList where endNumStar equals to UPDATED_END_NUM_STAR
        defaultRatingQuestionShouldNotBeFound("endNumStar.equals=" + UPDATED_END_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByEndNumStarIsInShouldWork() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where endNumStar in DEFAULT_END_NUM_STAR or UPDATED_END_NUM_STAR
        defaultRatingQuestionShouldBeFound("endNumStar.in=" + DEFAULT_END_NUM_STAR + "," + UPDATED_END_NUM_STAR);

        // Get all the ratingQuestionList where endNumStar equals to UPDATED_END_NUM_STAR
        defaultRatingQuestionShouldNotBeFound("endNumStar.in=" + UPDATED_END_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByEndNumStarIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where endNumStar is not null
        defaultRatingQuestionShouldBeFound("endNumStar.specified=true");

        // Get all the ratingQuestionList where endNumStar is null
        defaultRatingQuestionShouldNotBeFound("endNumStar.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByEndNumStarIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where endNumStar greater than or equals to DEFAULT_END_NUM_STAR
        defaultRatingQuestionShouldBeFound("endNumStar.greaterOrEqualThan=" + DEFAULT_END_NUM_STAR);

        // Get all the ratingQuestionList where endNumStar greater than or equals to UPDATED_END_NUM_STAR
        defaultRatingQuestionShouldNotBeFound("endNumStar.greaterOrEqualThan=" + UPDATED_END_NUM_STAR);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByEndNumStarIsLessThanSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where endNumStar less than or equals to DEFAULT_END_NUM_STAR
        defaultRatingQuestionShouldNotBeFound("endNumStar.lessThan=" + DEFAULT_END_NUM_STAR);

        // Get all the ratingQuestionList where endNumStar less than or equals to UPDATED_END_NUM_STAR
        defaultRatingQuestionShouldBeFound("endNumStar.lessThan=" + UPDATED_END_NUM_STAR);
    }


    @Test
    @Transactional
    public void getAllRatingQuestionsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where createdDate equals to DEFAULT_CREATED_DATE
        defaultRatingQuestionShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the ratingQuestionList where createdDate equals to UPDATED_CREATED_DATE
        defaultRatingQuestionShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultRatingQuestionShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the ratingQuestionList where createdDate equals to UPDATED_CREATED_DATE
        defaultRatingQuestionShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where createdDate is not null
        defaultRatingQuestionShouldBeFound("createdDate.specified=true");

        // Get all the ratingQuestionList where createdDate is null
        defaultRatingQuestionShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where createdDate greater than or equals to DEFAULT_CREATED_DATE
        defaultRatingQuestionShouldBeFound("createdDate.greaterOrEqualThan=" + DEFAULT_CREATED_DATE);

        // Get all the ratingQuestionList where createdDate greater than or equals to UPDATED_CREATED_DATE
        defaultRatingQuestionShouldNotBeFound("createdDate.greaterOrEqualThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where createdDate less than or equals to DEFAULT_CREATED_DATE
        defaultRatingQuestionShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the ratingQuestionList where createdDate less than or equals to UPDATED_CREATED_DATE
        defaultRatingQuestionShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllRatingQuestionsByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultRatingQuestionShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the ratingQuestionList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultRatingQuestionShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultRatingQuestionShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the ratingQuestionList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultRatingQuestionShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where updatedDate is not null
        defaultRatingQuestionShouldBeFound("updatedDate.specified=true");

        // Get all the ratingQuestionList where updatedDate is null
        defaultRatingQuestionShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByUpdatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where updatedDate greater than or equals to DEFAULT_UPDATED_DATE
        defaultRatingQuestionShouldBeFound("updatedDate.greaterOrEqualThan=" + DEFAULT_UPDATED_DATE);

        // Get all the ratingQuestionList where updatedDate greater than or equals to UPDATED_UPDATED_DATE
        defaultRatingQuestionShouldNotBeFound("updatedDate.greaterOrEqualThan=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByUpdatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where updatedDate less than or equals to DEFAULT_UPDATED_DATE
        defaultRatingQuestionShouldNotBeFound("updatedDate.lessThan=" + DEFAULT_UPDATED_DATE);

        // Get all the ratingQuestionList where updatedDate less than or equals to UPDATED_UPDATED_DATE
        defaultRatingQuestionShouldBeFound("updatedDate.lessThan=" + UPDATED_UPDATED_DATE);
    }


    @Test
    @Transactional
    public void getAllRatingQuestionsByAnswerTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where answerType equals to DEFAULT_ANSWER_TYPE
        defaultRatingQuestionShouldBeFound("answerType.equals=" + DEFAULT_ANSWER_TYPE);

        // Get all the ratingQuestionList where answerType equals to UPDATED_ANSWER_TYPE
        defaultRatingQuestionShouldNotBeFound("answerType.equals=" + UPDATED_ANSWER_TYPE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByAnswerTypeIsInShouldWork() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where answerType in DEFAULT_ANSWER_TYPE or UPDATED_ANSWER_TYPE
        defaultRatingQuestionShouldBeFound("answerType.in=" + DEFAULT_ANSWER_TYPE + "," + UPDATED_ANSWER_TYPE);

        // Get all the ratingQuestionList where answerType equals to UPDATED_ANSWER_TYPE
        defaultRatingQuestionShouldNotBeFound("answerType.in=" + UPDATED_ANSWER_TYPE);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByAnswerTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where answerType is not null
        defaultRatingQuestionShouldBeFound("answerType.specified=true");

        // Get all the ratingQuestionList where answerType is null
        defaultRatingQuestionShouldNotBeFound("answerType.specified=false");
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByAnswerCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where answerCategory equals to DEFAULT_ANSWER_CATEGORY
        defaultRatingQuestionShouldBeFound("answerCategory.equals=" + DEFAULT_ANSWER_CATEGORY);

        // Get all the ratingQuestionList where answerCategory equals to UPDATED_ANSWER_CATEGORY
        defaultRatingQuestionShouldNotBeFound("answerCategory.equals=" + UPDATED_ANSWER_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByAnswerCategoryIsInShouldWork() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where answerCategory in DEFAULT_ANSWER_CATEGORY or UPDATED_ANSWER_CATEGORY
        defaultRatingQuestionShouldBeFound("answerCategory.in=" + DEFAULT_ANSWER_CATEGORY + "," + UPDATED_ANSWER_CATEGORY);

        // Get all the ratingQuestionList where answerCategory equals to UPDATED_ANSWER_CATEGORY
        defaultRatingQuestionShouldNotBeFound("answerCategory.in=" + UPDATED_ANSWER_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllRatingQuestionsByAnswerCategoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        // Get all the ratingQuestionList where answerCategory is not null
        defaultRatingQuestionShouldBeFound("answerCategory.specified=true");

        // Get all the ratingQuestionList where answerCategory is null
        defaultRatingQuestionShouldNotBeFound("answerCategory.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultRatingQuestionShouldBeFound(String filter) throws Exception {
        restRatingQuestionMockMvc.perform(get("/api/rating-questions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ratingQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].startNumStar").value(hasItem(DEFAULT_START_NUM_STAR)))
            .andExpect(jsonPath("$.[*].endNumStar").value(hasItem(DEFAULT_END_NUM_STAR)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(sameInstant(DEFAULT_UPDATED_DATE))))
            .andExpect(jsonPath("$.[*].answerType").value(hasItem(DEFAULT_ANSWER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].answerCategory").value(hasItem(DEFAULT_ANSWER_CATEGORY.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultRatingQuestionShouldNotBeFound(String filter) throws Exception {
        restRatingQuestionMockMvc.perform(get("/api/rating-questions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingRatingQuestion() throws Exception {
        // Get the ratingQuestion
        restRatingQuestionMockMvc.perform(get("/api/rating-questions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRatingQuestion() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        int databaseSizeBeforeUpdate = ratingQuestionRepository.findAll().size();

        // Update the ratingQuestion
        RatingQuestion updatedRatingQuestion = ratingQuestionRepository.findById(ratingQuestion.getId()).get();
        // Disconnect from session so that the updates on updatedRatingQuestion are not directly saved in db
        em.detach(updatedRatingQuestion);
        updatedRatingQuestion
            .content(UPDATED_CONTENT)
            .type(UPDATED_TYPE)
            .startNumStar(UPDATED_START_NUM_STAR)
            .endNumStar(UPDATED_END_NUM_STAR)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .answerType(UPDATED_ANSWER_TYPE)
            .answerCategory(UPDATED_ANSWER_CATEGORY);
        RatingQuestionDTO ratingQuestionDTO = ratingQuestionMapper.toDto(updatedRatingQuestion);

        restRatingQuestionMockMvc.perform(put("/api/rating-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingQuestionDTO)))
            .andExpect(status().isOk());

        // Validate the RatingQuestion in the database
        List<RatingQuestion> ratingQuestionList = ratingQuestionRepository.findAll();
        assertThat(ratingQuestionList).hasSize(databaseSizeBeforeUpdate);
        RatingQuestion testRatingQuestion = ratingQuestionList.get(ratingQuestionList.size() - 1);
        assertThat(testRatingQuestion.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testRatingQuestion.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testRatingQuestion.getStartNumStar()).isEqualTo(UPDATED_START_NUM_STAR);
        assertThat(testRatingQuestion.getEndNumStar()).isEqualTo(UPDATED_END_NUM_STAR);
        assertThat(testRatingQuestion.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testRatingQuestion.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testRatingQuestion.getAnswerType()).isEqualTo(UPDATED_ANSWER_TYPE);
        assertThat(testRatingQuestion.getAnswerCategory()).isEqualTo(UPDATED_ANSWER_CATEGORY);
    }

    @Test
    @Transactional
    public void updateNonExistingRatingQuestion() throws Exception {
        int databaseSizeBeforeUpdate = ratingQuestionRepository.findAll().size();

        // Create the RatingQuestion
        RatingQuestionDTO ratingQuestionDTO = ratingQuestionMapper.toDto(ratingQuestion);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRatingQuestionMockMvc.perform(put("/api/rating-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingQuestionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RatingQuestion in the database
        List<RatingQuestion> ratingQuestionList = ratingQuestionRepository.findAll();
        assertThat(ratingQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRatingQuestion() throws Exception {
        // Initialize the database
        ratingQuestionRepository.saveAndFlush(ratingQuestion);

        int databaseSizeBeforeDelete = ratingQuestionRepository.findAll().size();

        // Get the ratingQuestion
        restRatingQuestionMockMvc.perform(delete("/api/rating-questions/{id}", ratingQuestion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RatingQuestion> ratingQuestionList = ratingQuestionRepository.findAll();
        assertThat(ratingQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RatingQuestion.class);
        RatingQuestion ratingQuestion1 = new RatingQuestion();
        ratingQuestion1.setId(1L);
        RatingQuestion ratingQuestion2 = new RatingQuestion();
        ratingQuestion2.setId(ratingQuestion1.getId());
        assertThat(ratingQuestion1).isEqualTo(ratingQuestion2);
        ratingQuestion2.setId(2L);
        assertThat(ratingQuestion1).isNotEqualTo(ratingQuestion2);
        ratingQuestion1.setId(null);
        assertThat(ratingQuestion1).isNotEqualTo(ratingQuestion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RatingQuestionDTO.class);
        RatingQuestionDTO ratingQuestionDTO1 = new RatingQuestionDTO();
        ratingQuestionDTO1.setId(1L);
        RatingQuestionDTO ratingQuestionDTO2 = new RatingQuestionDTO();
        assertThat(ratingQuestionDTO1).isNotEqualTo(ratingQuestionDTO2);
        ratingQuestionDTO2.setId(ratingQuestionDTO1.getId());
        assertThat(ratingQuestionDTO1).isEqualTo(ratingQuestionDTO2);
        ratingQuestionDTO2.setId(2L);
        assertThat(ratingQuestionDTO1).isNotEqualTo(ratingQuestionDTO2);
        ratingQuestionDTO1.setId(null);
        assertThat(ratingQuestionDTO1).isNotEqualTo(ratingQuestionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(ratingQuestionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(ratingQuestionMapper.fromId(null)).isNull();
    }
}
