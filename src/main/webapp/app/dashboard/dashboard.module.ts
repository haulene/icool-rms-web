// import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
// import {FormsModule} from '@angular/forms';
import {DashboardComponent} from './dashboard.component';
import {dashboardRoute} from './dashboard.route';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientJsonpModule} from '@angular/common/http';
import {DetailModule} from './detail/detail.module';
import {MenuModule} from './menu/menu.module';
import {BoardModule} from './board/board.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const ENTITY_STATES = [...dashboardRoute];


@NgModule({
    imports: [
        // BrowserModule,
        // BrowserAnimationsModule,
        // RoutingModule,
        FormsModule,
        // HttpClientModule,
        BoardModule,
        MenuModule,
        DetailModule,
        HttpClientJsonpModule,
        RouterModule.forChild(ENTITY_STATES),
    ],
    declarations: [
        DashboardComponent,
    ],
    bootstrap: [
        DashboardComponent
    ]
})
export class DashboardModule {
    
}
