import {Routes} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {BoardComponent} from './board/board.component';
import {DetailComponent} from './detail/detail.component';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';

export const dashboardRoute: Routes = [
    {
        path: '',
        redirectTo: 'main-board',
        pathMatch: 'full',
    },
    {
        path: '',
        component: DashboardComponent
    },
    {
        path: 'main-board',
        component: BoardComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'flexbaRmsWebApp.dashboard.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dashboard/detail',
        component: DetailComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'flexbaRmsWebApp.dashboard.title'
        },
        canActivate: [UserRouteAccessService]
    }
]

