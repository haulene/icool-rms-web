import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ProdConfig } from './blocks/config/prod.config';
import { FlexbaRmsWebAppModule } from './app.module';
import "froala-editor/js/froala_editor.pkgd.min.js";
import "d3/dist/d3.js";
import "semantic-ui/dist/semantic.min.js"
import * as $ from 'jquery';
import d3 from 'd3';
window["d3"] = d3;
window["$"] = $;
window["jQuery"] = $;



ProdConfig();

if (module['hot']) {
    module['hot'].accept();
}

platformBrowserDynamic()
    .bootstrapModule(FlexbaRmsWebAppModule, { preserveWhitespaces: true })
    .then(success => console.log(`Application started`))
    .catch(err => console.error(err));
