import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexbaRmsWebSharedModule } from 'app/shared';
import { NgxEditorModule } from 'ngx-editor';
import { customerMergeRoute } from './customer-merge.route';
import { CustomerMergeComponent } from './customer-merge.component';
import { AgGridModule } from 'ag-grid-angular';
import { WikipediaService } from 'app/customers/typeahead-http';
import { CustomerService } from 'app/customers/customer.service';

const ENTITY_STATES = [...customerMergeRoute];

@NgModule({
    imports: [FlexbaRmsWebSharedModule,
        NgbModule,
        NgxEditorModule,
        FormsModule,
        ReactiveFormsModule,
        AgGridModule.withComponents([]),
        // FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CustomerMergeComponent
    ],
    providers: [
        CustomerService,
        WikipediaService,
    ],
    exports: [
    ],
    entryComponents: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FlexbaRmsWebCustomerMergeModule { }
