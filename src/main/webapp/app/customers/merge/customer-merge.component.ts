import { Component, OnInit, Injectable } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { JhiDataUtils } from "ng-jhipster";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable, of, forkJoin } from "rxjs";
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  map,
  tap,
  switchMap
} from "rxjs/operators";
import { WikipediaService } from "app/customers/typeahead-http";
import {
  Customer,
  ICustomer,
  MergeCustomer
} from "app/shared/model/customer.model";
import { SERVER_FLEXBA_RMS } from "app/app.constants";
import { MembershipHistoty } from "app/shared/model/membership-history.model";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ConfirmationDialog } from "app/core/modal/confirmation-dialog";
import { AlertDialog } from "app/core/modal/alert-dialog";
import { CustomerService } from "app/customers/customer.service";

@Component({
  selector: "jhi-customer-merge",
  templateUrl: "./customer-merge.component.html",
  styleUrls: ["./customer-merge.scss"]
})
export class CustomerMergeComponent implements OnInit {
  private gridApi;
  private gridApi2;
  valid: boolean;
  model: any;
  searching = false;
  searchFailed = false;
  SERVER_FLEXBA_RMS: string;
  validateMessage: string;
  deleteOldCustomer: boolean = true;
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => (this.searching = true)),
      switchMap(term =>
        this._service.search(term).pipe(
          tap(() => (this.searchFailed = false)),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          })
        )
      ),
      tap(() => (this.searching = false))
    );
  formatter = (result: Customer) => result.fullName + "-" + result.tel2;
  levels = [];
  keepLevelName: string = "";

  rowData: Customer[];
  columnDefs: {}[];

  rowData2: Customer[];
  columnDefs2: {}[];

  level: { name: string; code: string };
  rowClassRules: {
    "main-account": (params: any) => any;
    "removed-account": (params: any) => boolean;
  };
  clicked: any;
  keepLevelCode: string;
  keepCustomer: any;

  constructor(
    private dataUtils: JhiDataUtils,
    private activatedRoute: ActivatedRoute,
    private _service: WikipediaService,
    private modalService: NgbModal,
    private customerService: CustomerService
  ) {
    this.SERVER_FLEXBA_RMS = SERVER_FLEXBA_RMS;
  }

  ngOnInit() {
    this.columnDefs = [
      {
        headerName: "Full name",
        field: "fullName",
        checkboxSelection: true
      },
      {
        headerName: "Phone",
        field: "tel2"
      },
      {
        headerName: "Email",
        field: "email"
      },
      {
        headerName: "Level",
        field: "levelCode"
      },
      {
        headerName: "Points",
        field: "points"
      },
      {
        headerName: "Transfer Points",
        field: "transferPoints"
      }
    ];

    this.rowData = [];

    this.rowClassRules = {
      "main-account": function(params) {
        return params.data.selected;
      },
      "removed-account": function(params) {
        return !params.data.selected;
      }
    };

    this.columnDefs2 = [
      {
        headerName: "Full name",
        field: "fullName"
      },
      {
        headerName: "Phone",
        field: "tel2"
      },
      {
        headerName: "Email",
        field: "email"
      },
      {
        headerName: "Level",
        field: "levelCode"
      },
      {
        headerName: "Points",
        field: "points"
      },
      {
        headerName: "Transfer Points",
        field: "transferPoints"
      }
    ];

    this.rowData2 = [];
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }

  selectedItem(event) {
    var existed = false;
    this.rowData.forEach(element => {
      if (element["customerCode"] == event.item.customerCode) {
        existed = true;
      }
    });
    if (!existed) {
      this.populateMembershipHistory(event.item);
      this.rowData.push(event.item);
      var params = { force: true };
      this.gridApi.setRowData(this.rowData);
      this.gridApi.refreshCells(params);
    }
    this.updateLevels();
  }

  getMembershipHistory(customer: Customer): MembershipHistoty {
    let rs: MembershipHistoty = null;
    if (customer.membershipHistoties) {
      customer.membershipHistoties.forEach(membership => {
        if (membership.status == 1) {
          rs = membership;
        }
      });
    }
    return rs;
  }

  populateMembershipHistory(customer: Customer) {
    let mem: MembershipHistoty = this.getMembershipHistory(customer);
    if (mem) {
      customer.levelCode = mem.membershipLevelCode;
      customer.levelName = mem.membershipLevelCode;
      customer.points = mem.remainPoints;
      customer.transferPoints = mem.transferablePoints
        ? mem.transferablePoints
        : 0;
    } else {
      customer.levelCode = "TV";
      customer.levelName = "TV";
      customer.points = 0;
      customer.transferPoints = 0;
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
  }

  onGridReady2(params) {
    this.gridApi2 = params.api;
  }

  selectedItem2(event) {
    this.rowData2 = [];
    this.populateMembershipHistory(event.item);
    this.rowData2.push(event.item);
    var params = { force: true };
    this.gridApi2.setRowData(this.rowData2);
    this.gridApi2.refreshCells(params);
    this.updateValidation();
    this.updateLevels();
  }

  onSelectionChanged($event) {
    let me = this;
    var selectedRows = this.gridApi.getSelectedRows();
    selectedRows.forEach(function(selectedRow, index) {
      console.log(selectedRow);
      me.selectedItem2({
        item: selectedRow
      });
      me.keepCustomer = selectedRow;
      me.updateValidation();
    });

    if (selectedRows.length <= 0) {
      this.rowData2 = [];
      this.gridApi2.setRowData(this.rowData2);
      var params = { force: true };
      this.gridApi2.refreshCells(params);
      this.updateValidation();
    }
    this.updateLevels();
  }

  updateLevels() {
    this.levels = [];
    this.levels.push({
      code: "Blank",
      name: "---"
    });
    this.rowData.forEach(element => {
      if (
        this.levels.filter(value => {
          return value && value.code == element["levelCode"];
        }).length <= 0
      ) {
        this.levels.push({
          code: element["levelCode"],
          name: element["levelName"]
        });
      }
    });
    if (
      this.levels.filter(value => {
        return value && value.code == this.keepLevelCode;
      }).length <= 0
    ) {
      this.keepLevelCode = null;
    }
  }

  updateValidation() {
    var ok = false;
    if (this.rowData.length > 1 && this.rowData2.length == 1) {
      ok = true;
    } else {
      if (this.rowData.length <= 1) {
        this.validateMessage = "Please search at least 2 customers to merge.";
      }

      if (this.rowData2.length != 1) {
        this.validateMessage = "Please select at main customer to merge.";
      }
    }
    this.valid = ok;
  }

  processMerge(): Observable<ICustomer> {
    let merge: MergeCustomer = new MergeCustomer();
    merge.keepCustomer = this.rowData2[0];
    merge.keepCustomerCode = merge.keepCustomer.customerCode;
    merge.removedCustomers = [];
    merge.deleteOldCustomer = this.deleteOldCustomer;
    this.rowData.forEach(element => {
      if (element["customerCode"] != merge.keepCustomer.customerCode) {
        merge.removedCustomers.push(element);
      }
    });
    if (this.keepLevelCode) {
      merge.choosenLevelCode = this.keepLevelCode;
    }
    return this.customerService.merge(merge);
  }

  private reloadData() {}

  onMerge(event) {
    let me = this;
    this.updateValidation();
    if (this.valid) {
      const modalRef = this.modalService.open(ConfirmationDialog);
      modalRef.componentInstance.title = "Confirm";
      modalRef.componentInstance.message =
        "Do you really want to merge these customers?";
      modalRef.componentInstance.confirmFn = () => {
        me.processMerge().subscribe(
          (res: Customer) => {
            const modalRef3 = this.modalService.open(AlertDialog);
            modalRef3.componentInstance.name = "Merge successfull";
            this.handleMergeSuccess();
          },
          error => {
            // const modalRef3 = this.modalService.open(AlertDialog);
            // modalRef3.componentInstance.name = "Merge error";
          }
        );
      };
    } else {
      const modalRef2 = this.modalService.open(AlertDialog);
      modalRef2.componentInstance.name = this.validateMessage;
    }
  }

  handleMergeSuccess() {
    // Reload customers data
    // Reload main account
    let customer: Customer = this.rowData2[0];
    let array = [];
    array.push(this.customerService.find(customer.id));
    if (!this.deleteOldCustomer) {
      this.rowData.forEach(element => {
        if (element["customerCode"] != customer.customerCode) {
          array.push(this.customerService.find(element.id));
        }
      });
    }
    forkJoin(array).subscribe(rs => {
      this.rowData2 = [rs[0].body];
      if (this.deleteOldCustomer) {
        this.rowData = [];
      } else {
        for (let index = 0; index < rs.length; index++) {
          if (index != 0) {
            this.rowData.push(rs[index].body);
          }
        }
      }
      this.gridApi2.setRowData(this.rowData2);
      var params = { force: true };
      this.gridApi2.refreshCells(params);
      this.gridApi.setRowData(this.rowData);
      this.gridApi.refreshCells(params);

      // this.updateLevels();
    });
  }

  clearAll(event) {
    this.rowData = [];
    this.rowData2 = [];
    this.gridApi2.setRowData(this.rowData2);
    var params = { force: true };
    this.gridApi2.setRowData(this.rowData);
    this.gridApi2.refreshCells(params);
    this.gridApi.setRowData(this.rowData);
    this.gridApi.refreshCells(params);
    this.deleteOldCustomer = true;
    this.keepLevelName = null;
    this.keepLevelCode = null;
  }

  selectLevel(name) {
    if (name == "---") {
      this.keepLevelName = null;
      this.keepLevelCode = null;
      return;
    }
    this.levels.forEach(level => {
      if (level.name == name) {
        this.keepLevelName = level.name;
        this.keepLevelCode = level.code;
      }
    });
  }
}
