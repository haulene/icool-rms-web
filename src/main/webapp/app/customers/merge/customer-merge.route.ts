import { CustomerMergeComponent } from './customer-merge.component';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';

export const customerMergeRoute: Routes = [
    {
        path: '',
        component: CustomerMergeComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'flexbaRmsWebApp.customer-merge.title'
        },
        canActivate: [UserRouteAccessService]
    }
]