import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_FORMAT } from "app/shared/constants/input.constants";
import { map } from "rxjs/operators";

import { SERVER_API_URL } from "app/app.constants";
import { createRequestOption } from "app/shared";
// import { IPromotion } from "app/shared/model/promotion.model";
import {
  ICustomer,
  MergeCustomer,
  Customer
} from "app/shared/model/customer.model";
import {
  MembershipHistoty,
  IMembershipHistoty
} from "app/shared/model/membership-history.model";

type EntityResponseType = HttpResponse<ICustomer>;
type EntityArrayResponseType = HttpResponse<ICustomer[]>;
type EntityCusResponseType = HttpResponse<ICustomer>;

@Injectable({ providedIn: "root" })
export class CustomerService {
  private resourceUrl = SERVER_API_URL + "api/customers";
  private resourceMergeUrl = SERVER_API_URL + "api/customers/merge";

  promotionTypes = [
    {
      code: 0,
      name: "News"
    },
    {
      code: 1,
      name: "Redeem"
    },
    {
      code: 2,
      name: "Public codes"
    },
    {
      code: 3,
      name: "Unique Codes"
    },
    {
      code: 5,
      name: "Referrer promotion"
    },
    {
      code: 6,
      name: "Referral promotion"
    }
  ];

  promotionStatuses = [
    {
      code: 0,
      name: "InActive"
    },
    {
      code: 1,
      name: "Active"
    }
  ];

  constructor(private http: HttpClient) {}

  merge(mergeCustomer: MergeCustomer): Observable<ICustomer> {
    const options = createRequestOption(mergeCustomer);
    return this.http
      .post<ICustomer>(this.resourceMergeUrl, mergeCustomer, {
        params: options,
        observe: "response"
      })
      .pipe(map((res: EntityCusResponseType) => res.body))
      .pipe(map(this.convertDateCusFromServer));
  }

  private convertDateCusFromServer(res: ICustomer): ICustomer {
    if (res) {
      let element = res;
      if (element.membershipHistoties) {
        element.membershipHistoties.forEach(mem => {
          mem.effectedDate =
            mem.effectedDate != null ? moment(mem.effectedDate) : null;
          mem.expiredDate =
            mem.expiredDate != null ? moment(mem.expiredDate) : null;
          mem.startDate = mem.startDate != null ? moment(mem.startDate) : null;
          mem.updateDate =
            mem.updateDate != null ? moment(mem.updateDate) : null;
        });
      }
    }
    return res;
  }

  create(promotion: ICustomer): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(promotion);
    return this.http
      .post<ICustomer>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(promotion: ICustomer): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(promotion);
    return this.http
      .put<ICustomer>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICustomer>(`${this.resourceUrl}/${id}`, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICustomer[]>(this.resourceUrl, {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: EntityArrayResponseType) =>
          this.convertDateArrayFromServer(res)
        )
      );
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {
      observe: "response"
    });
  }

  private convertDateFromClient(customer: ICustomer): ICustomer {
    const copy: ICustomer = Object.assign({}, customer, {});
    return copy;
  }

  private convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body.membershipHistoties) {
      res.body.membershipHistoties.forEach(mem => {
        mem.effectedDate =
          mem.effectedDate != null ? moment(mem.effectedDate) : null;
        mem.expiredDate =
          mem.expiredDate != null ? moment(mem.expiredDate) : null;
        mem.startDate = mem.startDate != null ? moment(mem.startDate) : null;
        mem.updateDate = mem.updateDate != null ? moment(mem.updateDate) : null;
      });
    }
    this.populateMembershipHistory(res.body);
    return res;
  }

  private convertDateArrayFromServer(
    res: EntityArrayResponseType
  ): EntityArrayResponseType {
    res.body.forEach((customer: ICustomer) => {
      if (customer.membershipHistoties) {
        customer.membershipHistoties.forEach(mem => {
          mem.effectedDate =
            mem.effectedDate != null ? moment(mem.effectedDate) : null;
          mem.expiredDate =
            mem.expiredDate != null ? moment(mem.expiredDate) : null;
          mem.startDate = mem.startDate != null ? moment(mem.startDate) : null;
          mem.updateDate =
            mem.updateDate != null ? moment(mem.updateDate) : null;
        });
        this.populateMembershipHistory(customer);
      }
    });
    return res;
  }
  populateMembershipHistory(customer: ICustomer) {
    let mem: IMembershipHistoty = this.getMembershipHistory(customer);
    if (mem) {
      customer.levelCode = mem.membershipLevelCode;
      customer.levelName = mem.membershipLevelCode;
      customer.points = mem.remainPoints;
      customer.transferPoints = mem.transferablePoints
        ? mem.transferablePoints
        : 0;
    } else {
      customer.levelCode = "TV";
      customer.levelName = "TV";
      customer.points = 0;
      customer.transferPoints = 0;
    }
  }
  getMembershipHistory(customer: ICustomer): IMembershipHistoty {
    let rs: IMembershipHistoty = null;
    if (customer.membershipHistoties) {
      customer.membershipHistoties.forEach(membership => {
        if (membership.status == 1) {
          rs = membership;
        }
      });
    }
    return rs;
  }
}
