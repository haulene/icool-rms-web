import { Component, Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap } from 'rxjs/operators';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICustomer } from '../shared/model/customer.model';
import * as moment from 'moment';
const WIKI_URL = 'api/customers/suggest?page=0&size=20&tel2.contains=0948213&customerCode.contains=C00000000113';
const PARAMS = new HttpParams({
    fromObject: {
        action: 'opensearch',
        format: 'json',
        origin: '*'
    }
});

type EntityResponseType = HttpResponse<ICustomer>;
type EntityArrayResponseType = HttpResponse<ICustomer[]>;

@Injectable()
export class WikipediaService {
    private resourceUrl = SERVER_API_URL + 'api/customers/suggest';
    
    constructor(private http: HttpClient) { }

    query(req?: any): Observable<ICustomer[]> {
        const options = createRequestOption(req);
        return this.http
            .get<ICustomer[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => res.body)).pipe(map(this.convertDateArrayFromServer));
    }

    private convertDateArrayFromServer(res: ICustomer[]): ICustomer[] {
        res.forEach(element => {
            if (element.membershipHistoties) {
                element.membershipHistoties.forEach(mem => {
                    mem.effectedDate = mem.effectedDate != null ? moment(mem.effectedDate) : null;
                    mem.expiredDate = mem.expiredDate != null ? moment(mem.expiredDate) : null;
                    mem.startDate = mem.startDate != null ? moment(mem.startDate) : null;
                    mem.updateDate = mem.updateDate != null ? moment(mem.updateDate) : null;
                });
            }
        });
        return res;
    }

    search(term: string) {
        var options = {
            page: 0,
            size: 10,
            'tel2.contains': term,
            'customerCode.contains': term,
            'email.contains': term,
            'fullName.contains': term
        }
        return this.query(options);
    }
}