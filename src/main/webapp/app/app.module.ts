import "./vendor.ts";

import { NgModule, Injector } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import {
  Ng2Webstorage,
  LocalStorageService,
  SessionStorageService
} from "ngx-webstorage";
import { JhiEventManager } from "ng-jhipster";

import { AuthInterceptor } from "./blocks/interceptor/auth.interceptor";
import { AuthExpiredInterceptor } from "./blocks/interceptor/auth-expired.interceptor";
import { ErrorHandlerInterceptor } from "./blocks/interceptor/errorhandler.interceptor";
import { NotificationInterceptor } from "./blocks/interceptor/notification.interceptor";
import { FlexbaRmsWebSharedModule } from "app/shared";
import { FlexbaRmsWebCoreModule } from "app/core";
import { FlexbaRmsWebAppRoutingModule } from "./app-routing.module";
import { FlexbaRmsWebHomeModule } from "./home/home.module";
import { FlexbaRmsWebAccountModule } from "./account/account.module";
import { FlexbaRmsWebEntityModule } from "./entities/entity.module";
// jhipster-needle-angular-add-module-import JHipster will add new module here
import {
  JhiMainComponent,
  NavbarComponent,
  FooterComponent,
  PageRibbonComponent,
  ActiveMenuDirective,
  ErrorComponent
} from "./layouts";
import { LoaderService } from "app/core/loader/loader-service";
import { LoadingModule, ANIMATION_TYPES } from "ngx-loading";
import { LoaderInterceptor } from "app/core/loader/loader-interceptor";
import { NgbModal, NgbRating } from "@ng-bootstrap/ng-bootstrap";
import { NgbdModalContent } from "./blocks/interceptor/ngbd-modal-content";
import { ConfirmationDialog } from "app/core/modal/confirmation-dialog";
import { AlertDialog } from "app/core/modal/alert-dialog";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
// import { QRCodeModule } from 'angularx-qrcode';
// import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LoadingModule,
    // QRCodeModule,
    // QuillModule,
    // NgxEditorModule,
    FlexbaRmsWebAppRoutingModule,
    Ng2Webstorage.forRoot({ prefix: "jhi", separator: "-" }),
    FlexbaRmsWebSharedModule,
    FlexbaRmsWebCoreModule,
    FlexbaRmsWebHomeModule,
    FlexbaRmsWebAccountModule,
    FlexbaRmsWebEntityModule
    // jhipster-needle-angular-add-module JHipster will add new module here
  ],
  declarations: [
    JhiMainComponent,
    NavbarComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    FooterComponent,
    NgbdModalContent,
    ConfirmationDialog,
    AlertDialog
  ],
  entryComponents: [
    NgbdModalContent,
    ConfirmationDialog,
    AlertDialog,
    NgbRating
  ],
  providers: [
    LoaderService,
    NgbModal,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true,
      deps: [LoaderService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
      deps: [LocalStorageService, SessionStorageService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true,
      deps: [Injector]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true,
      deps: [JhiEventManager, NgbModal]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotificationInterceptor,
      multi: true,
      deps: [Injector]
    }
  ],
  bootstrap: [JhiMainComponent]
})
export class FlexbaRmsWebAppModule {}
