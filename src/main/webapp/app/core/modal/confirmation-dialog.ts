import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'confirmation-dialog',
    template: `
    <div class="modal-header">
      <h4 class="modal-title" style="color:red">{{title}}</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>{{message}}</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" (click)="activeModal.close('Close click')">
            <fa-icon [icon]="'ban'"></fa-icon>&nbsp;<span>No</span>
        </button>
        <button class="btn btn-danger" (click)="confirm()">
            <fa-icon [icon]="'times'"></fa-icon>&nbsp;<span>Yes</span>
        </button>
    </div>
  `
})
export class ConfirmationDialog {

    @Input() message;
    @Input() title;
    confirmFn: Function;

    mode: "confirm"; // confirm/ ret

    constructor(public activeModal: NgbActiveModal) { }


    confirm() {
        this.activeModal.close();
        this.confirmFn.apply(this, []);
    }
}