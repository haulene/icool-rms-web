import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class LoaderService {
    // A BehaviourSubject is an Observable with a default value
    public isLoading = false;

    constructor() { }
}