import { Moment } from "moment";

export interface IRatingAnswer {
  id?: number;
  ratingId?: number;
  ratingQuestionId?: number;
  type?: number;
  value?: string;
  answerCategory?: string;
  valueTexts?: string[];
}

export class RatingAnswer implements IRatingAnswer {
  constructor(
    public id?: number,
    public ratingId?: number,
    public ratingQuestionId?: number,
    type?: number,
    public value?: string,
    public answerCategory?: string,
    public valueTexts?: string[]
  ) {}
}
