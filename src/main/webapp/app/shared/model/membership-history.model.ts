import { Moment } from 'moment';

export interface IMembershipHistoty {
    id?: number;
    customerCode?: string;
    membershipLevelCode?: string;
    effectedDate?: Moment;
    startDate?: Moment;
    expiredDate?: Moment;
    status?: number;
    accumulationPoints?: number;
    pendingPoints?: number;
    activatedPoints?: number;
    expiredPoints?: number;
    redeemPoints?: number;
    remainPoints?: number;
    updateDate?: Moment;
    transferablePoints?: number;
    attempt?: number;
}

export class MembershipHistoty implements IMembershipHistoty {
    constructor(
        public id: number,
        public customerCode: string,
        public membershipLevelCode: string,
        public effectedDate: Moment,
        public startDate: Moment,
        public expiredDate: Moment,
        public status: number,
        public accumulationPoints: number,
        public pendingPoints: number,
        public activatedPoints: number,
        public expiredPoints: number,
        public redeemPoints: number,
        public remainPoints: number,
        public updateDate: Moment,
        public transferablePoints: number,
        public transferaattemptblePoints: number,
    ) {
    }
}
