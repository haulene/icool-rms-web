import { Moment } from "moment";
import { IRatingAnswer, RatingAnswer } from "./rating-answer.model";
import { ICategory, Category } from "./category.model";

export interface IRatingQuestion {
  id?: number;
  content?: string;
  type?: number;
  typeName?: string;
  startNumStar?: number;
  endNumStar?: number;
  createdDate?: Moment;
  updatedDate?: Moment;
  answerType?: string;
  answerTypeName?: string;
  answerCategory?: string;
  answerCategoryName?: string;
  answerred?: boolean;
  answer?: IRatingAnswer;
  suggestAnswers?: ICategory[];
}

export class RatingQuestion implements IRatingQuestion {
  constructor(
    public id?: number,
    public content?: string,
    public type?: number,
    public typeName?: string,
    public startNumStar?: number,
    public endNumStar?: number,
    public createdDate?: Moment,
    public updatedDate?: Moment,
    public answerType?: string,
    public answerTypeName?: string,
    public answerCategory?: string,
    public answerCategoryName?: string,
    public answerred?: boolean,
    public answer?: RatingAnswer,
    public suggestAnswers?: Category[]
  ) {}
}
