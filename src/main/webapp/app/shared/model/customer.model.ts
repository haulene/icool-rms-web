import {
  IMembershipHistoty,
  MembershipHistoty
} from "./membership-history.model";

export interface ICustomer {
  id?: number;
  customerCode?: string;
  firstName?: string;
  middleName?: string;
  lastName?: string;
  fullName?: string;
  gender?: number;
  tel2?: string;
  tel1?: string;
  tel3?: string;

  // Display UI
  levelCode?: string;
  levelName?: string;
  points?: number;
  transferPoints?: number;

  membershipHistoties?: IMembershipHistoty[];
}

export class Customer implements ICustomer {
  constructor(
    public id: number,
    public customerCode: string,
    public firstName: string,
    public middleName: string,
    public lastName: string,
    public fullName: string,
    public gender: number,
    public tel2: string,
    public tel1: string,
    public tel3: string,
    public levelCode: string,
    public levelName: string,
    public points: number,
    public transferPoints: number,
    public membershipHistoties: MembershipHistoty[]
  ) {}
}

export class MergeCustomer {
  public keepCustomerCode: string;
  public keepCustomer: any;
  public choosenLevelCode: string; // If null auto select higher
  public deleteOldCustomer: boolean;
  removedCustomers: any[];
}
