import { Moment } from "moment";

export interface IVoucherGift {
  id?: number;
  voucherGiftCode?: string;
  voucherGiftName?: string;
  voucherCategory?: number;
  voucherType?: number;
  customerCode?: string;
  customerName?: string;
  customerPhone?: string;
  totalValue?: number;
  availableValue?: number;
  validFrom?: Moment;
  validTo?: Moment;
  status?: number;
  refSku?: string;
  storeCode?: string;
  note?: string;
  createdBy?: string;
  expiredDuration?: number;
  effectedDay?: number;
  voucherValue?: number;
  usedCustomerCode?: string;
  usedCustomerName?: string;
  usedCustomerPhone?: string;
  updateFlag?: boolean;
  maxAppliedAmount?: number;
  max_appliedTimePerCustomer?: number;
  voucherAppliedType?: number;
  updateDate?: Moment;
  deleteFlag?: boolean;
  personalId?: string;
  voucherGiftId?: number;
}

export class VoucherGift implements IVoucherGift {
  constructor(
    public id?: number,
    public voucherGiftCode?: string,
    public voucherGiftName?: string,
    public voucherCategory?: number,
    public voucherType?: number,
    public customerCode?: string,
    public customerName?: string,
    public customerPhone?: string,
    public totalValue?: number,
    public availableValue?: number,
    public validFrom?: Moment,
    public validTo?: Moment,
    public status?: number,
    public refSku?: string,
    public storeCode?: string,
    public note?: string,
    public createdBy?: string,
    public expiredDuration?: number,
    public effectedDay?: number,
    public voucherValue?: number,
    public usedCustomerCode?: string,
    public usedCustomerName?: string,
    public usedCustomerPhone?: string,
    public updateFlag?: boolean,
    public maxAppliedAmount?: number,
    public max_appliedTimePerCustomer?: number,
    public voucherAppliedType?: number,
    public updateDate?: Moment,
    public deleteFlag?: boolean,
    public personalId?: string,
    public voucherGiftId?: number
  ) {
    this.updateFlag = false;
    this.deleteFlag = false;
  }
}
