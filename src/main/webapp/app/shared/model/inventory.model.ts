import { Moment } from "moment";

export interface IInventory {
  id?: number;
  inventoryCode?: string;
  inventoryName?: string;
  regionCode?: string;
  inventoryType?: number;
  brandCode?: string;
  address?: string;
  tel?: string;
  fax?: string;
  email?: string;
  status?: number;
  channel?: string;
  allowPickupFlag?: boolean;
  allowUrgentOrderFlag?: boolean;
  allowBookStudioFlag?: boolean;
  updateDate?: Moment;
  deleteFlag?: boolean;
  latitude?: string;
  longitude?: string;
}

export class Inventory implements IInventory {
  constructor(
    public id?: number,
    public inventoryCode?: string,
    public inventoryName?: string,
    public regionCode?: string,
    public inventoryType?: number,
    public brandCode?: string,
    public address?: string,
    public tel?: string,
    public fax?: string,
    public email?: string,
    public status?: number,
    public channel?: string,
    public allowPickupFlag?: boolean,
    public allowUrgentOrderFlag?: boolean,
    public allowBookStudioFlag?: boolean,
    public updateDate?: Moment,
    public deleteFlag?: boolean,
    public latitude?: string,
    public longitude?: string
  ) {
    this.allowPickupFlag = false;
    this.allowUrgentOrderFlag = false;
    this.allowBookStudioFlag = false;
    this.deleteFlag = false;
  }
}
