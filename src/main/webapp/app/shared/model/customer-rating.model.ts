import { Moment } from "moment";

export interface ICustomerRating {
  id?: number;
  numStar?: number;
  customerCode?: string;
  source?: string;
  inventoryCode?: string;
  inventoryName?: string;
  type?: number;
  comment?: string;
  updatedDate?: Moment;
  createdDate?: Moment;
  deleteFlag?: number;
  typeName?: string;
  customer?: any;
}

export class CustomerRating implements ICustomerRating {
  constructor(
    public id?: number,
    public numStar?: number,
    public customerCode?: string,
    public source?: string,
    public inventoryCode?: string,
    public inventoryName?: string,
    public type?: number,
    public comment?: string,
    public updatedDate?: Moment,
    public createdDate?: Moment,
    public deleteFlag?: number,
    public typeName?: string,
    public customer?: any
  ) {}
}
