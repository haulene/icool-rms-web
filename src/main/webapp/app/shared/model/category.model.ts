import { Moment } from "moment";

export interface ICategory {
  id?: number;
  parentId?: number;
  categoryClass?: string;
  categoryCode?: string;
  categoryName?: string;
  description?: string;
  sortOrder?: number;
  updateDate?: Moment;
  deleteFlag?: number;
  answerrred?: boolean;
}

export class Category implements ICategory {
  constructor(
    public id?: number,
    public parentId?: number,
    public categoryClass?: string,
    public categoryCode?: string,
    public categoryName?: string,
    public description?: string,
    public sortOrder?: number,
    public updateDate?: Moment,
    public deleteFlag?: number,
    public answerrred?: boolean
  ) {}
}
