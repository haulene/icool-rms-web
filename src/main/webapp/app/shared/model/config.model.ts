import { Moment } from "moment";

export interface IConfig {
  id?: string;
  configValue?: string;
  syncConfigFlag?: string;
  allowChangeSyncConfigFlag?: boolean;
  updateDate?: Moment;
  channel?: string;
}

export class Config implements IConfig {
  constructor(
    public id?: string,
    public configValue?: string,
    public syncConfigFlag?: string,
    public allowChangeSyncConfigFlag?: boolean,
    public updateDate?: Moment,
    public channel?: string
  ) {
    this.allowChangeSyncConfigFlag = false;
  }
}
