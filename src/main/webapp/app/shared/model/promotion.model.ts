import { Moment } from "moment";
import { ISale, Sale } from "./sale.model";

export interface IPromotion {
  id?: number;
  saleId?: string;
  saleName?: string;
  couponCode?: string;
  promotionName?: string;
  description?: string;
  htmlDescription?: string;
  startDate?: Moment;
  endDate?: Moment;
  status?: number;
  promotionStatusName?: string;
  stopNextRule?: boolean;
  sortOrder?: number;
  useOrgPriceFlag?: boolean;
  appliedOn?: number;
  ruleDataContentType?: string;
  ruleData?: any;
  droolsRule?: string;
  allocationDataContentType?: string;
  allocationData?: any;
  version?: number;
  runMode?: number;
  onStore?: string;
  promotionType?: number;
  promotionTypeName?: string;
  autoFlag?: boolean;
  updateDate?: Moment;
  deleteFlag?: boolean;
  channel?: string;
  jsCondition?: string;
  jsBonus?: string;
  redeemFlag?: boolean;
  imagePath?: string;
  imagePath2?: string;
  imageFullPath?: string;
  imageFullPath2?: string;
  redeemPoints?: number;
  numPresentees?: number;
  imageBlob1ContentType?: string;
  imageBlob1?: any;
  imageBlob2ContentType?: string;
  imageBlob2?: any;
  publishStartDate?: Moment;
  publishEndDate?: Moment;
  sale?: ISale;
}

export class Promotion implements IPromotion {
  constructor(
    public id?: number,
    public saleId?: string,
    public saleName?: string,
    public couponCode?: string,
    public promotionName?: string,
    public description?: string,
    public htmlDescription?: string,
    public startDate?: Moment,
    public endDate?: Moment,
    public status?: number,
    public promotionStatusName?: string,
    public stopNextRule?: boolean,
    public sortOrder?: number,
    public useOrgPriceFlag?: boolean,
    public appliedOn?: number,
    public ruleDataContentType?: string,
    public ruleData?: any,
    public droolsRule?: string,
    public allocationDataContentType?: string,
    public allocationData?: any,
    public version?: number,
    public runMode?: number,
    public onStore?: string,
    public promotionType?: number,
    public promotionTypeName?: string,
    public autoFlag?: boolean,
    public updateDate?: Moment,
    public deleteFlag?: boolean,
    public channel?: string,
    public jsCondition?: string,
    public jsBonus?: string,
    public redeemFlag?: boolean,
    public imagePath?: string,
    public imagePath2?: string,
    public imageFullPath?: string,
    public imageFullPath2?: string,
    public redeemPoints?: number,
    public numPresentees?: number,
    public imageBlob1ContentType?: string,
    public imageBlob1?: any,
    public imageBlob2ContentType?: string,
    public imageBlob2?: any,
    public publishStartDate?: Moment,
    public publishEndDate?: Moment,
    public sale?: Sale
  ) {
    this.stopNextRule = false;
    this.appliedOn = 1;
    this.version = 0;
    this.runMode = 0;
    this.promotionType = 0;
    this.useOrgPriceFlag = false;
    this.autoFlag = false;
    this.deleteFlag = false;
    this.redeemFlag = false;
    this.promotionTypeName = "News";
    this.numPresentees = 30;
    this.status = 1;
    this.promotionStatusName = "Active";
  }
}
