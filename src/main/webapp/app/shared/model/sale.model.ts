import { Moment } from "moment";

export interface ISale {
  id?: string;
  code?: string;
  name?: string;
  isBegin?: number;
  isEnd?: number;
  begin?: Moment;
  end?: Moment;
  trangthai?: number;
  saleType?: number;
}

export class Sale implements ISale {
  constructor(
    public id?: string,
    public code?: string,
    public name?: string,
    public isBegin?: number,
    public isEnd?: number,
    public begin?: Moment,
    public end?: Moment,
    public trangthai?: number,
    public saleType?: number
  ) {}
}
