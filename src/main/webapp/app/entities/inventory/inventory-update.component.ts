import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_TIME_FORMAT } from "app/shared/constants/input.constants";

import { IInventory } from "app/shared/model/inventory.model";
import { InventoryService } from "./inventory.service";

@Component({
  selector: "jhi-inventory-update",
  templateUrl: "./inventory-update.component.html"
})
export class InventoryUpdateComponent implements OnInit {
  private _inventory: IInventory;
  isSaving: boolean;
  updateDate: string;

  constructor(
    private inventoryService: InventoryService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ inventory }) => {
      this.inventory = inventory;
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    this.inventory.updateDate = moment(this.updateDate, DATE_TIME_FORMAT);
    if (this.inventory.id !== undefined) {
      this.subscribeToSaveResponse(
        this.inventoryService.update(this.inventory)
      );
    } else {
      this.subscribeToSaveResponse(
        this.inventoryService.create(this.inventory)
      );
    }
  }

  private subscribeToSaveResponse(
    result: Observable<HttpResponse<IInventory>>
  ) {
    result.subscribe(
      (res: HttpResponse<IInventory>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  private onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError() {
    this.isSaving = false;
  }
  get inventory() {
    return this._inventory;
  }

  set inventory(inventory: IInventory) {
    this._inventory = inventory;
    this.updateDate = moment(inventory.updateDate).format(DATE_TIME_FORMAT);
  }
}
