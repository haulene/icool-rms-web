import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import { FlexbaRmsWebPromotionModule } from "./promotion/promotion.module";
import { FlexbaRmsWebCustomerRatingModule } from "./customer-rating/customer-rating.module";
import { FlexbaRmsWebConfigModule } from "./config/config.module";
import { FlexbaRmsWebInventoryModule } from "./inventory/inventory.module";
import { FlexbaRmsWebRatingQuestionModule } from "./rating-question/rating-question.module";
import { FlexbaRmsWebCategoryModule } from "./category/category.module";
import { FlexbaRmsWebVoucherGiftModule } from "./voucher-gift/voucher-gift.module";
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
  // prettier-ignore
  imports: [
        FlexbaRmsWebPromotionModule,
        FlexbaRmsWebCustomerRatingModule,
        FlexbaRmsWebConfigModule,
        FlexbaRmsWebInventoryModule,
        FlexbaRmsWebRatingQuestionModule,
        FlexbaRmsWebCategoryModule,
        FlexbaRmsWebVoucherGiftModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FlexbaRmsWebEntityModule {}
