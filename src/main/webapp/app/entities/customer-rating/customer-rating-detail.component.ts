import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ICustomerRating } from "app/shared/model/customer-rating.model";
import { InventoryService } from "../inventory/inventory.service";
import { CustomerRatingService, IType } from "./customer-rating.service";
import { IInventory } from "app/shared/model/inventory.model";
import { forkJoin } from "rxjs";

@Component({
  selector: "jhi-customer-rating-detail",
  templateUrl: "./customer-rating-detail.component.html"
})
export class CustomerRatingDetailComponent implements OnInit {
  customerRating: ICustomerRating;
  inventoryTypes: IType[] = [];

  inventories: IInventory[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private inventoryService: InventoryService,
    private customerRatingService: CustomerRatingService
  ) {}

  ngOnInit() {
    // load Inventory types:
    forkJoin(
      this.customerRatingService.getInventoryTypes({
        page: 0,
        size: 10,
        "typeClass.equals": "INVENTORY_TYPE"
      }),
      this.inventoryService.query({
        page: 0,
        size: 500
      })
    ).subscribe(data => {
      this.inventoryTypes = data[0].body;
      this.inventories = data[1].body;
      this.activatedRoute.data.subscribe(({ customerRating }) => {
        this.customerRating = customerRating;
        let inventoryType = customerRating.type;
        if (inventoryType) {
          if (this.inventoryTypes) {
            this.inventoryTypes.forEach(inventoryType => {
              if (inventoryType.typeCode == customerRating.type) {
                customerRating.typeName = inventoryType.typeName;
              }
            });
          }
        }

        let inventoryCode = customerRating.inventoryCode;
        if (inventoryCode) {
          if (this.inventories) {
            this.inventories.forEach(inventory => {
              if (inventory.inventoryCode == inventoryCode) {
                customerRating.inventoryName = inventory.inventoryName;
              }
            });
          }
        }
      });
    });
  }

  previousState() {
    window.history.back();
  }
}
