import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_FORMAT } from "app/shared/constants/input.constants";
import { map } from "rxjs/operators";

import { SERVER_API_URL } from "app/app.constants";
import { createRequestOption } from "app/shared";
import { ICustomerRating } from "app/shared/model/customer-rating.model";
import { Moment } from "moment";
import { IRatingQuestion } from "app/shared/model/rating-question.model";
import { IRatingAnswer } from "app/shared/model/rating-answer.model";

type EntityResponseType = HttpResponse<ICustomerRating>;
type EntityArrayResponseType = HttpResponse<ICustomerRating[]>;

export interface IType {
  id?: string;
  typeClass?: string;
  typeCode?: string;
  typeName?: string;
  typeNameEn?: string;
  note?: string;
  sortOrder?: number;
  updateDate?: Moment;
  deleteFlag?: number;
}

export class Type implements IType {
  constructor(
    public id?: string,
    public typeClass?: string,
    public typeCode?: string,
    public typeName?: string,
    public typeNameEn?: string,
    public note?: string,
    public sortOrder?: number,
    public updateDate?: Moment,
    public deleteFlag?: number
  ) {}
}

type InventoryEntityResponseType = HttpResponse<Type>;
type InventoryEntityResponseTypeEntityArrayResponseType = HttpResponse<IType[]>;

type QuestionEntityResponseType = HttpResponse<IRatingQuestion>;
type QuestionEntityArrayResponseType = HttpResponse<IRatingQuestion[]>;
type AnswerEntityResponseType = HttpResponse<IRatingAnswer>;
type AnswerEntityArrayResponseType = HttpResponse<IRatingAnswer[]>;

@Injectable({ providedIn: "root" })
export class CustomerRatingService {
  private resourceUrl = SERVER_API_URL + "api/customer-ratings";

  constructor(private http: HttpClient) {}

  create(customerRating: ICustomerRating): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(customerRating);
    return this.http
      .post<ICustomerRating>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(customerRating: ICustomerRating): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(customerRating);
    return this.http
      .put<ICustomerRating>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICustomerRating>(`${this.resourceUrl}/${id}`, {
        observe: "response"
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICustomerRating[]>(this.resourceUrl, {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: EntityArrayResponseType) =>
          this.convertDateArrayFromServer(res)
        )
      );
  }

  queryQuestion(req?: any): Observable<QuestionEntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRatingQuestion[]>("api/rating-questions", {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: QuestionEntityArrayResponseType) =>
          this.convertQuestionDateArrayFromServer(res)
        )
      );
  }

  queryAnswer(req?: any): Observable<AnswerEntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRatingAnswer[]>("api/rating-answers", {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: AnswerEntityArrayResponseType) =>
          this.convertAnswerDateArrayFromServer(res)
        )
      );
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {
      observe: "response"
    });
  }

  private convertDateFromClient(
    customerRating: ICustomerRating
  ): ICustomerRating {
    const copy: ICustomerRating = Object.assign({}, customerRating, {
      updatedDate:
        customerRating.updatedDate != null &&
        customerRating.updatedDate.isValid()
          ? customerRating.updatedDate.toJSON()
          : null,
      createdDate:
        customerRating.createdDate != null &&
        customerRating.createdDate.isValid()
          ? customerRating.createdDate.toJSON()
          : null
    });
    return copy;
  }

  private convertDateFromServer(res: EntityResponseType): EntityResponseType {
    res.body.updatedDate =
      res.body.updatedDate != null ? moment(res.body.updatedDate) : null;
    res.body.createdDate =
      res.body.createdDate != null ? moment(res.body.createdDate) : null;
    return res;
  }

  private convertAnswerDateArrayFromServer(
    res: AnswerEntityArrayResponseType
  ): AnswerEntityArrayResponseType {
    res.body.forEach((q: IRatingAnswer) => {});
    return res;
  }

  private convertQuestionDateArrayFromServer(
    res: QuestionEntityArrayResponseType
  ): QuestionEntityArrayResponseType {
    res.body.forEach((q: IRatingQuestion) => {
      q.updatedDate = q.updatedDate != null ? moment(q.updatedDate) : null;
      q.createdDate = q.createdDate != null ? moment(q.createdDate) : null;
    });
    return res;
  }

  private convertDateArrayFromServer(
    res: EntityArrayResponseType
  ): EntityArrayResponseType {
    res.body.forEach((customerRating: ICustomerRating) => {
      customerRating.updatedDate =
        customerRating.updatedDate != null
          ? moment(customerRating.updatedDate)
          : null;
      customerRating.createdDate =
        customerRating.createdDate != null
          ? moment(customerRating.createdDate)
          : null;
    });
    return res;
  }

  private convertDateTypeArrayFromServer(
    res: InventoryEntityResponseTypeEntityArrayResponseType
  ): InventoryEntityResponseTypeEntityArrayResponseType {
    res.body.forEach((type: IType) => {
      type.updateDate =
        type.updateDate != null ? moment(type.updateDate) : null;
    });
    return res;
  }

  public getInventoryTypes(
    req?: any
  ): Observable<InventoryEntityResponseTypeEntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IType[]>("api/types", { params: options, observe: "response" })
      .pipe(
        map((res: InventoryEntityResponseTypeEntityArrayResponseType) =>
          this.convertDateTypeArrayFromServer(res)
        )
      );
  }
}
