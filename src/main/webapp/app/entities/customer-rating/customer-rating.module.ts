import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";

import { FlexbaRmsWebSharedModule } from "app/shared";
import {
  CustomerRatingComponent,
  CustomerRatingDetailComponent,
  CustomerRatingUpdateComponent,
  CustomerRatingDeletePopupComponent,
  CustomerRatingDeleteDialogComponent,
  customerRatingRoute,
  customerRatingPopupRoute
} from "./";
import { NgbRating } from "@ng-bootstrap/ng-bootstrap";

const ENTITY_STATES = [...customerRatingRoute, ...customerRatingPopupRoute];

@NgModule({
  imports: [FlexbaRmsWebSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CustomerRatingComponent,
    CustomerRatingDetailComponent,
    CustomerRatingUpdateComponent,
    CustomerRatingDeleteDialogComponent,
    CustomerRatingDeletePopupComponent
  ],
  entryComponents: [
    CustomerRatingComponent,
    CustomerRatingUpdateComponent,
    CustomerRatingDeleteDialogComponent,
    CustomerRatingDeletePopupComponent,
    NgbRating
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FlexbaRmsWebCustomerRatingModule {}
