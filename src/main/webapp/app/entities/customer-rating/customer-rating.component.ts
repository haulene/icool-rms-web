import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription, Observable, forkJoin } from "rxjs";
import { JhiEventManager, JhiParseLinks, JhiAlertService } from "ng-jhipster";

import { ICustomerRating } from "app/shared/model/customer-rating.model";
import { Principal } from "app/core";

import { ITEMS_PER_PAGE } from "app/shared";
import { CustomerRatingService, IType } from "./customer-rating.service";
import { InventoryService } from "../inventory/inventory.service";
import { IInventory } from "app/shared/model/inventory.model";
import { IRatingQuestion } from "app/shared/model/rating-question.model";
import { IRatingAnswer } from "app/shared/model/rating-answer.model";

@Component({
  selector: "jhi-customer-rating",
  templateUrl: "./customer-rating.component.html",
  styleUrls: ["customer-rating.scss"]
})
export class CustomerRatingComponent implements OnInit, OnDestroy {
  currentAccount: any;
  customerRatings: ICustomerRating[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  queryCount: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  inventoryTypes: IType[] = [
    {
      typeCode: "Blank",
      typeName: "---"
    },
    {
      typeCode: "0",
      typeName: "Room"
    },
    {
      typeCode: "1",
      typeName: "Box"
    }
  ];

  inventories: IInventory[] = [];

  searchObject: {
    customerCode: string;
    inventoryName: string;
    inventoryCode: string;
    inventoryTypeName: string;
    inventoryType: string;
    receiptNumber: string;
    mvLink: string;
    numStar: number;
  } = {
    customerCode: null,
    inventoryName: null,
    inventoryCode: null,
    inventoryTypeName: null,
    inventoryType: null,
    receiptNumber: null,
    mvLink: null,
    numStar: null
  };

  constructor(
    private customerRatingService: CustomerRatingService,
    private parseLinks: JhiParseLinks,
    private jhiAlertService: JhiAlertService,
    private principal: Principal,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private inventoryService: InventoryService,
    private eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  public selectInventory(code) {
    // this.searchObject.inventoryType = typeCode;
    this.inventories.forEach(ob => {
      if (code == "Blank") {
        delete this.searchObject["inventoryCode"];
        if (ob.inventoryCode == code) {
          this.searchObject.inventoryName = ob.inventoryName;
          $("#field_inventory").val(code);
        }
      } else if (ob.inventoryCode == code) {
        this.searchObject.inventoryName = ob.inventoryName;
        this.searchObject.inventoryCode = ob.inventoryCode;
        $("#field_inventory").val(this.searchObject.inventoryCode);
      }
    });
  }

  public selectInventoryType(typeCode) {
    // this.searchObject.inventoryType = typeCode;
    this.inventoryTypes.forEach(ob => {
      if (typeCode == "Blank") {
        delete this.searchObject["inventoryType"];
        // delete this.searchObject["inventoryTypeName"];
        if (ob.typeCode == typeCode) {
          this.searchObject.inventoryTypeName = ob.typeName;
          $("#field_inventoryType").val(typeCode);
        }
      } else {
        if (ob.typeCode == typeCode) {
          this.searchObject.inventoryTypeName = ob.typeName;
          this.searchObject.inventoryType = ob.typeCode;
          $("#field_inventoryType").val(this.searchObject.inventoryType);
        }
      }
    });
  }

  public onSubmit() {
    this.loadAll();
  }

  loadAll() {
    let options = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };

    if (this.searchObject.customerCode) {
      options["customerCode.contains"] = this.searchObject.customerCode;
    }
    if (this.searchObject.numStar) {
      options["numStar.equals"] = this.searchObject.numStar;
    }
    if (this.searchObject.receiptNumber) {
      options["source.contains"] = this.searchObject.receiptNumber;
    }
    if (this.searchObject.mvLink) {
      options["source.contains"] = this.searchObject.mvLink;
    }
    if (this.searchObject.inventoryType) {
      options["type.equals"] = parseInt(this.searchObject.inventoryType);
    }
    if (this.searchObject.inventoryCode) {
      options["inventoryCode.equals"] = parseInt(
        this.searchObject.inventoryCode
      );
    }

    this.customerRatingService
      .query(options)
      .subscribe(
        (res: HttpResponse<ICustomerRating[]>) =>
          this.paginateCustomerRatings(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(["/customer-rating"], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + "," + (this.reverse ? "asc" : "desc")
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      "/customer-rating",
      {
        page: this.page,
        sort: this.predicate + "," + (this.reverse ? "asc" : "desc")
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    // load Inventory types:
    forkJoin(
      this.customerRatingService.getInventoryTypes({
        page: 0,
        size: 10,
        "typeClass.equals": "INVENTORY_TYPE"
      }),
      this.inventoryService.query({
        page: 0,
        size: 500
      })
    ).subscribe(data => {
      // this.inventoryTypes = data[0].body;
      this.inventories = [];
      this.inventories.push({
        inventoryCode: "Blank",
        inventoryName: "---"
      });
      Array.prototype.push.apply(this.inventories, data[1].body);

      this.loadAll();
      this.principal.identity().then(account => {
        this.currentAccount = account;
      });
      this.registerChangeInCustomerRatings();
    });
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICustomerRating) {
    return item.id;
  }

  registerChangeInCustomerRatings() {
    this.eventSubscriber = this.eventManager.subscribe(
      "customerRatingListModification",
      response => this.loadAll()
    );
  }

  sort() {
    const result = [this.predicate + "," + (this.reverse ? "asc" : "desc")];
    if (this.predicate !== "id") {
      result.push("id");
    }
    return result;
  }

  private paginateCustomerRatings(
    data: ICustomerRating[],
    headers: HttpHeaders
  ) {
    this.links = this.parseLinks.parse(headers.get("link"));
    this.totalItems = parseInt(headers.get("X-Total-Count"), 10);
    this.queryCount = this.totalItems;
    this.customerRatings = data;
    data.forEach(element => {
      let inventoryType = element.type;
      if (inventoryType != null && inventoryType != undefined) {
        if (this.inventoryTypes) {
          this.inventoryTypes.forEach(inventoryType => {
            if (element.type.toString() == inventoryType.typeCode) {
              element.typeName = inventoryType.typeName;
              // if (inventoryType.typeCode == "0") {
              //   element.typeName = "Room"
              // } else if (inventoryType.typeCode == "1") {
              //   element.typeName = "Box"
              // }
            }
          });
        }
      }

      let inventoryCode = element.inventoryCode;
      if (inventoryCode != null && inventoryCode != undefined) {
        if (this.inventories) {
          this.inventories.forEach(inventory => {
            if (inventory.inventoryCode == inventoryCode) {
              element.inventoryName = inventory.inventoryName;
            }
          });
        }
      }
    });
  }

  private onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
