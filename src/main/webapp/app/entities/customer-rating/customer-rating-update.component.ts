import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable, forkJoin } from "rxjs";
import * as moment from "moment";
import { DATE_TIME_FORMAT } from "app/shared/constants/input.constants";

import { ICustomerRating } from "app/shared/model/customer-rating.model";
import { CustomerRatingService, IType } from "./customer-rating.service";
import { IInventory } from "app/shared/model/inventory.model";
import { InventoryService } from "../inventory/inventory.service";
import { IRatingQuestion } from "app/shared/model/rating-question.model";
import { IRatingAnswer } from "app/shared/model/rating-answer.model";
import { CategoryService } from "../category";
import { ICategory } from "app/shared/model/category.model";

@Component({
  selector: "jhi-customer-rating-update",
  templateUrl: "./customer-rating-update.component.html",
  styleUrls: ["customer-rating-update.scss"]
})
export class CustomerRatingUpdateComponent implements OnInit {
  private _customerRating: ICustomerRating;
  isSaving: boolean;
  updatedDate: string;
  createdDate: string;
  currentRate = 2;
  loadFinished: boolean = false;
  showForm = false;

  TEXT = "2";
  MULTISELECT = "0";
  SELECT = "1";

  inventoryTypes: IType[] = [
    {
      typeCode: "Blank",
      typeName: "---"
    },
    {
      typeCode: "0",
      typeName: "Room"
    },
    {
      typeCode: "1",
      typeName: "Box"
    }
  ];

  inventories: IInventory[] = [];

  questions: IRatingQuestion[] = [];
  answers: IRatingAnswer[] = [];
  roomCategories: ICategory[] = [];
  boxCategories: ICategory[] = [];

  constructor(
    private customerRatingService: CustomerRatingService,
    private activatedRoute: ActivatedRoute,
    private inventoryService: InventoryService,
    public cateService: CategoryService
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.params.subscribe(d => {
      let id = parseInt(d["id"]);
      forkJoin(
        this.customerRatingService.getInventoryTypes({
          page: 0,
          size: 10,
          "typeClass.equals": "INVENTORY_TYPE"
        }),
        this.inventoryService.query({
          page: 0,
          size: 500
        }),
        this.customerRatingService.queryQuestion({
          page: 0,
          size: 500
        }),
        this.customerRatingService.queryAnswer({
          "ratingId.equals": id,
          page: 0,
          size: 500
        }),
        this.cateService.query({
          "categoryClass.equals": "RATING_OBJECT_ROOM",
          page: 0,
          size: 500
        }),
        this.cateService.query({
          "categoryClass.equals": "RATING_OBJECT_BOX",
          page: 0,
          size: 500
        })
      ).subscribe(data => {
        // this.inventoryTypes = data[0].body;
        this.inventories = data[1].body;
        this.questions = data[2].body;
        this.answers = data[3].body;

        this.roomCategories = data[4].body;
        this.boxCategories = data[5].body;
        this.questions.forEach(q => {
          this.answers.forEach(a => {
            if (q.id == a.ratingQuestionId) {
              q.answerred = true;
              q.answer = a;
              //
              if (q.answerType != this.TEXT) {
                q.suggestAnswers =
                  q.type == 0 ? this.roomCategories : this.boxCategories;
                if (a.value != null && a.value != undefined) {
                  a.valueTexts = a.value.split(";");
                  q.suggestAnswers.forEach(an => {
                    if (
                      a.valueTexts.filter(b => {
                        return an.categoryCode == b;
                      }).length > 0
                    ) {
                      an.answerrred = true;
                    } else {
                      an.answerrred = false;
                    }
                  });
                }
              }
            }
          });
        });
        this.questions = this.questions.filter(q => {
          return q.answerred;
        });
        this.activatedRoute.data.subscribe(({ customerRating }) => {
          this.customerRating = customerRating;
          let inventoryType = customerRating.type;
          if (inventoryType) {
            if (this.inventoryTypes) {
              this.inventoryTypes.forEach(inventoryType => {
                if (inventoryType.typeCode == customerRating.type) {
                  customerRating.typeName = inventoryType.typeName;
                }
              });
            }
          }

          let inventoryCode = customerRating.inventoryCode;
          if (inventoryCode) {
            if (this.inventories) {
              this.inventories.forEach(inventory => {
                if (inventory.inventoryCode == inventoryCode) {
                  customerRating.inventoryName = inventory.inventoryName;
                }
              });
            }
          }
          this.loadFinished = true;
        });
      });
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    this.customerRating.updatedDate = moment(
      this.updatedDate,
      DATE_TIME_FORMAT
    );
    this.customerRating.createdDate = moment(
      this.createdDate,
      DATE_TIME_FORMAT
    );
    if (this.customerRating.id !== undefined) {
      this.subscribeToSaveResponse(
        this.customerRatingService.update(this.customerRating)
      );
    } else {
      this.subscribeToSaveResponse(
        this.customerRatingService.create(this.customerRating)
      );
    }
  }

  private subscribeToSaveResponse(
    result: Observable<HttpResponse<ICustomerRating>>
  ) {
    result.subscribe(
      (res: HttpResponse<ICustomerRating>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  private onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError() {
    this.isSaving = false;
  }
  get customerRating() {
    return this._customerRating;
  }

  set customerRating(customerRating: ICustomerRating) {
    this._customerRating = customerRating;
    this.updatedDate = moment(customerRating.updatedDate).format(
      DATE_TIME_FORMAT
    );
    this.createdDate = moment(customerRating.createdDate).format(
      DATE_TIME_FORMAT
    );
  }
}
