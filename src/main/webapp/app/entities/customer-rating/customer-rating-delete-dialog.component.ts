import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICustomerRating } from 'app/shared/model/customer-rating.model';
import { CustomerRatingService } from './customer-rating.service';

@Component({
    selector: 'jhi-customer-rating-delete-dialog',
    templateUrl: './customer-rating-delete-dialog.component.html'
})
export class CustomerRatingDeleteDialogComponent {
    customerRating: ICustomerRating;

    constructor(
        private customerRatingService: CustomerRatingService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.customerRatingService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'customerRatingListModification',
                content: 'Deleted an customerRating'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-customer-rating-delete-popup',
    template: ''
})
export class CustomerRatingDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ customerRating }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CustomerRatingDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.customerRating = customerRating;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
