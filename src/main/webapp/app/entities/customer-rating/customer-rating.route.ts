import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CustomerRating } from 'app/shared/model/customer-rating.model';
import { CustomerRatingService } from './customer-rating.service';
import { CustomerRatingComponent } from './customer-rating.component';
import { CustomerRatingDetailComponent } from './customer-rating-detail.component';
import { CustomerRatingUpdateComponent } from './customer-rating-update.component';
import { CustomerRatingDeletePopupComponent } from './customer-rating-delete-dialog.component';
import { ICustomerRating } from 'app/shared/model/customer-rating.model';

@Injectable({ providedIn: 'root' })
export class CustomerRatingResolve implements Resolve<ICustomerRating> {
    constructor(private service: CustomerRatingService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((customerRating: HttpResponse<CustomerRating>) => customerRating.body));
        }
        return of(new CustomerRating());
    }
}

export const customerRatingRoute: Routes = [
    {
        path: 'customer-rating',
        component: CustomerRatingComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'flexbaRmsWebApp.customerRating.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'customer-rating/:id/view',
        component: CustomerRatingDetailComponent,
        resolve: {
            customerRating: CustomerRatingResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'flexbaRmsWebApp.customerRating.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'customer-rating/new',
        component: CustomerRatingUpdateComponent,
        resolve: {
            customerRating: CustomerRatingResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'flexbaRmsWebApp.customerRating.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'customer-rating/:id/edit',
        component: CustomerRatingUpdateComponent,
        resolve: {
            customerRating: CustomerRatingResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'flexbaRmsWebApp.customerRating.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const customerRatingPopupRoute: Routes = [
    {
        path: 'customer-rating/:id/delete',
        component: CustomerRatingDeletePopupComponent,
        resolve: {
            customerRating: CustomerRatingResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'flexbaRmsWebApp.customerRating.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
