import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_FORMAT } from "app/shared/constants/input.constants";
import { map } from "rxjs/operators";

import { SERVER_API_URL } from "app/app.constants";
import { createRequestOption } from "app/shared";
import { IConfig } from "app/shared/model/config.model";

type EntityResponseType = HttpResponse<IConfig>;
type EntityArrayResponseType = HttpResponse<IConfig[]>;

@Injectable({ providedIn: "root" })
export class ConfigService {
  private resourceUrl = SERVER_API_URL + "api/configs";

  constructor(private http: HttpClient) {}

  create(config: IConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(config);
    return this.http
      .post<IConfig>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(config: IConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(config);
    return this.http
      .put<IConfig>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IConfig>(`${this.resourceUrl}/${id}`, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IConfig[]>(this.resourceUrl, {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: EntityArrayResponseType) =>
          this.convertDateArrayFromServer(res)
        )
      );
  }

  delete(id: string): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {
      observe: "response"
    });
  }

  private convertDateFromClient(config: IConfig): IConfig {
    const copy: IConfig = Object.assign({}, config, {
      updateDate:
        config.updateDate != null && config.updateDate.isValid()
          ? config.updateDate.toJSON()
          : null
    });
    return copy;
  }

  private convertDateFromServer(res: EntityResponseType): EntityResponseType {
    res.body.updateDate =
      res.body.updateDate != null ? moment(res.body.updateDate) : null;
    return res;
  }

  private convertDateArrayFromServer(
    res: EntityArrayResponseType
  ): EntityArrayResponseType {
    res.body.forEach((config: IConfig) => {
      config.updateDate =
        config.updateDate != null ? moment(config.updateDate) : null;
    });
    return res;
  }
}
