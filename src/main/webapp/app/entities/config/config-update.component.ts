import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_TIME_FORMAT } from "app/shared/constants/input.constants";

import { IConfig } from "app/shared/model/config.model";
import { ConfigService } from "./config.service";

@Component({
  selector: "jhi-config-update",
  templateUrl: "./config-update.component.html"
})
export class ConfigUpdateComponent implements OnInit {
  private _config: IConfig;
  isSaving: boolean;
  updateDate: string;
  isCreate = false;

  constructor(
    private configService: ConfigService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isSaving = false;

    if (this.activatedRoute.toString().indexOf("config/new") != -1) {
      this.isCreate = true;
    }
    this.activatedRoute.data.subscribe(({ config }) => {
      this.config = config;
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    this.config.updateDate = moment(this.updateDate, DATE_TIME_FORMAT);
    if (!this.isCreate) {
      this.subscribeToSaveResponse(this.configService.update(this.config));
    } else {
      this.subscribeToSaveResponse(this.configService.create(this.config));
    }
  }

  private subscribeToSaveResponse(result: Observable<HttpResponse<IConfig>>) {
    result.subscribe(
      (res: HttpResponse<IConfig>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  private onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError() {
    this.isSaving = false;
  }
  get config() {
    return this._config;
  }

  set config(config: IConfig) {
    this._config = config;
    this.updateDate = moment(config.updateDate).format(DATE_TIME_FORMAT);
  }
}
