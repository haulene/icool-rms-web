import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IRatingQuestion } from "app/shared/model/rating-question.model";

@Component({
  selector: "jhi-rating-question-detail",
  templateUrl: "./rating-question-detail.component.html"
})
export class RatingQuestionDetailComponent implements OnInit {
  ratingQuestion: IRatingQuestion;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ ratingQuestion }) => {
      this.ratingQuestion = ratingQuestion;
    });
  }

  previousState() {
    window.history.back();
  }
}
