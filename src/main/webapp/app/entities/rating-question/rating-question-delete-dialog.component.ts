import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import {
  NgbActiveModal,
  NgbModal,
  NgbModalRef
} from "@ng-bootstrap/ng-bootstrap";
import { JhiEventManager } from "ng-jhipster";

import { IRatingQuestion } from "app/shared/model/rating-question.model";
import { RatingQuestionService } from "./rating-question.service";

@Component({
  selector: "jhi-rating-question-delete-dialog",
  templateUrl: "./rating-question-delete-dialog.component.html"
})
export class RatingQuestionDeleteDialogComponent {
  ratingQuestion: IRatingQuestion;

  constructor(
    private ratingQuestionService: RatingQuestionService,
    public activeModal: NgbActiveModal,
    private eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss("cancel");
  }

  confirmDelete(id: number) {
    this.ratingQuestionService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: "ratingQuestionListModification",
        content: "Deleted an ratingQuestion"
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: "jhi-rating-question-delete-popup",
  template: ""
})
export class RatingQuestionDeletePopupComponent implements OnInit, OnDestroy {
  private ngbModalRef: NgbModalRef;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ ratingQuestion }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(
          RatingQuestionDeleteDialogComponent as Component,
          { size: "lg", backdrop: "static" }
        );
        this.ngbModalRef.componentInstance.ratingQuestion = ratingQuestion;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate([{ outlets: { popup: null } }], {
              replaceUrl: true,
              queryParamsHandling: "merge"
            });
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate([{ outlets: { popup: null } }], {
              replaceUrl: true,
              queryParamsHandling: "merge"
            });
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
