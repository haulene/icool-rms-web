import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_TIME_FORMAT } from "app/shared/constants/input.constants";

import { IRatingQuestion } from "app/shared/model/rating-question.model";
import { RatingQuestionService } from "./rating-question.service";
import { IType } from "../customer-rating/customer-rating.service";

@Component({
  selector: "jhi-rating-question-update",
  templateUrl: "./rating-question-update.component.html",
  styleUrls: ["rating-question-update.scss"]
})
export class RatingQuestionUpdateComponent implements OnInit {
  private _ratingQuestion: IRatingQuestion;
  isSaving: boolean;
  createdDate: string;
  updatedDate: string;

  types: IType[] = [
    {
      typeCode: "0",
      typeName: "Room"
    },
    {
      typeCode: "1",
      typeName: "Box"
    }
  ];

  answerTypes: {
    typeCode: string;
    typeName: string;
  }[] = [
    {
      typeCode: "0",
      typeName: "Multi select"
    },
    {
      typeCode: "1",
      typeName: "Single select"
    },
    {
      typeCode: "2",
      typeName: "Free text"
    }
  ];

  answerCategories: {
    typeCode: string;
    typeName: string;
  }[] = [
    {
      typeCode: "RATING_OBJECT_BOX",
      typeName: "On box"
    },
    {
      typeCode: "RATING_OBJECT_ROOM",
      typeName: "On room"
    }
  ];

  constructor(
    private ratingQuestionService: RatingQuestionService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ ratingQuestion }) => {
      this.ratingQuestion = ratingQuestion;
      let inventoryType = this.ratingQuestion.type;
      if (inventoryType != null && inventoryType != undefined) {
        if (this.types) {
          this.types.forEach(inventoryType => {
            if (this.ratingQuestion.type.toString() == inventoryType.typeCode) {
              this.ratingQuestion.typeName = inventoryType.typeName;
            }
          });
        }
      }

      let answerType = this.ratingQuestion.answerType;
      if (answerType != null && answerType != undefined) {
        if (this.answerTypes) {
          this.answerTypes.forEach(answerTypeO => {
            if (answerType.toString() == answerTypeO.typeCode) {
              this.ratingQuestion.answerTypeName = answerTypeO.typeName;
            }
          });
        }
      }
      let answerCategory = this.ratingQuestion.answerCategory;
      if (answerCategory != null && answerCategory != undefined) {
        if (this.answerCategories) {
          this.answerCategories.forEach(answerCategoryO => {
            if (answerCategory.toString() == answerCategoryO.typeCode) {
              this.ratingQuestion.answerCategoryName = answerCategoryO.typeName;
            }
          });
        }
      }
    });
  }

  selectType(typeCode) {
    this.types.forEach(type => {
      if (type.typeCode == typeCode) {
        this.ratingQuestion.type = parseInt(type.typeCode);
        this.ratingQuestion.typeName = type.typeName;
        $("#field_type").val(typeCode);
      }
    });
  }

  selectAnswerType(typeCode) {
    this.answerTypes.forEach(type => {
      if (type.typeCode == typeCode) {
        this.ratingQuestion.answerType = type.typeCode;
        this.ratingQuestion.answerTypeName = type.typeName;
        $("#field_answerType").val(typeCode);
      }
    });
  }

  selectAnswerCategory(typeCode) {
    this.answerCategories.forEach(type => {
      if (type.typeCode == typeCode) {
        this.ratingQuestion.answerCategory = type.typeCode;
        this.ratingQuestion.answerCategoryName = type.typeName;
        $("#field_answerCategory").val(typeCode);
      }
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    this.ratingQuestion.createdDate = moment(
      this.createdDate,
      DATE_TIME_FORMAT
    );
    this.ratingQuestion.updatedDate = moment(
      this.updatedDate,
      DATE_TIME_FORMAT
    );
    if (this.ratingQuestion.id !== undefined) {
      this.subscribeToSaveResponse(
        this.ratingQuestionService.update(this.ratingQuestion)
      );
    } else {
      this.subscribeToSaveResponse(
        this.ratingQuestionService.create(this.ratingQuestion)
      );
    }
  }

  private subscribeToSaveResponse(
    result: Observable<HttpResponse<IRatingQuestion>>
  ) {
    result.subscribe(
      (res: HttpResponse<IRatingQuestion>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  private onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError() {
    this.isSaving = false;
  }
  get ratingQuestion() {
    return this._ratingQuestion;
  }

  set ratingQuestion(ratingQuestion: IRatingQuestion) {
    this._ratingQuestion = ratingQuestion;
    this.createdDate = moment(ratingQuestion.createdDate).format(
      DATE_TIME_FORMAT
    );
    this.updatedDate = moment(ratingQuestion.updatedDate).format(
      DATE_TIME_FORMAT
    );
  }
}
