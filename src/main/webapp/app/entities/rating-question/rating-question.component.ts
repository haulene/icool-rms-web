import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { JhiEventManager, JhiParseLinks, JhiAlertService } from "ng-jhipster";

import { IRatingQuestion } from "app/shared/model/rating-question.model";
import { Principal } from "app/core";

import { ITEMS_PER_PAGE } from "app/shared";
import { RatingQuestionService } from "./rating-question.service";
import { IType } from "../customer-rating/customer-rating.service";

@Component({
  selector: "jhi-rating-question",
  templateUrl: "./rating-question.component.html"
})
export class RatingQuestionComponent implements OnInit, OnDestroy {
  currentAccount: any;
  ratingQuestions: IRatingQuestion[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  queryCount: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  inventoryTypes: IType[] = [
    {
      typeCode: "Blank",
      typeName: "---"
    },
    {
      typeCode: "0",
      typeName: "Room"
    },
    {
      typeCode: "1",
      typeName: "Box"
    }
  ];

  answerTypes: {
    typeCode: string;
    typeName: string;
  }[] = [
    {
      typeCode: "0",
      typeName: "Multi select"
    },
    {
      typeCode: "1",
      typeName: "Single select"
    },
    {
      typeCode: "2",
      typeName: "Free text"
    }
  ];

  constructor(
    private ratingQuestionService: RatingQuestionService,
    private parseLinks: JhiParseLinks,
    private jhiAlertService: JhiAlertService,
    private principal: Principal,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.ratingQuestionService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IRatingQuestion[]>) =>
          this.paginateRatingQuestions(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(["/rating-question"], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + "," + (this.reverse ? "asc" : "desc")
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      "/rating-question",
      {
        page: this.page,
        sort: this.predicate + "," + (this.reverse ? "asc" : "desc")
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.principal.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInRatingQuestions();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IRatingQuestion) {
    return item.id;
  }

  registerChangeInRatingQuestions() {
    this.eventSubscriber = this.eventManager.subscribe(
      "ratingQuestionListModification",
      response => this.loadAll()
    );
  }

  sort() {
    const result = [this.predicate + "," + (this.reverse ? "asc" : "desc")];
    if (this.predicate !== "id") {
      result.push("id");
    }
    return result;
  }

  private paginateRatingQuestions(
    data: IRatingQuestion[],
    headers: HttpHeaders
  ) {
    this.links = this.parseLinks.parse(headers.get("link"));
    this.totalItems = parseInt(headers.get("X-Total-Count"), 10);
    this.queryCount = this.totalItems;
    this.ratingQuestions = data;
    data.forEach(element => {
      let inventoryType = element.type;
      if (inventoryType != null && inventoryType != undefined) {
        if (this.inventoryTypes) {
          this.inventoryTypes.forEach(inventoryType => {
            if (element.type.toString() == inventoryType.typeCode) {
              element.typeName = inventoryType.typeName;
            }
          });
        }
      }

      let answerType = element.answerType;
      if (answerType != null && answerType != undefined) {
        if (this.answerTypes) {
          this.answerTypes.forEach(answerTypeO => {
            if (answerType.toString() == answerTypeO.typeCode) {
              element.answerTypeName = answerTypeO.typeName;
            }
          });
        }
      }
    });
  }

  private onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
