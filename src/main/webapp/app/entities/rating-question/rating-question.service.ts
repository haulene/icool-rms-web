import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_FORMAT } from "app/shared/constants/input.constants";
import { map } from "rxjs/operators";

import { SERVER_API_URL } from "app/app.constants";
import { createRequestOption } from "app/shared";
import { IRatingQuestion } from "app/shared/model/rating-question.model";

type EntityResponseType = HttpResponse<IRatingQuestion>;
type EntityArrayResponseType = HttpResponse<IRatingQuestion[]>;

@Injectable({ providedIn: "root" })
export class RatingQuestionService {
  private resourceUrl = SERVER_API_URL + "api/rating-questions";

  constructor(private http: HttpClient) {}

  create(ratingQuestion: IRatingQuestion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ratingQuestion);
    return this.http
      .post<IRatingQuestion>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(ratingQuestion: IRatingQuestion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ratingQuestion);
    return this.http
      .put<IRatingQuestion>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRatingQuestion>(`${this.resourceUrl}/${id}`, {
        observe: "response"
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRatingQuestion[]>(this.resourceUrl, {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: EntityArrayResponseType) =>
          this.convertDateArrayFromServer(res)
        )
      );
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {
      observe: "response"
    });
  }

  private convertDateFromClient(
    ratingQuestion: IRatingQuestion
  ): IRatingQuestion {
    const copy: IRatingQuestion = Object.assign({}, ratingQuestion, {
      createdDate:
        ratingQuestion.createdDate != null &&
        ratingQuestion.createdDate.isValid()
          ? ratingQuestion.createdDate.toJSON()
          : null,
      updatedDate:
        ratingQuestion.updatedDate != null &&
        ratingQuestion.updatedDate.isValid()
          ? ratingQuestion.updatedDate.toJSON()
          : null
    });
    return copy;
  }

  private convertDateFromServer(res: EntityResponseType): EntityResponseType {
    res.body.createdDate =
      res.body.createdDate != null ? moment(res.body.createdDate) : null;
    res.body.updatedDate =
      res.body.updatedDate != null ? moment(res.body.updatedDate) : null;
    return res;
  }

  private convertDateArrayFromServer(
    res: EntityArrayResponseType
  ): EntityArrayResponseType {
    res.body.forEach((ratingQuestion: IRatingQuestion) => {
      ratingQuestion.createdDate =
        ratingQuestion.createdDate != null
          ? moment(ratingQuestion.createdDate)
          : null;
      ratingQuestion.updatedDate =
        ratingQuestion.updatedDate != null
          ? moment(ratingQuestion.updatedDate)
          : null;
    });
    return res;
  }
}
