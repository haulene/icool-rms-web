import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";

import { FlexbaRmsWebSharedModule } from "app/shared";
import {
  RatingQuestionComponent,
  RatingQuestionDetailComponent,
  RatingQuestionUpdateComponent,
  RatingQuestionDeletePopupComponent,
  RatingQuestionDeleteDialogComponent,
  ratingQuestionRoute,
  ratingQuestionPopupRoute
} from "./";

const ENTITY_STATES = [...ratingQuestionRoute, ...ratingQuestionPopupRoute];

@NgModule({
  imports: [FlexbaRmsWebSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RatingQuestionComponent,
    RatingQuestionDetailComponent,
    RatingQuestionUpdateComponent,
    RatingQuestionDeleteDialogComponent,
    RatingQuestionDeletePopupComponent
  ],
  entryComponents: [
    RatingQuestionComponent,
    RatingQuestionUpdateComponent,
    RatingQuestionDeleteDialogComponent,
    RatingQuestionDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FlexbaRmsWebRatingQuestionModule {}
