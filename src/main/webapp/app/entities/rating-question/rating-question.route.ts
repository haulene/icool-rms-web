import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Routes
} from "@angular/router";
import { JhiPaginationUtil, JhiResolvePagingParams } from "ng-jhipster";
import { UserRouteAccessService } from "app/core";
import { of } from "rxjs";
import { map } from "rxjs/operators";
import { RatingQuestion } from "app/shared/model/rating-question.model";
import { RatingQuestionService } from "./rating-question.service";
import { RatingQuestionComponent } from "./rating-question.component";
import { RatingQuestionDetailComponent } from "./rating-question-detail.component";
import { RatingQuestionUpdateComponent } from "./rating-question-update.component";
import { RatingQuestionDeletePopupComponent } from "./rating-question-delete-dialog.component";
import { IRatingQuestion } from "app/shared/model/rating-question.model";

@Injectable({ providedIn: "root" })
export class RatingQuestionResolve implements Resolve<IRatingQuestion> {
  constructor(private service: RatingQuestionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = route.params["id"] ? route.params["id"] : null;
    if (id) {
      return this.service
        .find(id)
        .pipe(
          map(
            (ratingQuestion: HttpResponse<RatingQuestion>) =>
              ratingQuestion.body
          )
        );
    }
    return of(new RatingQuestion());
  }
}

export const ratingQuestionRoute: Routes = [
  {
    path: "rating-question",
    component: RatingQuestionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ["ROLE_USER"],
      defaultSort: "id,asc",
      pageTitle: "flexbaRmsWebApp.ratingQuestion.home.title"
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: "rating-question/:id/view",
    component: RatingQuestionDetailComponent,
    resolve: {
      ratingQuestion: RatingQuestionResolve
    },
    data: {
      authorities: ["ROLE_USER"],
      pageTitle: "flexbaRmsWebApp.ratingQuestion.home.title"
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: "rating-question/new",
    component: RatingQuestionUpdateComponent,
    resolve: {
      ratingQuestion: RatingQuestionResolve
    },
    data: {
      authorities: ["ROLE_USER"],
      pageTitle: "flexbaRmsWebApp.ratingQuestion.home.title"
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: "rating-question/:id/edit",
    component: RatingQuestionUpdateComponent,
    resolve: {
      ratingQuestion: RatingQuestionResolve
    },
    data: {
      authorities: ["ROLE_USER"],
      pageTitle: "flexbaRmsWebApp.ratingQuestion.home.title"
    },
    canActivate: [UserRouteAccessService]
  }
];

export const ratingQuestionPopupRoute: Routes = [
  {
    path: "rating-question/:id/delete",
    component: RatingQuestionDeletePopupComponent,
    resolve: {
      ratingQuestion: RatingQuestionResolve
    },
    data: {
      authorities: ["ROLE_USER"],
      pageTitle: "flexbaRmsWebApp.ratingQuestion.home.title"
    },
    canActivate: [UserRouteAccessService],
    outlet: "popup"
  }
];
