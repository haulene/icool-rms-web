import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import {
  NgbActiveModal,
  NgbModal,
  NgbModalRef
} from "@ng-bootstrap/ng-bootstrap";
import { JhiEventManager } from "ng-jhipster";

import { IVoucherGift } from "app/shared/model/voucher-gift.model";
import { VoucherGiftService } from "./voucher-gift.service";

@Component({
  selector: "jhi-voucher-gift-delete-dialog",
  templateUrl: "./voucher-gift-delete-dialog.component.html"
})
export class VoucherGiftDeleteDialogComponent {
  voucherGift: IVoucherGift;

  constructor(
    private voucherGiftService: VoucherGiftService,
    public activeModal: NgbActiveModal,
    private eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss("cancel");
  }

  confirmDelete(id: number) {
    this.voucherGiftService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: "voucherGiftListModification",
        content: "Deleted an voucherGift"
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: "jhi-voucher-gift-delete-popup",
  template: ""
})
export class VoucherGiftDeletePopupComponent implements OnInit, OnDestroy {
  private ngbModalRef: NgbModalRef;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ voucherGift }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(
          VoucherGiftDeleteDialogComponent as Component,
          { size: "lg", backdrop: "static" }
        );
        this.ngbModalRef.componentInstance.voucherGift = voucherGift;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate([{ outlets: { popup: null } }], {
              replaceUrl: true,
              queryParamsHandling: "merge"
            });
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate([{ outlets: { popup: null } }], {
              replaceUrl: true,
              queryParamsHandling: "merge"
            });
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
