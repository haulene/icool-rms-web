import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { JhiEventManager, JhiParseLinks, JhiAlertService } from "ng-jhipster";

import { IVoucherGift } from "app/shared/model/voucher-gift.model";
import { Principal } from "app/core";

import { ITEMS_PER_PAGE } from "app/shared";
import { VoucherGiftService } from "./voucher-gift.service";

@Component({
  selector: "jhi-voucher-gift",
  templateUrl: "./voucher-gift.component.html"
})
export class VoucherGiftComponent implements OnInit, OnDestroy {
  currentAccount: any;
  voucherGifts: IVoucherGift[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  queryCount: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  searchObject: {
    refSku: string;
    customerCode: string;
    status: number;
  } = {
    refSku: null,
    customerCode: null,
    status: null
  };

  statuses: any[];

  constructor(
    private voucherGiftService: VoucherGiftService,
    private parseLinks: JhiParseLinks,
    private jhiAlertService: JhiAlertService,
    private principal: Principal,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.statuses = [
      {
        value: 0,
        name: "Issued"
      },
      {
        value: 1,
        name: "Open"
      },
      {
        value: 0,
        name: "Closed"
      }
    ];
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll(fromSearch: boolean = false) {
    if (fromSearch) {
      this.page = 1;
    }
    let res = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };
    // fromSearch = true;
    if (this.searchObject.refSku) {
      res["refSku.contains"] = this.searchObject.refSku;
    }

    if (
      this.searchObject.status != null &&
      this.searchObject.status != undefined
    ) {
      res["status.equals"] = this.searchObject.status;
    }
    if (this.searchObject.customerCode) {
      res["customerCode.contains"] = this.searchObject.customerCode;
    }

    this.voucherGiftService
      .query(res)
      .subscribe(
        (res: HttpResponse<IVoucherGift[]>) =>
          this.paginateVoucherGifts(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(["/voucher-gift"], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + "," + (this.reverse ? "asc" : "desc")
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      "/voucher-gift",
      {
        page: this.page,
        sort: this.predicate + "," + (this.reverse ? "asc" : "desc")
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.principal.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInVoucherGifts();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IVoucherGift) {
    return item.id;
  }

  registerChangeInVoucherGifts() {
    this.eventSubscriber = this.eventManager.subscribe(
      "voucherGiftListModification",
      response => this.loadAll()
    );
  }

  sort() {
    const result = [this.predicate + "," + (this.reverse ? "asc" : "desc")];
    if (this.predicate !== "id") {
      result.push("id");
    }
    return result;
  }

  private paginateVoucherGifts(data: IVoucherGift[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get("link"));
    this.totalItems = parseInt(headers.get("X-Total-Count"), 10);
    this.queryCount = this.totalItems;
    this.voucherGifts = data;
  }

  private onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  onSearch() {
    this.loadAll(true);
  }
}
