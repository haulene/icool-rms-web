import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Routes
} from "@angular/router";
import { JhiPaginationUtil, JhiResolvePagingParams } from "ng-jhipster";
import { UserRouteAccessService } from "app/core";
import { of } from "rxjs";
import { map } from "rxjs/operators";
import { VoucherGift } from "app/shared/model/voucher-gift.model";
import { VoucherGiftService } from "./voucher-gift.service";
import { VoucherGiftComponent } from "./voucher-gift.component";
import { VoucherGiftDetailComponent } from "./voucher-gift-detail.component";
import { VoucherGiftUpdateComponent } from "./voucher-gift-update.component";
import { VoucherGiftDeletePopupComponent } from "./voucher-gift-delete-dialog.component";
import { IVoucherGift } from "app/shared/model/voucher-gift.model";

@Injectable({ providedIn: "root" })
export class VoucherGiftResolve implements Resolve<IVoucherGift> {
  constructor(private service: VoucherGiftService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = route.params["id"] ? route.params["id"] : null;
    if (id) {
      return this.service
        .find(id)
        .pipe(
          map((voucherGift: HttpResponse<VoucherGift>) => voucherGift.body)
        );
    }
    return of(new VoucherGift());
  }
}

export const voucherGiftRoute: Routes = [
  {
    path: "voucher-gift",
    component: VoucherGiftComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ["ROLE_USER"],
      defaultSort: "id,asc",
      pageTitle: "flexbaRmsWebApp.voucherGift.home.title"
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: "voucher-gift/:id/view",
    component: VoucherGiftDetailComponent,
    resolve: {
      voucherGift: VoucherGiftResolve
    },
    data: {
      authorities: ["ROLE_USER"],
      pageTitle: "flexbaRmsWebApp.voucherGift.home.title"
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: "voucher-gift/new",
    component: VoucherGiftUpdateComponent,
    resolve: {
      voucherGift: VoucherGiftResolve
    },
    data: {
      authorities: ["ROLE_USER"],
      pageTitle: "flexbaRmsWebApp.voucherGift.home.title"
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: "voucher-gift/:id/edit",
    component: VoucherGiftUpdateComponent,
    resolve: {
      voucherGift: VoucherGiftResolve
    },
    data: {
      authorities: ["ROLE_USER"],
      pageTitle: "flexbaRmsWebApp.voucherGift.home.title"
    },
    canActivate: [UserRouteAccessService]
  }
];

export const voucherGiftPopupRoute: Routes = [
  {
    path: "voucher-gift/:id/delete",
    component: VoucherGiftDeletePopupComponent,
    resolve: {
      voucherGift: VoucherGiftResolve
    },
    data: {
      authorities: ["ROLE_USER"],
      pageTitle: "flexbaRmsWebApp.voucherGift.home.title"
    },
    canActivate: [UserRouteAccessService],
    outlet: "popup"
  }
];
