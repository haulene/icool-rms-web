import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";

import { FlexbaRmsWebSharedModule } from "app/shared";
import {
  VoucherGiftComponent,
  VoucherGiftDetailComponent,
  VoucherGiftUpdateComponent,
  VoucherGiftDeletePopupComponent,
  VoucherGiftDeleteDialogComponent,
  voucherGiftRoute,
  voucherGiftPopupRoute
} from "./";

const ENTITY_STATES = [...voucherGiftRoute, ...voucherGiftPopupRoute];

@NgModule({
  imports: [FlexbaRmsWebSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    VoucherGiftComponent,
    VoucherGiftDetailComponent,
    VoucherGiftUpdateComponent,
    VoucherGiftDeleteDialogComponent,
    VoucherGiftDeletePopupComponent
  ],
  entryComponents: [
    VoucherGiftComponent,
    VoucherGiftUpdateComponent,
    VoucherGiftDeleteDialogComponent,
    VoucherGiftDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FlexbaRmsWebVoucherGiftModule {}
