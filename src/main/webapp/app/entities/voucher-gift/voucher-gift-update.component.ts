import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_TIME_FORMAT } from "app/shared/constants/input.constants";

import { IVoucherGift } from "app/shared/model/voucher-gift.model";
import { VoucherGiftService } from "./voucher-gift.service";

@Component({
  selector: "jhi-voucher-gift-update",
  templateUrl: "./voucher-gift-update.component.html"
})
export class VoucherGiftUpdateComponent implements OnInit {
  private _voucherGift: IVoucherGift;
  isSaving: boolean;
  validFrom: string;
  validTo: string;
  updateDate: string;

  constructor(
    private voucherGiftService: VoucherGiftService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ voucherGift }) => {
      this.voucherGift = voucherGift;
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    this.voucherGift.validFrom = moment(this.validFrom, DATE_TIME_FORMAT);
    this.voucherGift.validTo = moment(this.validTo, DATE_TIME_FORMAT);
    this.voucherGift.updateDate = moment(this.updateDate, DATE_TIME_FORMAT);
    if (this.voucherGift.id !== undefined) {
      this.subscribeToSaveResponse(
        this.voucherGiftService.update(this.voucherGift)
      );
    } else {
      this.subscribeToSaveResponse(
        this.voucherGiftService.create(this.voucherGift)
      );
    }
  }

  private subscribeToSaveResponse(
    result: Observable<HttpResponse<IVoucherGift>>
  ) {
    result.subscribe(
      (res: HttpResponse<IVoucherGift>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  private onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError() {
    this.isSaving = false;
  }
  get voucherGift() {
    return this._voucherGift;
  }

  set voucherGift(voucherGift: IVoucherGift) {
    this._voucherGift = voucherGift;
    this.validFrom = moment(voucherGift.validFrom).format(DATE_TIME_FORMAT);
    this.validTo = moment(voucherGift.validTo).format(DATE_TIME_FORMAT);
    this.updateDate = moment(voucherGift.updateDate).format(DATE_TIME_FORMAT);
  }
}
