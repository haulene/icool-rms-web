import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IVoucherGift } from "app/shared/model/voucher-gift.model";

@Component({
  selector: "jhi-voucher-gift-detail",
  templateUrl: "./voucher-gift-detail.component.html"
})
export class VoucherGiftDetailComponent implements OnInit {
  voucherGift: IVoucherGift;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ voucherGift }) => {
      this.voucherGift = voucherGift;
    });
  }

  previousState() {
    window.history.back();
  }
}
