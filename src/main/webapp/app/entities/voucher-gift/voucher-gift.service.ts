import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_FORMAT } from "app/shared/constants/input.constants";
import { map } from "rxjs/operators";

import { SERVER_API_URL } from "app/app.constants";
import { createRequestOption } from "app/shared";
import { IVoucherGift } from "app/shared/model/voucher-gift.model";

type EntityResponseType = HttpResponse<IVoucherGift>;
type EntityArrayResponseType = HttpResponse<IVoucherGift[]>;

@Injectable({ providedIn: "root" })
export class VoucherGiftService {
  private resourceUrl = SERVER_API_URL + "api/voucher-gifts";

  constructor(private http: HttpClient) {}

  create(voucherGift: IVoucherGift): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(voucherGift);
    return this.http
      .post<IVoucherGift>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(voucherGift: IVoucherGift): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(voucherGift);
    return this.http
      .put<IVoucherGift>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IVoucherGift>(`${this.resourceUrl}/${id}`, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IVoucherGift[]>(this.resourceUrl, {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: EntityArrayResponseType) =>
          this.convertDateArrayFromServer(res)
        )
      );
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {
      observe: "response"
    });
  }

  private convertDateFromClient(voucherGift: IVoucherGift): IVoucherGift {
    const copy: IVoucherGift = Object.assign({}, voucherGift, {
      validFrom:
        voucherGift.validFrom != null && voucherGift.validFrom.isValid()
          ? voucherGift.validFrom.toJSON()
          : null,
      validTo:
        voucherGift.validTo != null && voucherGift.validTo.isValid()
          ? voucherGift.validTo.toJSON()
          : null,
      updateDate:
        voucherGift.updateDate != null && voucherGift.updateDate.isValid()
          ? voucherGift.updateDate.toJSON()
          : null
    });
    return copy;
  }

  private convertDateFromServer(res: EntityResponseType): EntityResponseType {
    res.body.validFrom =
      res.body.validFrom != null ? moment(res.body.validFrom) : null;
    res.body.validTo =
      res.body.validTo != null ? moment(res.body.validTo) : null;
    res.body.updateDate =
      res.body.updateDate != null ? moment(res.body.updateDate) : null;
    return res;
  }

  private convertDateArrayFromServer(
    res: EntityArrayResponseType
  ): EntityArrayResponseType {
    res.body.forEach((voucherGift: IVoucherGift) => {
      voucherGift.validFrom =
        voucherGift.validFrom != null ? moment(voucherGift.validFrom) : null;
      voucherGift.validTo =
        voucherGift.validTo != null ? moment(voucherGift.validTo) : null;
      voucherGift.updateDate =
        voucherGift.updateDate != null ? moment(voucherGift.updateDate) : null;
    });
    return res;
  }
}
