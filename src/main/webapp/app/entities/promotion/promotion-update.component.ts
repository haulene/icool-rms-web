import { Component, OnInit, ElementRef, NgZone } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_TIME_FORMAT } from "app/shared/constants/input.constants";
import { JhiDataUtils } from "ng-jhipster";

import { IPromotion, Promotion } from "app/shared/model/promotion.model";
import { PromotionService } from "./promotion.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AlertDialog } from "app/core/modal/alert-dialog";
import { ISale } from "app/shared/model/sale.model";
declare var jQuery: any;

@Component({
  selector: "jhi-promotion-update",
  templateUrl: "./promotion-update.component.html",
  styleUrls: ["promotion-update.scss"]
})
export class PromotionUpdateComponent implements OnInit {
  private _promotion: IPromotion;
  isSaving: boolean;
  startDatep: any;
  endDateDp: any;
  updateDate: string;
  publishStartDateDp: any;
  publishEndDateDp: any;
  heroForm: FormGroup;
  timmer: any;

  public options: Object = {
    charCounterCount: true,
    toolbarButtons: ["bold", "italic", "underline", "paragraphFormat", "alert"],
    toolbarButtonsXS: [
      "bold",
      "italic",
      "underline",
      "paragraphFormat",
      "alert"
    ],
    toolbarButtonsSM: [
      "bold",
      "italic",
      "underline",
      "paragraphFormat",
      "alert"
    ],
    toolbarButtonsMD: [
      "bold",
      "italic",
      "underline",
      "paragraphFormat",
      "alert"
    ]
  };

  promotionTypes = [
    {
      code: 0,
      name: "News"
    },
    {
      code: 1,
      name: "Redeem"
    },
    {
      code: 2,
      name: "Public codes"
    },
    {
      code: 3,
      name: "Unique Codes"
    },
    {
      code: 5,
      name: "Referrer promotion"
    },
    {
      code: 6,
      name: "Referral promotion"
    }
  ];

  promotionStatuses = [
    {
      code: 0,
      name: "InActive"
    },
    {
      code: 1,
      name: "Active"
    }
  ];

  sales: ISale[] = [];

  constructor(
    private dataUtils: JhiDataUtils,
    private promotionService: PromotionService,
    private elementRef: ElementRef,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    public _ngZone: NgZone
  ) {
    // /this.promotion = new Promotion();
  }

  public changeCouponCode() {
    let couponCode = this.promotion.couponCode;
    clearTimeout(this.timmer);
    this.timmer = setTimeout(async () => {
      this.promotionService
        .querySale({
          "code.equals": couponCode,
          "trangthai.equals": 4
        })
        .subscribe(res => {
          this.sales = res.body;
          if (this.sales && this.sales.length > 0) {
            this.promotion.sale = this.sales[0];
            this.promotion.saleId = this.sales[0].id;
          }
        });
    }, 200);
  }

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ promotion }) => {
      this.promotion = promotion;
      if (this.promotion.saleId)
        this.heroForm = new FormGroup({
          id: new FormControl(this.promotion.id, [Validators.required]),
          saleId: new FormControl(this.promotion.saleId, [Validators.required]),
          couponCode: new FormControl(this.promotion.couponCode, [
            Validators.required
          ]),
          promotionName: new FormControl(this.promotion.promotionName, [
            Validators.required
          ]),

          description: new FormControl(this.promotion.description, [
            Validators.required
          ]),

          redeemFlag: new FormControl(this.promotion.redeemFlag, [
            Validators.required
          ]),

          redeemPoints: new FormControl(this.promotion.redeemPoints, []),

          numPresentees: new FormControl(this.promotion.numPresentees, []),

          startDate: new FormControl(this.promotion.startDate, [
            Validators.required
          ]),

          endDate: new FormControl(this.promotion.endDate, [
            Validators.required
          ]),

          status: new FormControl(this.promotion.status, [Validators.required]),

          promotionType: new FormControl(this.promotion.promotionType, [
            Validators.required
          ]),

          publishStartDate: new FormControl(this.promotion.publishStartDate, [
            Validators.required
          ]),

          publishEndDate: new FormControl(this.promotion.publishEndDate, [
            Validators.required
          ]),
          sortOrder: new FormControl(this.promotion.sortOrder, [
            Validators.required
          ]),
          imageBlob2ContentType: new FormControl(
            this.promotion.imageBlob2ContentType,
            []
          ),
          imageBlob1ContentType: new FormControl(
            this.promotion.imageBlob1ContentType,
            []
          ),
          imageBlob1: new FormControl(this.promotion.imageBlob1, []),
          imageBlob2: new FormControl(this.promotion.imageBlob2, [])
        });
      // jQuery(function() {
      //     window.setTimeout(function() {
      //         jQuery('textarea').froalaEditor();
      //     }, 3000);

      // });
    });
  }

  select(name) {
    this.promotionTypes.forEach(promotionType => {
      if (promotionType.name == name) {
        this.promotion.promotionType = promotionType.code;
        this.promotion.promotionTypeName = promotionType.name;
        $("#field_promotionType").val(this.promotion.promotionType);
      }
    });
  }

  selectStatus(name) {
    this.promotionStatuses.forEach(promotionStatus => {
      if (promotionStatus.name == name) {
        this.promotion.status = promotionStatus.code;
        this.promotion.promotionStatusName = promotionStatus.name;
        $("#field_status").val(this.promotion.status);
      }
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, entity, field, isImage) {
    this.dataUtils.setFileData(event, entity, field, isImage);
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.dataUtils.clearInputImage(
      this.promotion,
      this.elementRef,
      field,
      fieldContentType,
      idInput
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;

    // Validate sale id in case: Public codes, Redeme, Referal Code, Unique code
    if (this.promotion.promotionType != 0) {
      // not news
      if (!this.promotion.saleId) {
        //
        const modalRef2 = this.modalService.open(AlertDialog);
        modalRef2.componentInstance.name =
          "You must fill sale id for Public codes, Redeme, Referal Code, Unique code";
        return;
      }
      if (this.promotion.promotionType == 5 && !this.promotion.numPresentees) {
        //
        const modalRef2 = this.modalService.open(AlertDialog);
        modalRef2.componentInstance.name =
          "You must fill numPresentees for Referral Promotion";
        return;
      }
    }
    this.promotion.updateDate = moment(this.updateDate, DATE_TIME_FORMAT);
    if (this.promotion.id !== undefined) {
      this.subscribeToSaveResponse(
        this.promotionService.update(this.promotion)
      );
    } else {
      this.subscribeToSaveResponse(
        this.promotionService.create(this.promotion)
      );
    }
  }

  private subscribeToSaveResponse(
    result: Observable<HttpResponse<IPromotion>>
  ) {
    result.subscribe(
      (res: HttpResponse<IPromotion>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  private onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError() {
    this.isSaving = false;
  }
  get promotion() {
    return this._promotion;
  }

  set promotion(promotion: IPromotion) {
    this._promotion = promotion;
    this.updateDate = moment(promotion.updateDate).format(DATE_TIME_FORMAT);
  }
}
