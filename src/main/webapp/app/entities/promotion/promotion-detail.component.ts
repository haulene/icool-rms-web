import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IPromotion } from 'app/shared/model/promotion.model';

@Component({
    selector: 'jhi-promotion-detail',
    templateUrl: './promotion-detail.component.html'
})
export class PromotionDetailComponent implements OnInit {
    promotion: IPromotion;

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ promotion }) => {
            this.promotion = promotion;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
