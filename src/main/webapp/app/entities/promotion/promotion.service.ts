import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { DATE_FORMAT } from "app/shared/constants/input.constants";
import { map } from "rxjs/operators";

import { SERVER_API_URL } from "app/app.constants";
import { createRequestOption } from "app/shared";
import { IPromotion } from "app/shared/model/promotion.model";
import { ISale } from "app/shared/model/sale.model";

type EntityResponseType = HttpResponse<IPromotion>;
type EntityArrayResponseType = HttpResponse<IPromotion[]>;

type SaleEntityResponseType = HttpResponse<ISale>;
type SaleEntityArrayResponseType = HttpResponse<ISale[]>;

@Injectable({ providedIn: "root" })
export class PromotionService {
  private resourceUrl = SERVER_API_URL + "api/promotions";

  promotionTypes = [
    {
      code: 0,
      name: "News"
    },
    {
      code: 1,
      name: "Redeem"
    },
    {
      code: 2,
      name: "Public codes"
    },
    {
      code: 3,
      name: "Unique Codes"
    },
    {
      code: 5,
      name: "Referrer promotion"
    },
    {
      code: 6,
      name: "Referral promotion"
    }
  ];

  promotionStatuses = [
    {
      code: 0,
      name: "InActive"
    },
    {
      code: 1,
      name: "Active"
    }
  ];

  constructor(private http: HttpClient) {}

  create(promotion: IPromotion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(promotion);
    return this.http
      .post<IPromotion>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(promotion: IPromotion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(promotion);
    return this.http
      .put<IPromotion>(this.resourceUrl, copy, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPromotion>(`${this.resourceUrl}/${id}`, { observe: "response" })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPromotion[]>(this.resourceUrl, {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: EntityArrayResponseType) =>
          this.convertDateArrayFromServer(res)
        )
      );
  }

  querySale(req?: any): Observable<SaleEntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISale[]>("api/sales", {
        params: options,
        observe: "response"
      })
      .pipe(
        map((res: SaleEntityArrayResponseType) =>
          this.convertDateSaleArrayFromServer(res)
        )
      );
  }

  private convertDateSaleArrayFromServer(
    res: SaleEntityArrayResponseType
  ): SaleEntityArrayResponseType {
    res.body.forEach((sale: ISale) => {
      sale.begin = sale.begin != null ? moment(sale.begin) : null;
      sale.end = sale.end != null ? moment(sale.end) : null;
    });
    return res;
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {
      observe: "response"
    });
  }

  private convertDateFromClient(promotion: IPromotion): IPromotion {
    const copy: IPromotion = Object.assign({}, promotion, {
      startDate:
        promotion.startDate != null && promotion.startDate.isValid()
          ? promotion.startDate.format(DATE_FORMAT)
          : null,
      endDate:
        promotion.endDate != null && promotion.endDate.isValid()
          ? promotion.endDate.format(DATE_FORMAT)
          : null,
      updateDate:
        promotion.updateDate != null && promotion.updateDate.isValid()
          ? promotion.updateDate.toJSON()
          : null,
      publishStartDate:
        promotion.publishStartDate != null &&
        promotion.publishStartDate.isValid()
          ? promotion.publishStartDate.format(DATE_FORMAT)
          : null,
      publishEndDate:
        promotion.publishEndDate != null && promotion.publishEndDate.isValid()
          ? promotion.publishEndDate.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  private convertDateFromServer(res: EntityResponseType): EntityResponseType {
    res.body.startDate =
      res.body.startDate != null ? moment(res.body.startDate) : null;
    res.body.endDate =
      res.body.endDate != null ? moment(res.body.endDate) : null;
    res.body.updateDate =
      res.body.updateDate != null ? moment(res.body.updateDate) : null;
    res.body.publishStartDate =
      res.body.publishStartDate != null
        ? moment(res.body.publishStartDate)
        : null;
    res.body.publishEndDate =
      res.body.publishEndDate != null ? moment(res.body.publishEndDate) : null;
    this.promotionTypes.forEach(promotionType => {
      if (promotionType.code == res.body.promotionType) {
        res.body.promotionTypeName = promotionType.name;
      }
    });
    this.promotionStatuses.forEach(promotionStatus => {
      if (promotionStatus.code == res.body.status) {
        res.body.promotionStatusName = promotionStatus.name;
      }
    });
    if (!res.body.saleId) {
      if (res.body.sale) {
        res.body.saleId = res.body.sale.id;
      }
    }
    return res;
  }

  private convertDateArrayFromServer(
    res: EntityArrayResponseType
  ): EntityArrayResponseType {
    res.body.forEach((promotion: IPromotion) => {
      promotion.startDate =
        promotion.startDate != null ? moment(promotion.startDate) : null;
      promotion.endDate =
        promotion.endDate != null ? moment(promotion.endDate) : null;
      promotion.updateDate =
        promotion.updateDate != null ? moment(promotion.updateDate) : null;
      promotion.publishStartDate =
        promotion.publishStartDate != null
          ? moment(promotion.publishStartDate)
          : null;
      promotion.publishEndDate =
        promotion.publishEndDate != null
          ? moment(promotion.publishEndDate)
          : null;
      this.promotionTypes.forEach(promotionType => {
        if (promotionType.code == promotion.promotionType) {
          promotion.promotionTypeName = promotionType.name;
        }
      });

      this.promotionStatuses.forEach(promotionStatus => {
        if (promotionStatus.code == promotion.status) {
          promotion.promotionStatusName = promotionStatus.name;
        }
      });
      if (!promotion.saleId) {
        if (promotion.sale) {
          promotion.saleId = promotion.sale.id;
        }
      }
    });
    return res;
  }
}
