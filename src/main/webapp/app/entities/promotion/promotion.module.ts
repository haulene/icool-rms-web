import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RequiredIfDirective } from 'app/core/validation/required-if.directive';
import { FlexbaRmsWebSharedModule } from 'app/shared';
import { NgxEditorModule } from 'ngx-editor';
import {
    PromotionComponent,
    PromotionDetailComponent,
    PromotionUpdateComponent,
    PromotionDeletePopupComponent,
    PromotionDeleteDialogComponent,
    promotionRoute,
    promotionPopupRoute
} from './';

const ENTITY_STATES = [...promotionRoute, ...promotionPopupRoute];

@NgModule({
    imports: [FlexbaRmsWebSharedModule, 
        NgbModule,
        NgxEditorModule,
        FormsModule,
        ReactiveFormsModule,
        // FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RequiredIfDirective,
        PromotionComponent,
        PromotionDetailComponent,
        PromotionUpdateComponent,
        PromotionDeleteDialogComponent,
        PromotionDeletePopupComponent,
    ],
    entryComponents: [PromotionComponent, PromotionUpdateComponent, PromotionDeleteDialogComponent, PromotionDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FlexbaRmsWebPromotionModule {}
