import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import {
  JhiEventManager,
  JhiParseLinks,
  JhiAlertService,
  JhiDataUtils
} from "ng-jhipster";

import { IPromotion } from "app/shared/model/promotion.model";
import { Principal } from "app/core";

import { ITEMS_PER_PAGE } from "app/shared";
import { PromotionService } from "./promotion.service";
import { SERVER_API_URL } from "app/app.constants";

@Component({
  selector: "jhi-promotion",
  templateUrl: "./promotion.component.html",
  styleUrls: ["promotion.scss"]
})
export class PromotionComponent implements OnInit, OnDestroy {
  currentAccount: any;
  promotions: IPromotion[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  queryCount: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  loading: true;

  searchObject: {
    couponCode: string;
  } = {
    couponCode: null
  };

  public options: Object = {
    charCounterCount: true,
    htmlAllowedAttrs: {
      readonly: true
    },
    toolbarButtons: ["bold", "italic", "underline", "paragraphFormat", "alert"],
    toolbarButtonsXS: [
      "bold",
      "italic",
      "underline",
      "paragraphFormat",
      "alert"
    ],
    toolbarButtonsSM: [
      "bold",
      "italic",
      "underline",
      "paragraphFormat",
      "alert"
    ],
    toolbarButtonsMD: [
      "bold",
      "italic",
      "underline",
      "paragraphFormat",
      "alert"
    ]
  };

  constructor(
    private promotionService: PromotionService,
    private parseLinks: JhiParseLinks,
    private jhiAlertService: JhiAlertService,
    private principal: Principal,
    private activatedRoute: ActivatedRoute,
    private dataUtils: JhiDataUtils,
    private router: Router,
    private eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  onSearch() {
    this.loadAll(true);
  }

  loadAll(fromSearch: boolean = false) {
    let res = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };
    if (fromSearch && this.searchObject.couponCode) {
      res = {
        page: 0,
        size: this.itemsPerPage,
        sort: this.sort()
      };
      res["couponCode.contains"] = this.searchObject.couponCode;
    }
    this.promotionService
      .query(res)
      .subscribe(
        (res: HttpResponse<IPromotion[]>) =>
          this.paginatePromotions(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(["/promotion"], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + "," + (this.reverse ? "asc" : "desc")
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      "/promotion",
      {
        page: this.page,
        sort: this.predicate + "," + (this.reverse ? "asc" : "desc")
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.principal.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInPromotions();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IPromotion) {
    return item.id;
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  registerChangeInPromotions() {
    this.eventSubscriber = this.eventManager.subscribe(
      "promotionListModification",
      response => this.loadAll()
    );
  }

  sort() {
    const result = [this.predicate + "," + (this.reverse ? "asc" : "desc")];
    if (this.predicate !== "id") {
      result.push("id");
    }
    return result;
  }

  private paginatePromotions(data: IPromotion[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get("link"));
    this.totalItems = parseInt(headers.get("X-Total-Count"), 10);
    this.queryCount = this.totalItems;
    this.promotions = data;
    this.promotions.forEach(pro => {
      if (pro.imagePath) {
        pro.imageFullPath =
          SERVER_API_URL + "api/resources/download/promotion/" + pro.imagePath;
      }
      if (pro.imagePath2) {
        pro.imageFullPath2 =
          SERVER_API_URL + "api/resources/download/promotion/" + pro.imagePath2;
      }
    });
  }

  private onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
