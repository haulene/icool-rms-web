import { JhiEventManager } from 'ng-jhipster';
import { HttpInterceptor, HttpRequest, HttpErrorResponse, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NgbdModalContent } from './ngbd-modal-content';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

export class ErrorHandlerInterceptor implements HttpInterceptor {
    constructor(private eventManager: JhiEventManager, private modalService: NgbModal) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                (event: HttpEvent<any>) => {},
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        if (!(err.status === 401 && (err.message === '' || (err.url && err.url.includes('/api/account'))))) {
                            if (this.eventManager !== undefined) {
                                this.eventManager.broadcast({ name: 'flexbaRmsWebApp.httpError', content: err });
                                // Show error Dialog 
                                const modalRef = this.modalService.open(NgbdModalContent);
                                modalRef.componentInstance.name = err.error.title;
                            }
                        }
                    }
                }
            )
        );
    }
}
