import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPromotion } from 'app/shared/model/promotion.model';
import { ICustomer, MergeCustomer } from 'app/shared/model/customer.model';

@Injectable({ providedIn: 'root' })
export class QRService {
    private resourceQRCodeUrl = SERVER_API_URL + 'api/customers/encryptQRCode';

    constructor(private http: HttpClient) { }

    generateQRCode(text: string): Observable<string> {
        const options = createRequestOption({
            plainText: text,
        });
        return this.http
            .get(this.resourceQRCodeUrl, {
                responseType: "text",
                params: options
            }).map(response => response);
    }
}
