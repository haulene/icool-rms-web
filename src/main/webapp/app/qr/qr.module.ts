import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexbaRmsWebSharedModule } from 'app/shared';
import { NgxEditorModule } from 'ngx-editor';
import { qrRoute } from 'app/qr/qr.route';
import { QRComponent } from './qr.component';
import { AgGridModule } from 'ag-grid-angular';
import { WikipediaService } from 'app/customers/typeahead-http';
import { CustomerService } from 'app/customers/customer.service';
import { QRCodeModule } from 'angularx-qrcode';
import { QRService } from './qr.service';
import { NgJsonEditorModule } from 'ang-jsoneditor' 

const ENTITY_STATES = [...qrRoute];

@NgModule({
    imports: [FlexbaRmsWebSharedModule,
        NgbModule,
        NgxEditorModule,
        QRCodeModule,
        FormsModule,
        ReactiveFormsModule,
        AgGridModule.withComponents([]),
        NgJsonEditorModule,
        // FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        QRComponent
    ],
    providers: [
        CustomerService,
        WikipediaService,
        QRService,
    ],
    exports: [
    ],
    entryComponents: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FlexbaRmsWebQRModule { }
