import { Component, OnInit, Injectable, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, tap, switchMap } from 'rxjs/operators';
import { WikipediaService } from 'app/customers/typeahead-http';
import { Customer, ICustomer, MergeCustomer } from 'app/shared/model/customer.model';
import { SERVER_FLEXBA_RMS } from 'app/app.constants';
import { MembershipHistoty } from 'app/shared/model/membership-history.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialog } from 'app/core/modal/confirmation-dialog';
import { AlertDialog } from 'app/core/modal/alert-dialog';
import { CustomerService } from 'app/customers/customer.service';
import { QRService } from 'app/qr/qr.service';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';

@Component({
    selector: 'jhi-qr',
    templateUrl: './qr.component.html',
    styleUrls: ['./qr.scss']
})
export class QRComponent implements OnInit {
    qrCodeTypeName: string;

    public editorOptions: JsonEditorOptions;
    public data: any;

    @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;

    qrCodeTypes = [
        {
            code: "MV",
            name: "MV",
        },
        {
            code: "PB",
            name: "Public codes",
        },
        {
            code: "POINT",
            name: "Points",
        },
        {
            code: "UNIQUE_CODES",
            name: "Mã dự thưởng",
        }
    ]
    qrCodeTypeCode: string;
    text: string = "";
    plainTextObject: object = {
        "type": "public_voucher",
        "data": {
          "publicVoucherCode": "ICOOLTHANHTHAI"
        }
      };

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute, private _service: WikipediaService
        , private modalService: NgbModal, private customerService: CustomerService, private qrService: QRService, private ngZone: NgZone) {
            this.editorOptions = new JsonEditorOptions();
            this.editorOptions.modes = ['text', 'code', 'tree', 'view'];
    }

    ngOnInit() {
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    selectQRCodeType(name) {
        this.qrCodeTypes.forEach(qrCodeType => {
            if (qrCodeType.name == name) {
                this.qrCodeTypeName = qrCodeType.name;
                this.qrCodeTypeCode = qrCodeType.code;
                $("#field_qrCodeType").val(this.qrCodeTypeCode);
            }
        });
    }

    save() {
        //this.plainTextObject = this.editor.data;
        this.qrService.generateQRCode(this.editor.getText()).subscribe((rs) => {
            this.ngZone.run(()=>{
                this.text = rs;
            })
        });
    }

}
