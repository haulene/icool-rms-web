import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute, navbarRoute } from './layouts';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
    imports: [
        RouterModule.forRoot(
            [
                ...LAYOUT_ROUTES,
                {
                    path: 'admin',
                    loadChildren: './admin/admin.module#FlexbaRmsWebAdminModule'
                },
                {
                    path: 'customers-merge',
                    loadChildren: './customers/merge/customer-merge.module#FlexbaRmsWebCustomerMergeModule'
                },
                {
                    path: 'qr',
                    loadChildren: './qr/qr.module#FlexbaRmsWebQRModule'
                },
                {
                    path: 'dashboard',
                    loadChildren: './dashboard/dashboard.module#DashboardModule'
                }
            ],
            { useHash: true, enableTracing: DEBUG_INFO_ENABLED }
        )
    ],
    exports: [RouterModule]
})
export class FlexbaRmsWebAppRoutingModule {}
