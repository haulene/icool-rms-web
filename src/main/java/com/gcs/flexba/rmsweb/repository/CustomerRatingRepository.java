package com.gcs.flexba.rmsweb.repository;

import com.gcs.flexba.rmsweb.domain.CustomerRating;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CustomerRating entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerRatingRepository extends JpaRepository<CustomerRating, Long>, JpaSpecificationExecutor<CustomerRating> {

}
