package com.gcs.flexba.rmsweb.repository;

import com.gcs.flexba.rmsweb.domain.RatingQuestion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RatingQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RatingQuestionRepository extends JpaRepository<RatingQuestion, Long>, JpaSpecificationExecutor<RatingQuestion> {

}
