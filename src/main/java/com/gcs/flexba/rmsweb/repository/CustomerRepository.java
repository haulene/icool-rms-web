package com.gcs.flexba.rmsweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gcs.flexba.rmsweb.domain.Customer;

/**
 * Spring Data repository for the Customer entity.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {

	@Query(value = " select c from Customer c where c.customerCode=:customerCode")
	Customer getByCode(@Param("customerCode") String customerCode);
}
