package com.gcs.flexba.rmsweb.repository.iml;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gcs.flexba.rmsweb.repository.VoucherRepository;

@Repository
public class VoucherRepositoryImpl implements VoucherRepository {

	/**
	 * Entity Manager.
	 **/
	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unused")
	private static String INSERT_VC_GIFT_NATIVE_SQL = "INSERT INTO voucher_gift_tbl(voucher_gift_code,voucher_gift_name, valid_from,valid_to, ref_sku, [status], update_date) SELECT c.code,s.name,iif(s.is_begin =1, s.[begin], null) as valid_from,iif(s.[is_end] =1,s.[end], null) as valid_to, "
			+ ":promotionCode as ref_sku, IIF( c.activated = 0 AND c.trangthai = 4  AND s.trangthai = 4, 0, 2) as [status], CURRENT_TIMESTAMP FROM Sale AS s, Coupon AS c WHERE c.sale_id = s.id  AND c.activated = 0 AND c.trangthai = 4 AND  "
			+ "(s.code = :promotionCode OR s.id = :saleId) AND s.trangthai = 4\n"
			+ "AND NOT EXISTS (SELECT 1 FROM voucher_gift_tbl v WHERE v.voucher_gift_code=c.code) ;";
	
	private static String INSERT_VC_GIFT_NATIVE_SQL_V2 = "INSERT INTO voucher_gift_tbl(voucher_gift_code,voucher_gift_name, valid_from,valid_to, ref_sku, [status], update_date) "
			+ "SELECT c.code,s.name,iif(s.is_begin =1, s.[begin], null) as valid_from,iif(s.[is_end] =1,s.[end], null) as valid_to, :promotionCode as ref_sku, "
			+ "IIF(c.trangthai = 4  AND s.trangthai = 4, IIF(c.activated = 0, 0, IIF(c.activated = 1, 2, 1)), 2) as [status], "
			+ "CURRENT_TIMESTAMP FROM Sale AS s, Coupon AS c WHERE c.sale_id = s.id  AND c.activated = 0 AND c.trangthai = 4 AND  (s.code = :promotionCode OR s.id = :saleId) AND s.trangthai = 4"
			+ " AND NOT EXISTS (select * from voucher_gift_tbl v where v.voucher_gift_code = c.code )";

	private static String DELETE_VC_GIFT_NATIVE_SQL = "DELETE FROM voucher_gift_tbl where ref_sku = :promotionCode AND (customer_code IS NULL OR customer_code = '')";

	@Override
	public void insertVCGiftsByPromotion(String saleId, String promotionCode) {
		Query query = entityManager.createNativeQuery(INSERT_VC_GIFT_NATIVE_SQL_V2);
		query.setParameter("saleId", saleId);
		query.setParameter("promotionCode", promotionCode);
		query.executeUpdate();
	}

	@Override
	public void deleteVCGiftsByPromotion(String saleId, String promotionCode) {
		Query query = entityManager.createNativeQuery(DELETE_VC_GIFT_NATIVE_SQL);
		query.setParameter("promotionCode", promotionCode);
		query.executeUpdate();
	}

}
