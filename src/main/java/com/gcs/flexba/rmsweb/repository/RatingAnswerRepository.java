package com.gcs.flexba.rmsweb.repository;

import com.gcs.flexba.rmsweb.domain.RatingAnswer;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RatingAnswer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RatingAnswerRepository extends JpaRepository<RatingAnswer, Long>, JpaSpecificationExecutor<RatingAnswer> {

}
