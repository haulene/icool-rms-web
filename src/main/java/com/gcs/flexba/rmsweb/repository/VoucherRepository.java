package com.gcs.flexba.rmsweb.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface VoucherRepository {

	void insertVCGiftsByPromotion(String saleId, String promotionCode);

	void deleteVCGiftsByPromotion(String saleId, String promotionCode);

}