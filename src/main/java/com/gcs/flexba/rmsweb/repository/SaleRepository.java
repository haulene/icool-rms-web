package com.gcs.flexba.rmsweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gcs.flexba.rmsweb.domain.Sale;

/**
 * Spring Data repository for the Sale entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SaleRepository extends JpaRepository<Sale, String>, JpaSpecificationExecutor<Sale> {
	
	@Query(value = " select s from Sale s where s.id=:id")
	Sale findOnebyId(@Param("id") String id);
	
	@Query(value = " select s from Sale s where s.code=:code and s.trangthai=4")
	List<Sale> findOnebyCode(@Param("code") String code);
}
