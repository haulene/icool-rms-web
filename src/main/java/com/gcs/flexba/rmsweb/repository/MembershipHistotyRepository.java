package com.gcs.flexba.rmsweb.repository;

import com.gcs.flexba.rmsweb.domain.MembershipHistoty;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MembershipHistoty entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MembershipHistotyRepository extends JpaRepository<MembershipHistoty, Long>, JpaSpecificationExecutor<MembershipHistoty> {

}
