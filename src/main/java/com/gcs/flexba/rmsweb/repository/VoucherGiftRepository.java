package com.gcs.flexba.rmsweb.repository;

import com.gcs.flexba.rmsweb.domain.VoucherGift;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the VoucherGift entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VoucherGiftRepository extends JpaRepository<VoucherGift, Long>, JpaSpecificationExecutor<VoucherGift> {

}
