package com.gcs.flexba.rmsweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gcs.flexba.rmsweb.domain.Promotion;

/**
 * Spring Data repository for the Promotion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PromotionRepository extends JpaRepository<Promotion, Long>, JpaSpecificationExecutor<Promotion> {
	
	@Query(value = " select p from Promotion p where p.id=:id")
	Promotion findOnebyId(@Param("id") Long id);
	
	@Query(value = " select p from Promotion p where p.couponCode=:couponCode")
	Promotion findOnebyCouponCode(@Param("couponCode") String couponCode);
	
}
