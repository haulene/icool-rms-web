package com.gcs.flexba.rmsweb.repository.iml;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gcs.flexba.rmsweb.repository.CRMRepository;

@Repository
public class CRMRepositoryImpl implements CRMRepository {

	/**
	 * Entity Manager.
	 **/
	@Autowired
	private EntityManager entityManager;

	@Override
	public void mergeCustomers(String mainCustomerCode, String oldCustomerCode, String startLevelCode, String mergeBy, boolean delete) {
		StoredProcedureQuery query = this.entityManager.createNamedStoredProcedureQuery("MergeCustomer");
		query.setParameter("@CustomerCode", mainCustomerCode);
		query.setParameter("@OldCustomerCode", oldCustomerCode);
		query.setParameter("@Level", startLevelCode);
		query.setParameter("@MergeBy", mergeBy);
		query.setParameter("@IsDeleteOldCustomer", delete);
		
		query.executeUpdate();
	}
}