package com.gcs.flexba.rmsweb.repository;

import org.springframework.stereotype.Repository;

@Repository

public interface CRMRepository {

	void mergeCustomers(String mainCustomerCode, String oldCustomerCode, String startLevelCode, String mergeBy, boolean b);

}