package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.BooleanFilter;

import com.gcs.flexba.rmsweb.domain.Customer;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.CustomerRepository;
import com.gcs.flexba.rmsweb.service.dto.CustomerCriteria;

import com.gcs.flexba.rmsweb.service.dto.CustomerDTO;
import com.gcs.flexba.rmsweb.service.mapper.CustomerMapper;

/**
 * Service for executing complex queries for Customer entities in the database.
 * The main input is a {@link CustomerCriteria} which gets converted to
 * {@link Specification}, in a way that all the filters must apply. It returns a
 * {@link List} of {@link CustomerDTO} or a {@link Page} of {@link CustomerDTO}
 * which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CustomerQueryService extends QueryService<Customer> {

	private final Logger log = LoggerFactory.getLogger(CustomerQueryService.class);

	private final CustomerRepository customerRepository;

	private final CustomerMapper customerMapper;

	public CustomerQueryService(CustomerRepository customerRepository, CustomerMapper customerMapper) {
		this.customerRepository = customerRepository;
		this.customerMapper = customerMapper;
	}

	/**
	 * Return a {@link List} of {@link CustomerDTO} which matches the criteria from
	 * the database
	 * 
	 * @param criteria The object which holds all the filters, which the entities
	 *                 should match.
	 * @return the matching entities.
	 */
	@Transactional(readOnly = true)
	public List<CustomerDTO> findByCriteria(CustomerCriteria criteria) {
		log.debug("find by criteria : {}", criteria);
		final Specification<Customer> specification = createSpecification(criteria);
		return customerMapper.toDto(customerRepository.findAll(specification));
	}

	/**
	 * Return a {@link Page} of {@link CustomerDTO} which matches the criteria from
	 * the database
	 * 
	 * @param criteria The object which holds all the filters, which the entities
	 *                 should match.
	 * @param page     The page, which should be returned.
	 * @return the matching entities.
	 */
	@Transactional(readOnly = true)
	public Page<CustomerDTO> findByCriteria(CustomerCriteria criteria, Pageable page) {
		log.debug("find by criteria : {}, page: {}", criteria, page);
		final Specification<Customer> specification = createSpecification(criteria);
		return customerRepository.findAll(specification, page).map(customerMapper::toDto);
	}

	/**
	 * Function to convert CustomerCriteria to a {@link Specification}
	 */
	private Specification<Customer> createSpecification(CustomerCriteria criteria) {
		Specification<Customer> specification = Specification.where(null);
		if (criteria != null) {
			if (criteria.getId() != null) {
				specification = specification.and(buildSpecification(criteria.getId(), Customer_.id));
			}
			if (criteria.getCustomerCode() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getCustomerCode(), Customer_.customerCode));
			}
			if (criteria.getFirstName() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getFirstName(), Customer_.firstName));
			}
			if (criteria.getMiddleName() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getMiddleName(), Customer_.middleName));
			}
			if (criteria.getLastName() != null) {
				specification = specification.and(buildStringSpecification(criteria.getLastName(), Customer_.lastName));
			}
			if (criteria.getFullName() != null) {
				specification = specification.and(buildStringSpecification(criteria.getFullName(), Customer_.fullName));
			}
			if (criteria.getGender() != null) {
				specification = specification.and(buildRangeSpecification(criteria.getGender(), Customer_.gender));
			}
			if (criteria.getTel2() != null) {
				specification = specification.and(buildStringSpecification(criteria.getTel2(), Customer_.tel2));
			}
			if (criteria.getTel1() != null) {
				specification = specification.and(buildStringSpecification(criteria.getTel1(), Customer_.tel1));
			}
			if (criteria.getTel3() != null) {
				specification = specification.and(buildStringSpecification(criteria.getTel3(), Customer_.tel3));
			}
			if (criteria.getFacebookId() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getFacebookId(), Customer_.facebookId));
			}
			if (criteria.getMembershipHistotyId() != null) {
				specification = specification.and(buildReferringEntitySpecification(criteria.getMembershipHistotyId(),
						Customer_.membershipHistoties, MembershipHistoty_.id));
			}
		}
		return specification;
	}

	/**
	 * Function to convert CustomerCriteria to a {@link Specification}
	 */
	private Specification<Customer> createSpecificationBySuggestion(CustomerCriteria criteria) {
		Specification<Customer> specification = Specification.where(null);
		if (criteria != null) {
			if (criteria.getId() != null) {
				specification = specification.or(buildSpecification(criteria.getId(), Customer_.id));
			}
			if (criteria.getCustomerCode() != null) {
				specification = specification
						.or(buildStringSpecification(criteria.getCustomerCode(), Customer_.customerCode));
			}
			if (criteria.getFirstName() != null) {
				specification = specification
						.or(buildStringSpecification(criteria.getFirstName(), Customer_.firstName));
			}
			if (criteria.getMiddleName() != null) {
				specification = specification
						.or(buildStringSpecification(criteria.getMiddleName(), Customer_.middleName));
			}
			if (criteria.getLastName() != null) {
				specification = specification.or(buildStringSpecification(criteria.getLastName(), Customer_.lastName));
			}
			if (criteria.getFullName() != null) {
				specification = specification.or(buildStringSpecification(criteria.getFullName(), Customer_.fullName));
			}
			if (criteria.getGender() != null) {
				specification = specification.or(buildRangeSpecification(criteria.getGender(), Customer_.gender));
			}
			if (criteria.getTel2() != null) {
				specification = specification.or(buildStringSpecification(criteria.getTel2(), Customer_.tel2));
			}
			if (criteria.getTel1() != null) {
				specification = specification.or(buildStringSpecification(criteria.getTel1(), Customer_.tel1));
			}
			if (criteria.getTel3() != null) {
				specification = specification.or(buildStringSpecification(criteria.getTel3(), Customer_.tel3));
			}
			if (criteria.getFacebookId() != null) {
				specification = specification
						.or(buildStringSpecification(criteria.getFacebookId(), Customer_.facebookId));
			}
			BooleanFilter delete = new BooleanFilter();
			delete.setEquals(false);
			specification  = specification.and(buildSpecification(delete, Customer_.deleteFlag));
		}
		return specification;
	}

	/**
	 * Return a {@link Page} of {@link CustomerDTO} which matches the criteria from
	 * the database
	 * 
	 * @param criteria The object which holds all the filters, which the entities
	 *                 should match.
	 * @param page     The page, which should be returned.
	 * @return the matching entities.
	 */
	@Transactional(readOnly = true)
	public Page<CustomerDTO> findByCriteriaBySuggestion(CustomerCriteria criteria, Pageable page) {
		log.debug("find by criteria : {}, page: {}", criteria, page);
		final Specification<Customer> specification = createSpecificationBySuggestion(criteria);
		return customerRepository.findAll(specification, page).map(customerMapper::toDto);
	}

}
