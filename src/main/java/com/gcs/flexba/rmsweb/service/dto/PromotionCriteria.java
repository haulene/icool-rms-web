package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Promotion entity. This class is used in PromotionResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /promotions?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PromotionCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter couponCode;

    private StringFilter promotionName;

    private StringFilter description;

    private StringFilter htmlDescription;

    private LocalDateFilter start_date;

    private LocalDateFilter endDate;

    private IntegerFilter status;

    private BooleanFilter stopNextRule;

    private IntegerFilter sortOrder;

    private BooleanFilter useOrgPriceFlag;

    private IntegerFilter appliedOn;

    private StringFilter droolsRule;

    private IntegerFilter version;

    private IntegerFilter runMode;

    private StringFilter onStore;

    private IntegerFilter promotionType;

    private BooleanFilter autoFlag;

    private ZonedDateTimeFilter updateDate;

    private BooleanFilter deleteFlag;

    private StringFilter channel;

    private StringFilter jsCondition;

    private StringFilter jsBonus;

    private BooleanFilter redeemFlag;

    private StringFilter imagePath;

    private StringFilter imagePath2;

    private DoubleFilter redeemPoints;

    private LocalDateFilter publishStartDate;

    private LocalDateFilter publishEndDate;

    public PromotionCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(StringFilter couponCode) {
        this.couponCode = couponCode;
    }

    public StringFilter getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(StringFilter promotionName) {
        this.promotionName = promotionName;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getHtmlDescription() {
        return htmlDescription;
    }

    public void setHtmlDescription(StringFilter htmlDescription) {
        this.htmlDescription = htmlDescription;
    }

    public LocalDateFilter getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDateFilter start_date) {
        this.start_date = start_date;
    }

    public LocalDateFilter getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateFilter endDate) {
        this.endDate = endDate;
    }

    public IntegerFilter getStatus() {
        return status;
    }

    public void setStatus(IntegerFilter status) {
        this.status = status;
    }

    public BooleanFilter getStopNextRule() {
        return stopNextRule;
    }

    public void setStopNextRule(BooleanFilter stopNextRule) {
        this.stopNextRule = stopNextRule;
    }

    public IntegerFilter getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(IntegerFilter sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BooleanFilter getUseOrgPriceFlag() {
        return useOrgPriceFlag;
    }

    public void setUseOrgPriceFlag(BooleanFilter useOrgPriceFlag) {
        this.useOrgPriceFlag = useOrgPriceFlag;
    }

    public IntegerFilter getAppliedOn() {
        return appliedOn;
    }

    public void setAppliedOn(IntegerFilter appliedOn) {
        this.appliedOn = appliedOn;
    }

    public StringFilter getDroolsRule() {
        return droolsRule;
    }

    public void setDroolsRule(StringFilter droolsRule) {
        this.droolsRule = droolsRule;
    }

    public IntegerFilter getVersion() {
        return version;
    }

    public void setVersion(IntegerFilter version) {
        this.version = version;
    }

    public IntegerFilter getRunMode() {
        return runMode;
    }

    public void setRunMode(IntegerFilter runMode) {
        this.runMode = runMode;
    }

    public StringFilter getOnStore() {
        return onStore;
    }

    public void setOnStore(StringFilter onStore) {
        this.onStore = onStore;
    }

    public IntegerFilter getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(IntegerFilter promotionType) {
        this.promotionType = promotionType;
    }

    public BooleanFilter getAutoFlag() {
        return autoFlag;
    }

    public void setAutoFlag(BooleanFilter autoFlag) {
        this.autoFlag = autoFlag;
    }

    public ZonedDateTimeFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTimeFilter updateDate) {
        this.updateDate = updateDate;
    }

    public BooleanFilter getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(BooleanFilter deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public StringFilter getChannel() {
        return channel;
    }

    public void setChannel(StringFilter channel) {
        this.channel = channel;
    }

    public StringFilter getJsCondition() {
        return jsCondition;
    }

    public void setJsCondition(StringFilter jsCondition) {
        this.jsCondition = jsCondition;
    }

    public StringFilter getJsBonus() {
        return jsBonus;
    }

    public void setJsBonus(StringFilter jsBonus) {
        this.jsBonus = jsBonus;
    }

    public BooleanFilter getRedeemFlag() {
        return redeemFlag;
    }

    public void setRedeemFlag(BooleanFilter redeemFlag) {
        this.redeemFlag = redeemFlag;
    }

    public StringFilter getImagePath() {
        return imagePath;
    }

    public void setImagePath(StringFilter imagePath) {
        this.imagePath = imagePath;
    }

    public StringFilter getImagePath2() {
        return imagePath2;
    }

    public void setImagePath2(StringFilter imagePath2) {
        this.imagePath2 = imagePath2;
    }

    public DoubleFilter getRedeemPoints() {
        return redeemPoints;
    }

    public void setRedeemPoints(DoubleFilter redeemPoints) {
        this.redeemPoints = redeemPoints;
    }

    public LocalDateFilter getPublishStartDate() {
        return publishStartDate;
    }

    public void setPublishStartDate(LocalDateFilter publishStartDate) {
        this.publishStartDate = publishStartDate;
    }

    public LocalDateFilter getPublishEndDate() {
        return publishEndDate;
    }

    public void setPublishEndDate(LocalDateFilter publishEndDate) {
        this.publishEndDate = publishEndDate;
    }

    @Override
    public String toString() {
        return "PromotionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (couponCode != null ? "couponCode=" + couponCode + ", " : "") +
                (promotionName != null ? "promotionName=" + promotionName + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (htmlDescription != null ? "htmlDescription=" + htmlDescription + ", " : "") +
                (start_date != null ? "start_date=" + start_date + ", " : "") +
                (endDate != null ? "endDate=" + endDate + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (stopNextRule != null ? "stopNextRule=" + stopNextRule + ", " : "") +
                (sortOrder != null ? "sortOrder=" + sortOrder + ", " : "") +
                (useOrgPriceFlag != null ? "useOrgPriceFlag=" + useOrgPriceFlag + ", " : "") +
                (appliedOn != null ? "appliedOn=" + appliedOn + ", " : "") +
                (droolsRule != null ? "droolsRule=" + droolsRule + ", " : "") +
                (version != null ? "version=" + version + ", " : "") +
                (runMode != null ? "runMode=" + runMode + ", " : "") +
                (onStore != null ? "onStore=" + onStore + ", " : "") +
                (promotionType != null ? "promotionType=" + promotionType + ", " : "") +
                (autoFlag != null ? "autoFlag=" + autoFlag + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (deleteFlag != null ? "deleteFlag=" + deleteFlag + ", " : "") +
                (channel != null ? "channel=" + channel + ", " : "") +
                (jsCondition != null ? "jsCondition=" + jsCondition + ", " : "") +
                (jsBonus != null ? "jsBonus=" + jsBonus + ", " : "") +
                (redeemFlag != null ? "redeemFlag=" + redeemFlag + ", " : "") +
                (imagePath != null ? "imagePath=" + imagePath + ", " : "") +
                (imagePath2 != null ? "imagePath2=" + imagePath2 + ", " : "") +
                (redeemPoints != null ? "redeemPoints=" + redeemPoints + ", " : "") +
                (publishStartDate != null ? "publishStartDate=" + publishStartDate + ", " : "") +
                (publishEndDate != null ? "publishEndDate=" + publishEndDate + ", " : "") +
            "}";
    }

}
