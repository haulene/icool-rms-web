package com.gcs.flexba.rmsweb.service.dto;

import java.util.List;

public class MergeCustomerDTO {
	
	private String keepCustomerCode;
	private CustomerDTO keepCustomer;
	private String choosenLevelCode; // If null auto select higher
	private boolean deleteOldCustomer;
	
	List<CustomerDTO> removedCustomers;

	public String getKeepCustomerCode() {
		return keepCustomerCode;
	}

	public CustomerDTO getKeepCustomer() {
		return keepCustomer;
	}

	public String getChoosenLevelCode() {
		return choosenLevelCode;
	}

	public List<CustomerDTO> getRemovedCustomers() {
		return removedCustomers;
	}

	public void setKeepCustomerCode(String keepCustomerCode) {
		this.keepCustomerCode = keepCustomerCode;
	}

	public void setKeepCustomer(CustomerDTO keepCustomer) {
		this.keepCustomer = keepCustomer;
	}

	public void setChoosenLevelCode(String choosenLevelCode) {
		this.choosenLevelCode = choosenLevelCode;
	}

	public void setRemovedCustomers(List<CustomerDTO> removedCustomers) {
		this.removedCustomers = removedCustomers;
	}

	public boolean isDeleteOldCustomer() {
		return deleteOldCustomer;
	}

	public void setDeleteOldCustomer(boolean deleteOldCustomer) {
		this.deleteOldCustomer = deleteOldCustomer;
	}
	
	
	
}
