package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the MembershipHistoty entity. This class is used in MembershipHistotyResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /membership-histoties?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MembershipHistotyCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter customerCode;

    private StringFilter membershipLevelCode;

    private ZonedDateTimeFilter effectedDate;

    private ZonedDateTimeFilter startDate;

    private ZonedDateTimeFilter expiredDate;

    private IntegerFilter status;

    private DoubleFilter accumulationPoints;

    private DoubleFilter pendingPoints;

    private DoubleFilter activatedPoints;

    private DoubleFilter expiredPoints;

    private DoubleFilter redeemPoints;

    private DoubleFilter remainPoints;

    private ZonedDateTimeFilter updateDate;

    private DoubleFilter transferablePoints;

    private IntegerFilter attempt;

    private IntegerFilter deleteFlag;

    private LongFilter customerId;

    public MembershipHistotyCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(StringFilter customerCode) {
        this.customerCode = customerCode;
    }

    public StringFilter getMembershipLevelCode() {
        return membershipLevelCode;
    }

    public void setMembershipLevelCode(StringFilter membershipLevelCode) {
        this.membershipLevelCode = membershipLevelCode;
    }

    public ZonedDateTimeFilter getEffectedDate() {
        return effectedDate;
    }

    public void setEffectedDate(ZonedDateTimeFilter effectedDate) {
        this.effectedDate = effectedDate;
    }

    public ZonedDateTimeFilter getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTimeFilter startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTimeFilter getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(ZonedDateTimeFilter expiredDate) {
        this.expiredDate = expiredDate;
    }

    public IntegerFilter getStatus() {
        return status;
    }

    public void setStatus(IntegerFilter status) {
        this.status = status;
    }

    public DoubleFilter getAccumulationPoints() {
        return accumulationPoints;
    }

    public void setAccumulationPoints(DoubleFilter accumulationPoints) {
        this.accumulationPoints = accumulationPoints;
    }

    public DoubleFilter getPendingPoints() {
        return pendingPoints;
    }

    public void setPendingPoints(DoubleFilter pendingPoints) {
        this.pendingPoints = pendingPoints;
    }

    public DoubleFilter getActivatedPoints() {
        return activatedPoints;
    }

    public void setActivatedPoints(DoubleFilter activatedPoints) {
        this.activatedPoints = activatedPoints;
    }

    public DoubleFilter getExpiredPoints() {
        return expiredPoints;
    }

    public void setExpiredPoints(DoubleFilter expiredPoints) {
        this.expiredPoints = expiredPoints;
    }

    public DoubleFilter getRedeemPoints() {
        return redeemPoints;
    }

    public void setRedeemPoints(DoubleFilter redeemPoints) {
        this.redeemPoints = redeemPoints;
    }

    public DoubleFilter getRemainPoints() {
        return remainPoints;
    }

    public void setRemainPoints(DoubleFilter remainPoints) {
        this.remainPoints = remainPoints;
    }

    public ZonedDateTimeFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTimeFilter updateDate) {
        this.updateDate = updateDate;
    }

    public DoubleFilter getTransferablePoints() {
        return transferablePoints;
    }

    public void setTransferablePoints(DoubleFilter transferablePoints) {
        this.transferablePoints = transferablePoints;
    }

    public IntegerFilter getAttempt() {
        return attempt;
    }

    public void setAttempt(IntegerFilter attempt) {
        this.attempt = attempt;
    }

    public IntegerFilter getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(IntegerFilter deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public LongFilter getCustomerId() {
        return customerId;
    }

    public void setCustomerId(LongFilter customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "MembershipHistotyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (customerCode != null ? "customerCode=" + customerCode + ", " : "") +
                (membershipLevelCode != null ? "membershipLevelCode=" + membershipLevelCode + ", " : "") +
                (effectedDate != null ? "effectedDate=" + effectedDate + ", " : "") +
                (startDate != null ? "startDate=" + startDate + ", " : "") +
                (expiredDate != null ? "expiredDate=" + expiredDate + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (accumulationPoints != null ? "accumulationPoints=" + accumulationPoints + ", " : "") +
                (pendingPoints != null ? "pendingPoints=" + pendingPoints + ", " : "") +
                (activatedPoints != null ? "activatedPoints=" + activatedPoints + ", " : "") +
                (expiredPoints != null ? "expiredPoints=" + expiredPoints + ", " : "") +
                (redeemPoints != null ? "redeemPoints=" + redeemPoints + ", " : "") +
                (remainPoints != null ? "remainPoints=" + remainPoints + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (transferablePoints != null ? "transferablePoints=" + transferablePoints + ", " : "") +
                (attempt != null ? "attempt=" + attempt + ", " : "") +
                (deleteFlag != null ? "deleteFlag=" + deleteFlag + ", " : "") +
                (customerId != null ? "customerId=" + customerId + ", " : "") +
            "}";
    }

}
