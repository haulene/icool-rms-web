package com.gcs.flexba.rmsweb.service.mapper;

import com.gcs.flexba.rmsweb.domain.*;
import com.gcs.flexba.rmsweb.service.dto.RatingQuestionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RatingQuestion and its DTO RatingQuestionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RatingQuestionMapper extends EntityMapper<RatingQuestionDTO, RatingQuestion> {



    default RatingQuestion fromId(Long id) {
        if (id == null) {
            return null;
        }
        RatingQuestion ratingQuestion = new RatingQuestion();
        ratingQuestion.setId(id);
        return ratingQuestion;
    }
}
