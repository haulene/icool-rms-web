package com.gcs.flexba.rmsweb.service.impl;

import com.gcs.flexba.rmsweb.service.MembershipHistotyService;
import com.gcs.flexba.rmsweb.domain.MembershipHistoty;
import com.gcs.flexba.rmsweb.repository.MembershipHistotyRepository;
import com.gcs.flexba.rmsweb.service.dto.MembershipHistotyDTO;
import com.gcs.flexba.rmsweb.service.mapper.MembershipHistotyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing MembershipHistoty.
 */
@Service
@Transactional
public class MembershipHistotyServiceImpl implements MembershipHistotyService {

    private final Logger log = LoggerFactory.getLogger(MembershipHistotyServiceImpl.class);

    private final MembershipHistotyRepository membershipHistotyRepository;

    private final MembershipHistotyMapper membershipHistotyMapper;

    public MembershipHistotyServiceImpl(MembershipHistotyRepository membershipHistotyRepository, MembershipHistotyMapper membershipHistotyMapper) {
        this.membershipHistotyRepository = membershipHistotyRepository;
        this.membershipHistotyMapper = membershipHistotyMapper;
    }

    /**
     * Save a membershipHistoty.
     *
     * @param membershipHistotyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public MembershipHistotyDTO save(MembershipHistotyDTO membershipHistotyDTO) {
        log.debug("Request to save MembershipHistoty : {}", membershipHistotyDTO);
        MembershipHistoty membershipHistoty = membershipHistotyMapper.toEntity(membershipHistotyDTO);
        membershipHistoty = membershipHistotyRepository.save(membershipHistoty);
        return membershipHistotyMapper.toDto(membershipHistoty);
    }

    /**
     * Get all the membershipHistoties.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MembershipHistotyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MembershipHistoties");
        return membershipHistotyRepository.findAll(pageable)
            .map(membershipHistotyMapper::toDto);
    }


    /**
     * Get one membershipHistoty by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MembershipHistotyDTO> findOne(Long id) {
        log.debug("Request to get MembershipHistoty : {}", id);
        return membershipHistotyRepository.findById(id)
            .map(membershipHistotyMapper::toDto);
    }

    /**
     * Delete the membershipHistoty by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MembershipHistoty : {}", id);
        membershipHistotyRepository.deleteById(id);
    }
}
