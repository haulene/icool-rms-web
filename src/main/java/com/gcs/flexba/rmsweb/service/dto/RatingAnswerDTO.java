package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RatingAnswer entity.
 */
public class RatingAnswerDTO implements Serializable {

    private Long id;

    private Integer ratingId;

    private Integer ratingQuestionId;

    private Integer type;

    private String value;

    private String answerCategory;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRatingId() {
        return ratingId;
    }

    public void setRatingId(Integer ratingId) {
        this.ratingId = ratingId;
    }

    public Integer getRatingQuestionId() {
        return ratingQuestionId;
    }

    public void setRatingQuestionId(Integer ratingQuestionId) {
        this.ratingQuestionId = ratingQuestionId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAnswerCategory() {
        return answerCategory;
    }

    public void setAnswerCategory(String answerCategory) {
        this.answerCategory = answerCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RatingAnswerDTO ratingAnswerDTO = (RatingAnswerDTO) o;
        if (ratingAnswerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ratingAnswerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RatingAnswerDTO{" +
            "id=" + getId() +
            ", ratingId=" + getRatingId() +
            ", ratingQuestionId=" + getRatingQuestionId() +
            ", type=" + getType() +
            ", value='" + getValue() + "'" +
            ", answerCategory='" + getAnswerCategory() + "'" +
            "}";
    }
}
