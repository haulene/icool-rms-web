package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Type entity. This class is used in TypeResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /types?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TypeCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter typeClass;

    private StringFilter typeCode;

    private StringFilter typeName;

    private StringFilter typeNameEn;

    private StringFilter note;

    private IntegerFilter sortOrder;

    private ZonedDateTimeFilter updateDate;

    private BooleanFilter deleteFlag;

    public TypeCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTypeClass() {
        return typeClass;
    }

    public void setTypeClass(StringFilter typeClass) {
        this.typeClass = typeClass;
    }

    public StringFilter getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(StringFilter typeCode) {
        this.typeCode = typeCode;
    }

    public StringFilter getTypeName() {
        return typeName;
    }

    public void setTypeName(StringFilter typeName) {
        this.typeName = typeName;
    }

    public StringFilter getTypeNameEn() {
        return typeNameEn;
    }

    public void setTypeNameEn(StringFilter typeNameEn) {
        this.typeNameEn = typeNameEn;
    }

    public StringFilter getNote() {
        return note;
    }

    public void setNote(StringFilter note) {
        this.note = note;
    }

    public IntegerFilter getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(IntegerFilter sortOrder) {
        this.sortOrder = sortOrder;
    }

    public ZonedDateTimeFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTimeFilter updateDate) {
        this.updateDate = updateDate;
    }

    public BooleanFilter getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(BooleanFilter deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @Override
    public String toString() {
        return "TypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (typeClass != null ? "typeClass=" + typeClass + ", " : "") +
                (typeCode != null ? "typeCode=" + typeCode + ", " : "") +
                (typeName != null ? "typeName=" + typeName + ", " : "") +
                (typeNameEn != null ? "typeNameEn=" + typeNameEn + ", " : "") +
                (note != null ? "note=" + note + ", " : "") +
                (sortOrder != null ? "sortOrder=" + sortOrder + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (deleteFlag != null ? "deleteFlag=" + deleteFlag + ", " : "") +
            "}";
    }

}
