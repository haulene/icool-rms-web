package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Config entity. This class is used in ConfigResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /configs?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ConfigCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private StringFilter id;

    private StringFilter configValue;

    private StringFilter syncConfigFlag;

    private BooleanFilter allowChangeSyncConfigFlag;

    private ZonedDateTimeFilter updateDate;

    private StringFilter channel;

    public ConfigCriteria() {
    }

    public StringFilter getId() {
        return id;
    }

    public void setId(StringFilter id) {
        this.id = id;
    }

    public StringFilter getConfigValue() {
        return configValue;
    }

    public void setConfigValue(StringFilter configValue) {
        this.configValue = configValue;
    }

    public StringFilter getSyncConfigFlag() {
        return syncConfigFlag;
    }

    public void setSyncConfigFlag(StringFilter syncConfigFlag) {
        this.syncConfigFlag = syncConfigFlag;
    }

    public BooleanFilter getAllowChangeSyncConfigFlag() {
        return allowChangeSyncConfigFlag;
    }

    public void setAllowChangeSyncConfigFlag(BooleanFilter allowChangeSyncConfigFlag) {
        this.allowChangeSyncConfigFlag = allowChangeSyncConfigFlag;
    }

    public ZonedDateTimeFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTimeFilter updateDate) {
        this.updateDate = updateDate;
    }

    public StringFilter getChannel() {
        return channel;
    }

    public void setChannel(StringFilter channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        return "ConfigCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (configValue != null ? "configValue=" + configValue + ", " : "") +
                (syncConfigFlag != null ? "syncConfigFlag=" + syncConfigFlag + ", " : "") +
                (allowChangeSyncConfigFlag != null ? "allowChangeSyncConfigFlag=" + allowChangeSyncConfigFlag + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (channel != null ? "channel=" + channel + ", " : "") +
            "}";
    }

}
