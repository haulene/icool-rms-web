package com.gcs.flexba.rmsweb.service.mapper;

import com.gcs.flexba.rmsweb.domain.*;
import com.gcs.flexba.rmsweb.service.dto.RatingAnswerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RatingAnswer and its DTO RatingAnswerDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RatingAnswerMapper extends EntityMapper<RatingAnswerDTO, RatingAnswer> {



    default RatingAnswer fromId(Long id) {
        if (id == null) {
            return null;
        }
        RatingAnswer ratingAnswer = new RatingAnswer();
        ratingAnswer.setId(id);
        return ratingAnswer;
    }
}
