package com.gcs.flexba.rmsweb.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Config entity.
 */
public class ConfigDTO implements Serializable {

    private String id;

    private String configValue;

    private String syncConfigFlag;

    private Boolean allowChangeSyncConfigFlag;

    private ZonedDateTime updateDate;

    private String channel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public String getSyncConfigFlag() {
        return syncConfigFlag;
    }

    public void setSyncConfigFlag(String syncConfigFlag) {
        this.syncConfigFlag = syncConfigFlag;
    }

    public Boolean isAllowChangeSyncConfigFlag() {
        return allowChangeSyncConfigFlag;
    }

    public void setAllowChangeSyncConfigFlag(Boolean allowChangeSyncConfigFlag) {
        this.allowChangeSyncConfigFlag = allowChangeSyncConfigFlag;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConfigDTO configDTO = (ConfigDTO) o;
        if (configDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), configDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConfigDTO{" +
            "id=" + getId() +
            ", configValue='" + getConfigValue() + "'" +
            ", syncConfigFlag='" + getSyncConfigFlag() + "'" +
            ", allowChangeSyncConfigFlag='" + isAllowChangeSyncConfigFlag() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", channel='" + getChannel() + "'" +
            "}";
    }
}
