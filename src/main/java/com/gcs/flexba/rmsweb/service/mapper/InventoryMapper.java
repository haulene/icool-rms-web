package com.gcs.flexba.rmsweb.service.mapper;

import com.gcs.flexba.rmsweb.domain.*;
import com.gcs.flexba.rmsweb.service.dto.InventoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Inventory and its DTO InventoryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InventoryMapper extends EntityMapper<InventoryDTO, Inventory> {



    default Inventory fromId(Long id) {
        if (id == null) {
            return null;
        }
        Inventory inventory = new Inventory();
        inventory.setId(id);
        return inventory;
    }
}
