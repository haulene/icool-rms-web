package com.gcs.flexba.rmsweb.service;

import com.gcs.flexba.rmsweb.service.dto.PromotionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Promotion.
 */
public interface PromotionService {

    /**
     * Save a promotion.
     *
     * @param promotionDTO the entity to save
     * @return the persisted entity
     * @throws Exception 
     */
    PromotionDTO save(PromotionDTO promotionDTO) throws Exception;

    /**
     * Get all the promotions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     * @throws Exception 
     */
    Page<PromotionDTO> findAll(Pageable pageable) throws Exception;


    /**
     * Get the "id" promotion.
     *
     * @param id the id of the entity
     * @return the entity
     * @throws Exception 
     */
    Optional<PromotionDTO> findOne(Long id) throws Exception;

    /**
     * Delete the "id" promotion.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
