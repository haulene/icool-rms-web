package com.gcs.flexba.rmsweb.service;

import com.gcs.flexba.rmsweb.service.dto.VoucherGiftDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing VoucherGift.
 */
public interface VoucherGiftService {

    /**
     * Save a voucherGift.
     *
     * @param voucherGiftDTO the entity to save
     * @return the persisted entity
     */
    VoucherGiftDTO save(VoucherGiftDTO voucherGiftDTO);

    /**
     * Get all the voucherGifts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<VoucherGiftDTO> findAll(Pageable pageable);


    /**
     * Get the "id" voucherGift.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<VoucherGiftDTO> findOne(Long id);

    /**
     * Delete the "id" voucherGift.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
