package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.gcs.flexba.rmsweb.domain.CustomerRating;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.CustomerRatingRepository;
import com.gcs.flexba.rmsweb.service.dto.CustomerRatingCriteria;

import com.gcs.flexba.rmsweb.service.dto.CustomerRatingDTO;
import com.gcs.flexba.rmsweb.service.mapper.CustomerRatingMapper;

/**
 * Service for executing complex queries for CustomerRating entities in the database.
 * The main input is a {@link CustomerRatingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CustomerRatingDTO} or a {@link Page} of {@link CustomerRatingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CustomerRatingQueryService extends QueryService<CustomerRating> {

    private final Logger log = LoggerFactory.getLogger(CustomerRatingQueryService.class);

    private final CustomerRatingRepository customerRatingRepository;

    private final CustomerRatingMapper customerRatingMapper;

    public CustomerRatingQueryService(CustomerRatingRepository customerRatingRepository, CustomerRatingMapper customerRatingMapper) {
        this.customerRatingRepository = customerRatingRepository;
        this.customerRatingMapper = customerRatingMapper;
    }

    /**
     * Return a {@link List} of {@link CustomerRatingDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CustomerRatingDTO> findByCriteria(CustomerRatingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CustomerRating> specification = createSpecification(criteria);
        return customerRatingMapper.toDto(customerRatingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CustomerRatingDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CustomerRatingDTO> findByCriteria(CustomerRatingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CustomerRating> specification = createSpecification(criteria);
        return customerRatingRepository.findAll(specification, page)
            .map(customerRatingMapper::toDto);
    }

    /**
     * Function to convert CustomerRatingCriteria to a {@link Specification}
     */
    private Specification<CustomerRating> createSpecification(CustomerRatingCriteria criteria) {
        Specification<CustomerRating> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CustomerRating_.id));
            }
            if (criteria.getNumStar() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumStar(), CustomerRating_.numStar));
            }
            if (criteria.getCustomerCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCustomerCode(), CustomerRating_.customerCode));
            }
            if (criteria.getSource() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSource(), CustomerRating_.source));
            }
            if (criteria.getInventoryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInventoryCode(), CustomerRating_.inventoryCode));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getType(), CustomerRating_.type));
            }
            if (criteria.getComment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComment(), CustomerRating_.comment));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), CustomerRating_.updatedDate));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CustomerRating_.createdDate));
            }
            if (criteria.getDeleteFlag() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeleteFlag(), CustomerRating_.deleteFlag));
            }
        }
        return specification;
    }

}
