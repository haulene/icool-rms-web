package com.gcs.flexba.rmsweb.service;

import com.gcs.flexba.rmsweb.service.dto.CustomerRatingDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing CustomerRating.
 */
public interface CustomerRatingService {

    /**
     * Save a customerRating.
     *
     * @param customerRatingDTO the entity to save
     * @return the persisted entity
     */
    CustomerRatingDTO save(CustomerRatingDTO customerRatingDTO);

    /**
     * Get all the customerRatings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<CustomerRatingDTO> findAll(Pageable pageable);


    /**
     * Get the "id" customerRating.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CustomerRatingDTO> findOne(Long id);

    /**
     * Delete the "id" customerRating.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
