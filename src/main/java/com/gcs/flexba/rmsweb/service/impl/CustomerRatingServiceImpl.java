package com.gcs.flexba.rmsweb.service.impl;

import com.gcs.flexba.rmsweb.service.CustomerRatingService;
import com.gcs.flexba.rmsweb.domain.CustomerRating;
import com.gcs.flexba.rmsweb.repository.CustomerRatingRepository;
import com.gcs.flexba.rmsweb.service.dto.CustomerRatingDTO;
import com.gcs.flexba.rmsweb.service.mapper.CustomerRatingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing CustomerRating.
 */
@Service
@Transactional
public class CustomerRatingServiceImpl implements CustomerRatingService {

    private final Logger log = LoggerFactory.getLogger(CustomerRatingServiceImpl.class);

    private final CustomerRatingRepository customerRatingRepository;

    private final CustomerRatingMapper customerRatingMapper;

    public CustomerRatingServiceImpl(CustomerRatingRepository customerRatingRepository, CustomerRatingMapper customerRatingMapper) {
        this.customerRatingRepository = customerRatingRepository;
        this.customerRatingMapper = customerRatingMapper;
    }

    /**
     * Save a customerRating.
     *
     * @param customerRatingDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CustomerRatingDTO save(CustomerRatingDTO customerRatingDTO) {
        log.debug("Request to save CustomerRating : {}", customerRatingDTO);
        CustomerRating customerRating = customerRatingMapper.toEntity(customerRatingDTO);
        customerRating = customerRatingRepository.save(customerRating);
        return customerRatingMapper.toDto(customerRating);
    }

    /**
     * Get all the customerRatings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CustomerRatingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CustomerRatings");
        return customerRatingRepository.findAll(pageable)
            .map(customerRatingMapper::toDto);
    }


    /**
     * Get one customerRating by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CustomerRatingDTO> findOne(Long id) {
        log.debug("Request to get CustomerRating : {}", id);
        return customerRatingRepository.findById(id)
            .map(customerRatingMapper::toDto);
    }

    /**
     * Delete the customerRating by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CustomerRating : {}", id);
        customerRatingRepository.deleteById(id);
    }
}
