package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.gcs.flexba.rmsweb.domain.MembershipHistoty;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.MembershipHistotyRepository;
import com.gcs.flexba.rmsweb.service.dto.MembershipHistotyCriteria;

import com.gcs.flexba.rmsweb.service.dto.MembershipHistotyDTO;
import com.gcs.flexba.rmsweb.service.mapper.MembershipHistotyMapper;

/**
 * Service for executing complex queries for MembershipHistoty entities in the database.
 * The main input is a {@link MembershipHistotyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MembershipHistotyDTO} or a {@link Page} of {@link MembershipHistotyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MembershipHistotyQueryService extends QueryService<MembershipHistoty> {

    private final Logger log = LoggerFactory.getLogger(MembershipHistotyQueryService.class);

    private final MembershipHistotyRepository membershipHistotyRepository;

    private final MembershipHistotyMapper membershipHistotyMapper;

    public MembershipHistotyQueryService(MembershipHistotyRepository membershipHistotyRepository, MembershipHistotyMapper membershipHistotyMapper) {
        this.membershipHistotyRepository = membershipHistotyRepository;
        this.membershipHistotyMapper = membershipHistotyMapper;
    }

    /**
     * Return a {@link List} of {@link MembershipHistotyDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MembershipHistotyDTO> findByCriteria(MembershipHistotyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MembershipHistoty> specification = createSpecification(criteria);
        return membershipHistotyMapper.toDto(membershipHistotyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MembershipHistotyDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MembershipHistotyDTO> findByCriteria(MembershipHistotyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MembershipHistoty> specification = createSpecification(criteria);
        return membershipHistotyRepository.findAll(specification, page)
            .map(membershipHistotyMapper::toDto);
    }

    /**
     * Function to convert MembershipHistotyCriteria to a {@link Specification}
     */
    private Specification<MembershipHistoty> createSpecification(MembershipHistotyCriteria criteria) {
        Specification<MembershipHistoty> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MembershipHistoty_.id));
            }
            if (criteria.getMembershipLevelCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMembershipLevelCode(), MembershipHistoty_.membershipLevelCode));
            }
            if (criteria.getEffectedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEffectedDate(), MembershipHistoty_.effectedDate));
            }
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), MembershipHistoty_.startDate));
            }
            if (criteria.getExpiredDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExpiredDate(), MembershipHistoty_.expiredDate));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), MembershipHistoty_.status));
            }
            if (criteria.getAccumulationPoints() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAccumulationPoints(), MembershipHistoty_.accumulationPoints));
            }
            if (criteria.getPendingPoints() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPendingPoints(), MembershipHistoty_.pendingPoints));
            }
            if (criteria.getActivatedPoints() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActivatedPoints(), MembershipHistoty_.activatedPoints));
            }
            if (criteria.getExpiredPoints() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExpiredPoints(), MembershipHistoty_.expiredPoints));
            }
            if (criteria.getRedeemPoints() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRedeemPoints(), MembershipHistoty_.redeemPoints));
            }
            if (criteria.getRemainPoints() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRemainPoints(), MembershipHistoty_.remainPoints));
            }
            if (criteria.getUpdateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateDate(), MembershipHistoty_.updateDate));
            }
            if (criteria.getTransferablePoints() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTransferablePoints(), MembershipHistoty_.transferablePoints));
            }
            if (criteria.getDeleteFlag() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeleteFlag(), MembershipHistoty_.deleteFlag));
            }
            if (criteria.getCustomerId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCustomerId(), MembershipHistoty_.customer, Customer_.id));
            }
        }
        return specification;
    }

}
