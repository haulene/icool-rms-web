package com.gcs.flexba.rmsweb.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the MembershipHistoty entity.
 */
public class MembershipHistotyDTO implements Serializable {

    private Long id;

    private String customerCode;

    private String membershipLevelCode;

    private ZonedDateTime effectedDate;

    private ZonedDateTime startDate;

    private ZonedDateTime expiredDate;

    private Integer status;

    private Double accumulationPoints;

    private Double pendingPoints;

    private Double activatedPoints;

    private Double expiredPoints;

    private Double redeemPoints;

    private Double remainPoints;

    private ZonedDateTime updateDate;

    private Double transferablePoints;

    private Integer deleteFlag;

    private Long customerId;

    private String customerCustomerCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getMembershipLevelCode() {
        return membershipLevelCode;
    }

    public void setMembershipLevelCode(String membershipLevelCode) {
        this.membershipLevelCode = membershipLevelCode;
    }

    public ZonedDateTime getEffectedDate() {
        return effectedDate;
    }

    public void setEffectedDate(ZonedDateTime effectedDate) {
        this.effectedDate = effectedDate;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(ZonedDateTime expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getAccumulationPoints() {
        return accumulationPoints;
    }

    public void setAccumulationPoints(Double accumulationPoints) {
        this.accumulationPoints = accumulationPoints;
    }

    public Double getPendingPoints() {
        return pendingPoints;
    }

    public void setPendingPoints(Double pendingPoints) {
        this.pendingPoints = pendingPoints;
    }

    public Double getActivatedPoints() {
        return activatedPoints;
    }

    public void setActivatedPoints(Double activatedPoints) {
        this.activatedPoints = activatedPoints;
    }

    public Double getExpiredPoints() {
        return expiredPoints;
    }

    public void setExpiredPoints(Double expiredPoints) {
        this.expiredPoints = expiredPoints;
    }

    public Double getRedeemPoints() {
        return redeemPoints;
    }

    public void setRedeemPoints(Double redeemPoints) {
        this.redeemPoints = redeemPoints;
    }

    public Double getRemainPoints() {
        return remainPoints;
    }

    public void setRemainPoints(Double remainPoints) {
        this.remainPoints = remainPoints;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Double getTransferablePoints() {
        return transferablePoints;
    }

    public void setTransferablePoints(Double transferablePoints) {
        this.transferablePoints = transferablePoints;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerCustomerCode() {
        return customerCustomerCode;
    }

    public void setCustomerCustomerCode(String customerCustomerCode) {
        this.customerCustomerCode = customerCustomerCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MembershipHistotyDTO membershipHistotyDTO = (MembershipHistotyDTO) o;
        if (membershipHistotyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), membershipHistotyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MembershipHistotyDTO{" +
            "id=" + getId() +
            ", customerCode='" + getCustomerCode() + "'" +
            ", membershipLevelCode='" + getMembershipLevelCode() + "'" +
            ", effectedDate='" + getEffectedDate() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", expiredDate='" + getExpiredDate() + "'" +
            ", status=" + getStatus() +
            ", accumulationPoints=" + getAccumulationPoints() +
            ", pendingPoints=" + getPendingPoints() +
            ", activatedPoints=" + getActivatedPoints() +
            ", expiredPoints=" + getExpiredPoints() +
            ", redeemPoints=" + getRedeemPoints() +
            ", remainPoints=" + getRemainPoints() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", transferablePoints=" + getTransferablePoints() +
            ", deleteFlag=" + getDeleteFlag() +
            ", customer=" + getCustomerId() +
            ", customer='" + getCustomerCustomerCode() + "'" +
            "}";
    }
}
