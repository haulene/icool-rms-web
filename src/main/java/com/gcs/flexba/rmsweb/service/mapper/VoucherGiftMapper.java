package com.gcs.flexba.rmsweb.service.mapper;

import com.gcs.flexba.rmsweb.domain.*;
import com.gcs.flexba.rmsweb.service.dto.VoucherGiftDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity VoucherGift and its DTO VoucherGiftDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VoucherGiftMapper extends EntityMapper<VoucherGiftDTO, VoucherGift> {



    default VoucherGift fromId(Long id) {
        if (id == null) {
            return null;
        }
        VoucherGift voucherGift = new VoucherGift();
        voucherGift.setId(id);
        return voucherGift;
    }
}
