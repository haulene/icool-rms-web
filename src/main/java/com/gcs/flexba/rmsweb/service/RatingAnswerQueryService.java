package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.gcs.flexba.rmsweb.domain.RatingAnswer;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.RatingAnswerRepository;
import com.gcs.flexba.rmsweb.service.dto.RatingAnswerCriteria;

import com.gcs.flexba.rmsweb.service.dto.RatingAnswerDTO;
import com.gcs.flexba.rmsweb.service.mapper.RatingAnswerMapper;

/**
 * Service for executing complex queries for RatingAnswer entities in the database.
 * The main input is a {@link RatingAnswerCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RatingAnswerDTO} or a {@link Page} of {@link RatingAnswerDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RatingAnswerQueryService extends QueryService<RatingAnswer> {

    private final Logger log = LoggerFactory.getLogger(RatingAnswerQueryService.class);

    private final RatingAnswerRepository ratingAnswerRepository;

    private final RatingAnswerMapper ratingAnswerMapper;

    public RatingAnswerQueryService(RatingAnswerRepository ratingAnswerRepository, RatingAnswerMapper ratingAnswerMapper) {
        this.ratingAnswerRepository = ratingAnswerRepository;
        this.ratingAnswerMapper = ratingAnswerMapper;
    }

    /**
     * Return a {@link List} of {@link RatingAnswerDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RatingAnswerDTO> findByCriteria(RatingAnswerCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RatingAnswer> specification = createSpecification(criteria);
        return ratingAnswerMapper.toDto(ratingAnswerRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RatingAnswerDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RatingAnswerDTO> findByCriteria(RatingAnswerCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RatingAnswer> specification = createSpecification(criteria);
        return ratingAnswerRepository.findAll(specification, page)
            .map(ratingAnswerMapper::toDto);
    }

    /**
     * Function to convert RatingAnswerCriteria to a {@link Specification}
     */
    private Specification<RatingAnswer> createSpecification(RatingAnswerCriteria criteria) {
        Specification<RatingAnswer> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RatingAnswer_.id));
            }
            if (criteria.getRatingId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRatingId(), RatingAnswer_.ratingId));
            }
            if (criteria.getRatingQuestionId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRatingQuestionId(), RatingAnswer_.ratingQuestionId));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getType(), RatingAnswer_.type));
            }
            if (criteria.getValue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValue(), RatingAnswer_.value));
            }
            if (criteria.getAnswerCategory() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAnswerCategory(), RatingAnswer_.answerCategory));
            }
        }
        return specification;
    }

}
