package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.gcs.flexba.rmsweb.domain.VoucherGift;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.VoucherGiftRepository;
import com.gcs.flexba.rmsweb.service.dto.VoucherGiftCriteria;

import com.gcs.flexba.rmsweb.service.dto.VoucherGiftDTO;
import com.gcs.flexba.rmsweb.service.mapper.VoucherGiftMapper;

/**
 * Service for executing complex queries for VoucherGift entities in the database.
 * The main input is a {@link VoucherGiftCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link VoucherGiftDTO} or a {@link Page} of {@link VoucherGiftDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class VoucherGiftQueryService extends QueryService<VoucherGift> {

    private final Logger log = LoggerFactory.getLogger(VoucherGiftQueryService.class);

    private final VoucherGiftRepository voucherGiftRepository;

    private final VoucherGiftMapper voucherGiftMapper;

    public VoucherGiftQueryService(VoucherGiftRepository voucherGiftRepository, VoucherGiftMapper voucherGiftMapper) {
        this.voucherGiftRepository = voucherGiftRepository;
        this.voucherGiftMapper = voucherGiftMapper;
    }

    /**
     * Return a {@link List} of {@link VoucherGiftDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<VoucherGiftDTO> findByCriteria(VoucherGiftCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<VoucherGift> specification = createSpecification(criteria);
        return voucherGiftMapper.toDto(voucherGiftRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link VoucherGiftDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<VoucherGiftDTO> findByCriteria(VoucherGiftCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<VoucherGift> specification = createSpecification(criteria);
        return voucherGiftRepository.findAll(specification, page)
            .map(voucherGiftMapper::toDto);
    }

    /**
     * Function to convert VoucherGiftCriteria to a {@link Specification}
     */
    private Specification<VoucherGift> createSpecification(VoucherGiftCriteria criteria) {
        Specification<VoucherGift> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), VoucherGift_.id));
            }
            if (criteria.getVoucherGiftCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVoucherGiftCode(), VoucherGift_.voucherGiftCode));
            }
            if (criteria.getVoucherGiftName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVoucherGiftName(), VoucherGift_.voucherGiftName));
            }
            if (criteria.getVoucherCategory() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoucherCategory(), VoucherGift_.voucherCategory));
            }
            if (criteria.getVoucherType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoucherType(), VoucherGift_.voucherType));
            }
            if (criteria.getCustomerCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCustomerCode(), VoucherGift_.customerCode));
            }
            if (criteria.getCustomerName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCustomerName(), VoucherGift_.customerName));
            }
            if (criteria.getCustomerPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCustomerPhone(), VoucherGift_.customerPhone));
            }
            if (criteria.getTotalValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalValue(), VoucherGift_.totalValue));
            }
            if (criteria.getAvailableValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAvailableValue(), VoucherGift_.availableValue));
            }
            if (criteria.getValidFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValidFrom(), VoucherGift_.validFrom));
            }
            if (criteria.getValidTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValidTo(), VoucherGift_.validTo));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), VoucherGift_.status));
            }
            if (criteria.getRefSku() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRefSku(), VoucherGift_.refSku));
            }
            if (criteria.getStoreCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStoreCode(), VoucherGift_.storeCode));
            }
            if (criteria.getNote() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNote(), VoucherGift_.note));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), VoucherGift_.createdBy));
            }
            if (criteria.getExpiredDuration() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExpiredDuration(), VoucherGift_.expiredDuration));
            }
            if (criteria.getEffectedDay() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEffectedDay(), VoucherGift_.effectedDay));
            }
            if (criteria.getVoucherValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoucherValue(), VoucherGift_.voucherValue));
            }
            if (criteria.getUsedCustomerCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUsedCustomerCode(), VoucherGift_.usedCustomerCode));
            }
            if (criteria.getUsedCustomerName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUsedCustomerName(), VoucherGift_.usedCustomerName));
            }
            if (criteria.getUsedCustomerPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUsedCustomerPhone(), VoucherGift_.usedCustomerPhone));
            }
            if (criteria.getUpdateFlag() != null) {
                specification = specification.and(buildSpecification(criteria.getUpdateFlag(), VoucherGift_.updateFlag));
            }
            if (criteria.getMaxAppliedAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaxAppliedAmount(), VoucherGift_.maxAppliedAmount));
            }
            if (criteria.getMax_appliedTimePerCustomer() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMax_appliedTimePerCustomer(), VoucherGift_.max_appliedTimePerCustomer));
            }
            if (criteria.getVoucherAppliedType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoucherAppliedType(), VoucherGift_.voucherAppliedType));
            }
            if (criteria.getUpdateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateDate(), VoucherGift_.updateDate));
            }
            if (criteria.getDeleteFlag() != null) {
                specification = specification.and(buildSpecification(criteria.getDeleteFlag(), VoucherGift_.deleteFlag));
            }
            if (criteria.getPersonalId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPersonalId(), VoucherGift_.personalId));
            }
        }
        return specification;
    }

}
