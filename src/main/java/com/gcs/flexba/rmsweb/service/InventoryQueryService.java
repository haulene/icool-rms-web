package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.gcs.flexba.rmsweb.domain.Inventory;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.InventoryRepository;
import com.gcs.flexba.rmsweb.service.dto.InventoryCriteria;

import com.gcs.flexba.rmsweb.service.dto.InventoryDTO;
import com.gcs.flexba.rmsweb.service.mapper.InventoryMapper;

/**
 * Service for executing complex queries for Inventory entities in the database.
 * The main input is a {@link InventoryCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link InventoryDTO} or a {@link Page} of {@link InventoryDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class InventoryQueryService extends QueryService<Inventory> {

    private final Logger log = LoggerFactory.getLogger(InventoryQueryService.class);

    private final InventoryRepository inventoryRepository;

    private final InventoryMapper inventoryMapper;

    public InventoryQueryService(InventoryRepository inventoryRepository, InventoryMapper inventoryMapper) {
        this.inventoryRepository = inventoryRepository;
        this.inventoryMapper = inventoryMapper;
    }

    /**
     * Return a {@link List} of {@link InventoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<InventoryDTO> findByCriteria(InventoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Inventory> specification = createSpecification(criteria);
        return inventoryMapper.toDto(inventoryRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link InventoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<InventoryDTO> findByCriteria(InventoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Inventory> specification = createSpecification(criteria);
        return inventoryRepository.findAll(specification, page)
            .map(inventoryMapper::toDto);
    }

    /**
     * Function to convert InventoryCriteria to a {@link Specification}
     */
    private Specification<Inventory> createSpecification(InventoryCriteria criteria) {
        Specification<Inventory> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Inventory_.id));
            }
            if (criteria.getInventoryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInventoryCode(), Inventory_.inventoryCode));
            }
            if (criteria.getInventoryName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInventoryName(), Inventory_.inventoryName));
            }
            if (criteria.getRegionCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegionCode(), Inventory_.regionCode));
            }
            if (criteria.getInventoryType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInventoryType(), Inventory_.inventoryType));
            }
            if (criteria.getBrandCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBrandCode(), Inventory_.brandCode));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Inventory_.address));
            }
            if (criteria.getTel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTel(), Inventory_.tel));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), Inventory_.fax));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), Inventory_.email));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), Inventory_.status));
            }
            if (criteria.getChannel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChannel(), Inventory_.channel));
            }
            if (criteria.getAllowPickupFlag() != null) {
                specification = specification.and(buildSpecification(criteria.getAllowPickupFlag(), Inventory_.allowPickupFlag));
            }
            if (criteria.getAllowUrgentOrderFlag() != null) {
                specification = specification.and(buildSpecification(criteria.getAllowUrgentOrderFlag(), Inventory_.allowUrgentOrderFlag));
            }
            if (criteria.getAllowBookStudioFlag() != null) {
                specification = specification.and(buildSpecification(criteria.getAllowBookStudioFlag(), Inventory_.allowBookStudioFlag));
            }
            if (criteria.getUpdateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateDate(), Inventory_.updateDate));
            }
            if (criteria.getDeleteFlag() != null) {
                specification = specification.and(buildSpecification(criteria.getDeleteFlag(), Inventory_.deleteFlag));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLatitude(), Inventory_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLongitude(), Inventory_.longitude));
            }
        }
        return specification;
    }

}
