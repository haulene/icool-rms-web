package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Customer entity. This class is used in CustomerResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /customers?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CustomerCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter customerCode;

    private StringFilter firstName;

    private StringFilter middleName;

    private StringFilter lastName;

    private StringFilter fullName;

    private IntegerFilter gender;

    private StringFilter tel2;

    private StringFilter tel1;

    private StringFilter tel3;

    private StringFilter facebookId;

    private LongFilter membershipHistotyId;

    public CustomerCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(StringFilter customerCode) {
        this.customerCode = customerCode;
    }

    public StringFilter getFirstName() {
        return firstName;
    }

    public void setFirstName(StringFilter firstName) {
        this.firstName = firstName;
    }

    public StringFilter getMiddleName() {
        return middleName;
    }

    public void setMiddleName(StringFilter middleName) {
        this.middleName = middleName;
    }

    public StringFilter getLastName() {
        return lastName;
    }

    public void setLastName(StringFilter lastName) {
        this.lastName = lastName;
    }

    public StringFilter getFullName() {
        return fullName;
    }

    public void setFullName(StringFilter fullName) {
        this.fullName = fullName;
    }

    public IntegerFilter getGender() {
        return gender;
    }

    public void setGender(IntegerFilter gender) {
        this.gender = gender;
    }

    public StringFilter getTel2() {
        return tel2;
    }

    public void setTel2(StringFilter tel2) {
        this.tel2 = tel2;
    }

    public StringFilter getTel1() {
        return tel1;
    }

    public void setTel1(StringFilter tel1) {
        this.tel1 = tel1;
    }

    public StringFilter getTel3() {
        return tel3;
    }

    public void setTel3(StringFilter tel3) {
        this.tel3 = tel3;
    }

    public StringFilter getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(StringFilter facebookId) {
        this.facebookId = facebookId;
    }

    public LongFilter getMembershipHistotyId() {
        return membershipHistotyId;
    }

    public void setMembershipHistotyId(LongFilter membershipHistotyId) {
        this.membershipHistotyId = membershipHistotyId;
    }

    @Override
    public String toString() {
        return "CustomerCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (customerCode != null ? "customerCode=" + customerCode + ", " : "") +
                (firstName != null ? "firstName=" + firstName + ", " : "") +
                (middleName != null ? "middleName=" + middleName + ", " : "") +
                (lastName != null ? "lastName=" + lastName + ", " : "") +
                (fullName != null ? "fullName=" + fullName + ", " : "") +
                (gender != null ? "gender=" + gender + ", " : "") +
                (tel2 != null ? "tel2=" + tel2 + ", " : "") +
                (tel1 != null ? "tel1=" + tel1 + ", " : "") +
                (tel3 != null ? "tel3=" + tel3 + ", " : "") +
                (facebookId != null ? "facebookId=" + facebookId + ", " : "") +
                (membershipHistotyId != null ? "membershipHistotyId=" + membershipHistotyId + ", " : "") +
            "}";
    }

}
