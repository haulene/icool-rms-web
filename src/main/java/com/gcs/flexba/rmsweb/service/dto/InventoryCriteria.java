package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Inventory entity. This class is used in InventoryResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /inventories?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InventoryCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter inventoryCode;

    private StringFilter inventoryName;

    private StringFilter regionCode;

    private IntegerFilter inventoryType;

    private StringFilter brandCode;

    private StringFilter address;

    private StringFilter tel;

    private StringFilter fax;

    private StringFilter email;

    private IntegerFilter status;

    private StringFilter channel;

    private BooleanFilter allowPickupFlag;

    private BooleanFilter allowUrgentOrderFlag;

    private BooleanFilter allowBookStudioFlag;

    private ZonedDateTimeFilter updateDate;

    private BooleanFilter deleteFlag;

    private StringFilter latitude;

    private StringFilter longitude;

    public InventoryCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getInventoryCode() {
        return inventoryCode;
    }

    public void setInventoryCode(StringFilter inventoryCode) {
        this.inventoryCode = inventoryCode;
    }

    public StringFilter getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(StringFilter inventoryName) {
        this.inventoryName = inventoryName;
    }

    public StringFilter getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(StringFilter regionCode) {
        this.regionCode = regionCode;
    }

    public IntegerFilter getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(IntegerFilter inventoryType) {
        this.inventoryType = inventoryType;
    }

    public StringFilter getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(StringFilter brandCode) {
        this.brandCode = brandCode;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getTel() {
        return tel;
    }

    public void setTel(StringFilter tel) {
        this.tel = tel;
    }

    public StringFilter getFax() {
        return fax;
    }

    public void setFax(StringFilter fax) {
        this.fax = fax;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public IntegerFilter getStatus() {
        return status;
    }

    public void setStatus(IntegerFilter status) {
        this.status = status;
    }

    public StringFilter getChannel() {
        return channel;
    }

    public void setChannel(StringFilter channel) {
        this.channel = channel;
    }

    public BooleanFilter getAllowPickupFlag() {
        return allowPickupFlag;
    }

    public void setAllowPickupFlag(BooleanFilter allowPickupFlag) {
        this.allowPickupFlag = allowPickupFlag;
    }

    public BooleanFilter getAllowUrgentOrderFlag() {
        return allowUrgentOrderFlag;
    }

    public void setAllowUrgentOrderFlag(BooleanFilter allowUrgentOrderFlag) {
        this.allowUrgentOrderFlag = allowUrgentOrderFlag;
    }

    public BooleanFilter getAllowBookStudioFlag() {
        return allowBookStudioFlag;
    }

    public void setAllowBookStudioFlag(BooleanFilter allowBookStudioFlag) {
        this.allowBookStudioFlag = allowBookStudioFlag;
    }

    public ZonedDateTimeFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTimeFilter updateDate) {
        this.updateDate = updateDate;
    }

    public BooleanFilter getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(BooleanFilter deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public StringFilter getLatitude() {
        return latitude;
    }

    public void setLatitude(StringFilter latitude) {
        this.latitude = latitude;
    }

    public StringFilter getLongitude() {
        return longitude;
    }

    public void setLongitude(StringFilter longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "InventoryCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (inventoryCode != null ? "inventoryCode=" + inventoryCode + ", " : "") +
                (inventoryName != null ? "inventoryName=" + inventoryName + ", " : "") +
                (regionCode != null ? "regionCode=" + regionCode + ", " : "") +
                (inventoryType != null ? "inventoryType=" + inventoryType + ", " : "") +
                (brandCode != null ? "brandCode=" + brandCode + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (tel != null ? "tel=" + tel + ", " : "") +
                (fax != null ? "fax=" + fax + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (channel != null ? "channel=" + channel + ", " : "") +
                (allowPickupFlag != null ? "allowPickupFlag=" + allowPickupFlag + ", " : "") +
                (allowUrgentOrderFlag != null ? "allowUrgentOrderFlag=" + allowUrgentOrderFlag + ", " : "") +
                (allowBookStudioFlag != null ? "allowBookStudioFlag=" + allowBookStudioFlag + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (deleteFlag != null ? "deleteFlag=" + deleteFlag + ", " : "") +
                (latitude != null ? "latitude=" + latitude + ", " : "") +
                (longitude != null ? "longitude=" + longitude + ", " : "") +
            "}";
    }

}
