package com.gcs.flexba.rmsweb.service.impl;

import com.gcs.flexba.rmsweb.service.CustomerService;
import com.gcs.flexba.rmsweb.domain.Customer;
import com.gcs.flexba.rmsweb.repository.CRMRepository;
import com.gcs.flexba.rmsweb.repository.CustomerRepository;
import com.gcs.flexba.rmsweb.security.SecurityUtils;
import com.gcs.flexba.rmsweb.service.dto.CustomerDTO;
import com.gcs.flexba.rmsweb.service.dto.MergeCustomerDTO;
import com.gcs.flexba.rmsweb.service.mapper.CustomerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Customer.
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	private final CustomerRepository customerRepository;

	@Autowired
	private CRMRepository cRMRepository;

	private final CustomerMapper customerMapper;

	public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper) {
		this.customerRepository = customerRepository;
		this.customerMapper = customerMapper;
	}

	/**
	 * Save a customer.
	 *
	 * @param customerDTO the entity to save
	 * @return the persisted entity
	 */
	@Override
	public CustomerDTO save(CustomerDTO customerDTO) {
		log.debug("Request to save Customer : {}", customerDTO);
		Customer customer = customerMapper.toEntity(customerDTO);
		customer = customerRepository.save(customer);
		return customerMapper.toDto(customer);
	}

	/**
	 * Get all the customers.
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<CustomerDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Customers");
		return customerRepository.findAll(pageable).map(customerMapper::toDto);
	}

	/**
	 * Get one customer by id.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<CustomerDTO> findOne(Long id) {
		log.debug("Request to get Customer : {}", id);
		return customerRepository.findById(id).map(customerMapper::toDto);
	}

	/**
	 * Delete the customer by id.
	 *
	 * @param id the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Customer : {}", id);
		customerRepository.deleteById(id);
	}

	@Override
	public CustomerDTO mergeCustomer(MergeCustomerDTO mergeCustomerDTO) {
		String username = SecurityUtils.getCurrentUserLogin().get();
		for (CustomerDTO custDTO : mergeCustomerDTO.getRemovedCustomers()) {
			cRMRepository.mergeCustomers(mergeCustomerDTO.getKeepCustomerCode(), custDTO.getCustomerCode(),
					mergeCustomerDTO.getChoosenLevelCode(), username, mergeCustomerDTO.isDeleteOldCustomer());
		}
		return this.customerMapper.toDto(customerRepository.getByCode(mergeCustomerDTO.getKeepCustomerCode()));
	}
}
