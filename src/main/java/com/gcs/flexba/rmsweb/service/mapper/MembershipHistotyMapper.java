package com.gcs.flexba.rmsweb.service.mapper;

import com.gcs.flexba.rmsweb.domain.*;
import com.gcs.flexba.rmsweb.service.dto.MembershipHistotyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MembershipHistoty and its DTO MembershipHistotyDTO.
 */
@Mapper(componentModel = "spring", uses = {CustomerMapper.class})
public interface MembershipHistotyMapper extends EntityMapper<MembershipHistotyDTO, MembershipHistoty> {

    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "customer.customerCode", target = "customerCustomerCode")
    MembershipHistotyDTO toDto(MembershipHistoty membershipHistoty);

    @Mapping(source = "customerId", target = "customer")
    MembershipHistoty toEntity(MembershipHistotyDTO membershipHistotyDTO);

    default MembershipHistoty fromId(Long id) {
        if (id == null) {
            return null;
        }
        MembershipHistoty membershipHistoty = new MembershipHistoty();
        membershipHistoty.setId(id);
        return membershipHistoty;
    }
}
