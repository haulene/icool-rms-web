package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.gcs.flexba.rmsweb.domain.Sale;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.SaleRepository;
import com.gcs.flexba.rmsweb.service.dto.SaleCriteria;

import com.gcs.flexba.rmsweb.service.dto.SaleDTO;
import com.gcs.flexba.rmsweb.service.mapper.SaleMapper;

/**
 * Service for executing complex queries for Sale entities in the database.
 * The main input is a {@link SaleCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SaleDTO} or a {@link Page} of {@link SaleDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SaleQueryService extends QueryService<Sale> {

    private final Logger log = LoggerFactory.getLogger(SaleQueryService.class);

    private final SaleRepository saleRepository;

    private final SaleMapper saleMapper;

    public SaleQueryService(SaleRepository saleRepository, SaleMapper saleMapper) {
        this.saleRepository = saleRepository;
        this.saleMapper = saleMapper;
    }

    /**
     * Return a {@link List} of {@link SaleDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SaleDTO> findByCriteria(SaleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sale> specification = createSpecification(criteria);
        return saleMapper.toDto(saleRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SaleDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SaleDTO> findByCriteria(SaleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sale> specification = createSpecification(criteria);
        return saleRepository.findAll(specification, page)
            .map(saleMapper::toDto);
    }

    /**
     * Function to convert SaleCriteria to a {@link Specification}
     */
    private Specification<Sale> createSpecification(SaleCriteria criteria) {
        Specification<Sale> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getId(), Sale_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Sale_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Sale_.name));
            }
            if (criteria.getIsBegin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIsBegin(), Sale_.isBegin));
            }
            if (criteria.getIsEnd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIsEnd(), Sale_.isEnd));
            }
            if (criteria.getBegin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBegin(), Sale_.begin));
            }
            if (criteria.getEnd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEnd(), Sale_.end));
            }
            if (criteria.getTrangthai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTrangthai(), Sale_.trangthai));
            }
            if (criteria.getSaleType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSaleType(), Sale_.saleType));
            }
        }
        return specification;
    }

}
