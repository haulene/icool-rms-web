package com.gcs.flexba.rmsweb.service.impl;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gcs.flexba.rmsweb.domain.Promotion;
import com.gcs.flexba.rmsweb.domain.Sale;
import com.gcs.flexba.rmsweb.repository.PromotionRepository;
import com.gcs.flexba.rmsweb.repository.SaleRepository;
import com.gcs.flexba.rmsweb.repository.VoucherRepository;
import com.gcs.flexba.rmsweb.service.PromotionService;
import com.gcs.flexba.rmsweb.service.dto.PromotionDTO;
import com.gcs.flexba.rmsweb.service.mapper.PromotionMapper;
import com.gcs.flexba.rmsweb.web.rest.errors.BadRequestAlertException;
import com.gcs.flexba.rmsweb.web.rest.util.PromotionUtil;

/**
 * Service Implementation for managing Promotion.
 */
@Service
@Transactional
public class PromotionServiceImpl implements PromotionService {

	private final Logger log = LoggerFactory.getLogger(PromotionServiceImpl.class);

	private final PromotionRepository promotionRepository;

	private final PromotionMapper promotionMapper;

	@Value(value = "${folders.image.promotion}")
	private String promotionImageFolder;

	@Autowired
	private VoucherRepository voucherRepository;

	@Autowired
	private SaleRepository saleRepository;

	public PromotionServiceImpl(PromotionRepository promotionRepository, PromotionMapper promotionMapper) {
		this.promotionRepository = promotionRepository;
		this.promotionMapper = promotionMapper;
	}

	/**
	 * Save a promotion.
	 *
	 * @param promotionDTO the entity to save
	 * @return the persisted entity
	 * @throws Exception
	 */
	@Override
	public PromotionDTO save(PromotionDTO promotionDTO) throws Exception {
		log.debug("Request to save Promotion : {}", promotionDTO);

		if (promotionDTO.getImageBlob1() != null) {
			promotionDTO.setImagePath(this.saveImageToPath(promotionDTO.getImageBlob1(), promotionDTO.getImagePath(),
					promotionDTO.getCouponCode() + "_1.png"));
		}
		if (promotionDTO.getImageBlob2() != null) {
			promotionDTO.setImagePath2(this.saveImageToPath(promotionDTO.getImageBlob2(), promotionDTO.getImagePath2(),
					promotionDTO.getCouponCode() + "_2.png"));
		}

		// Validate saleId
		String saleId = promotionDTO.getSaleId();
		if (saleId != null) {
			if (saleId.trim().isEmpty()) {
				saleId = null;
				promotionDTO.setSaleId(null);
			}
		}
		if (saleId != null) {
			// Check existed sale Id in Sale Table
			Sale sale = saleRepository.findOnebyId(saleId);
			if (sale == null || sale.getId() == null) {
				throw new BadRequestAlertException("Sale is not exist!", "Promotion", "001");
			} else {
				// If Coupon code not equals with code of sale
				if (!sale.getCode().equals(promotionDTO.getCouponCode())) {
					throw new BadRequestAlertException("Coupon code not correct with Sale: " + saleId, "Promotion", "003");
				}
			}
		}

		// Validate Coupon code

		Promotion promotion = promotionMapper.toEntity(promotionDTO);
		promotion.setAppliedOn(1);
		promotion.setUpdateDate(ZonedDateTime.now());
		Promotion oldPromotion = promotionDTO.getId() != null ? promotionRepository.findOnebyId(promotionDTO.getId())
				: null;
		if (oldPromotion == null || oldPromotion.getId() == null) {
			// New
			// Validate Coupon code when create new
			Promotion couponExistedPromotion = promotionRepository.findOnebyCouponCode(promotionDTO.getCouponCode());
			if (couponExistedPromotion != null) {
				throw new BadRequestAlertException("Coupon code is existed. Please choose other.", "Promotion", "002");
			}
		}
		promotion = promotionRepository.save(promotion);

		if (oldPromotion == null || oldPromotion.getId() == null) {

			if ((PromotionUtil.haveVCGiftInserting(promotionDTO)) && promotionDTO.getSaleId() != null) {
				this.updateVoucher(promotionDTO, oldPromotion);
			}
		} else {
			// Update
//			boolean updateVCHist = promotionDTO.isRedeemFlag() != oldPromotion.isRedeemFlag()
//					&& promotionDTO.getSaleId() != null;
//			if (updateVCHist) {
//				this.updateVoucher(promotionDTO, oldPromotion);
//			}
			if ((PromotionUtil.haveVCGiftInserting(promotionDTO)) && promotionDTO.getSaleId() != null) {
				this.updateVoucher(promotionDTO, oldPromotion);
			}
			// this.updateVoucher(promotionDTO, oldPromotion);
		}
		return promotionMapper.toDto(promotion);
	}

	private void updateVoucher(PromotionDTO promotionDTO, Promotion oldPromotion) {
		if (PromotionUtil.haveVCGiftInserting(promotionDTO)) {
			voucherRepository.deleteVCGiftsByPromotion(promotionDTO.getSaleId(), promotionDTO.getCouponCode());
			voucherRepository.insertVCGiftsByPromotion(promotionDTO.getSaleId(), promotionDTO.getCouponCode());
		} else {
			voucherRepository.deleteVCGiftsByPromotion(promotionDTO.getSaleId(), promotionDTO.getCouponCode());
		}
	}

	private String saveImageToPath(byte[] data, String orgPath, String newFileName) throws Exception {
		String rs = newFileName;
		String filePath = FilenameUtils.concat(this.promotionImageFolder, newFileName);
		if (orgPath == null || orgPath.trim().isEmpty()) {
			// Don't have image path
		} else {
			// Have image path
			rs = orgPath;
			filePath = FilenameUtils.concat(this.promotionImageFolder, orgPath);
		}
		FileUtils.writeByteArrayToFile(new File(filePath), data);
		return rs;
	}

	private void setBlobForPromotion(PromotionDTO dto) throws IOException {
		if (dto.getImageBlob1() == null && dto.getImagePath() != null && !dto.getImagePath().isEmpty()) {
			String filePath = FilenameUtils.concat(this.promotionImageFolder, dto.getImagePath());
			dto.setImageBlob1(FileUtils.readFileToByteArray(new File(filePath)));
			String ex = dto.getImagePath().substring(dto.getImagePath().indexOf(".") + 1);
			dto.setImageBlob1ContentType("image/" + ex);
		}
		if (dto.getImageBlob2() == null && dto.getImagePath2() != null && !dto.getImagePath2().isEmpty()) {
			String filePath = FilenameUtils.concat(this.promotionImageFolder, dto.getImagePath2());
			dto.setImageBlob2(FileUtils.readFileToByteArray(new File(filePath)));
			String ex = dto.getImagePath2().substring(dto.getImagePath2().indexOf(".") + 1);
			dto.setImageBlob2ContentType("image/" + ex);
		}
	}

	/**
	 * Get all the promotions.
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 * @throws Exception
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<PromotionDTO> findAll(Pageable pageable) throws Exception {
		log.debug("Request to get all Promotions");
		Page<PromotionDTO> rs = promotionRepository.findAll(pageable).map(promotionMapper::toDto);
		for (PromotionDTO promotionDTO : rs) {
			this.setBlobForPromotion(promotionDTO);
		}
		return rs;
	}

	/**
	 * Get one promotion by id.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 * @throws Exception
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<PromotionDTO> findOne(Long id) throws Exception {
		log.debug("Request to get Promotion : {}", id);
		Optional<PromotionDTO> rs = promotionRepository.findById(id).map(promotionMapper::toDto);
		this.setBlobForPromotion(rs.get());
		return rs;
	}

	/**
	 * Delete the promotion by id.
	 *
	 * @param id the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Promotion : {}", id);
		Promotion d = promotionRepository.findOnebyId(id);
		promotionRepository.deleteById(id);
		if (!PromotionUtil.haveVCGiftInserting(d) && d.getSaleId() != null) {
			voucherRepository.deleteVCGiftsByPromotion(d.getSaleId(), d.getCouponCode());
		}
	}

	public String getPromotionImageFolder() {
		return promotionImageFolder;
	}

	public void setPromotionImageFolder(String promotionImageFolder) {
		this.promotionImageFolder = promotionImageFolder;
	}
}
