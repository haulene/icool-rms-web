package com.gcs.flexba.rmsweb.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the VoucherGift entity.
 */
public class VoucherGiftDTO implements Serializable {

    private Long id;

    private String voucherGiftCode;

    private String voucherGiftName;

    private Integer voucherCategory;

    private Integer voucherType;

    private String customerCode;

    private String customerName;

    private String customerPhone;

    private Double totalValue;

    private Double availableValue;

    private ZonedDateTime validFrom;

    private ZonedDateTime validTo;

    private Integer status;

    private String refSku;

    private String storeCode;

    private String note;

    private String createdBy;

    private Integer expiredDuration;

    private Integer effectedDay;

    private BigDecimal voucherValue;

    private String usedCustomerCode;

    private String usedCustomerName;

    private String usedCustomerPhone;

    private Boolean updateFlag;

    private BigDecimal maxAppliedAmount;

    private Integer max_appliedTimePerCustomer;

    private Integer voucherAppliedType;

    private ZonedDateTime updateDate;

    private Boolean deleteFlag;

    private String personalId;

    private Integer voucherGiftId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoucherGiftCode() {
        return voucherGiftCode;
    }

    public void setVoucherGiftCode(String voucherGiftCode) {
        this.voucherGiftCode = voucherGiftCode;
    }

    public String getVoucherGiftName() {
        return voucherGiftName;
    }

    public void setVoucherGiftName(String voucherGiftName) {
        this.voucherGiftName = voucherGiftName;
    }

    public Integer getVoucherCategory() {
        return voucherCategory;
    }

    public void setVoucherCategory(Integer voucherCategory) {
        this.voucherCategory = voucherCategory;
    }

    public Integer getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(Integer voucherType) {
        this.voucherType = voucherType;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public Double getAvailableValue() {
        return availableValue;
    }

    public void setAvailableValue(Double availableValue) {
        this.availableValue = availableValue;
    }

    public ZonedDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(ZonedDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public ZonedDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(ZonedDateTime validTo) {
        this.validTo = validTo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRefSku() {
        return refSku;
    }

    public void setRefSku(String refSku) {
        this.refSku = refSku;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getExpiredDuration() {
        return expiredDuration;
    }

    public void setExpiredDuration(Integer expiredDuration) {
        this.expiredDuration = expiredDuration;
    }

    public Integer getEffectedDay() {
        return effectedDay;
    }

    public void setEffectedDay(Integer effectedDay) {
        this.effectedDay = effectedDay;
    }

    public BigDecimal getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(BigDecimal voucherValue) {
        this.voucherValue = voucherValue;
    }

    public String getUsedCustomerCode() {
        return usedCustomerCode;
    }

    public void setUsedCustomerCode(String usedCustomerCode) {
        this.usedCustomerCode = usedCustomerCode;
    }

    public String getUsedCustomerName() {
        return usedCustomerName;
    }

    public void setUsedCustomerName(String usedCustomerName) {
        this.usedCustomerName = usedCustomerName;
    }

    public String getUsedCustomerPhone() {
        return usedCustomerPhone;
    }

    public void setUsedCustomerPhone(String usedCustomerPhone) {
        this.usedCustomerPhone = usedCustomerPhone;
    }

    public Boolean isUpdateFlag() {
        return updateFlag;
    }

    public void setUpdateFlag(Boolean updateFlag) {
        this.updateFlag = updateFlag;
    }

    public BigDecimal getMaxAppliedAmount() {
        return maxAppliedAmount;
    }

    public void setMaxAppliedAmount(BigDecimal maxAppliedAmount) {
        this.maxAppliedAmount = maxAppliedAmount;
    }

    public Integer getMax_appliedTimePerCustomer() {
        return max_appliedTimePerCustomer;
    }

    public void setMax_appliedTimePerCustomer(Integer max_appliedTimePerCustomer) {
        this.max_appliedTimePerCustomer = max_appliedTimePerCustomer;
    }

    public Integer getVoucherAppliedType() {
        return voucherAppliedType;
    }

    public void setVoucherAppliedType(Integer voucherAppliedType) {
        this.voucherAppliedType = voucherAppliedType;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean isDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public Integer getVoucherGiftId() {
        return voucherGiftId;
    }

    public void setVoucherGiftId(Integer voucherGiftId) {
        this.voucherGiftId = voucherGiftId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VoucherGiftDTO voucherGiftDTO = (VoucherGiftDTO) o;
        if (voucherGiftDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), voucherGiftDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VoucherGiftDTO{" +
            "id=" + getId() +
            ", voucherGiftCode='" + getVoucherGiftCode() + "'" +
            ", voucherGiftName='" + getVoucherGiftName() + "'" +
            ", voucherCategory=" + getVoucherCategory() +
            ", voucherType=" + getVoucherType() +
            ", customerCode='" + getCustomerCode() + "'" +
            ", customerName='" + getCustomerName() + "'" +
            ", customerPhone='" + getCustomerPhone() + "'" +
            ", totalValue=" + getTotalValue() +
            ", availableValue=" + getAvailableValue() +
            ", validFrom='" + getValidFrom() + "'" +
            ", validTo='" + getValidTo() + "'" +
            ", status=" + getStatus() +
            ", refSku='" + getRefSku() + "'" +
            ", storeCode='" + getStoreCode() + "'" +
            ", note='" + getNote() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", expiredDuration=" + getExpiredDuration() +
            ", effectedDay=" + getEffectedDay() +
            ", voucherValue=" + getVoucherValue() +
            ", usedCustomerCode='" + getUsedCustomerCode() + "'" +
            ", usedCustomerName='" + getUsedCustomerName() + "'" +
            ", usedCustomerPhone='" + getUsedCustomerPhone() + "'" +
            ", updateFlag='" + isUpdateFlag() + "'" +
            ", maxAppliedAmount=" + getMaxAppliedAmount() +
            ", max_appliedTimePerCustomer=" + getMax_appliedTimePerCustomer() +
            ", voucherAppliedType=" + getVoucherAppliedType() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", deleteFlag='" + isDeleteFlag() + "'" +
            ", personalId='" + getPersonalId() + "'" +
            ", voucherGiftId=" + getVoucherGiftId() +
            "}";
    }
}
