package com.gcs.flexba.rmsweb.service.impl;

import com.gcs.flexba.rmsweb.service.RatingQuestionService;
import com.gcs.flexba.rmsweb.domain.RatingQuestion;
import com.gcs.flexba.rmsweb.repository.RatingQuestionRepository;
import com.gcs.flexba.rmsweb.service.dto.RatingQuestionDTO;
import com.gcs.flexba.rmsweb.service.mapper.RatingQuestionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing RatingQuestion.
 */
@Service
@Transactional
public class RatingQuestionServiceImpl implements RatingQuestionService {

    private final Logger log = LoggerFactory.getLogger(RatingQuestionServiceImpl.class);

    private final RatingQuestionRepository ratingQuestionRepository;

    private final RatingQuestionMapper ratingQuestionMapper;

    public RatingQuestionServiceImpl(RatingQuestionRepository ratingQuestionRepository, RatingQuestionMapper ratingQuestionMapper) {
        this.ratingQuestionRepository = ratingQuestionRepository;
        this.ratingQuestionMapper = ratingQuestionMapper;
    }

    /**
     * Save a ratingQuestion.
     *
     * @param ratingQuestionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RatingQuestionDTO save(RatingQuestionDTO ratingQuestionDTO) {
        log.debug("Request to save RatingQuestion : {}", ratingQuestionDTO);
        RatingQuestion ratingQuestion = ratingQuestionMapper.toEntity(ratingQuestionDTO);
        ratingQuestion = ratingQuestionRepository.save(ratingQuestion);
        return ratingQuestionMapper.toDto(ratingQuestion);
    }

    /**
     * Get all the ratingQuestions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RatingQuestionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RatingQuestions");
        return ratingQuestionRepository.findAll(pageable)
            .map(ratingQuestionMapper::toDto);
    }


    /**
     * Get one ratingQuestion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RatingQuestionDTO> findOne(Long id) {
        log.debug("Request to get RatingQuestion : {}", id);
        return ratingQuestionRepository.findById(id)
            .map(ratingQuestionMapper::toDto);
    }

    /**
     * Delete the ratingQuestion by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RatingQuestion : {}", id);
        ratingQuestionRepository.deleteById(id);
    }
}
