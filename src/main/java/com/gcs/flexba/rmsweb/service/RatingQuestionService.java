package com.gcs.flexba.rmsweb.service;

import com.gcs.flexba.rmsweb.service.dto.RatingQuestionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RatingQuestion.
 */
public interface RatingQuestionService {

    /**
     * Save a ratingQuestion.
     *
     * @param ratingQuestionDTO the entity to save
     * @return the persisted entity
     */
    RatingQuestionDTO save(RatingQuestionDTO ratingQuestionDTO);

    /**
     * Get all the ratingQuestions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RatingQuestionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" ratingQuestion.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RatingQuestionDTO> findOne(Long id);

    /**
     * Delete the "id" ratingQuestion.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
