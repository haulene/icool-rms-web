package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.gcs.flexba.rmsweb.domain.Config;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.ConfigRepository;
import com.gcs.flexba.rmsweb.service.dto.ConfigCriteria;

import com.gcs.flexba.rmsweb.service.dto.ConfigDTO;
import com.gcs.flexba.rmsweb.service.mapper.ConfigMapper;

/**
 * Service for executing complex queries for Config entities in the database.
 * The main input is a {@link ConfigCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ConfigDTO} or a {@link Page} of {@link ConfigDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ConfigQueryService extends QueryService<Config> {

    private final Logger log = LoggerFactory.getLogger(ConfigQueryService.class);

    private final ConfigRepository configRepository;

    private final ConfigMapper configMapper;

    public ConfigQueryService(ConfigRepository configRepository, ConfigMapper configMapper) {
        this.configRepository = configRepository;
        this.configMapper = configMapper;
    }

    /**
     * Return a {@link List} of {@link ConfigDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ConfigDTO> findByCriteria(ConfigCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Config> specification = createSpecification(criteria);
        return configMapper.toDto(configRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ConfigDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ConfigDTO> findByCriteria(ConfigCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Config> specification = createSpecification(criteria);
        return configRepository.findAll(specification, page)
            .map(configMapper::toDto);
    }

    /**
     * Function to convert ConfigCriteria to a {@link Specification}
     */
    private Specification<Config> createSpecification(ConfigCriteria criteria) {
        Specification<Config> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Config_.id));
            }
            if (criteria.getConfigValue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getConfigValue(), Config_.configValue));
            }
            if (criteria.getSyncConfigFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSyncConfigFlag(), Config_.syncConfigFlag));
            }
            if (criteria.getAllowChangeSyncConfigFlag() != null) {
                specification = specification.and(buildSpecification(criteria.getAllowChangeSyncConfigFlag(), Config_.allowChangeSyncConfigFlag));
            }
            if (criteria.getUpdateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateDate(), Config_.updateDate));
            }
            if (criteria.getChannel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChannel(), Config_.channel));
            }
        }
        return specification;
    }

}
