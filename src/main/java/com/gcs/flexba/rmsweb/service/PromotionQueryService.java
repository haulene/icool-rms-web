package com.gcs.flexba.rmsweb.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

// for static metamodels
import com.gcs.flexba.rmsweb.domain.Promotion;
import com.gcs.flexba.rmsweb.domain.Promotion_;
import com.gcs.flexba.rmsweb.repository.PromotionRepository;
import com.gcs.flexba.rmsweb.service.dto.PromotionCriteria;
import com.gcs.flexba.rmsweb.service.dto.PromotionDTO;
import com.gcs.flexba.rmsweb.service.mapper.PromotionMapper;

import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for Promotion entities in the database.
 * The main input is a {@link PromotionCriteria} which gets converted to
 * {@link Specification}, in a way that all the filters must apply. It returns a
 * {@link List} of {@link PromotionDTO} or a {@link Page} of
 * {@link PromotionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PromotionQueryService extends QueryService<Promotion> {

	private final Logger log = LoggerFactory.getLogger(PromotionQueryService.class);

	private final PromotionRepository promotionRepository;

	private final PromotionMapper promotionMapper;

	@Value(value = "${folders.image.promotion}")
	private String promotionImageFolder;

	public PromotionQueryService(PromotionRepository promotionRepository, PromotionMapper promotionMapper) {
		this.promotionRepository = promotionRepository;
		this.promotionMapper = promotionMapper;
	}

	/**
	 * Return a {@link List} of {@link PromotionDTO} which matches the criteria from
	 * the database
	 * 
	 * @param criteria The object which holds all the filters, which the entities
	 *                 should match.
	 * @return the matching entities.
	 * @throws Exception
	 */
	@Transactional(readOnly = true)
	public List<PromotionDTO> findByCriteria(PromotionCriteria criteria) throws Exception {
		log.debug("find by criteria : {}", criteria);
		final Specification<Promotion> specification = createSpecification(criteria);
		List<PromotionDTO> rs = promotionMapper.toDto(promotionRepository.findAll(specification));
		for (PromotionDTO promotionDTO : rs) {
//			this.setBlobForPromotion(promotionDTO);
		}
		return rs;
	}

	/**
	 * Return a {@link Page} of {@link PromotionDTO} which matches the criteria from
	 * the database
	 * 
	 * @param criteria The object which holds all the filters, which the entities
	 *                 should match.
	 * @param page     The page, which should be returned.
	 * @return the matching entities.
	 * @throws Exception 
	 */
	@Transactional(readOnly = true)
	public Page<PromotionDTO> findByCriteria(PromotionCriteria criteria, Pageable page) throws Exception {
		log.debug("find by criteria : {}, page: {}", criteria, page);
		final Specification<Promotion> specification = createSpecification(criteria);
		Page<PromotionDTO> rs = promotionRepository.findAll(specification, page).map(promotionMapper::toDto);
		for (PromotionDTO promotionDTO : rs) {
//			this.setBlobForPromotion(promotionDTO);
		}
		return rs;
	}

	private void setBlobForPromotion(PromotionDTO dto) throws IOException {
		if (dto.getImageBlob1() == null && dto.getImagePath() != null && !dto.getImagePath().isEmpty()) {
			String filePath = FilenameUtils.concat(this.promotionImageFolder, dto.getImagePath());
			dto.setImageBlob1(FileUtils.readFileToByteArray(new File(filePath)));
			String ex = dto.getImagePath().substring(dto.getImagePath().indexOf(".") + 1);
			dto.setImageBlob1ContentType("image/" + ex);
		}
		if (dto.getImageBlob2() == null && dto.getImagePath2() != null && !dto.getImagePath2().isEmpty()) {
			String filePath = FilenameUtils.concat(this.promotionImageFolder, dto.getImagePath2());
			dto.setImageBlob2(FileUtils.readFileToByteArray(new File(filePath)));
			String ex = dto.getImagePath2().substring(dto.getImagePath2().indexOf(".") + 1);
			dto.setImageBlob2ContentType("image/" + ex);
		}
	}

	/**
	 * Function to convert PromotionCriteria to a {@link Specification}
	 */
	private Specification<Promotion> createSpecification(PromotionCriteria criteria) {
		Specification<Promotion> specification = Specification.where(null);
		if (criteria != null) {
			if (criteria.getId() != null) {
				specification = specification.and(buildSpecification(criteria.getId(), Promotion_.id));
			}
			if (criteria.getCouponCode() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getCouponCode(), Promotion_.couponCode));
			}
			if (criteria.getPromotionName() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getPromotionName(), Promotion_.promotionName));
			}
			if (criteria.getDescription() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getDescription(), Promotion_.description));
			}
			if (criteria.getHtmlDescription() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getHtmlDescription(), Promotion_.htmlDescription));
			}
			if (criteria.getStart_date() != null) {
				specification = specification
						.and(buildRangeSpecification(criteria.getStart_date(), Promotion_.startDate));
			}
			if (criteria.getEndDate() != null) {
				specification = specification.and(buildRangeSpecification(criteria.getEndDate(), Promotion_.endDate));
			}
			if (criteria.getStatus() != null) {
				specification = specification.and(buildRangeSpecification(criteria.getStatus(), Promotion_.status));
			}
			if (criteria.getStopNextRule() != null) {
				specification = specification
						.and(buildSpecification(criteria.getStopNextRule(), Promotion_.stopNextRule));
			}
			if (criteria.getSortOrder() != null) {
				specification = specification
						.and(buildRangeSpecification(criteria.getSortOrder(), Promotion_.sortOrder));
			}
			if (criteria.getUseOrgPriceFlag() != null) {
				specification = specification
						.and(buildSpecification(criteria.getUseOrgPriceFlag(), Promotion_.useOrgPriceFlag));
			}
			if (criteria.getAppliedOn() != null) {
				specification = specification
						.and(buildRangeSpecification(criteria.getAppliedOn(), Promotion_.appliedOn));
			}
			if (criteria.getDroolsRule() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getDroolsRule(), Promotion_.droolsRule));
			}
			if (criteria.getVersion() != null) {
				specification = specification.and(buildRangeSpecification(criteria.getVersion(), Promotion_.version));
			}
			if (criteria.getRunMode() != null) {
				specification = specification.and(buildRangeSpecification(criteria.getRunMode(), Promotion_.runMode));
			}
			if (criteria.getOnStore() != null) {
				specification = specification.and(buildStringSpecification(criteria.getOnStore(), Promotion_.onStore));
			}
			if (criteria.getPromotionType() != null) {
				specification = specification
						.and(buildRangeSpecification(criteria.getPromotionType(), Promotion_.promotionType));
			}
			if (criteria.getAutoFlag() != null) {
				specification = specification.and(buildSpecification(criteria.getAutoFlag(), Promotion_.autoFlag));
			}
			if (criteria.getUpdateDate() != null) {
				specification = specification
						.and(buildRangeSpecification(criteria.getUpdateDate(), Promotion_.updateDate));
			}
			if (criteria.getDeleteFlag() != null) {
				specification = specification.and(buildSpecification(criteria.getDeleteFlag(), Promotion_.deleteFlag));
			}
			if (criteria.getChannel() != null) {
				specification = specification.and(buildStringSpecification(criteria.getChannel(), Promotion_.channel));
			}
			if (criteria.getJsCondition() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getJsCondition(), Promotion_.jsCondition));
			}
			if (criteria.getJsBonus() != null) {
				specification = specification.and(buildStringSpecification(criteria.getJsBonus(), Promotion_.jsBonus));
			}
			if (criteria.getRedeemFlag() != null) {
				specification = specification.and(buildSpecification(criteria.getRedeemFlag(), Promotion_.redeemFlag));
			}
			if (criteria.getImagePath() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getImagePath(), Promotion_.imagePath));
			}
			if (criteria.getImagePath2() != null) {
				specification = specification
						.and(buildStringSpecification(criteria.getImagePath2(), Promotion_.imagePath2));
			}
			if (criteria.getRedeemPoints() != null) {
				specification = specification
						.and(buildRangeSpecification(criteria.getRedeemPoints(), Promotion_.redeemPoints));
			}
			if (criteria.getPublishStartDate() != null) {
				specification = specification
						.and(buildRangeSpecification(criteria.getPublishStartDate(), Promotion_.publishStartDate));
			}
			if (criteria.getPublishEndDate() != null) {
				specification = specification
						.and(buildRangeSpecification(criteria.getPublishEndDate(), Promotion_.publishEndDate));
			}
		}
		return specification;
	}

}
