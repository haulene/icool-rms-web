package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Lob;

/**
 * A DTO for the Promotion entity.
 */
public class PromotionDTO implements Serializable {

    private Long id;

    private String couponCode;

    private String promotionName;

    private String description;

    private String htmlDescription;

    private LocalDate startDate;

    private LocalDate endDate;

    private Integer status;

    private Boolean stopNextRule;

    private Integer sortOrder;

    private Boolean useOrgPriceFlag;

    private Integer appliedOn;
    
    private SaleDTO sale;

    @Lob
    private byte[] ruleData;
    private String ruleDataContentType;

    private String droolsRule;

    @Lob
    private byte[] allocationData;
    private String allocationDataContentType;

    private Integer version;

    private Integer runMode;

    private String onStore;

    private Integer promotionType;

    private Boolean autoFlag;

    private ZonedDateTime updateDate;

    private Boolean deleteFlag;

    private String channel;

    private String jsCondition;

    private String jsBonus;

    private Boolean redeemFlag;

    private String imagePath;

    private String imagePath2;

    private Double redeemPoints;

    @Lob
    private byte[] imageBlob1;
    private String imageBlob1ContentType;

    @Lob
    private byte[] imageBlob2;
    private String imageBlob2ContentType;

    private LocalDate publishStartDate;

    private LocalDate publishEndDate;
    
    private String saleId;
    
    private Integer numPresentees;

    public String getSaleId() {
		return saleId;
	}

	public void setSaleId(String saleId) {
		this.saleId = saleId;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHtmlDescription() {
        return htmlDescription;
    }

    public void setHtmlDescription(String htmlDescription) {
        this.htmlDescription = htmlDescription;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean isStopNextRule() {
        return stopNextRule;
    }

    public void setStopNextRule(Boolean stopNextRule) {
        this.stopNextRule = stopNextRule;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean isUseOrgPriceFlag() {
        return useOrgPriceFlag;
    }

    public void setUseOrgPriceFlag(Boolean useOrgPriceFlag) {
        this.useOrgPriceFlag = useOrgPriceFlag;
    }

    public Integer getAppliedOn() {
        return appliedOn;
    }

    public void setAppliedOn(Integer appliedOn) {
        this.appliedOn = appliedOn;
    }

    public byte[] getRuleData() {
        return ruleData;
    }

    public void setRuleData(byte[] ruleData) {
        this.ruleData = ruleData;
    }

    public String getRuleDataContentType() {
        return ruleDataContentType;
    }

    public void setRuleDataContentType(String ruleDataContentType) {
        this.ruleDataContentType = ruleDataContentType;
    }

    public String getDroolsRule() {
        return droolsRule;
    }

    public void setDroolsRule(String droolsRule) {
        this.droolsRule = droolsRule;
    }

    public byte[] getAllocationData() {
        return allocationData;
    }

    public void setAllocationData(byte[] allocationData) {
        this.allocationData = allocationData;
    }

    public String getAllocationDataContentType() {
        return allocationDataContentType;
    }

    public void setAllocationDataContentType(String allocationDataContentType) {
        this.allocationDataContentType = allocationDataContentType;
    }

    public SaleDTO getSale() {
		return sale;
	}

	public void setSale(SaleDTO sale) {
		this.sale = sale;
	}

	public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getRunMode() {
        return runMode;
    }

    public void setRunMode(Integer runMode) {
        this.runMode = runMode;
    }

    public String getOnStore() {
        return onStore;
    }

    public void setOnStore(String onStore) {
        this.onStore = onStore;
    }

    public Integer getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Integer promotionType) {
        this.promotionType = promotionType;
    }

    public Boolean isAutoFlag() {
        return autoFlag;
    }

    public void setAutoFlag(Boolean autoFlag) {
        this.autoFlag = autoFlag;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean isDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getJsCondition() {
        return jsCondition;
    }

    public void setJsCondition(String jsCondition) {
        this.jsCondition = jsCondition;
    }

    public String getJsBonus() {
        return jsBonus;
    }

    public void setJsBonus(String jsBonus) {
        this.jsBonus = jsBonus;
    }

    public Boolean isRedeemFlag() {
        return redeemFlag;
    }

    public void setRedeemFlag(Boolean redeemFlag) {
        this.redeemFlag = redeemFlag;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath2() {
        return imagePath2;
    }

    public void setImagePath2(String imagePath2) {
        this.imagePath2 = imagePath2;
    }

    public Double getRedeemPoints() {
        return redeemPoints;
    }

    public void setRedeemPoints(Double redeemPoints) {
        this.redeemPoints = redeemPoints;
    }

    public byte[] getImageBlob1() {
        return imageBlob1;
    }

    public void setImageBlob1(byte[] imageBlob1) {
        this.imageBlob1 = imageBlob1;
    }

    public String getImageBlob1ContentType() {
        return imageBlob1ContentType;
    }

    public void setImageBlob1ContentType(String imageBlob1ContentType) {
        this.imageBlob1ContentType = imageBlob1ContentType;
    }

    public byte[] getImageBlob2() {
        return imageBlob2;
    }

    public void setImageBlob2(byte[] imageBlob2) {
        this.imageBlob2 = imageBlob2;
    }

    public String getImageBlob2ContentType() {
        return imageBlob2ContentType;
    }

    public void setImageBlob2ContentType(String imageBlob2ContentType) {
        this.imageBlob2ContentType = imageBlob2ContentType;
    }

    public LocalDate getPublishStartDate() {
        return publishStartDate;
    }

    public void setPublishStartDate(LocalDate publishStartDate) {
        this.publishStartDate = publishStartDate;
    }

    public LocalDate getPublishEndDate() {
        return publishEndDate;
    }

    public void setPublishEndDate(LocalDate publishEndDate) {
        this.publishEndDate = publishEndDate;
    }

    public Integer getNumPresentees() {
		return numPresentees;
	}

	public void setNumPresentees(Integer numPresentees) {
		this.numPresentees = numPresentees;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PromotionDTO promotionDTO = (PromotionDTO) o;
        if (promotionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), promotionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PromotionDTO{" +
            "id=" + getId() +
            ", couponCode='" + getCouponCode() + "'" +
            ", promotionName='" + getPromotionName() + "'" +
            ", description='" + getDescription() + "'" +
            ", htmlDescription='" + getHtmlDescription() + "'" +
            ", start_date='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", status=" + getStatus() +
            ", stopNextRule='" + isStopNextRule() + "'" +
            ", sortOrder=" + getSortOrder() +
            ", useOrgPriceFlag='" + isUseOrgPriceFlag() + "'" +
            ", appliedOn=" + getAppliedOn() +
            ", ruleData='" + getRuleData() + "'" +
            ", droolsRule='" + getDroolsRule() + "'" +
            ", allocationData='" + getAllocationData() + "'" +
            ", version=" + getVersion() +
            ", runMode=" + getRunMode() +
            ", onStore='" + getOnStore() + "'" +
            ", promotionType=" + getPromotionType() +
            ", autoFlag='" + isAutoFlag() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", deleteFlag='" + isDeleteFlag() + "'" +
            ", channel='" + getChannel() + "'" +
            ", jsCondition='" + getJsCondition() + "'" +
            ", jsBonus='" + getJsBonus() + "'" +
            ", redeemFlag='" + isRedeemFlag() + "'" +
            ", imagePath='" + getImagePath() + "'" +
            ", imagePath2='" + getImagePath2() + "'" +
            ", redeemPoints=" + getRedeemPoints() +
            ", imageBlob1='" + getImageBlob1() + "'" +
            ", imageBlob2='" + getImageBlob2() + "'" +
            ", publishStartDate='" + getPublishStartDate() + "'" +
            ", publishEndDate='" + getPublishEndDate() + "'" +
            "}";
    }
}
