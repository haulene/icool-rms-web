package com.gcs.flexba.rmsweb.service.mapper;

import com.gcs.flexba.rmsweb.domain.*;
import com.gcs.flexba.rmsweb.service.dto.CustomerRatingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CustomerRating and its DTO CustomerRatingDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CustomerRatingMapper extends EntityMapper<CustomerRatingDTO, CustomerRating> {



    default CustomerRating fromId(Long id) {
        if (id == null) {
            return null;
        }
        CustomerRating customerRating = new CustomerRating();
        customerRating.setId(id);
        return customerRating;
    }
}
