package com.gcs.flexba.rmsweb.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Inventory entity.
 */
public class InventoryDTO implements Serializable {

    private Long id;

    private String inventoryCode;

    private String inventoryName;

    private String regionCode;

    private Integer inventoryType;

    private String brandCode;

    private String address;

    private String tel;

    private String fax;

    private String email;

    private Integer status;

    private String channel;

    private Boolean allowPickupFlag;

    private Boolean allowUrgentOrderFlag;

    private Boolean allowBookStudioFlag;

    private ZonedDateTime updateDate;

    private Boolean deleteFlag;

    private String latitude;

    private String longitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInventoryCode() {
        return inventoryCode;
    }

    public void setInventoryCode(String inventoryCode) {
        this.inventoryCode = inventoryCode;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public Integer getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(Integer inventoryType) {
        this.inventoryType = inventoryType;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Boolean isAllowPickupFlag() {
        return allowPickupFlag;
    }

    public void setAllowPickupFlag(Boolean allowPickupFlag) {
        this.allowPickupFlag = allowPickupFlag;
    }

    public Boolean isAllowUrgentOrderFlag() {
        return allowUrgentOrderFlag;
    }

    public void setAllowUrgentOrderFlag(Boolean allowUrgentOrderFlag) {
        this.allowUrgentOrderFlag = allowUrgentOrderFlag;
    }

    public Boolean isAllowBookStudioFlag() {
        return allowBookStudioFlag;
    }

    public void setAllowBookStudioFlag(Boolean allowBookStudioFlag) {
        this.allowBookStudioFlag = allowBookStudioFlag;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean isDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InventoryDTO inventoryDTO = (InventoryDTO) o;
        if (inventoryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), inventoryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InventoryDTO{" +
            "id=" + getId() +
            ", inventoryCode='" + getInventoryCode() + "'" +
            ", inventoryName='" + getInventoryName() + "'" +
            ", regionCode='" + getRegionCode() + "'" +
            ", inventoryType=" + getInventoryType() +
            ", brandCode='" + getBrandCode() + "'" +
            ", address='" + getAddress() + "'" +
            ", tel='" + getTel() + "'" +
            ", fax='" + getFax() + "'" +
            ", email='" + getEmail() + "'" +
            ", status=" + getStatus() +
            ", channel='" + getChannel() + "'" +
            ", allowPickupFlag='" + isAllowPickupFlag() + "'" +
            ", allowUrgentOrderFlag='" + isAllowUrgentOrderFlag() + "'" +
            ", allowBookStudioFlag='" + isAllowBookStudioFlag() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", deleteFlag='" + isDeleteFlag() + "'" +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            "}";
    }
}
