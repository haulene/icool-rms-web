package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Sale entity. This class is used in SaleResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /sales?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SaleCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private StringFilter id;

    private StringFilter code;

    private StringFilter name;

    private IntegerFilter isBegin;

    private IntegerFilter isEnd;

    private ZonedDateTimeFilter begin;

    private ZonedDateTimeFilter end;

    private IntegerFilter trangthai;

    private IntegerFilter saleType;

    public SaleCriteria() {
    }

    public StringFilter getId() {
        return id;
    }

    public void setId(StringFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getIsBegin() {
        return isBegin;
    }

    public void setIsBegin(IntegerFilter isBegin) {
        this.isBegin = isBegin;
    }

    public IntegerFilter getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(IntegerFilter isEnd) {
        this.isEnd = isEnd;
    }

    public ZonedDateTimeFilter getBegin() {
        return begin;
    }

    public void setBegin(ZonedDateTimeFilter begin) {
        this.begin = begin;
    }

    public ZonedDateTimeFilter getEnd() {
        return end;
    }

    public void setEnd(ZonedDateTimeFilter end) {
        this.end = end;
    }

    public IntegerFilter getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(IntegerFilter trangthai) {
        this.trangthai = trangthai;
    }

    public IntegerFilter getSaleType() {
        return saleType;
    }

    public void setSaleType(IntegerFilter saleType) {
        this.saleType = saleType;
    }

    @Override
    public String toString() {
        return "SaleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (isBegin != null ? "isBegin=" + isBegin + ", " : "") +
                (isEnd != null ? "isEnd=" + isEnd + ", " : "") +
                (begin != null ? "begin=" + begin + ", " : "") +
                (end != null ? "end=" + end + ", " : "") +
                (trangthai != null ? "trangthai=" + trangthai + ", " : "") +
                (saleType != null ? "saleType=" + saleType + ", " : "") +
            "}";
    }

}
