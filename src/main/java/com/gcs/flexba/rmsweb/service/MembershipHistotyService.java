package com.gcs.flexba.rmsweb.service;

import com.gcs.flexba.rmsweb.service.dto.MembershipHistotyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing MembershipHistoty.
 */
public interface MembershipHistotyService {

    /**
     * Save a membershipHistoty.
     *
     * @param membershipHistotyDTO the entity to save
     * @return the persisted entity
     */
    MembershipHistotyDTO save(MembershipHistotyDTO membershipHistotyDTO);

    /**
     * Get all the membershipHistoties.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<MembershipHistotyDTO> findAll(Pageable pageable);


    /**
     * Get the "id" membershipHistoty.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<MembershipHistotyDTO> findOne(Long id);

    /**
     * Delete the "id" membershipHistoty.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
