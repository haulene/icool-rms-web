package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;


import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the VoucherGift entity. This class is used in VoucherGiftResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /voucher-gifts?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class VoucherGiftCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter voucherGiftCode;

    private StringFilter voucherGiftName;

    private IntegerFilter voucherCategory;

    private IntegerFilter voucherType;

    private StringFilter customerCode;

    private StringFilter customerName;

    private StringFilter customerPhone;

    private DoubleFilter totalValue;

    private DoubleFilter availableValue;

    private ZonedDateTimeFilter validFrom;

    private ZonedDateTimeFilter validTo;

    private IntegerFilter status;

    private StringFilter refSku;

    private StringFilter storeCode;

    private StringFilter note;

    private StringFilter createdBy;

    private IntegerFilter expiredDuration;

    private IntegerFilter effectedDay;

    private BigDecimalFilter voucherValue;

    private StringFilter usedCustomerCode;

    private StringFilter usedCustomerName;

    private StringFilter usedCustomerPhone;

    private BooleanFilter updateFlag;

    private BigDecimalFilter maxAppliedAmount;

    private IntegerFilter max_appliedTimePerCustomer;

    private IntegerFilter voucherAppliedType;

    private ZonedDateTimeFilter updateDate;

    private BooleanFilter deleteFlag;

    private StringFilter personalId;

    private IntegerFilter voucherGiftId;

    public VoucherGiftCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getVoucherGiftCode() {
        return voucherGiftCode;
    }

    public void setVoucherGiftCode(StringFilter voucherGiftCode) {
        this.voucherGiftCode = voucherGiftCode;
    }

    public StringFilter getVoucherGiftName() {
        return voucherGiftName;
    }

    public void setVoucherGiftName(StringFilter voucherGiftName) {
        this.voucherGiftName = voucherGiftName;
    }

    public IntegerFilter getVoucherCategory() {
        return voucherCategory;
    }

    public void setVoucherCategory(IntegerFilter voucherCategory) {
        this.voucherCategory = voucherCategory;
    }

    public IntegerFilter getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(IntegerFilter voucherType) {
        this.voucherType = voucherType;
    }

    public StringFilter getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(StringFilter customerCode) {
        this.customerCode = customerCode;
    }

    public StringFilter getCustomerName() {
        return customerName;
    }

    public void setCustomerName(StringFilter customerName) {
        this.customerName = customerName;
    }

    public StringFilter getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(StringFilter customerPhone) {
        this.customerPhone = customerPhone;
    }

    public DoubleFilter getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(DoubleFilter totalValue) {
        this.totalValue = totalValue;
    }

    public DoubleFilter getAvailableValue() {
        return availableValue;
    }

    public void setAvailableValue(DoubleFilter availableValue) {
        this.availableValue = availableValue;
    }

    public ZonedDateTimeFilter getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(ZonedDateTimeFilter validFrom) {
        this.validFrom = validFrom;
    }

    public ZonedDateTimeFilter getValidTo() {
        return validTo;
    }

    public void setValidTo(ZonedDateTimeFilter validTo) {
        this.validTo = validTo;
    }

    public IntegerFilter getStatus() {
        return status;
    }

    public void setStatus(IntegerFilter status) {
        this.status = status;
    }

    public StringFilter getRefSku() {
        return refSku;
    }

    public void setRefSku(StringFilter refSku) {
        this.refSku = refSku;
    }

    public StringFilter getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(StringFilter storeCode) {
        this.storeCode = storeCode;
    }

    public StringFilter getNote() {
        return note;
    }

    public void setNote(StringFilter note) {
        this.note = note;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public IntegerFilter getExpiredDuration() {
        return expiredDuration;
    }

    public void setExpiredDuration(IntegerFilter expiredDuration) {
        this.expiredDuration = expiredDuration;
    }

    public IntegerFilter getEffectedDay() {
        return effectedDay;
    }

    public void setEffectedDay(IntegerFilter effectedDay) {
        this.effectedDay = effectedDay;
    }

    public BigDecimalFilter getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(BigDecimalFilter voucherValue) {
        this.voucherValue = voucherValue;
    }

    public StringFilter getUsedCustomerCode() {
        return usedCustomerCode;
    }

    public void setUsedCustomerCode(StringFilter usedCustomerCode) {
        this.usedCustomerCode = usedCustomerCode;
    }

    public StringFilter getUsedCustomerName() {
        return usedCustomerName;
    }

    public void setUsedCustomerName(StringFilter usedCustomerName) {
        this.usedCustomerName = usedCustomerName;
    }

    public StringFilter getUsedCustomerPhone() {
        return usedCustomerPhone;
    }

    public void setUsedCustomerPhone(StringFilter usedCustomerPhone) {
        this.usedCustomerPhone = usedCustomerPhone;
    }

    public BooleanFilter getUpdateFlag() {
        return updateFlag;
    }

    public void setUpdateFlag(BooleanFilter updateFlag) {
        this.updateFlag = updateFlag;
    }

    public BigDecimalFilter getMaxAppliedAmount() {
        return maxAppliedAmount;
    }

    public void setMaxAppliedAmount(BigDecimalFilter maxAppliedAmount) {
        this.maxAppliedAmount = maxAppliedAmount;
    }

    public IntegerFilter getMax_appliedTimePerCustomer() {
        return max_appliedTimePerCustomer;
    }

    public void setMax_appliedTimePerCustomer(IntegerFilter max_appliedTimePerCustomer) {
        this.max_appliedTimePerCustomer = max_appliedTimePerCustomer;
    }

    public IntegerFilter getVoucherAppliedType() {
        return voucherAppliedType;
    }

    public void setVoucherAppliedType(IntegerFilter voucherAppliedType) {
        this.voucherAppliedType = voucherAppliedType;
    }

    public ZonedDateTimeFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTimeFilter updateDate) {
        this.updateDate = updateDate;
    }

    public BooleanFilter getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(BooleanFilter deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public StringFilter getPersonalId() {
        return personalId;
    }

    public void setPersonalId(StringFilter personalId) {
        this.personalId = personalId;
    }

    public IntegerFilter getVoucherGiftId() {
        return voucherGiftId;
    }

    public void setVoucherGiftId(IntegerFilter voucherGiftId) {
        this.voucherGiftId = voucherGiftId;
    }

    @Override
    public String toString() {
        return "VoucherGiftCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (voucherGiftCode != null ? "voucherGiftCode=" + voucherGiftCode + ", " : "") +
                (voucherGiftName != null ? "voucherGiftName=" + voucherGiftName + ", " : "") +
                (voucherCategory != null ? "voucherCategory=" + voucherCategory + ", " : "") +
                (voucherType != null ? "voucherType=" + voucherType + ", " : "") +
                (customerCode != null ? "customerCode=" + customerCode + ", " : "") +
                (customerName != null ? "customerName=" + customerName + ", " : "") +
                (customerPhone != null ? "customerPhone=" + customerPhone + ", " : "") +
                (totalValue != null ? "totalValue=" + totalValue + ", " : "") +
                (availableValue != null ? "availableValue=" + availableValue + ", " : "") +
                (validFrom != null ? "validFrom=" + validFrom + ", " : "") +
                (validTo != null ? "validTo=" + validTo + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (refSku != null ? "refSku=" + refSku + ", " : "") +
                (storeCode != null ? "storeCode=" + storeCode + ", " : "") +
                (note != null ? "note=" + note + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (expiredDuration != null ? "expiredDuration=" + expiredDuration + ", " : "") +
                (effectedDay != null ? "effectedDay=" + effectedDay + ", " : "") +
                (voucherValue != null ? "voucherValue=" + voucherValue + ", " : "") +
                (usedCustomerCode != null ? "usedCustomerCode=" + usedCustomerCode + ", " : "") +
                (usedCustomerName != null ? "usedCustomerName=" + usedCustomerName + ", " : "") +
                (usedCustomerPhone != null ? "usedCustomerPhone=" + usedCustomerPhone + ", " : "") +
                (updateFlag != null ? "updateFlag=" + updateFlag + ", " : "") +
                (maxAppliedAmount != null ? "maxAppliedAmount=" + maxAppliedAmount + ", " : "") +
                (max_appliedTimePerCustomer != null ? "max_appliedTimePerCustomer=" + max_appliedTimePerCustomer + ", " : "") +
                (voucherAppliedType != null ? "voucherAppliedType=" + voucherAppliedType + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (deleteFlag != null ? "deleteFlag=" + deleteFlag + ", " : "") +
                (personalId != null ? "personalId=" + personalId + ", " : "") +
                (voucherGiftId != null ? "voucherGiftId=" + voucherGiftId + ", " : "") +
            "}";
    }

}
