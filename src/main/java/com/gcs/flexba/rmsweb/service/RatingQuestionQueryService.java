package com.gcs.flexba.rmsweb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.gcs.flexba.rmsweb.domain.RatingQuestion;
import com.gcs.flexba.rmsweb.domain.*; // for static metamodels
import com.gcs.flexba.rmsweb.repository.RatingQuestionRepository;
import com.gcs.flexba.rmsweb.service.dto.RatingQuestionCriteria;

import com.gcs.flexba.rmsweb.service.dto.RatingQuestionDTO;
import com.gcs.flexba.rmsweb.service.mapper.RatingQuestionMapper;

/**
 * Service for executing complex queries for RatingQuestion entities in the database.
 * The main input is a {@link RatingQuestionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RatingQuestionDTO} or a {@link Page} of {@link RatingQuestionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RatingQuestionQueryService extends QueryService<RatingQuestion> {

    private final Logger log = LoggerFactory.getLogger(RatingQuestionQueryService.class);

    private final RatingQuestionRepository ratingQuestionRepository;

    private final RatingQuestionMapper ratingQuestionMapper;

    public RatingQuestionQueryService(RatingQuestionRepository ratingQuestionRepository, RatingQuestionMapper ratingQuestionMapper) {
        this.ratingQuestionRepository = ratingQuestionRepository;
        this.ratingQuestionMapper = ratingQuestionMapper;
    }

    /**
     * Return a {@link List} of {@link RatingQuestionDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RatingQuestionDTO> findByCriteria(RatingQuestionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<RatingQuestion> specification = createSpecification(criteria);
        return ratingQuestionMapper.toDto(ratingQuestionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RatingQuestionDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RatingQuestionDTO> findByCriteria(RatingQuestionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<RatingQuestion> specification = createSpecification(criteria);
        return ratingQuestionRepository.findAll(specification, page)
            .map(ratingQuestionMapper::toDto);
    }

    /**
     * Function to convert RatingQuestionCriteria to a {@link Specification}
     */
    private Specification<RatingQuestion> createSpecification(RatingQuestionCriteria criteria) {
        Specification<RatingQuestion> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RatingQuestion_.id));
            }
            if (criteria.getContent() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContent(), RatingQuestion_.content));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getType(), RatingQuestion_.type));
            }
            if (criteria.getStartNumStar() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartNumStar(), RatingQuestion_.startNumStar));
            }
            if (criteria.getEndNumStar() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndNumStar(), RatingQuestion_.endNumStar));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), RatingQuestion_.createdDate));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), RatingQuestion_.updatedDate));
            }
            if (criteria.getAnswerType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAnswerType(), RatingQuestion_.answerType));
            }
            if (criteria.getAnswerCategory() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAnswerCategory(), RatingQuestion_.answerCategory));
            }
        }
        return specification;
    }

}
