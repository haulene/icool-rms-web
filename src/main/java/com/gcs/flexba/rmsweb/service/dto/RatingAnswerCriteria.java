package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the RatingAnswer entity. This class is used in RatingAnswerResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /rating-answers?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RatingAnswerCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private IntegerFilter ratingId;

    private IntegerFilter ratingQuestionId;

    private IntegerFilter type;

    private StringFilter value;

    private StringFilter answerCategory;

    public RatingAnswerCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getRatingId() {
        return ratingId;
    }

    public void setRatingId(IntegerFilter ratingId) {
        this.ratingId = ratingId;
    }

    public IntegerFilter getRatingQuestionId() {
        return ratingQuestionId;
    }

    public void setRatingQuestionId(IntegerFilter ratingQuestionId) {
        this.ratingQuestionId = ratingQuestionId;
    }

    public IntegerFilter getType() {
        return type;
    }

    public void setType(IntegerFilter type) {
        this.type = type;
    }

    public StringFilter getValue() {
        return value;
    }

    public void setValue(StringFilter value) {
        this.value = value;
    }

    public StringFilter getAnswerCategory() {
        return answerCategory;
    }

    public void setAnswerCategory(StringFilter answerCategory) {
        this.answerCategory = answerCategory;
    }

    @Override
    public String toString() {
        return "RatingAnswerCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ratingId != null ? "ratingId=" + ratingId + ", " : "") +
                (ratingQuestionId != null ? "ratingQuestionId=" + ratingQuestionId + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (value != null ? "value=" + value + ", " : "") +
                (answerCategory != null ? "answerCategory=" + answerCategory + ", " : "") +
            "}";
    }

}
