package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the CustomerRating entity. This class is used in CustomerRatingResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /customer-ratings?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CustomerRatingCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private IntegerFilter numStar;

    private StringFilter customerCode;

    private StringFilter source;

    private StringFilter inventoryCode;

    private IntegerFilter type;

    private StringFilter comment;

    private ZonedDateTimeFilter updatedDate;

    private ZonedDateTimeFilter createdDate;

    private IntegerFilter deleteFlag;

    public CustomerRatingCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getNumStar() {
        return numStar;
    }

    public void setNumStar(IntegerFilter numStar) {
        this.numStar = numStar;
    }

    public StringFilter getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(StringFilter customerCode) {
        this.customerCode = customerCode;
    }

    public StringFilter getSource() {
        return source;
    }

    public void setSource(StringFilter source) {
        this.source = source;
    }

    public StringFilter getInventoryCode() {
        return inventoryCode;
    }

    public void setInventoryCode(StringFilter inventoryCode) {
        this.inventoryCode = inventoryCode;
    }

    public IntegerFilter getType() {
        return type;
    }

    public void setType(IntegerFilter type) {
        this.type = type;
    }

    public StringFilter getComment() {
        return comment;
    }

    public void setComment(StringFilter comment) {
        this.comment = comment;
    }

    public ZonedDateTimeFilter getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(ZonedDateTimeFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public ZonedDateTimeFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTimeFilter createdDate) {
        this.createdDate = createdDate;
    }

    public IntegerFilter getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(IntegerFilter deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @Override
    public String toString() {
        return "CustomerRatingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (numStar != null ? "numStar=" + numStar + ", " : "") +
                (customerCode != null ? "customerCode=" + customerCode + ", " : "") +
                (source != null ? "source=" + source + ", " : "") +
                (inventoryCode != null ? "inventoryCode=" + inventoryCode + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (comment != null ? "comment=" + comment + ", " : "") +
                (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (deleteFlag != null ? "deleteFlag=" + deleteFlag + ", " : "") +
            "}";
    }

}
