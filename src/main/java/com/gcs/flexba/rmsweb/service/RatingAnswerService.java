package com.gcs.flexba.rmsweb.service;

import com.gcs.flexba.rmsweb.service.dto.RatingAnswerDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RatingAnswer.
 */
public interface RatingAnswerService {

    /**
     * Save a ratingAnswer.
     *
     * @param ratingAnswerDTO the entity to save
     * @return the persisted entity
     */
    RatingAnswerDTO save(RatingAnswerDTO ratingAnswerDTO);

    /**
     * Get all the ratingAnswers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RatingAnswerDTO> findAll(Pageable pageable);


    /**
     * Get the "id" ratingAnswer.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RatingAnswerDTO> findOne(Long id);

    /**
     * Delete the "id" ratingAnswer.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
