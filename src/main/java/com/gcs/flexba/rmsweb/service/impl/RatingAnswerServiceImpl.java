package com.gcs.flexba.rmsweb.service.impl;

import com.gcs.flexba.rmsweb.service.RatingAnswerService;
import com.gcs.flexba.rmsweb.domain.RatingAnswer;
import com.gcs.flexba.rmsweb.repository.RatingAnswerRepository;
import com.gcs.flexba.rmsweb.service.dto.RatingAnswerDTO;
import com.gcs.flexba.rmsweb.service.mapper.RatingAnswerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing RatingAnswer.
 */
@Service
@Transactional
public class RatingAnswerServiceImpl implements RatingAnswerService {

    private final Logger log = LoggerFactory.getLogger(RatingAnswerServiceImpl.class);

    private final RatingAnswerRepository ratingAnswerRepository;

    private final RatingAnswerMapper ratingAnswerMapper;

    public RatingAnswerServiceImpl(RatingAnswerRepository ratingAnswerRepository, RatingAnswerMapper ratingAnswerMapper) {
        this.ratingAnswerRepository = ratingAnswerRepository;
        this.ratingAnswerMapper = ratingAnswerMapper;
    }

    /**
     * Save a ratingAnswer.
     *
     * @param ratingAnswerDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RatingAnswerDTO save(RatingAnswerDTO ratingAnswerDTO) {
        log.debug("Request to save RatingAnswer : {}", ratingAnswerDTO);
        RatingAnswer ratingAnswer = ratingAnswerMapper.toEntity(ratingAnswerDTO);
        ratingAnswer = ratingAnswerRepository.save(ratingAnswer);
        return ratingAnswerMapper.toDto(ratingAnswer);
    }

    /**
     * Get all the ratingAnswers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RatingAnswerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RatingAnswers");
        return ratingAnswerRepository.findAll(pageable)
            .map(ratingAnswerMapper::toDto);
    }


    /**
     * Get one ratingAnswer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RatingAnswerDTO> findOne(Long id) {
        log.debug("Request to get RatingAnswer : {}", id);
        return ratingAnswerRepository.findById(id)
            .map(ratingAnswerMapper::toDto);
    }

    /**
     * Delete the ratingAnswer by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RatingAnswer : {}", id);
        ratingAnswerRepository.deleteById(id);
    }
}
