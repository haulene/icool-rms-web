package com.gcs.flexba.rmsweb.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the RatingQuestion entity. This class is used in RatingQuestionResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /rating-questions?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RatingQuestionCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter content;

    private IntegerFilter type;

    private IntegerFilter startNumStar;

    private IntegerFilter endNumStar;

    private ZonedDateTimeFilter createdDate;

    private ZonedDateTimeFilter updatedDate;

    private StringFilter answerType;

    private StringFilter answerCategory;

    public RatingQuestionCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getContent() {
        return content;
    }

    public void setContent(StringFilter content) {
        this.content = content;
    }

    public IntegerFilter getType() {
        return type;
    }

    public void setType(IntegerFilter type) {
        this.type = type;
    }

    public IntegerFilter getStartNumStar() {
        return startNumStar;
    }

    public void setStartNumStar(IntegerFilter startNumStar) {
        this.startNumStar = startNumStar;
    }

    public IntegerFilter getEndNumStar() {
        return endNumStar;
    }

    public void setEndNumStar(IntegerFilter endNumStar) {
        this.endNumStar = endNumStar;
    }

    public ZonedDateTimeFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTimeFilter createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTimeFilter getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(ZonedDateTimeFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public StringFilter getAnswerType() {
        return answerType;
    }

    public void setAnswerType(StringFilter answerType) {
        this.answerType = answerType;
    }

    public StringFilter getAnswerCategory() {
        return answerCategory;
    }

    public void setAnswerCategory(StringFilter answerCategory) {
        this.answerCategory = answerCategory;
    }

    @Override
    public String toString() {
        return "RatingQuestionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (content != null ? "content=" + content + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (startNumStar != null ? "startNumStar=" + startNumStar + ", " : "") +
                (endNumStar != null ? "endNumStar=" + endNumStar + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
                (answerType != null ? "answerType=" + answerType + ", " : "") +
                (answerCategory != null ? "answerCategory=" + answerCategory + ", " : "") +
            "}";
    }

}
