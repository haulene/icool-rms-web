package com.gcs.flexba.rmsweb.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RatingQuestion entity.
 */
public class RatingQuestionDTO implements Serializable {

    private Long id;

    private String content;

    private Integer type;

    private Integer startNumStar;

    private Integer endNumStar;

    private ZonedDateTime createdDate;

    private ZonedDateTime updatedDate;

    private String answerType;

    private String answerCategory;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStartNumStar() {
        return startNumStar;
    }

    public void setStartNumStar(Integer startNumStar) {
        this.startNumStar = startNumStar;
    }

    public Integer getEndNumStar() {
        return endNumStar;
    }

    public void setEndNumStar(Integer endNumStar) {
        this.endNumStar = endNumStar;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getAnswerCategory() {
        return answerCategory;
    }

    public void setAnswerCategory(String answerCategory) {
        this.answerCategory = answerCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RatingQuestionDTO ratingQuestionDTO = (RatingQuestionDTO) o;
        if (ratingQuestionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ratingQuestionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RatingQuestionDTO{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", type=" + getType() +
            ", startNumStar=" + getStartNumStar() +
            ", endNumStar=" + getEndNumStar() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", answerType='" + getAnswerType() + "'" +
            ", answerCategory='" + getAnswerCategory() + "'" +
            "}";
    }
}
