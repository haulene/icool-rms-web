package com.gcs.flexba.rmsweb.service.impl;

import com.gcs.flexba.rmsweb.service.VoucherGiftService;
import com.gcs.flexba.rmsweb.domain.VoucherGift;
import com.gcs.flexba.rmsweb.repository.VoucherGiftRepository;
import com.gcs.flexba.rmsweb.service.dto.VoucherGiftDTO;
import com.gcs.flexba.rmsweb.service.mapper.VoucherGiftMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing VoucherGift.
 */
@Service
@Transactional
public class VoucherGiftServiceImpl implements VoucherGiftService {

    private final Logger log = LoggerFactory.getLogger(VoucherGiftServiceImpl.class);

    private final VoucherGiftRepository voucherGiftRepository;

    private final VoucherGiftMapper voucherGiftMapper;

    public VoucherGiftServiceImpl(VoucherGiftRepository voucherGiftRepository, VoucherGiftMapper voucherGiftMapper) {
        this.voucherGiftRepository = voucherGiftRepository;
        this.voucherGiftMapper = voucherGiftMapper;
    }

    /**
     * Save a voucherGift.
     *
     * @param voucherGiftDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public VoucherGiftDTO save(VoucherGiftDTO voucherGiftDTO) {
        log.debug("Request to save VoucherGift : {}", voucherGiftDTO);
        VoucherGift voucherGift = voucherGiftMapper.toEntity(voucherGiftDTO);
        voucherGift = voucherGiftRepository.save(voucherGift);
        return voucherGiftMapper.toDto(voucherGift);
    }

    /**
     * Get all the voucherGifts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<VoucherGiftDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VoucherGifts");
        return voucherGiftRepository.findAll(pageable)
            .map(voucherGiftMapper::toDto);
    }


    /**
     * Get one voucherGift by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<VoucherGiftDTO> findOne(Long id) {
        log.debug("Request to get VoucherGift : {}", id);
        return voucherGiftRepository.findById(id)
            .map(voucherGiftMapper::toDto);
    }

    /**
     * Delete the voucherGift by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete VoucherGift : {}", id);
        voucherGiftRepository.deleteById(id);
    }
}
