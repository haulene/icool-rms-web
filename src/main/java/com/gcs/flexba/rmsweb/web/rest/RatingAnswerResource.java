package com.gcs.flexba.rmsweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.gcs.flexba.rmsweb.service.RatingAnswerService;
import com.gcs.flexba.rmsweb.web.rest.errors.BadRequestAlertException;
import com.gcs.flexba.rmsweb.web.rest.util.HeaderUtil;
import com.gcs.flexba.rmsweb.web.rest.util.PaginationUtil;
import com.gcs.flexba.rmsweb.service.dto.RatingAnswerDTO;
import com.gcs.flexba.rmsweb.service.dto.RatingAnswerCriteria;
import com.gcs.flexba.rmsweb.service.RatingAnswerQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RatingAnswer.
 */
@RestController
@RequestMapping("/api")
public class RatingAnswerResource {

    private final Logger log = LoggerFactory.getLogger(RatingAnswerResource.class);

    private static final String ENTITY_NAME = "ratingAnswer";

    private final RatingAnswerService ratingAnswerService;

    private final RatingAnswerQueryService ratingAnswerQueryService;

    public RatingAnswerResource(RatingAnswerService ratingAnswerService, RatingAnswerQueryService ratingAnswerQueryService) {
        this.ratingAnswerService = ratingAnswerService;
        this.ratingAnswerQueryService = ratingAnswerQueryService;
    }

    /**
     * POST  /rating-answers : Create a new ratingAnswer.
     *
     * @param ratingAnswerDTO the ratingAnswerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ratingAnswerDTO, or with status 400 (Bad Request) if the ratingAnswer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rating-answers")
    @Timed
    public ResponseEntity<RatingAnswerDTO> createRatingAnswer(@RequestBody RatingAnswerDTO ratingAnswerDTO) throws URISyntaxException {
        log.debug("REST request to save RatingAnswer : {}", ratingAnswerDTO);
        if (ratingAnswerDTO.getId() != null) {
            throw new BadRequestAlertException("A new ratingAnswer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RatingAnswerDTO result = ratingAnswerService.save(ratingAnswerDTO);
        return ResponseEntity.created(new URI("/api/rating-answers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rating-answers : Updates an existing ratingAnswer.
     *
     * @param ratingAnswerDTO the ratingAnswerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ratingAnswerDTO,
     * or with status 400 (Bad Request) if the ratingAnswerDTO is not valid,
     * or with status 500 (Internal Server Error) if the ratingAnswerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rating-answers")
    @Timed
    public ResponseEntity<RatingAnswerDTO> updateRatingAnswer(@RequestBody RatingAnswerDTO ratingAnswerDTO) throws URISyntaxException {
        log.debug("REST request to update RatingAnswer : {}", ratingAnswerDTO);
        if (ratingAnswerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RatingAnswerDTO result = ratingAnswerService.save(ratingAnswerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ratingAnswerDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rating-answers : get all the ratingAnswers.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of ratingAnswers in body
     */
    @GetMapping("/rating-answers")
    @Timed
    public ResponseEntity<List<RatingAnswerDTO>> getAllRatingAnswers(RatingAnswerCriteria criteria, Pageable pageable) {
        log.debug("REST request to get RatingAnswers by criteria: {}", criteria);
        Page<RatingAnswerDTO> page = ratingAnswerQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rating-answers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rating-answers/:id : get the "id" ratingAnswer.
     *
     * @param id the id of the ratingAnswerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ratingAnswerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rating-answers/{id}")
    @Timed
    public ResponseEntity<RatingAnswerDTO> getRatingAnswer(@PathVariable Long id) {
        log.debug("REST request to get RatingAnswer : {}", id);
        Optional<RatingAnswerDTO> ratingAnswerDTO = ratingAnswerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ratingAnswerDTO);
    }

    /**
     * DELETE  /rating-answers/:id : delete the "id" ratingAnswer.
     *
     * @param id the id of the ratingAnswerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rating-answers/{id}")
    @Timed
    public ResponseEntity<Void> deleteRatingAnswer(@PathVariable Long id) {
        log.debug("REST request to delete RatingAnswer : {}", id);
        ratingAnswerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
