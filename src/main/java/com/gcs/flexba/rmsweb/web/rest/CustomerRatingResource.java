package com.gcs.flexba.rmsweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.gcs.flexba.rmsweb.service.CustomerRatingService;
import com.gcs.flexba.rmsweb.web.rest.errors.BadRequestAlertException;
import com.gcs.flexba.rmsweb.web.rest.util.HeaderUtil;
import com.gcs.flexba.rmsweb.web.rest.util.PaginationUtil;
import com.gcs.flexba.rmsweb.service.dto.CustomerRatingDTO;
import com.gcs.flexba.rmsweb.service.dto.CustomerRatingCriteria;
import com.gcs.flexba.rmsweb.service.CustomerRatingQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CustomerRating.
 */
@RestController
@RequestMapping("/api")
public class CustomerRatingResource {

    private final Logger log = LoggerFactory.getLogger(CustomerRatingResource.class);

    private static final String ENTITY_NAME = "customerRating";

    private final CustomerRatingService customerRatingService;

    private final CustomerRatingQueryService customerRatingQueryService;

    public CustomerRatingResource(CustomerRatingService customerRatingService, CustomerRatingQueryService customerRatingQueryService) {
        this.customerRatingService = customerRatingService;
        this.customerRatingQueryService = customerRatingQueryService;
    }

    /**
     * POST  /customer-ratings : Create a new customerRating.
     *
     * @param customerRatingDTO the customerRatingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customerRatingDTO, or with status 400 (Bad Request) if the customerRating has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-ratings")
    @Timed
    public ResponseEntity<CustomerRatingDTO> createCustomerRating(@RequestBody CustomerRatingDTO customerRatingDTO) throws URISyntaxException {
        log.debug("REST request to save CustomerRating : {}", customerRatingDTO);
        if (customerRatingDTO.getId() != null) {
            throw new BadRequestAlertException("A new customerRating cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerRatingDTO result = customerRatingService.save(customerRatingDTO);
        return ResponseEntity.created(new URI("/api/customer-ratings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /customer-ratings : Updates an existing customerRating.
     *
     * @param customerRatingDTO the customerRatingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customerRatingDTO,
     * or with status 400 (Bad Request) if the customerRatingDTO is not valid,
     * or with status 500 (Internal Server Error) if the customerRatingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/customer-ratings")
    @Timed
    public ResponseEntity<CustomerRatingDTO> updateCustomerRating(@RequestBody CustomerRatingDTO customerRatingDTO) throws URISyntaxException {
        log.debug("REST request to update CustomerRating : {}", customerRatingDTO);
        if (customerRatingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CustomerRatingDTO result = customerRatingService.save(customerRatingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customerRatingDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /customer-ratings : get all the customerRatings.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of customerRatings in body
     */
    @GetMapping("/customer-ratings")
    @Timed
    public ResponseEntity<List<CustomerRatingDTO>> getAllCustomerRatings(CustomerRatingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CustomerRatings by criteria: {}", criteria);
        Page<CustomerRatingDTO> page = customerRatingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customer-ratings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /customer-ratings/:id : get the "id" customerRating.
     *
     * @param id the id of the customerRatingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the customerRatingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/customer-ratings/{id}")
    @Timed
    public ResponseEntity<CustomerRatingDTO> getCustomerRating(@PathVariable Long id) {
        log.debug("REST request to get CustomerRating : {}", id);
        Optional<CustomerRatingDTO> customerRatingDTO = customerRatingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(customerRatingDTO);
    }

    /**
     * DELETE  /customer-ratings/:id : delete the "id" customerRating.
     *
     * @param id the id of the customerRatingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/customer-ratings/{id}")
    @Timed
    public ResponseEntity<Void> deleteCustomerRating(@PathVariable Long id) {
        log.debug("REST request to delete CustomerRating : {}", id);
        customerRatingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
