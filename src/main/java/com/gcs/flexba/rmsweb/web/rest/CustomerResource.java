package com.gcs.flexba.rmsweb.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.gcs.flexba.rmsweb.service.CustomerQueryService;
import com.gcs.flexba.rmsweb.service.CustomerService;
import com.gcs.flexba.rmsweb.service.dto.CustomerCriteria;
import com.gcs.flexba.rmsweb.service.dto.CustomerDTO;
import com.gcs.flexba.rmsweb.service.dto.MergeCustomerDTO;
import com.gcs.flexba.rmsweb.service.util.AES;
import com.gcs.flexba.rmsweb.web.rest.errors.BadRequestAlertException;
import com.gcs.flexba.rmsweb.web.rest.util.HeaderUtil;
import com.gcs.flexba.rmsweb.web.rest.util.PaginationUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Customer.
 */
@RestController
@RequestMapping("/api")
public class CustomerResource {

	private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

	private static final String ENTITY_NAME = "customer";

	private final CustomerService customerService;

	private final CustomerQueryService customerQueryService;

	public CustomerResource(CustomerService customerService, CustomerQueryService customerQueryService) {
		this.customerService = customerService;
		this.customerQueryService = customerQueryService;
	}

	/**
	 * POST /customers : Create a new customer.
	 *
	 * @param customerDTO the customerDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         customerDTO, or with status 400 (Bad Request) if the customer has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/customers")
	@Timed
	public ResponseEntity<CustomerDTO> createCustomer(@RequestBody CustomerDTO customerDTO) throws URISyntaxException {
		log.debug("REST request to save Customer : {}", customerDTO);
		if (customerDTO.getId() != null) {
			throw new BadRequestAlertException("A new customer cannot already have an ID", ENTITY_NAME, "idexists");
		}
		CustomerDTO result = customerService.save(customerDTO);
		return ResponseEntity.created(new URI("/api/customers/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * POST /customers : Create a new customer.
	 *
	 * @param customerDTO the customerDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         customerDTO, or with status 400 (Bad Request) if the customer has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/customers/merge")
	@Timed
	public ResponseEntity<CustomerDTO> mergeCustomer(@RequestBody MergeCustomerDTO mergeCustomerDTO)
			throws URISyntaxException {
		log.debug("REST request to merge Customer : {}", mergeCustomerDTO);
		if (mergeCustomerDTO.getKeepCustomer() == null) {
			throw new BadRequestAlertException("keepCustomer can not null", ENTITY_NAME, "idexists");
		}

		if (mergeCustomerDTO.getChoosenLevelCode() == null) {
			// throw new BadRequestAlertException("Level code can not null", ENTITY_NAME,
			// "idexists");
		}

		if (mergeCustomerDTO.getRemovedCustomers() == null || mergeCustomerDTO.getRemovedCustomers().isEmpty()) {
			throw new BadRequestAlertException("Removed Customers can not empty", ENTITY_NAME, "idexists");
		}
		CustomerDTO result = customerService.mergeCustomer(mergeCustomerDTO);
		return ResponseEntity.created(new URI("/api/customers/merge" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /customers : Updates an existing customer.
	 *
	 * @param customerDTO the customerDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         customerDTO, or with status 400 (Bad Request) if the customerDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         customerDTO couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/customers")
	@Timed
	public ResponseEntity<CustomerDTO> updateCustomer(@RequestBody CustomerDTO customerDTO) throws URISyntaxException {
		log.debug("REST request to update Customer : {}", customerDTO);
		if (customerDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		CustomerDTO result = customerService.save(customerDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customerDTO.getId().toString())).body(result);
	}

	/**
	 * GET /customers : get all the customers.
	 *
	 * @param pageable the pagination information
	 * @param criteria the criterias which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of customers in
	 *         body
	 */
	@GetMapping("/customers")
	@Timed
	public ResponseEntity<List<CustomerDTO>> getAllCustomers(CustomerCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Customers by criteria: {}", criteria);
		Page<CustomerDTO> page = customerQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customers");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /customers : get all the customers.
	 *
	 * @param pageable the pagination information
	 * @param criteria the criterias which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of customers in
	 *         body
	 */
	@GetMapping("/customers/suggest")
	@Timed
	public ResponseEntity<List<CustomerDTO>> getAllCustomersBySuggestion(CustomerCriteria criteria, Pageable pageable) {
		log.debug("REST request to get Customers by criteria: {}", criteria);
		Page<CustomerDTO> page = customerQueryService.findByCriteriaBySuggestion(criteria, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customers");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /customers/:id : get the "id" customer.
	 *
	 * @param id the id of the customerDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         customerDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/customers/{id}")
	@Timed
	public ResponseEntity<CustomerDTO> getCustomer(@PathVariable Long id) {
		log.debug("REST request to get Customer : {}", id);
		Optional<CustomerDTO> customerDTO = customerService.findOne(id);
		return ResponseUtil.wrapOrNotFound(customerDTO);
	}

	/**
	 * DELETE /customers/:id : delete the "id" customer.
	 *
	 * @param id the id of the customerDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/customers/{id}")
	@Timed
	public ResponseEntity<Void> deleteCustomer(@PathVariable Long id) {
		log.debug("REST request to delete Customer : {}", id);
		customerService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	@GetMapping("/customers/encryptQRCode")
	@Timed
	public ResponseEntity<String> encryptQRCode(@RequestParam("plainText") String planText) {
		final String secretKey = "i1!C6^O0)O0)L2@";
		String originalString = planText;
		String encryptedString = AES.encrypt(originalString, secretKey);
		// String decryptedString = AES.decrypt(encryptedString, secretKey);
		return new ResponseEntity<String>(encryptedString, HttpStatus.OK);
	}
}
