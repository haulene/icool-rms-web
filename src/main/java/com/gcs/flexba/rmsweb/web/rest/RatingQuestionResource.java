package com.gcs.flexba.rmsweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.gcs.flexba.rmsweb.service.RatingQuestionService;
import com.gcs.flexba.rmsweb.web.rest.errors.BadRequestAlertException;
import com.gcs.flexba.rmsweb.web.rest.util.HeaderUtil;
import com.gcs.flexba.rmsweb.web.rest.util.PaginationUtil;
import com.gcs.flexba.rmsweb.service.dto.RatingQuestionDTO;
import com.gcs.flexba.rmsweb.service.dto.RatingQuestionCriteria;
import com.gcs.flexba.rmsweb.service.RatingQuestionQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RatingQuestion.
 */
@RestController
@RequestMapping("/api")
public class RatingQuestionResource {

    private final Logger log = LoggerFactory.getLogger(RatingQuestionResource.class);

    private static final String ENTITY_NAME = "ratingQuestion";

    private final RatingQuestionService ratingQuestionService;

    private final RatingQuestionQueryService ratingQuestionQueryService;

    public RatingQuestionResource(RatingQuestionService ratingQuestionService, RatingQuestionQueryService ratingQuestionQueryService) {
        this.ratingQuestionService = ratingQuestionService;
        this.ratingQuestionQueryService = ratingQuestionQueryService;
    }

    /**
     * POST  /rating-questions : Create a new ratingQuestion.
     *
     * @param ratingQuestionDTO the ratingQuestionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ratingQuestionDTO, or with status 400 (Bad Request) if the ratingQuestion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rating-questions")
    @Timed
    public ResponseEntity<RatingQuestionDTO> createRatingQuestion(@RequestBody RatingQuestionDTO ratingQuestionDTO) throws URISyntaxException {
        log.debug("REST request to save RatingQuestion : {}", ratingQuestionDTO);
        if (ratingQuestionDTO.getId() != null) {
            throw new BadRequestAlertException("A new ratingQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RatingQuestionDTO result = ratingQuestionService.save(ratingQuestionDTO);
        return ResponseEntity.created(new URI("/api/rating-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rating-questions : Updates an existing ratingQuestion.
     *
     * @param ratingQuestionDTO the ratingQuestionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ratingQuestionDTO,
     * or with status 400 (Bad Request) if the ratingQuestionDTO is not valid,
     * or with status 500 (Internal Server Error) if the ratingQuestionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rating-questions")
    @Timed
    public ResponseEntity<RatingQuestionDTO> updateRatingQuestion(@RequestBody RatingQuestionDTO ratingQuestionDTO) throws URISyntaxException {
        log.debug("REST request to update RatingQuestion : {}", ratingQuestionDTO);
        if (ratingQuestionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RatingQuestionDTO result = ratingQuestionService.save(ratingQuestionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ratingQuestionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rating-questions : get all the ratingQuestions.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of ratingQuestions in body
     */
    @GetMapping("/rating-questions")
    @Timed
    public ResponseEntity<List<RatingQuestionDTO>> getAllRatingQuestions(RatingQuestionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get RatingQuestions by criteria: {}", criteria);
        Page<RatingQuestionDTO> page = ratingQuestionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rating-questions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rating-questions/:id : get the "id" ratingQuestion.
     *
     * @param id the id of the ratingQuestionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ratingQuestionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rating-questions/{id}")
    @Timed
    public ResponseEntity<RatingQuestionDTO> getRatingQuestion(@PathVariable Long id) {
        log.debug("REST request to get RatingQuestion : {}", id);
        Optional<RatingQuestionDTO> ratingQuestionDTO = ratingQuestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ratingQuestionDTO);
    }

    /**
     * DELETE  /rating-questions/:id : delete the "id" ratingQuestion.
     *
     * @param id the id of the ratingQuestionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rating-questions/{id}")
    @Timed
    public ResponseEntity<Void> deleteRatingQuestion(@PathVariable Long id) {
        log.debug("REST request to delete RatingQuestion : {}", id);
        ratingQuestionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
