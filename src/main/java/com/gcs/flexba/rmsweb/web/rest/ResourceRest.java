package com.gcs.flexba.rmsweb.web.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ResourceRest {
	
	@Value(value = "${folders.image.promotion}")
	private String promotionImageFolder;

	@RequestMapping(path = "/resources/download/promotion/{imagePath}", method = RequestMethod.GET)
	public ResponseEntity<Resource> download(@PathVariable String imagePath) throws IOException {
		File file = new File(FilenameUtils.concat(promotionImageFolder, imagePath));
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		return ResponseEntity.ok().contentLength(file.length())
				.contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
	}
}
