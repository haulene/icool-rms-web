/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gcs.flexba.rmsweb.web.rest.vm;
