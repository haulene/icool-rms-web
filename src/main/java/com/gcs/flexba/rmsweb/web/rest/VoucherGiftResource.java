package com.gcs.flexba.rmsweb.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.gcs.flexba.rmsweb.service.VoucherGiftService;
import com.gcs.flexba.rmsweb.web.rest.errors.BadRequestAlertException;
import com.gcs.flexba.rmsweb.web.rest.util.HeaderUtil;
import com.gcs.flexba.rmsweb.web.rest.util.PaginationUtil;
import com.gcs.flexba.rmsweb.service.dto.VoucherGiftDTO;
import com.gcs.flexba.rmsweb.service.dto.VoucherGiftCriteria;
import com.gcs.flexba.rmsweb.service.VoucherGiftQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing VoucherGift.
 */
@RestController
@RequestMapping("/api")
public class VoucherGiftResource {

    private final Logger log = LoggerFactory.getLogger(VoucherGiftResource.class);

    private static final String ENTITY_NAME = "voucherGift";

    private final VoucherGiftService voucherGiftService;

    private final VoucherGiftQueryService voucherGiftQueryService;

    public VoucherGiftResource(VoucherGiftService voucherGiftService, VoucherGiftQueryService voucherGiftQueryService) {
        this.voucherGiftService = voucherGiftService;
        this.voucherGiftQueryService = voucherGiftQueryService;
    }

    /**
     * POST  /voucher-gifts : Create a new voucherGift.
     *
     * @param voucherGiftDTO the voucherGiftDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new voucherGiftDTO, or with status 400 (Bad Request) if the voucherGift has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/voucher-gifts")
    @Timed
    public ResponseEntity<VoucherGiftDTO> createVoucherGift(@RequestBody VoucherGiftDTO voucherGiftDTO) throws URISyntaxException {
        log.debug("REST request to save VoucherGift : {}", voucherGiftDTO);
        if (voucherGiftDTO.getId() != null) {
            throw new BadRequestAlertException("A new voucherGift cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VoucherGiftDTO result = voucherGiftService.save(voucherGiftDTO);
        return ResponseEntity.created(new URI("/api/voucher-gifts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /voucher-gifts : Updates an existing voucherGift.
     *
     * @param voucherGiftDTO the voucherGiftDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated voucherGiftDTO,
     * or with status 400 (Bad Request) if the voucherGiftDTO is not valid,
     * or with status 500 (Internal Server Error) if the voucherGiftDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/voucher-gifts")
    @Timed
    public ResponseEntity<VoucherGiftDTO> updateVoucherGift(@RequestBody VoucherGiftDTO voucherGiftDTO) throws URISyntaxException {
        log.debug("REST request to update VoucherGift : {}", voucherGiftDTO);
        if (voucherGiftDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VoucherGiftDTO result = voucherGiftService.save(voucherGiftDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, voucherGiftDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /voucher-gifts : get all the voucherGifts.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of voucherGifts in body
     */
    @GetMapping("/voucher-gifts")
    @Timed
    public ResponseEntity<List<VoucherGiftDTO>> getAllVoucherGifts(VoucherGiftCriteria criteria, Pageable pageable) {
        log.debug("REST request to get VoucherGifts by criteria: {}", criteria);
        Page<VoucherGiftDTO> page = voucherGiftQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/voucher-gifts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /voucher-gifts/:id : get the "id" voucherGift.
     *
     * @param id the id of the voucherGiftDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the voucherGiftDTO, or with status 404 (Not Found)
     */
    @GetMapping("/voucher-gifts/{id}")
    @Timed
    public ResponseEntity<VoucherGiftDTO> getVoucherGift(@PathVariable Long id) {
        log.debug("REST request to get VoucherGift : {}", id);
        Optional<VoucherGiftDTO> voucherGiftDTO = voucherGiftService.findOne(id);
        return ResponseUtil.wrapOrNotFound(voucherGiftDTO);
    }

    /**
     * DELETE  /voucher-gifts/:id : delete the "id" voucherGift.
     *
     * @param id the id of the voucherGiftDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/voucher-gifts/{id}")
    @Timed
    public ResponseEntity<Void> deleteVoucherGift(@PathVariable Long id) {
        log.debug("REST request to delete VoucherGift : {}", id);
        voucherGiftService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
