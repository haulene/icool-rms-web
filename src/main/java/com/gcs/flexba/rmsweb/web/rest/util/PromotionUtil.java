package com.gcs.flexba.rmsweb.web.rest.util;

import com.gcs.flexba.rmsweb.domain.Promotion;
import com.gcs.flexba.rmsweb.service.dto.PromotionDTO;

public class PromotionUtil {
	
	public static boolean haveVCGiftInserting(PromotionDTO promotionDTO) {
		boolean rs = false;
		// 0: Redeme, 1:
//		promotionTypes = [{
//	        code: 0,
//	        name: "News",
//	    }, {
//	        code: 1,
//	        name: "Redeem",
//	    }, {
//	        code: 2,
//	        name: "Public codes",
//	    }, {
//	        code: 3,
//	        name: "Unique Codes",
//	    }, {
//        code: 4,
//        name: "Referral code",
//    }, {
//      code: 4,
//      name: "Referral promotion",
//  }];
		if ((promotionDTO.isRedeemFlag() && promotionDTO.getPromotionType() == 1)
				|| promotionDTO.getPromotionType() == 2 || promotionDTO.getPromotionType() == 3 || promotionDTO.getPromotionType() == 4 || promotionDTO.getPromotionType() == 5
				|| promotionDTO.getPromotionType() == 6) {
			rs = true;
		}
		return rs;
	}
	
	public static boolean haveVCGiftInserting(Promotion promotion) {
		boolean rs = false;
		// 0: Redeme, 1:
//		promotionTypes = [{
//	        code: 0,
//	        name: "News",
//	    }, {
//	        code: 1,
//	        name: "Redeem",
//	    }, {
//	        code: 2,
//	        name: "Public codes",
//	    }, {
//	        code: 3,
//	        name: "Unique Codes",
//	    }, {
//      code: 4,
//      name: "Referral code",
//  }, {
//      code: 5,
//      name: "Referral promotion",
//  }];
		if ((promotion.isRedeemFlag() && promotion.getPromotionType() == 1)
				|| promotion.getPromotionType() == 2 || promotion.getPromotionType() == 3 || promotion.getPromotionType() == 4|| promotion.getPromotionType() == 5
				|| promotion.getPromotionType() == 6) {
			rs = true;
		}
		return rs;
	}
}
