package com.gcs.flexba.rmsweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A RatingAnswer.
 */
@Entity
@Table(name = "rating_answer_tbl")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RatingAnswer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rating_answer_id")
    private Long id;

    @Column(name = "rating_id")
    private Integer ratingId;

    @Column(name = "rating_question_id")
    private Integer ratingQuestionId;

    @Column(name = "type")
    private Integer type;

    @Column(name = "value")
    private String value;

    @Column(name = "answer_category")
    private String answerCategory;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRatingId() {
        return ratingId;
    }

    public RatingAnswer ratingId(Integer ratingId) {
        this.ratingId = ratingId;
        return this;
    }

    public void setRatingId(Integer ratingId) {
        this.ratingId = ratingId;
    }

    public Integer getRatingQuestionId() {
        return ratingQuestionId;
    }

    public RatingAnswer ratingQuestionId(Integer ratingQuestionId) {
        this.ratingQuestionId = ratingQuestionId;
        return this;
    }

    public void setRatingQuestionId(Integer ratingQuestionId) {
        this.ratingQuestionId = ratingQuestionId;
    }

    public Integer getType() {
        return type;
    }

    public RatingAnswer type(Integer type) {
        this.type = type;
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public RatingAnswer value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAnswerCategory() {
        return answerCategory;
    }

    public RatingAnswer answerCategory(String answerCategory) {
        this.answerCategory = answerCategory;
        return this;
    }

    public void setAnswerCategory(String answerCategory) {
        this.answerCategory = answerCategory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RatingAnswer ratingAnswer = (RatingAnswer) o;
        if (ratingAnswer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ratingAnswer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RatingAnswer{" +
            "id=" + getId() +
            ", ratingId=" + getRatingId() +
            ", ratingQuestionId=" + getRatingQuestionId() +
            ", type=" + getType() +
            ", value='" + getValue() + "'" +
            ", answerCategory='" + getAnswerCategory() + "'" +
            "}";
    }
}
