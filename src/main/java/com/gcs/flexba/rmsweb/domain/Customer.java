package com.gcs.flexba.rmsweb.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Customer.
 */
@Entity
@Table(name = "customer_tbl")
@NamedStoredProcedureQuery(name = "MergeCustomer", procedureName = "MergeCustomer", parameters = {
		@StoredProcedureParameter(name = "@CustomerCode", type = String.class, mode = ParameterMode.IN),
		@StoredProcedureParameter(name = "@OldCustomerCode", type = String.class, mode = ParameterMode.IN),
		@StoredProcedureParameter(name = "@Level", type = String.class, mode = ParameterMode.IN),
		@StoredProcedureParameter(name = "@MergeBy", type = String.class, mode = ParameterMode.IN),
		@StoredProcedureParameter(name = "@IsDeleteOldCustomer", type = Boolean.class, mode = ParameterMode.IN) })
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private Long id;

    @Column(name = "customer_code")
    private String customerCode;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "tel_2")
    private String tel2;

    @Column(name = "tel_1")
    private String tel1;

    @Column(name = "tel_3")
    private String tel3;

    @Column(name = "facebook_id")
    private String facebookId;
    
    @Column(name = "image_path")
    private String imagePath;
    
    @Column(name = "delete_flag")
    private boolean deleteFlag;

    public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@OneToMany(mappedBy = "customer")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MembershipHistoty> membershipHistoties = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public Customer customerCode(String customerCode) {
        this.customerCode = customerCode;
        return this;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public Customer firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Customer middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public Customer lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public Customer fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getGender() {
        return gender;
    }

    public Customer gender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getTel2() {
        return tel2;
    }

    public Customer tel2(String tel2) {
        this.tel2 = tel2;
        return this;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getTel1() {
        return tel1;
    }

    public Customer tel1(String tel1) {
        this.tel1 = tel1;
        return this;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel3() {
        return tel3;
    }

    public Customer tel3(String tel3) {
        this.tel3 = tel3;
        return this;
    }

    public void setTel3(String tel3) {
        this.tel3 = tel3;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public Customer facebookId(String facebookId) {
        this.facebookId = facebookId;
        return this;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public Set<MembershipHistoty> getMembershipHistoties() {
        return membershipHistoties;
    }

    public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Customer membershipHistoties(Set<MembershipHistoty> membershipHistoties) {
        this.membershipHistoties = membershipHistoties;
        return this;
    }

    public Customer addMembershipHistoty(MembershipHistoty membershipHistoty) {
        this.membershipHistoties.add(membershipHistoty);
        membershipHistoty.setCustomer(this);
        return this;
    }

    public Customer removeMembershipHistoty(MembershipHistoty membershipHistoty) {
        this.membershipHistoties.remove(membershipHistoty);
        membershipHistoty.setCustomer(null);
        return this;
    }

    public void setMembershipHistoties(Set<MembershipHistoty> membershipHistoties) {
        this.membershipHistoties = membershipHistoties;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Customer customer = (Customer) o;
        if (customer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), customer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Customer{" +
            "id=" + getId() +
            ", customerCode='" + getCustomerCode() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", fullName='" + getFullName() + "'" +
            ", gender=" + getGender() +
            ", tel2='" + getTel2() + "'" +
            ", tel1='" + getTel1() + "'" +
            ", tel3='" + getTel3() + "'" +
            ", facebookId='" + getFacebookId() + "'" +
            "}";
    }
}
