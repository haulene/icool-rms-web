package com.gcs.flexba.rmsweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A CustomerRating.
 */
@Entity
@Table(name = "rating_tbl")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CustomerRating implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rating_id")
    private Long id;

    @Column(name = "num_star")
    private Integer numStar;

    @Column(name = "customer_code", insertable=false, updatable=false)
    private String customerCode;

    @Column(name = "source")
    private String source;

    @Column(name = "inventory_code")
    private String inventoryCode;

    @Column(name = "type")
    private Integer type;

    @Column(name = "comment")
    private String comment;

    @Column(name = "updated_date")
    private ZonedDateTime updatedDate;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "delete_flag")
    private Integer deleteFlag;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="customer_code", referencedColumnName = "customer_code")
    private Customer customer;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumStar() {
        return numStar;
    }

    public CustomerRating numStar(Integer numStar) {
        this.numStar = numStar;
        return this;
    }

    public void setNumStar(Integer numStar) {
        this.numStar = numStar;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public CustomerRating customerCode(String customerCode) {
        this.customerCode = customerCode;
        return this;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getSource() {
        return source;
    }

    public CustomerRating source(String source) {
        this.source = source;
        return this;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getInventoryCode() {
        return inventoryCode;
    }

    public CustomerRating inventoryCode(String inventoryCode) {
        this.inventoryCode = inventoryCode;
        return this;
    }

    public void setInventoryCode(String inventoryCode) {
        this.inventoryCode = inventoryCode;
    }

    public Integer getType() {
        return type;
    }

    public CustomerRating type(Integer type) {
        this.type = type;
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public CustomerRating comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ZonedDateTime getUpdatedDate() {
        return updatedDate;
    }

    public CustomerRating updatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public CustomerRating createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public CustomerRating deleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
        return this;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CustomerRating customerRating = (CustomerRating) o;
        if (customerRating.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), customerRating.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CustomerRating{" +
            "id=" + getId() +
            ", numStar=" + getNumStar() +
            ", customerCode='" + getCustomerCode() + "'" +
            ", source='" + getSource() + "'" +
            ", inventoryCode='" + getInventoryCode() + "'" +
            ", type=" + getType() +
            ", comment='" + getComment() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", deleteFlag=" + getDeleteFlag() +
            "}";
    }

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
