package com.gcs.flexba.rmsweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A RatingQuestion.
 */
@Entity
@Table(name = "rating_question_tbl")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RatingQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rating_question_id")
    private Long id;

    @Column(name = "content")
    private String content;

    @Column(name = "type")
    private Integer type;

    @Column(name = "start_num_star")
    private Integer startNumStar;

    @Column(name = "end_num_star")
    private Integer endNumStar;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "updated_date")
    private ZonedDateTime updatedDate;

    @Column(name = "answer_type")
    private String answerType;

    @Column(name = "answer_category")
    private String answerCategory;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public RatingQuestion content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getType() {
        return type;
    }

    public RatingQuestion type(Integer type) {
        this.type = type;
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStartNumStar() {
        return startNumStar;
    }

    public RatingQuestion startNumStar(Integer startNumStar) {
        this.startNumStar = startNumStar;
        return this;
    }

    public void setStartNumStar(Integer startNumStar) {
        this.startNumStar = startNumStar;
    }

    public Integer getEndNumStar() {
        return endNumStar;
    }

    public RatingQuestion endNumStar(Integer endNumStar) {
        this.endNumStar = endNumStar;
        return this;
    }

    public void setEndNumStar(Integer endNumStar) {
        this.endNumStar = endNumStar;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public RatingQuestion createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getUpdatedDate() {
        return updatedDate;
    }

    public RatingQuestion updatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getAnswerType() {
        return answerType;
    }

    public RatingQuestion answerType(String answerType) {
        this.answerType = answerType;
        return this;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getAnswerCategory() {
        return answerCategory;
    }

    public RatingQuestion answerCategory(String answerCategory) {
        this.answerCategory = answerCategory;
        return this;
    }

    public void setAnswerCategory(String answerCategory) {
        this.answerCategory = answerCategory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RatingQuestion ratingQuestion = (RatingQuestion) o;
        if (ratingQuestion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ratingQuestion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RatingQuestion{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", type=" + getType() +
            ", startNumStar=" + getStartNumStar() +
            ", endNumStar=" + getEndNumStar() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", answerType='" + getAnswerType() + "'" +
            ", answerCategory='" + getAnswerCategory() + "'" +
            "}";
    }
}
