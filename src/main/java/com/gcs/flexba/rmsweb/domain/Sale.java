package com.gcs.flexba.rmsweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Sale.
 */
@Entity
@Table(name = "Sale")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sale implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "is_begin")
    private Integer isBegin;

    @Column(name = "is_end")
    private Integer isEnd;

    @Column(name = "[begin]")
    private ZonedDateTime begin;

    @Column(name = "[end]")
    private ZonedDateTime end;

    @Column(name = "trangthai")
    private Integer trangthai;

    @Column(name = "sale_type")
    private Integer saleType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Sale code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public Sale name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsBegin() {
        return isBegin;
    }

    public Sale isBegin(Integer isBegin) {
        this.isBegin = isBegin;
        return this;
    }

    public void setIsBegin(Integer isBegin) {
        this.isBegin = isBegin;
    }

    public Integer getIsEnd() {
        return isEnd;
    }

    public Sale isEnd(Integer isEnd) {
        this.isEnd = isEnd;
        return this;
    }

    public void setIsEnd(Integer isEnd) {
        this.isEnd = isEnd;
    }

    public ZonedDateTime getBegin() {
        return begin;
    }

    public Sale begin(ZonedDateTime begin) {
        this.begin = begin;
        return this;
    }

    public void setBegin(ZonedDateTime begin) {
        this.begin = begin;
    }

    public ZonedDateTime getEnd() {
        return end;
    }

    public Sale end(ZonedDateTime end) {
        this.end = end;
        return this;
    }

    public void setEnd(ZonedDateTime end) {
        this.end = end;
    }

    public Integer getTrangthai() {
        return trangthai;
    }

    public Sale trangthai(Integer trangthai) {
        this.trangthai = trangthai;
        return this;
    }

    public void setTrangthai(Integer trangthai) {
        this.trangthai = trangthai;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public Sale saleType(Integer saleType) {
        this.saleType = saleType;
        return this;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sale sale = (Sale) o;
        if (sale.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sale.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Sale{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", isBegin=" + getIsBegin() +
            ", isEnd=" + getIsEnd() +
            ", begin='" + getBegin() + "'" +
            ", end='" + getEnd() + "'" +
            ", trangthai=" + getTrangthai() +
            ", saleType=" + getSaleType() +
            "}";
    }
}
