package com.gcs.flexba.rmsweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Config.
 */
@Entity
@Table(name = "config_tbl")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Config implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "config_key")
	private String id;

	@Column(name = "config_value")
	private String configValue;

	@Column(name = "sync_config_flag")
	private String syncConfigFlag;

	@Column(name = "allow_change_sync_config_flag")
	private Boolean allowChangeSyncConfigFlag;

	@Column(name = "update_date")
	private ZonedDateTime updateDate;

	@Column(name = "channel")
	private String channel;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getConfigValue() {
		return configValue;
	}

	public Config configValue(String configValue) {
		this.configValue = configValue;
		return this;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getSyncConfigFlag() {
		return syncConfigFlag;
	}

	public Config syncConfigFlag(String syncConfigFlag) {
		this.syncConfigFlag = syncConfigFlag;
		return this;
	}

	public void setSyncConfigFlag(String syncConfigFlag) {
		this.syncConfigFlag = syncConfigFlag;
	}

	public Boolean isAllowChangeSyncConfigFlag() {
		return allowChangeSyncConfigFlag;
	}

	public Config allowChangeSyncConfigFlag(Boolean allowChangeSyncConfigFlag) {
		this.allowChangeSyncConfigFlag = allowChangeSyncConfigFlag;
		return this;
	}

	public void setAllowChangeSyncConfigFlag(Boolean allowChangeSyncConfigFlag) {
		this.allowChangeSyncConfigFlag = allowChangeSyncConfigFlag;
	}

	public ZonedDateTime getUpdateDate() {
		return updateDate;
	}

	public Config updateDate(ZonedDateTime updateDate) {
		this.updateDate = updateDate;
		return this;
	}

	public void setUpdateDate(ZonedDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getChannel() {
		return channel;
	}

	public Config channel(String channel) {
		this.channel = channel;
		return this;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Config config = (Config) o;
		if (config.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), config.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Config{" + "id=" + getId() + ", configValue='" + getConfigValue() + "'" + ", syncConfigFlag='"
				+ getSyncConfigFlag() + "'" + ", allowChangeSyncConfigFlag='" + isAllowChangeSyncConfigFlag() + "'"
				+ ", updateDate='" + getUpdateDate() + "'" + ", channel='" + getChannel() + "'" + "}";
	}
}
