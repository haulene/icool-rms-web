package com.gcs.flexba.rmsweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A MembershipHistoty.
 */
@Entity
@Table(name = "membership_history_tbl")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MembershipHistoty implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "history_id")
	private Long id;

	@Column(name = "membership_level_code")
	private String membershipLevelCode;

	@Column(name = "effected_date")
	private ZonedDateTime effectedDate;

	@Column(name = "start_date")
	private ZonedDateTime startDate;

	@Column(name = "expired_date")
	private ZonedDateTime expiredDate;

	@Column(name = "status")
	private Integer status;

	@Column(name = "accumulation_points")
	private Double accumulationPoints;

	@Column(name = "pending_points")
	private Double pendingPoints;

	@Column(name = "activated_points")
	private Double activatedPoints;

	@Column(name = "expired_points")
	private Double expiredPoints;

	@Column(name = "redeem_points")
	private Double redeemPoints;

	@Column(name = "remain_points")
	private Double remainPoints;

	@Column(name = "update_date")
	private ZonedDateTime updateDate;

	@Column(name = "transferable_points")
	private Double transferablePoints;

	@Column(name = "delete_flag")
	private Integer deleteFlag;

	@ManyToOne
	@JsonIgnoreProperties("membershipHistoties")
	@JoinColumn(name = "customer_code", referencedColumnName="customer_code")
	private Customer customer;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMembershipLevelCode() {
		return membershipLevelCode;
	}

	public MembershipHistoty membershipLevelCode(String membershipLevelCode) {
		this.membershipLevelCode = membershipLevelCode;
		return this;
	}

	public void setMembershipLevelCode(String membershipLevelCode) {
		this.membershipLevelCode = membershipLevelCode;
	}

	public ZonedDateTime getEffectedDate() {
		return effectedDate;
	}

	public MembershipHistoty effectedDate(ZonedDateTime effectedDate) {
		this.effectedDate = effectedDate;
		return this;
	}

	public void setEffectedDate(ZonedDateTime effectedDate) {
		this.effectedDate = effectedDate;
	}

	public ZonedDateTime getStartDate() {
		return startDate;
	}

	public MembershipHistoty startDate(ZonedDateTime startDate) {
		this.startDate = startDate;
		return this;
	}

	public void setStartDate(ZonedDateTime startDate) {
		this.startDate = startDate;
	}

	public ZonedDateTime getExpiredDate() {
		return expiredDate;
	}

	public MembershipHistoty expiredDate(ZonedDateTime expiredDate) {
		this.expiredDate = expiredDate;
		return this;
	}

	public void setExpiredDate(ZonedDateTime expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Integer getStatus() {
		return status;
	}

	public MembershipHistoty status(Integer status) {
		this.status = status;
		return this;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Double getAccumulationPoints() {
		return accumulationPoints;
	}

	public MembershipHistoty accumulationPoints(Double accumulationPoints) {
		this.accumulationPoints = accumulationPoints;
		return this;
	}

	public void setAccumulationPoints(Double accumulationPoints) {
		this.accumulationPoints = accumulationPoints;
	}

	public Double getPendingPoints() {
		return pendingPoints;
	}

	public MembershipHistoty pendingPoints(Double pendingPoints) {
		this.pendingPoints = pendingPoints;
		return this;
	}

	public void setPendingPoints(Double pendingPoints) {
		this.pendingPoints = pendingPoints;
	}

	public Double getActivatedPoints() {
		return activatedPoints;
	}

	public MembershipHistoty activatedPoints(Double activatedPoints) {
		this.activatedPoints = activatedPoints;
		return this;
	}

	public void setActivatedPoints(Double activatedPoints) {
		this.activatedPoints = activatedPoints;
	}

	public Double getExpiredPoints() {
		return expiredPoints;
	}

	public MembershipHistoty expiredPoints(Double expiredPoints) {
		this.expiredPoints = expiredPoints;
		return this;
	}

	public void setExpiredPoints(Double expiredPoints) {
		this.expiredPoints = expiredPoints;
	}

	public Double getRedeemPoints() {
		return redeemPoints;
	}

	public MembershipHistoty redeemPoints(Double redeemPoints) {
		this.redeemPoints = redeemPoints;
		return this;
	}

	public void setRedeemPoints(Double redeemPoints) {
		this.redeemPoints = redeemPoints;
	}

	public Double getRemainPoints() {
		return remainPoints;
	}

	public MembershipHistoty remainPoints(Double remainPoints) {
		this.remainPoints = remainPoints;
		return this;
	}

	public void setRemainPoints(Double remainPoints) {
		this.remainPoints = remainPoints;
	}

	public ZonedDateTime getUpdateDate() {
		return updateDate;
	}

	public MembershipHistoty updateDate(ZonedDateTime updateDate) {
		this.updateDate = updateDate;
		return this;
	}

	public void setUpdateDate(ZonedDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public Double getTransferablePoints() {
		return transferablePoints;
	}

	public MembershipHistoty transferablePoints(Double transferablePoints) {
		this.transferablePoints = transferablePoints;
		return this;
	}

	public void setTransferablePoints(Double transferablePoints) {
		this.transferablePoints = transferablePoints;
	}

	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public MembershipHistoty deleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
		return this;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Customer getCustomer() {
		return customer;
	}

	public MembershipHistoty customer(Customer customer) {
		this.customer = customer;
		return this;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MembershipHistoty membershipHistoty = (MembershipHistoty) o;
		if (membershipHistoty.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), membershipHistoty.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "MembershipHistoty{" + "id=" + getId() + ", membershipLevelCode='" + getMembershipLevelCode() + "'"
				+ ", effectedDate='" + getEffectedDate() + "'" + ", startDate='" + getStartDate() + "'"
				+ ", expiredDate='" + getExpiredDate() + "'" + ", status=" + getStatus() + ", accumulationPoints="
				+ getAccumulationPoints() + ", pendingPoints=" + getPendingPoints() + ", activatedPoints="
				+ getActivatedPoints() + ", expiredPoints=" + getExpiredPoints() + ", redeemPoints=" + getRedeemPoints()
				+ ", remainPoints=" + getRemainPoints() + ", updateDate='" + getUpdateDate() + "'"
				+ ", transferablePoints=" + getTransferablePoints() + ", deleteFlag="
				+ getDeleteFlag() + "}";
	}
}
