package com.gcs.flexba.rmsweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Promotion.
 */
@Entity
@Table(name = "promotion_rule_tbl")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Promotion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "promotion_rule_id")
    private Long id;
    
    @Column(name = "sale_id")
    private String saleId;

    @Column(name = "coupon_code")
    private String couponCode;

    @Column(name = "promotion_name")
    private String promotionName;

    @Column(name = "description")
    private String description;

    @Column(name = "html_description")
    private String htmlDescription;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "status")
    private Integer status;

    @Column(name = "stop_next_rule")
    private Boolean stopNextRule;

    @Column(name = "sort_order")
    private Integer sortOrder;

    @Column(name = "use_org_price_flag")
    private Boolean useOrgPriceFlag;

    @Column(name = "applied_on")
    private Integer appliedOn;

    @Lob
    @Column(name = "rule_data")
    private byte[] ruleData;

    @Column(name = "rule_data_content_type")
    private String ruleDataContentType;

    @Column(name = "drools_rule")
    private String droolsRule;

    @Lob
    @Column(name = "allocation_data")
    private byte[] allocationData;

    @Column(name = "allocation_data_content_type")
    private String allocationDataContentType;

    @Column(name = "version")
    private Integer version;

    @Column(name = "run_mode")
    private Integer runMode;

    @Column(name = "on_store")
    private String onStore;

    @Column(name = "promotion_type")
    private Integer promotionType;

    @Column(name = "auto_flag")
    private Boolean autoFlag;

    @Column(name = "update_date")
    private ZonedDateTime updateDate;

    @Column(name = "delete_flag")
    private Boolean deleteFlag;

    @Column(name = "channel")
    private String channel;

    @Column(name = "js_condition")
    private String jsCondition;

    @Column(name = "js_bonus")
    private String jsBonus;

    @Column(name = "redeem_flag")
    private Boolean redeemFlag;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "image_path_2")
    private String imagePath2;

    @Column(name = "redeem_points")
    private Double redeemPoints;

    @Lob
    @Column(name = "image_blob_1")
    private byte[] imageBlob1;

    @Column(name = "image_blob_1_content_type")
    private String imageBlob1ContentType;

    @Lob
    @Column(name = "image_blob_2")
    private byte[] imageBlob2;

    @Column(name = "image_blob_2_content_type")
    private String imageBlob2ContentType;

    @Column(name = "publish_start_date")
    private LocalDate publishStartDate;

    @Column(name = "publish_end_date")
    private LocalDate publishEndDate;
    
    @Column(name = "num_presentees")
    private Integer numPresentees;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public Promotion couponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
    
    public String getSaleId() {
        return saleId;
    }

    public Promotion saleId(String saleId) {
        this.saleId = saleId;
        return this;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public Promotion promotionName(String promotionName) {
        this.promotionName = promotionName;
        return this;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getDescription() {
        return description;
    }

    public Promotion description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHtmlDescription() {
        return htmlDescription;
    }

    public Promotion htmlDescription(String htmlDescription) {
        this.htmlDescription = htmlDescription;
        return this;
    }

    public void setHtmlDescription(String htmlDescription) {
        this.htmlDescription = htmlDescription;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Promotion startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }
    
    public Promotion endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public Promotion status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean isStopNextRule() {
        return stopNextRule;
    }

    public Promotion stopNextRule(Boolean stopNextRule) {
        this.stopNextRule = stopNextRule;
        return this;
    }

    public void setStopNextRule(Boolean stopNextRule) {
        this.stopNextRule = stopNextRule;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public Promotion sortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
        return this;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean isUseOrgPriceFlag() {
        return useOrgPriceFlag;
    }

    public Promotion useOrgPriceFlag(Boolean useOrgPriceFlag) {
        this.useOrgPriceFlag = useOrgPriceFlag;
        return this;
    }

    public void setUseOrgPriceFlag(Boolean useOrgPriceFlag) {
        this.useOrgPriceFlag = useOrgPriceFlag;
    }

    public Integer getAppliedOn() {
        return appliedOn;
    }

    public Promotion appliedOn(Integer appliedOn) {
        this.appliedOn = appliedOn;
        return this;
    }

    public void setAppliedOn(Integer appliedOn) {
        this.appliedOn = appliedOn;
    }

    public byte[] getRuleData() {
        return ruleData;
    }

    public Promotion ruleData(byte[] ruleData) {
        this.ruleData = ruleData;
        return this;
    }

    public void setRuleData(byte[] ruleData) {
        this.ruleData = ruleData;
    }

    public String getRuleDataContentType() {
        return ruleDataContentType;
    }

    public Promotion ruleDataContentType(String ruleDataContentType) {
        this.ruleDataContentType = ruleDataContentType;
        return this;
    }

    public void setRuleDataContentType(String ruleDataContentType) {
        this.ruleDataContentType = ruleDataContentType;
    }

    public String getDroolsRule() {
        return droolsRule;
    }

    public Promotion droolsRule(String droolsRule) {
        this.droolsRule = droolsRule;
        return this;
    }

    public void setDroolsRule(String droolsRule) {
        this.droolsRule = droolsRule;
    }

    public byte[] getAllocationData() {
        return allocationData;
    }

    public Promotion allocationData(byte[] allocationData) {
        this.allocationData = allocationData;
        return this;
    }

    public void setAllocationData(byte[] allocationData) {
        this.allocationData = allocationData;
    }

    public String getAllocationDataContentType() {
        return allocationDataContentType;
    }

    public Promotion allocationDataContentType(String allocationDataContentType) {
        this.allocationDataContentType = allocationDataContentType;
        return this;
    }

    public void setAllocationDataContentType(String allocationDataContentType) {
        this.allocationDataContentType = allocationDataContentType;
    }

    public Integer getVersion() {
        return version;
    }

    public Promotion version(Integer version) {
        this.version = version;
        return this;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getRunMode() {
        return runMode;
    }

    public Promotion runMode(Integer runMode) {
        this.runMode = runMode;
        return this;
    }

    public void setRunMode(Integer runMode) {
        this.runMode = runMode;
    }

    public String getOnStore() {
        return onStore;
    }

    public Promotion onStore(String onStore) {
        this.onStore = onStore;
        return this;
    }

    public void setOnStore(String onStore) {
        this.onStore = onStore;
    }

    public Integer getPromotionType() {
        return promotionType;
    }

    public Promotion promotionType(Integer promotionType) {
        this.promotionType = promotionType;
        return this;
    }

    public void setPromotionType(Integer promotionType) {
        this.promotionType = promotionType;
    }

    public Boolean isAutoFlag() {
        return autoFlag;
    }

    public Promotion autoFlag(Boolean autoFlag) {
        this.autoFlag = autoFlag;
        return this;
    }

    public void setAutoFlag(Boolean autoFlag) {
        this.autoFlag = autoFlag;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public Promotion updateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean isDeleteFlag() {
        return deleteFlag;
    }

    public Promotion deleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
        return this;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getChannel() {
        return channel;
    }

    public Promotion channel(String channel) {
        this.channel = channel;
        return this;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getJsCondition() {
        return jsCondition;
    }

    public Promotion jsCondition(String jsCondition) {
        this.jsCondition = jsCondition;
        return this;
    }

    public void setJsCondition(String jsCondition) {
        this.jsCondition = jsCondition;
    }

    public String getJsBonus() {
        return jsBonus;
    }

    public Promotion jsBonus(String jsBonus) {
        this.jsBonus = jsBonus;
        return this;
    }

    public void setJsBonus(String jsBonus) {
        this.jsBonus = jsBonus;
    }

    public Boolean isRedeemFlag() {
        return redeemFlag;
    }

    public Promotion redeemFlag(Boolean redeemFlag) {
        this.redeemFlag = redeemFlag;
        return this;
    }

    public void setRedeemFlag(Boolean redeemFlag) {
        this.redeemFlag = redeemFlag;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Promotion imagePath(String imagePath) {
        this.imagePath = imagePath;
        return this;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath2() {
        return imagePath2;
    }

    public Promotion imagePath2(String imagePath2) {
        this.imagePath2 = imagePath2;
        return this;
    }

    public void setImagePath2(String imagePath2) {
        this.imagePath2 = imagePath2;
    }

    public Double getRedeemPoints() {
        return redeemPoints;
    }

    public Promotion redeemPoints(Double redeemPoints) {
        this.redeemPoints = redeemPoints;
        return this;
    }

    public void setRedeemPoints(Double redeemPoints) {
        this.redeemPoints = redeemPoints;
    }

    public byte[] getImageBlob1() {
        return imageBlob1;
    }

    public Promotion imageBlob1(byte[] imageBlob1) {
        this.imageBlob1 = imageBlob1;
        return this;
    }

    public void setImageBlob1(byte[] imageBlob1) {
        this.imageBlob1 = imageBlob1;
    }

    public String getImageBlob1ContentType() {
        return imageBlob1ContentType;
    }

    public Promotion imageBlob1ContentType(String imageBlob1ContentType) {
        this.imageBlob1ContentType = imageBlob1ContentType;
        return this;
    }

    public void setImageBlob1ContentType(String imageBlob1ContentType) {
        this.imageBlob1ContentType = imageBlob1ContentType;
    }

    public byte[] getImageBlob2() {
        return imageBlob2;
    }

    public Promotion imageBlob2(byte[] imageBlob2) {
        this.imageBlob2 = imageBlob2;
        return this;
    }

    public void setImageBlob2(byte[] imageBlob2) {
        this.imageBlob2 = imageBlob2;
    }

    public String getImageBlob2ContentType() {
        return imageBlob2ContentType;
    }

    public Promotion imageBlob2ContentType(String imageBlob2ContentType) {
        this.imageBlob2ContentType = imageBlob2ContentType;
        return this;
    }

    public void setImageBlob2ContentType(String imageBlob2ContentType) {
        this.imageBlob2ContentType = imageBlob2ContentType;
    }

    public LocalDate getPublishStartDate() {
        return publishStartDate;
    }

    public Promotion publishStartDate(LocalDate publishStartDate) {
        this.publishStartDate = publishStartDate;
        return this;
    }

    public void setPublishStartDate(LocalDate publishStartDate) {
        this.publishStartDate = publishStartDate;
    }

    public LocalDate getPublishEndDate() {
        return publishEndDate;
    }

    public Promotion publishEndDate(LocalDate publishEndDate) {
        this.publishEndDate = publishEndDate;
        return this;
    }

    public void setPublishEndDate(LocalDate publishEndDate) {
        this.publishEndDate = publishEndDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Promotion promotion = (Promotion) o;
        if (promotion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), promotion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Promotion{" +
            "id=" + getId() +
            ", couponCode='" + getCouponCode() + "'" +
            ", promotionName='" + getPromotionName() + "'" +
            ", description='" + getDescription() + "'" +
            ", htmlDescription='" + getHtmlDescription() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", status=" + getStatus() +
            ", stopNextRule='" + isStopNextRule() + "'" +
            ", sortOrder=" + getSortOrder() +
            ", useOrgPriceFlag='" + isUseOrgPriceFlag() + "'" +
            ", appliedOn=" + getAppliedOn() +
            ", ruleData='" + getRuleData() + "'" +
            ", ruleDataContentType='" + getRuleDataContentType() + "'" +
            ", droolsRule='" + getDroolsRule() + "'" +
            ", allocationData='" + getAllocationData() + "'" +
            ", allocationDataContentType='" + getAllocationDataContentType() + "'" +
            ", version=" + getVersion() +
            ", runMode=" + getRunMode() +
            ", onStore='" + getOnStore() + "'" +
            ", promotionType=" + getPromotionType() +
            ", autoFlag='" + isAutoFlag() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", deleteFlag='" + isDeleteFlag() + "'" +
            ", channel='" + getChannel() + "'" +
            ", jsCondition='" + getJsCondition() + "'" +
            ", jsBonus='" + getJsBonus() + "'" +
            ", redeemFlag='" + isRedeemFlag() + "'" +
            ", imagePath='" + getImagePath() + "'" +
            ", imagePath2='" + getImagePath2() + "'" +
            ", redeemPoints=" + getRedeemPoints() +
            ", imageBlob1='" + getImageBlob1() + "'" +
            ", imageBlob1ContentType='" + getImageBlob1ContentType() + "'" +
            ", imageBlob2='" + getImageBlob2() + "'" +
            ", imageBlob2ContentType='" + getImageBlob2ContentType() + "'" +
            ", publishStartDate='" + getPublishStartDate() + "'" +
            ", publishEndDate='" + getPublishEndDate() + "'" +
            "}";
    }

	public Integer getNumPresentees() {
		return numPresentees;
	}

	public void setNumPresentees(Integer numPresentees) {
		this.numPresentees = numPresentees;
	}
}
