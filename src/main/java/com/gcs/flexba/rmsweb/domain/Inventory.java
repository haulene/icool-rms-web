package com.gcs.flexba.rmsweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Inventory.
 */
@Entity
@Table(name = "inventory_tbl")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Inventory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inventory_id")
    private Long id;

    @Column(name = "inventory_code")
    private String inventoryCode;

    @Column(name = "inventory_name")
    private String inventoryName;

    @Column(name = "region_code")
    private String regionCode;

    @Column(name = "inventory_type")
    private Integer inventoryType;

    @Column(name = "brand_code")
    private String brandCode;

    @Column(name = "address")
    private String address;

    @Column(name = "tel")
    private String tel;

    @Column(name = "fax")
    private String fax;

    @Column(name = "email")
    private String email;

    @Column(name = "status")
    private Integer status;

    @Column(name = "channel")
    private String channel;

    @Column(name = "allow_pickup_flag")
    private Boolean allowPickupFlag;

    @Column(name = "allow_urgent_order_flag")
    private Boolean allowUrgentOrderFlag;

    @Column(name = "allow_book_studio_flag")
    private Boolean allowBookStudioFlag;

    @Column(name = "update_date")
    private ZonedDateTime updateDate;

    @Column(name = "delete_flag")
    private Boolean deleteFlag;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInventoryCode() {
        return inventoryCode;
    }

    public Inventory inventoryCode(String inventoryCode) {
        this.inventoryCode = inventoryCode;
        return this;
    }

    public void setInventoryCode(String inventoryCode) {
        this.inventoryCode = inventoryCode;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public Inventory inventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
        return this;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public Inventory regionCode(String regionCode) {
        this.regionCode = regionCode;
        return this;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public Integer getInventoryType() {
        return inventoryType;
    }

    public Inventory inventoryType(Integer inventoryType) {
        this.inventoryType = inventoryType;
        return this;
    }

    public void setInventoryType(Integer inventoryType) {
        this.inventoryType = inventoryType;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public Inventory brandCode(String brandCode) {
        this.brandCode = brandCode;
        return this;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getAddress() {
        return address;
    }

    public Inventory address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public Inventory tel(String tel) {
        this.tel = tel;
        return this;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFax() {
        return fax;
    }

    public Inventory fax(String fax) {
        this.fax = fax;
        return this;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public Inventory email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public Inventory status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getChannel() {
        return channel;
    }

    public Inventory channel(String channel) {
        this.channel = channel;
        return this;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Boolean isAllowPickupFlag() {
        return allowPickupFlag;
    }

    public Inventory allowPickupFlag(Boolean allowPickupFlag) {
        this.allowPickupFlag = allowPickupFlag;
        return this;
    }

    public void setAllowPickupFlag(Boolean allowPickupFlag) {
        this.allowPickupFlag = allowPickupFlag;
    }

    public Boolean isAllowUrgentOrderFlag() {
        return allowUrgentOrderFlag;
    }

    public Inventory allowUrgentOrderFlag(Boolean allowUrgentOrderFlag) {
        this.allowUrgentOrderFlag = allowUrgentOrderFlag;
        return this;
    }

    public void setAllowUrgentOrderFlag(Boolean allowUrgentOrderFlag) {
        this.allowUrgentOrderFlag = allowUrgentOrderFlag;
    }

    public Boolean isAllowBookStudioFlag() {
        return allowBookStudioFlag;
    }

    public Inventory allowBookStudioFlag(Boolean allowBookStudioFlag) {
        this.allowBookStudioFlag = allowBookStudioFlag;
        return this;
    }

    public void setAllowBookStudioFlag(Boolean allowBookStudioFlag) {
        this.allowBookStudioFlag = allowBookStudioFlag;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public Inventory updateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean isDeleteFlag() {
        return deleteFlag;
    }

    public Inventory deleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
        return this;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getLatitude() {
        return latitude;
    }

    public Inventory latitude(String latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Inventory longitude(String longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Inventory inventory = (Inventory) o;
        if (inventory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), inventory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Inventory{" +
            "id=" + getId() +
            ", inventoryCode='" + getInventoryCode() + "'" +
            ", inventoryName='" + getInventoryName() + "'" +
            ", regionCode='" + getRegionCode() + "'" +
            ", inventoryType=" + getInventoryType() +
            ", brandCode='" + getBrandCode() + "'" +
            ", address='" + getAddress() + "'" +
            ", tel='" + getTel() + "'" +
            ", fax='" + getFax() + "'" +
            ", email='" + getEmail() + "'" +
            ", status=" + getStatus() +
            ", channel='" + getChannel() + "'" +
            ", allowPickupFlag='" + isAllowPickupFlag() + "'" +
            ", allowUrgentOrderFlag='" + isAllowUrgentOrderFlag() + "'" +
            ", allowBookStudioFlag='" + isAllowBookStudioFlag() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", deleteFlag='" + isDeleteFlag() + "'" +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            "}";
    }
}
