package com.gcs.flexba.rmsweb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A VoucherGift.
 */
@Entity
@Table(name = "voucher_gift_tbl")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class VoucherGift implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "voucher_gift_id")
    private Long id;

    @Column(name = "voucher_gift_code")
    private String voucherGiftCode;

    @Column(name = "voucher_gift_name")
    private String voucherGiftName;

    @Column(name = "voucher_category")
    private Integer voucherCategory;

    @Column(name = "voucher_type")
    private Integer voucherType;

    @Column(name = "customer_code")
    private String customerCode;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "customer_phone")
    private String customerPhone;

    @Column(name = "total_value")
    private Double totalValue;

    @Column(name = "available_value")
    private Double availableValue;

    @Column(name = "valid_from")
    private ZonedDateTime validFrom;

    @Column(name = "valid_to")
    private ZonedDateTime validTo;

    @Column(name = "status")
    private Integer status;

    @Column(name = "ref_sku")
    private String refSku;

    @Column(name = "store_code")
    private String storeCode;

    @Column(name = "note")
    private String note;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "expired_duration")
    private Integer expiredDuration;

    @Column(name = "effected_day")
    private Integer effectedDay;

    @Column(name = "voucher_value", precision = 10, scale = 2)
    private BigDecimal voucherValue;

    @Column(name = "used_customer_code")
    private String usedCustomerCode;

    @Column(name = "used_customer_name")
    private String usedCustomerName;

    @Column(name = "used_customer_phone")
    private String usedCustomerPhone;

    @Column(name = "update_flag")
    private Boolean updateFlag;

    @Column(name = "max_applied_amount", precision = 10, scale = 2)
    private BigDecimal maxAppliedAmount;

    @Column(name = "max_applied_times_per_customer")
    private Integer max_appliedTimePerCustomer;

    @Column(name = "voucher_applied_type")
    private Integer voucherAppliedType;

    @Column(name = "update_date")
    private ZonedDateTime updateDate;

    @Column(name = "delete_flag")
    private Boolean deleteFlag;

    @Column(name = "personal_id")
    private String personalId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoucherGiftCode() {
        return voucherGiftCode;
    }

    public VoucherGift voucherGiftCode(String voucherGiftCode) {
        this.voucherGiftCode = voucherGiftCode;
        return this;
    }

    public void setVoucherGiftCode(String voucherGiftCode) {
        this.voucherGiftCode = voucherGiftCode;
    }

    public String getVoucherGiftName() {
        return voucherGiftName;
    }

    public VoucherGift voucherGiftName(String voucherGiftName) {
        this.voucherGiftName = voucherGiftName;
        return this;
    }

    public void setVoucherGiftName(String voucherGiftName) {
        this.voucherGiftName = voucherGiftName;
    }

    public Integer getVoucherCategory() {
        return voucherCategory;
    }

    public VoucherGift voucherCategory(Integer voucherCategory) {
        this.voucherCategory = voucherCategory;
        return this;
    }

    public void setVoucherCategory(Integer voucherCategory) {
        this.voucherCategory = voucherCategory;
    }

    public Integer getVoucherType() {
        return voucherType;
    }

    public VoucherGift voucherType(Integer voucherType) {
        this.voucherType = voucherType;
        return this;
    }

    public void setVoucherType(Integer voucherType) {
        this.voucherType = voucherType;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public VoucherGift customerCode(String customerCode) {
        this.customerCode = customerCode;
        return this;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public VoucherGift customerName(String customerName) {
        this.customerName = customerName;
        return this;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public VoucherGift customerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
        return this;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public VoucherGift totalValue(Double totalValue) {
        this.totalValue = totalValue;
        return this;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public Double getAvailableValue() {
        return availableValue;
    }

    public VoucherGift availableValue(Double availableValue) {
        this.availableValue = availableValue;
        return this;
    }

    public void setAvailableValue(Double availableValue) {
        this.availableValue = availableValue;
    }

    public ZonedDateTime getValidFrom() {
        return validFrom;
    }

    public VoucherGift validFrom(ZonedDateTime validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    public void setValidFrom(ZonedDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public ZonedDateTime getValidTo() {
        return validTo;
    }

    public VoucherGift validTo(ZonedDateTime validTo) {
        this.validTo = validTo;
        return this;
    }

    public void setValidTo(ZonedDateTime validTo) {
        this.validTo = validTo;
    }

    public Integer getStatus() {
        return status;
    }

    public VoucherGift status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRefSku() {
        return refSku;
    }

    public VoucherGift refSku(String refSku) {
        this.refSku = refSku;
        return this;
    }

    public void setRefSku(String refSku) {
        this.refSku = refSku;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public VoucherGift storeCode(String storeCode) {
        this.storeCode = storeCode;
        return this;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getNote() {
        return note;
    }

    public VoucherGift note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public VoucherGift createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getExpiredDuration() {
        return expiredDuration;
    }

    public VoucherGift expiredDuration(Integer expiredDuration) {
        this.expiredDuration = expiredDuration;
        return this;
    }

    public void setExpiredDuration(Integer expiredDuration) {
        this.expiredDuration = expiredDuration;
    }

    public Integer getEffectedDay() {
        return effectedDay;
    }

    public VoucherGift effectedDay(Integer effectedDay) {
        this.effectedDay = effectedDay;
        return this;
    }

    public void setEffectedDay(Integer effectedDay) {
        this.effectedDay = effectedDay;
    }

    public BigDecimal getVoucherValue() {
        return voucherValue;
    }

    public VoucherGift voucherValue(BigDecimal voucherValue) {
        this.voucherValue = voucherValue;
        return this;
    }

    public void setVoucherValue(BigDecimal voucherValue) {
        this.voucherValue = voucherValue;
    }

    public String getUsedCustomerCode() {
        return usedCustomerCode;
    }

    public VoucherGift usedCustomerCode(String usedCustomerCode) {
        this.usedCustomerCode = usedCustomerCode;
        return this;
    }

    public void setUsedCustomerCode(String usedCustomerCode) {
        this.usedCustomerCode = usedCustomerCode;
    }

    public String getUsedCustomerName() {
        return usedCustomerName;
    }

    public VoucherGift usedCustomerName(String usedCustomerName) {
        this.usedCustomerName = usedCustomerName;
        return this;
    }

    public void setUsedCustomerName(String usedCustomerName) {
        this.usedCustomerName = usedCustomerName;
    }

    public String getUsedCustomerPhone() {
        return usedCustomerPhone;
    }

    public VoucherGift usedCustomerPhone(String usedCustomerPhone) {
        this.usedCustomerPhone = usedCustomerPhone;
        return this;
    }

    public void setUsedCustomerPhone(String usedCustomerPhone) {
        this.usedCustomerPhone = usedCustomerPhone;
    }

    public Boolean isUpdateFlag() {
        return updateFlag;
    }

    public VoucherGift updateFlag(Boolean updateFlag) {
        this.updateFlag = updateFlag;
        return this;
    }

    public void setUpdateFlag(Boolean updateFlag) {
        this.updateFlag = updateFlag;
    }

    public BigDecimal getMaxAppliedAmount() {
        return maxAppliedAmount;
    }

    public VoucherGift maxAppliedAmount(BigDecimal maxAppliedAmount) {
        this.maxAppliedAmount = maxAppliedAmount;
        return this;
    }

    public void setMaxAppliedAmount(BigDecimal maxAppliedAmount) {
        this.maxAppliedAmount = maxAppliedAmount;
    }

    public Integer getMax_appliedTimePerCustomer() {
        return max_appliedTimePerCustomer;
    }

    public VoucherGift max_appliedTimePerCustomer(Integer max_appliedTimePerCustomer) {
        this.max_appliedTimePerCustomer = max_appliedTimePerCustomer;
        return this;
    }

    public void setMax_appliedTimePerCustomer(Integer max_appliedTimePerCustomer) {
        this.max_appliedTimePerCustomer = max_appliedTimePerCustomer;
    }

    public Integer getVoucherAppliedType() {
        return voucherAppliedType;
    }

    public VoucherGift voucherAppliedType(Integer voucherAppliedType) {
        this.voucherAppliedType = voucherAppliedType;
        return this;
    }

    public void setVoucherAppliedType(Integer voucherAppliedType) {
        this.voucherAppliedType = voucherAppliedType;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public VoucherGift updateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean isDeleteFlag() {
        return deleteFlag;
    }

    public VoucherGift deleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
        return this;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getPersonalId() {
        return personalId;
    }

    public VoucherGift personalId(String personalId) {
        this.personalId = personalId;
        return this;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VoucherGift voucherGift = (VoucherGift) o;
        if (voucherGift.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), voucherGift.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VoucherGift{" +
            "id=" + getId() +
            ", voucherGiftCode='" + getVoucherGiftCode() + "'" +
            ", voucherGiftName='" + getVoucherGiftName() + "'" +
            ", voucherCategory=" + getVoucherCategory() +
            ", voucherType=" + getVoucherType() +
            ", customerCode='" + getCustomerCode() + "'" +
            ", customerName='" + getCustomerName() + "'" +
            ", customerPhone='" + getCustomerPhone() + "'" +
            ", totalValue=" + getTotalValue() +
            ", availableValue=" + getAvailableValue() +
            ", validFrom='" + getValidFrom() + "'" +
            ", validTo='" + getValidTo() + "'" +
            ", status=" + getStatus() +
            ", refSku='" + getRefSku() + "'" +
            ", storeCode='" + getStoreCode() + "'" +
            ", note='" + getNote() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", expiredDuration=" + getExpiredDuration() +
            ", effectedDay=" + getEffectedDay() +
            ", voucherValue=" + getVoucherValue() +
            ", usedCustomerCode='" + getUsedCustomerCode() + "'" +
            ", usedCustomerName='" + getUsedCustomerName() + "'" +
            ", usedCustomerPhone='" + getUsedCustomerPhone() + "'" +
            ", updateFlag='" + isUpdateFlag() + "'" +
            ", maxAppliedAmount=" + getMaxAppliedAmount() +
            ", max_appliedTimePerCustomer=" + getMax_appliedTimePerCustomer() +
            ", voucherAppliedType=" + getVoucherAppliedType() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", deleteFlag='" + isDeleteFlag() + "'" +
            ", personalId='" + getPersonalId() + "'" +
            "}";
    }
}
