package com.gcs.flexba.rmsweb.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.gcs.flexba.rmsweb.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Promotion.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Sale.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Customer.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.MembershipHistoty.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Customer.class.getName() + ".membershipHistories", jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Customer.class.getName() + ".membershipHistoties", jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.CustomerRating.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Type.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Config.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Inventory.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.RatingQuestion.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.RatingAnswer.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(com.gcs.flexba.rmsweb.domain.VoucherGift.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
